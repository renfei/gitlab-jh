# frozen_string_literal: true

module JH
  module Gitlab
    module SubscriptionPortal
      extend ActiveSupport::Concern

      class_methods do
        extend ::Gitlab::Utils::Override

        override :default_subscriptions_url
        def default_subscriptions_url
          ::Gitlab.dev_or_test_env? ? 'https://customers.stg.gitlab.cn' : 'https://customers.gitlab.cn'
        end

        override :subscriptions_more_minutes_url
        def subscriptions_more_minutes_url
          ::Gitlab::Saas.commom_purchase_url
        end

        override :subscriptions_more_storage_url
        def subscriptions_more_storage_url
          ::Gitlab::Saas.commom_purchase_url
        end

        override :subscriptions_comparison_url
        def subscriptions_comparison_url
          "https://about.gitlab.cn/pricing/saas/feature-comparison"
        end
      end
    end
  end
end
