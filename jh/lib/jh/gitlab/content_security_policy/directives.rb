# frozen_string_literal: true

module JH
  module Gitlab
    module ContentSecurityPolicy
      module Directives
        extend ActiveSupport::Concern

        class_methods do
          extend ::Gitlab::Utils::Override

          override :frame_src
          def frame_src
            "https://*.qq.com/ https://www.google.com/recaptcha/ https://www.recaptcha.net/ https://content.googleapis.com https://content-compute.googleapis.com https://content-cloudbilling.googleapis.com https://content-cloudresourcemanager.googleapis.com"
          end

          override :script_src
          def script_src
            "'strict-dynamic' 'self' 'unsafe-inline' 'unsafe-eval' https://*.qq.com/ https://www.google.com/recaptcha/ https://www.recaptcha.net https://apis.google.com"
          end
        end
      end
    end
  end
end
