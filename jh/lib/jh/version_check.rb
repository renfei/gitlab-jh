# frozen_string_literal: true

module JH
  module VersionCheck
    extend ActiveSupport::Concern

    class_methods do
      extend ::Gitlab::Utils::Override

      override :host

      def host
        ENV['VERSION_APP_HOST'] || 'https://version.gitlab.cn'
      end
    end
  end
end
