---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
comments: false
description: Read through the GitLab installation methods.
type: index
---

# 安装 **(FREE SELF)**

<!--GitLab can be installed in most GNU/Linux distributions, and with several
cloud providers. To get the best experience from GitLab, you must balance
performance, reliability, ease of administration (backups, upgrades, and
troubleshooting), and the cost of hosting.-->

极狐GitLab 可以安装于大多数 GNU/Linux 发行版，以及一些云服务提供商。为了获得最佳体验，您必须在性能、可靠性、易管理性（备份、升级和故障排查）和托管成本之间取得平衡。

## 需求

<!--Before you install GitLab, be sure to review the [system requirements](requirements.md).
The system requirements include details about the minimum hardware, software,
database, and additional requirements to support GitLab.-->

在您安装极狐GitLab 之前，确保已阅读[系统需求](requirements.md)。系统需求包括关于支持极狐GitLab 的最低硬件配置、软件需求、数据库和其它需求的详细信息。

## 选择安装方式

<!--Depending on your platform, select from the following available methods to
install GitLab:-->

在以下可用安装方式中，根据您的平台选择安装极狐GitLab 的方式。

<!--
| Installation method                                            | Description | When to choose |
|----------------------------------------------------------------|-------------|----------------|
| [Linux 安装包](https://docs.gitlab.com/omnibus/installation/) | The official deb/rpm packages (also known as Omnibus GitLab) that contains a bundle of GitLab and the components it depends on, including PostgreSQL, Redis, and Sidekiq. | This is the recommended method for getting started. The Linux packages are mature, scalable, and are used today on GitLab.com. If you need additional flexibility and resilience, we recommend deploying GitLab as described in the [reference architecture documentation](../administration/reference_architectures/index.md). |
| [Helm charts](https://docs.gitlab.com/charts/)                 | The cloud native Helm chart for installing GitLab and all of its components on Kubernetes. | When installing GitLab on Kubernetes, there are some trade-offs that you need to be aware of: <br/>- Administration and troubleshooting requires Kubernetes knowledge.<br/>- It can be more expensive for smaller installations. The default installation requires more resources than a single node Linux package deployment, as most services are deployed in a redundant fashion.<br/>- There are some feature [limitations to be aware of](https://docs.gitlab.com/charts/#limitations).<br/><br/> Use this method if your infrastructure is built on Kubernetes and you're familiar with how it works. The methods for management, observability, and some concepts are different than traditional deployments. |
| [Docker](https://docs.gitlab.com/omnibus/docker/)              | The GitLab packages, Dockerized. | Use this method if you're familiar with Docker. |
| [Source](installation.md)                                      | Install GitLab and all of its components from scratch. | Use this method if none of the previous methods are available for your platform. Useful for unsupported systems like \*BSD.|
| [GitLab Environment Toolkit (GET)](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit#documentation) | The GitLab Environment toolkit provides a set of automation tools to deploy a [reference architecture](../administration/reference_architectures/index.md) on most major cloud providers. | Since GET is in beta and not yet recommended for production use, use this method if you want to test deploying GitLab in scalable environment. |
-->

| 安装方式                                            | 描述 | 何时选择 |
|----------------------------------------------------------------|-------------|----------------|
| [Linux 安装包](https://docs.gitlab.cn/omnibus/installation/) | 官方的 deb/rpm 安装包（也被称作 Omnibus GitLab）包含极狐GitLab 和依赖的组件，包括 PostgreSQL、Redis 和 Sidekiq。 | 入门的推荐方式。Linux 安装包是成熟的和可扩展的。<!--，现在用于 GitLab.cn。如果您需要额外的灵活性和弹性，我们建议您按照[参考架构文档](../administration/reference_architectures/index.md)中的说明部署极狐 GitLab。--> |
| [Helm charts](https://docs.gitlab.cn/charts/)                 | 用于在 Kubernetes 上安装极狐GitLab 及其所有组件的云原生 Helm chart。 | 在 Kubernetes 上安装极狐GitLab 时，您需要注意一些权衡：<br/>- 管理和故障排查需要 Kubernetes 知识。<br/>- 对于较小的安装，它可能会更昂贵。默认安装需要比单节点 Linux 包部署更多的资源，因为大多数服务都是以冗余方式部署的。<br/>- 有一些功能需要注意的限制。<!--[需要注意的限制](https://docs.gitlab.cn/charts/#limitations).--><br/><br/> 如果您的基础设施是基于 Kubernetes 构建的并且您熟悉它的工作原理，请使用此方法。管理方法、可观察性和一些概念与传统部署不同。 |

<!--
## Install GitLab on cloud providers

Regardless of the installation method, you can install GitLab on several cloud
providers, assuming the cloud provider supports it. Here are several possible installation
methods, the majority which use the Linux packages:

| Cloud provider                                                | Description |
|---------------------------------------------------------------|-------------|
| [AWS (HA)](aws/index.md)                                      | Install GitLab on AWS using the community AMIs provided by GitLab. |
| [Google Cloud Platform (GCP)](google_cloud_platform/index.md) | Install GitLab on a VM in GCP. |
| [Azure](azure/index.md)                                       | Install GitLab from Azure Marketplace. |
| [DigitalOcean](https://about.gitlab.com/blog/2016/04/27/getting-started-with-gitlab-and-digitalocean/) | Install GitLab on DigitalOcean. You can also [test GitLab on DigitalOcean using Docker Machine](digitaloceandocker.md). |
-->

## 下一步

<!--
After you complete the steps for installing GitLab, you can
[configure your instance](next_steps.md).
-->

在您完成安装极狐GitLab 的步骤后，您可以[设置您的实例](next_steps.md)。
