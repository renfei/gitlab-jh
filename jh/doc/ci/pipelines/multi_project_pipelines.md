---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 多项目流水线 **(FREE)**

<!--
> [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/199224) to GitLab Free in 12.8.
-->

您可以跨多个项目设置 [GitLab CI/CD](../index.md)，以便一个项目中的流水线可以触发另一个项目中的流水线。您可以在一个地方可视化整个流水线，包括所有跨项目的相互依赖关系。

例如，您可以从极狐GitLab 中的三个不同项目部署您的 Web 应用程序。
每个项目都有自己的构建、测试和部署过程。使用多项目流水线，您可以可视化整个流水线，包括所有三个项目的所有构建和测试阶段。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview, see the [Multi-project pipelines demo](https://www.youtube.com/watch?v=g_PIwBM1J84).


Multi-project pipelines are also useful for larger products that require cross-project interdependencies, like those
with a [microservices architecture](https://about.gitlab.com/blog/2016/08/16/trends-in-version-control-land-microservices/).
Learn more in the [Cross-project Pipeline Triggering and Visualization demo](https://about.gitlab.com/learn/)
at GitLab@learn, in the Continuous Integration section.
-->

如果您在下游私有项目中触发流水线，在上游项目的流水线页面，您可以查看：

- 项目名称。
- 流水线的状态。

如果您的公共项目可以在私有项目中触发下游流水线，请确保不存在保密问题。

## 创建多项目流水线

要创建多项目流水线，您可以：

- [在您的 `.gitlab-ci.yml` 文件中定义](#在您的-gitlab-ciyml-文件中定义多项目流水线)。
- [使用 API](#使用-api-创建多项目流水线)。

### 在您的 `.gitlab-ci.yml` 文件中定义多项目流水线

<!--
> [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/199224) to GitLab Free in 12.8.
-->

当在您的 `.gitlab-ci.yml` 文件中创建一个多项目流水线时，创建了所谓的*触发作业*。 例如：

```yaml
rspec:
  stage: test
  script: bundle exec rspec

staging:
  variables:
    ENVIRONMENT: staging
  stage: deploy
  trigger: my/deployment
```

在这个例子中，在 `rspec` 作业在 `test` 阶段成功后，`staging` 触发器作业开始。此作业的初始状态为 `pending`。

系统然后在 `my/deployment` 项目中创建一个下游流水线，一旦创建流水线，`staging` 作业就会成功。项目的完整路径是 `my/deployment`。

您可以查看流水线的状态，也可以显示[下游流水线的状态](#触发作业中触发流水线的镜像状态)。

创建上游流水线的用户也必须能够在下游项目（`my/deployment`）中创建流水线。如果未找到下游项目，或者用户没有在那里创建流水线的权限，则 `staging` 作业将标记为 *failed*。

#### 触发作业配置关键字

触发器作业只能使用一组有限的 GitLab CI/CD [配置关键字](../yaml/index.md)。
可用于触发器作业的关键字是：

- [`trigger`](../yaml/index.md#trigger)
- [`stage`](../yaml/index.md#stage)
- [`allow_failure`](../yaml/index.md#allow_failure)
- [`规则`](../yaml/index.md#rules)
- [`only` 和 `except`](../yaml/index.md#only--except)
- [`when`](../yaml/index.md#when)（只有 `on_success`、`on_failure` 或 `always` 的值）
- [`extends`](../yaml/index.md#extends)
- [`needs`](../yaml/index.md#needs)，不是[`needs:project`](../yaml/index.md#cross-project-artifact-downloads-with-needs)

#### 指定下游流水线分支

您可以为要使用的下游流水线指定分支名称。极狐GitLab 使用分支头部的提交来创建下游流水线。

```yaml
rspec:
  stage: test
  script: bundle exec rspec

staging:
  stage: deploy
  trigger:
    project: my/deployment
    branch: stable-11-2
```

使用：

- `project` 关键字指定下游项目的完整路径。
- `branch` 关键字用于指定由 `project` 指定的项目中的分支名称。
   12.4 及更高版本支持变量扩展。

在下游项目中的受保护分支上触发的流水线，使用在上游项目中运行触发器作业的用户的角色。如果用户无权针对受保护分支运行 CI/CD 流水线，则流水线将失败。请参阅[受保护分支的流水线安全](index.md#受保护分支上的流水线安全)。

#### 使用 `variables` 关键字将 CI/CD 变量传递到下游流水线

有时您可能希望将 CI/CD 变量传递给下游流水线。
你可以通过使用 `variables` 关键字来做到这一点。

```yaml
rspec:
  stage: test
  script: bundle exec rspec

staging:
  variables:
    ENVIRONMENT: staging
  stage: deploy
  trigger: my/deployment
```

`ENVIRONMENT` 变量被传递给下游流水线中定义的每个作业。当 GitLab Runner 选择作业时，它可以作为变量使用。

在以下配置中，`MY_VARIABLE` 变量被传递到在 `trigger-downstream` 作业排队时创建的下游流水线。这是因为 `trigger-downstream` 作业继承了在全局变量块中声明的变量，然后我们将这些变量传递给下游流水线。

```yaml
variables:
  MY_VARIABLE: my-value

trigger-downstream:
  variables:
    ENVIRONMENT: something
  trigger: my/project
```

您可以使用 [`inherit` 关键字](../yaml/index.md#inherit) 阻止全局变量到达下游流水线。
在此示例中，`MY_GLOBAL_VAR` 变量在触发流水线中不可用：

```yaml
variables:
  MY_GLOBAL_VAR: value

trigger-downstream:
  inherit:
    variables: false
  variables:
    MY_LOCAL_VAR: value
  trigger: my/project
```

您可能希望使用例如预定义变量来传递有关上游流水线的一些信息。为此，您可以使用中间值来传递任何变量。例如：

```yaml
downstream-job:
  variables:
    UPSTREAM_BRANCH: $CI_COMMIT_REF_NAME
  trigger: my/project
```

在这种情况下，带有与上游管道相关值的 `UPSTREAM_BRANCH` 变量被传递给 `downstream-job` 作业。它在所有下游构建的上下文中可用。

上游流水线优先于下游流水线。如果上游项目和下游项目中都定义了两个同名变量，则上游项目中定义的变量优先。

#### 使用变量继承将 CI/CD 变量传递给下游流水线

您可以使用 `dotenv` 变量继承<!--[`dotenv` 变量继承](../variables/index.md#pass-an-environment-variable-to-another-job)--> 和[跨项目产物下载](../yaml/index.md#cross-project-artifact-downloads-with-needs)。

在上游流水线中：

1. 将变量保存在 `.env` 文件中。
1. 将 `.env` 文件另存为 `dotenv` 报告。
1. 触发下游流水线。

   ```yaml
   build_vars:
     stage: build
     script:
       - echo "BUILD_VERSION=hello" >> build.env
     artifacts:
       reports:
         dotenv: build.env

   deploy:
     stage: deploy
     trigger: my/downstream_project
   ```

1. 在下游流水线中设置 `test` 作业，以 `needs` 继承上游项目中 `build_vars` 作业的变量。`test` 作业继承了 `dotenv` 报告中的变量，它可以在脚本中访问 `BUILD_VERSION`：

   ```yaml
   test:
     stage: test
     script:
       - echo $BUILD_VERSION
     needs:
       - project: my/upstream_project
         job: build_vars
         ref: master
         artifacts: true
   ```

#### 对多项目流水线使用 `rules` 或 `only`/`except`

对于多项目流水线，您可以使用 CI/CD 变量或 [`rules`](../yaml/index.md#rulesif) 关键字来控制作业行为<!--[控制作业行为](../jobs/job_control.md)-->。当使用 [`trigger`](../yaml/index.md#trigger) 关键字触发下游流水线时，[`$CI_PIPELINE_SOURCE` 预定义变量](../variables/predefined_variables.md) 的值作为所有作业的 `pipeline` 值。

如果您使用 [`only/except`](../yaml/index.md#only--except) 来控制作业行为，请使用  [`pipelines`](../yaml/index.md#onlyrefs--exceptrefs) 关键字。

#### 触发作业中触发流水线的镜像状态

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/11238) in GitLab Premium 12.3.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/199224) to GitLab Free in 12.8.
-->

您可以使用 `strategy:depend` 将流水线从触发流水线镜像到源触发作业。例如：

```yaml
trigger_job:
  trigger:
    project: my/project
    strategy: depend
```

### 使用 API 创建多项目流水线

<!--
> [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/31573) to GitLab Free in 12.4.
-->

当您使用 [`CI_JOB_TOKEN` 触发流水线](../jobs/ci_job_token.md) 时，系统会识别作业令牌的来源。流水线变得相关，因此您可以在流水线图上可视化它们的关系。

这些关系通过显示上游和下游流水线依赖项的入站和出站连接显示在流水线图中。

使用时：

- CI/CD 变量或 [`rules`](../yaml/index.md#rulesif) 来控制作业行为，<!--[`$CI_PIPELINE_SOURCE` 预定义变量的值](../variables/predefined_variables.md)-->`$CI_PIPELINE_SOURCE` 预定义变量的值是通过带有 `CI_JOB_TOKEN` 的 API 触发的多项目流水线的 `pipeline`。
- [`only/except`](../yaml/index.md#only--except) 控制作业行为，使用 `pipelines` 关键字。

## 上游项目重建时触发流水线 **(PREMIUM)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/9045) in GitLab 12.8.
-->

每当流水线为不同项目中的新标签完成时，您就可以触发项目中的流水线。

先决条件：

- 上游项目必须是[公开的](../../public_access/public_access.md)。
- 用户必须在上游项目中具有开发者角色。

在上游项目重建时触发流水线：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线订阅**。
1. 输入您要订阅的项目，格式为 `<namespace>/<project>`。例如，如果项目是 `https://gitlab.com/gitlab-jh/gitlab`，则使用 `gitlab-jh/gitlab`。
1. 选择 **订阅**。

为订阅项目中的新标签成功完成的任何流水线，现在都会触发当前项目默认分支上的流水线。对于上游和下游项目，上游流水线订阅的最大数量默认为 2。在自助管理实例上，管理员可以更改此限制<!--[限制](../../administration/instance_limits.md#number-of-cicd-subscriptions-to-a-project)-->。

## 多项目流水线可视化 **(PREMIUM)**

当您为项目配置 GitLab CI/CD 时，您可以在[流水线图](index.md#可视化流水线)上可视化[作业](index.md#配置流水线)的各个阶段。

![Multi-project pipeline graph](img/multi_project_pipeline_graph_v14_3.png)

在合并请求中，在 **流水线** 选项卡上，显示了多项目流水线迷你图。
当悬停（或点击触摸屏设备）时，它们会展开并彼此相邻显示。

![Multi-project mini graph](img/multi_pipeline_mini_graph.gif)
