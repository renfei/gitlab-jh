---
stage: Verify
group: Testing
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
disqus_identifier: 'https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html'
type: reference, howto
---

# 作业产物 **(FREE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/16675) in GitLab 12.4, artifacts in internal and private projects can be previewed when [GitLab Pages access control](../../administration/pages/index.md#access-control) is enabled.
-->

作业可以输出文件和目录的存档。此输出称为作业产物。

您可以使用 GitLab UI 或 API<!--[API](../../api/job_artifacts.md#get-job-artifacts)--> 下载作业产物。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview of job artifacts, watch the video [GitLab CI pipelines, artifacts, and environments](https://www.youtube.com/watch?v=PCKDICEe10s).
Or, for an introduction, watch [GitLab CI pipeline tutorial for beginners](https://www.youtube.com/watch?v=Jav4vbUrqII).

For administrator information about job artifact storage, see [administering job artifacts](../../administration/job_artifacts.md).
-->

## 创建作业产物

要创建作业产物，请在您的 `.gitlab-ci.yml` 文件中使用 `artifacts` 关键字：

```yaml
pdf:
  script: xelatex mycv.tex
  artifacts:
    paths:
      - mycv.pdf
    expire_in: 1 week
```

在此示例中，名为 `pdf` 的作业调用 `xelatex` 命令从LaTeX 源文件 `mycv.tex` 构建 PDF 文件。

`paths` 关键字确定要添加到作业产物的文件。
文件和目录的所有路径都相对于创建作业的仓库。

`expire_in` 关键字决定保留作业产物的时间。
您还可以[使用 UI 防止作业产物过期](#下载作业产物)。
如果未定义 `expire_in`，则使用实例范围的设置<!--[实例范围设置](../../user/admin_area/settings/continuous_integration.md#default-artifacts-expiration)-->。

如果您为同一个 ref 运行两种类型的流水线（如分支和调度），稍后完成的流水线将创建作业产物。

To disable artifact passing, define the job with empty [dependencies](../yaml/index.md#dependencies):

```yaml
job:
  stage: build
  script: make build
  dependencies: []
```

You may want to create artifacts only for tagged releases to avoid filling the
build server storage with temporary build artifacts. For example, use [`rules`](../yaml/index.md#rules)
to create artifacts only for tags:

```yaml
default-job:
  script:
    - mvn test -U
  rules:
    - if: $CI_COMMIT_BRANCH

release-job:
  script:
    - mvn package -U
  artifacts:
    paths:
      - target/*.war
  rules:
    - if: $CI_COMMIT_TAG
```

You can use wildcards for directories too. For example, if you want to get all the
files inside the directories that end with `xyz`:

```yaml
job:
  artifacts:
    paths:
      - path/*xyz/*
```

### 使用 CI/CD 变量定义产物名称

您可以使用 <!--[CI/CD 变量](../variables/index.md)-->CI/CD 变量来动态定义产物文件的名称。

例如，要使用当前作业的名称创建存档：

```yaml
job:
  artifacts:
    name: "$CI_JOB_NAME"
    paths:
      - binaries/
```

要使用当前分支或标签的名称创建档案，仅包括二进制目录：

```yaml
job:
  artifacts:
    name: "$CI_COMMIT_REF_NAME"
    paths:
      - binaries/
```

如果您的分支名称包含正斜杠（例如`feature/my-feature`），建议使用 `$CI_COMMIT_REF_SLUG` 而不是 `$CI_COMMIT_REF_NAME` 来正确命名产物。

要使用当前作业的名称和当前的分支或标签创建仅包含二进制文件目录的存档：

```yaml
job:
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    paths:
      - binaries/
```

要使用当前 [stage](../yaml/index.md#stages) 和分支名称的名称创建存档：

```yaml
job:
  artifacts:
    name: "$CI_JOB_STAGE-$CI_COMMIT_REF_NAME"
    paths:
      - binaries/
```

如果您使用 **Windows Batch** 来运行你的 shell 脚本，你必须用 `%` 替换 `$`：

```yaml
job:
  artifacts:
    name: "%CI_JOB_STAGE%-%CI_COMMIT_REF_NAME%"
    paths:
      - binaries/
```

如果您使用 **Windows PowerShell** 运行你的 shell 脚本，你必须用 `$env:` 替换 `$`：

```yaml
job:
  artifacts:
    name: "$env:CI_JOB_STAGE-$env:CI_COMMIT_REF_NAME"
    paths:
      - binaries/
```

### 从作业产物中排除文件

使用 [`artifacts:exclude`](../yaml/index.md#artifactsexclude) 防止文件被添加到产物存档中。

例如，将所有文件存储在 `binaries/` 中，而不是位于 `binaries/` 子目录中的 `*.o` 文件。

```yaml
artifacts:
  paths:
    - binaries/
  exclude:
    - binaries/**/*.o
```

与 [`artifacts:paths`](../yaml/index.md#artifactspaths) 不同，`exclude` 路径不是递归的。
要排除目录的所有内容，请明确匹配它们而不是匹配目录本身。

例如，要将所有文件存储在 `binaries/` 中，但不存储在 `temp/` 子目录中：

```yaml
artifacts:
  paths:
    - binaries/
  exclude:
    - binaries/temp/**/*
```

### 将未跟踪的文件添加到产物

使用 [`artifacts:untracked`](../yaml/index.md#artifactsuntracked) 将所有 Git 未跟踪文件添加为产物（以及在 [`artifacts:paths`](../yaml/index.md#artifactspaths))。

将所有 Git 未跟踪文件和文件保存在 `binaries` 中：

```yaml
artifacts:
  untracked: true
  paths:
    - binaries/
```

保存所有未跟踪的文件，但[排除](../yaml/index.md#artifactsexclude) `*.txt`：

```yaml
artifacts:
  untracked: true
  exclude:
    - "*.txt"
```


## 下载作业产物

您可以下载作业产物或查看作业存档：

- 在 **流水线** 页面上，流水线右侧：

  ![Job artifacts in Pipelines page](img/job_artifacts_pipelines_page_v13_11.png)

- 在 **作业** 页面上，作业右侧：

  ![Job artifacts in Jobs page](img/job_artifacts_jobs_page_v13_11.png)

- 在作业的详细信息页面上。 **保留** 按钮表示设置了一个 `expire_in` 值：

  ![Job artifacts browser button](img/job_artifacts_browser_button_v13_11.png)

- 在合并请求中，通过流水线详细信息：

  ![Job artifacts in merge request](img/job_artifacts_merge_request_v13_11.png)

- 浏览存档时：

  ![Job artifacts browser](img/job_artifacts_browser_v13_11.png)

  如果项目中启用了 GitLab Pages<!--[GitLab Pages](../../administration/pages/index.md)-->，则可以直接在浏览器中预览产物中的HTML文件。如果项目是内部或私有的，则必须启用 <!--[GitLab Pages 访问控制](../../administration/pages/index.md#access-control)--> GitLab Pages 访问控制才能预览 HTML 文件。

## 查看失败的作业产物

如果最新的作业未能上传产物，您可以在 UI 中看到该信息。

![Latest artifacts button](img/job_latest_artifacts_browser.png)

## 删除作业产物

WARNING:
这是一种会导致数据丢失的破坏性操作。谨慎使用。

您可以删除单个作业，这也会删除作业的产物和日志。您必须是：

- 作业的所有者。
- 项目的维护者。

要删除作业：

1. 转到作业的详细信息页面。
1. 在作业日志的右上角，选择 **删除作业日志** (**{remove}**)。
1. 在确认对话框中，选择 **OK**。

## 在合并请求 UI 中公开作业产物

使用 `artifacts:expose_as` 关键字在合并请求 UI 中公开作业工件。

例如，要匹配单个文件：

```yaml
test:
  script: ["echo 'test' > file.txt"]
  artifacts:
    expose_as: 'artifact 1'
    paths: ['file.txt']
```

通过此配置，系统添加了一个链接 **artifact 1** 到指向 `file.txt` 的相关合并请求。要访问该链接，请选择合并请求概览中管道图下方的 **查看已公开产物**。

匹配整个目录的示例：

```yaml
test:
  script: ["mkdir test && echo 'test' > test/file.txt"]
  artifacts:
    expose_as: 'artifact 1'
    paths: ['test/']
```

## 检索其他项目的作业产物

要从不同的项目中检索作业产物，您可能需要使用私有令牌来身份验证和下载<!--[身份验证和下载](../../api/job_artifacts.md#get-job-artifacts)-->产物。

## 搜索作业产物的工作原理

在 13.5 及更高版本中，[父子流水线](parent_child_pipelines.md) 的产物按从父到子的层次顺序进行搜索。例如，如果父流水线和子流水线都有一个同名的作业，则返回父流水线中的作业产物。

## 通过 URL 访问最新的作业产物

您可以使用 URL 从最新的成功流水线下载作业产物。

要下载整个产物档案：

```plaintext
https://example.com/<namespace>/<project>/-/jobs/artifacts/<ref>/download?job=<job_name>
```

要从产物下载单个文件：

```plaintext
https://example.com/<namespace>/<project>/-/jobs/artifacts/<ref>/raw/<path_to_file>?job=<job_name>
```

例如，要在 `gitlab-org` 命名空间的 `gitlab` 项目的 `main` 分支中下载名为 `coverage` 的作业的最新产物：

```plaintext
https://gitlab.com/gitlab-org/gitlab/-/jobs/artifacts/main/download?job=coverage
```

要从相同的产物下载文件 `review/index.html`：

```plaintext
https://gitlab.com/gitlab-org/gitlab/-/jobs/artifacts/main/raw/review/index.html?job=coverage
```

要浏览最新的作业产物：

```plaintext
https://example.com/<namespace>/<project>/-/jobs/artifacts/<ref>/browse?job=<job_name>
```

例如：

```plaintext
https://gitlab.com/gitlab-org/gitlab/-/jobs/artifacts/main/browse?job=coverage
```

要下载特定文件，包括 GitLab Pages<!--[GitLab Pages](../../administration/pages/index.md)--> 中显示的 HTML 文件：

```plaintext
https://example.com/<namespace>/<project>/-/jobs/artifacts/<ref>/file/<path>?job=<job_name>
```

例如，当作业 `coverage` 创建产物 `htmlcov/index.html` 时：

```plaintext
https://gitlab.com/gitlab-org/gitlab/-/jobs/artifacts/main/file/htmlcov/index.html?job=coverage
```

## 何时删除作业产物

有关何时删除作业产物的信息，请参阅 [`expire_in`](../yaml/index.md#artifactsexpire_in) 文档。

### 保留最近成功作业的产物

> - 引入于 13.0 版本。
> - 功能标志移除于 13.4 版本。
> - 支持通过 CI/CD 设置可选于 13.8 版本。

保留最新的产物可以在有大量作业或大型产物的项目中使用大量存储空间。如果项目中不需要最新的产物，您可以禁用此行为以节省空间：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **产物**。
1. 清除 **保留最近成功作业的产物** 复选框。

您可以在实例的 CI/CD 设置<!--[实例的 CI/CD 设置](../../user/admin_area/settings/continuous_integration.md#keep-the-latest-artifacts-for-all-jobs-in-the-latest-successful-pipelines)-->中，为自助管理实例上的所有项目禁用此行为。

当您禁用该功能时，最新的产物不会立即过期。
在最新的产物过期并被删除之前，必须运行新的流水线。

## 作业产物故障排查

### 错误消息 `No files to upload`

此消息之前通常会出现其他错误或警告，这些错误或警告指定了文件名以及未生成文件名的原因。检查作业日志以获取这些消息。

如果您没有发现有用的消息，请在激活 <!--[CI/CD 调试日志记录](../variables/index.md#debug-logging)-->CI/CD 调试日志记录后重试失败的作业。
此日志记录应提供帮助您进一步调查的信息。
