---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, index
last_update: 2019-07-03
---

# 合并请求流水线 **(FREE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/15310) in GitLab 11.6.
-->

在[基本配置](pipeline_architectures.md#基本流水线)中，每次将更改推送到分支时，极狐GitLab 都会运行一个流水线。

如果您希望流水线在与合并请求关联的提交上**仅**运行作业，您可以使用*合并请求流水线*。

这些流水线在 UI 中被标记为 `detached`，并且它们无权访问受保护的变量<!--[受保护的变量](../variables/index.md#protect-a-cicd-variable)-->。
否则，这些流水线与其它流水线相同。

合并请求的管道可以在您执行以下操作时运行：

- 创建一个新的合并请求。
- 为合并请求提交对源分支的更改。
- 从合并请求的 **流水线** 选项卡中选择 **运行流水线** 按钮。

如果将此功能与[流水线成功时合并](../../user/project/merge_requests/merge_when_pipeline_succeeds.md)一起使用，则合并请求的流水线优先于其它流水线。

## 先决条件

为合并请求启用流水线：

- 您的仓库必须是极狐GitLab 仓库，而不是<!--[外部仓库](../ci_cd_for_external_repos/index.md)-->。
- 您必须具有开发人员角色才能运行合并请求的流水线。

## 为合并请求配置流水线

要为合并请求配置流水线，您必须配置您的 [CI/CD 配置文件](../yaml/index.md)。
要这样做，您可以使用 [`rules`](#使用-rules-为合并请求运行流水线) 或 [`only/except`](#使用-only-或-except-为合并请求运行流水线)。

### 使用 `rules` 为合并请求运行流水线

建议您使用`rules` 关键字，该关键字在 [`workflow:rules` 模板](../yaml/index.md#workflowrules-模板)中可用。

### 使用 `only` 或 `except` 为合并请求运行流水线

您可以使用 `only/except` 关键字。但是，使用此方法，您必须为每个作业指定 `only: - merge_requests`。

在以下示例中，流水线包含一个配置为在合并请求上运行的 `test` 作业。
`build` 和 `deploy` 作业没有 `only: - merge_requests` 关键字，因此它们不会在合并请求上运行。

```yaml
build:
  stage: build
  script: ./build
  only:
    - main

test:
  stage: test
  script: ./test
  only:
    - merge_requests

deploy:
  stage: deploy
  script: ./deploy
  only:
    - main
```

#### 排除特定作业

当您使用 `only: [merge_requests]` 时，只有具有该关键字的作业才会在合并请求的上下文中运行。没有其他作业运行。

但是，您可以反转此行为并运行所有作业，但一两个作业除外。例如，您可能有一个包含作业 `A`、`B` 和 `C` 的流水线，并且您想要：

- 所有流水线始终运行 `A` 和 `B`。
- `C` 只为合并请求运行。

要实现此结果，请按如下方式配置您的 `.gitlab-ci.yml` 文件：

```yaml
.only-default: &only-default
  only:
    - main
    - merge_requests
    - tags

A:
  <<: *only-default
  script:
    - ...

B:
  <<: *only-default
  script:
    - ...

C:
  script:
    - ...
  only:
    - merge_requests
```

- `A` 和 `B` 始终运行，因为它们在所有情况下都会执行 `only` 规则。
- `C` 只为合并请求运行。除了合并请求流水线之外，它不会为任何流水线运行。

在此示例中，您不必将 `only` 规则添加到所有作业以使它们始终运行。您可以使用这种格式来设置 Review App，这有助于节省资源。

#### 排除特定分支

分支引用使用这种格式：`refs/heads/my-feature-branch`。
合并请求引用使用这种格式：`refs/merge-requests/:iid/head`。

由于这种差异，以下配置无法按预期工作：

```yaml
# Does not exclude a branch named "docs-my-fix"!
test:
  only: [merge_requests]
  except: [/^docs-/]
```

相反，使用 <!--[`$CI_COMMIT_REF_NAME` 预定义环境变量](../variables/predefined_variables.md)--> `$CI_COMMIT_REF_NAME` 预定义环境变量结合 [`only:variables`](../yaml/index.md#onlyvariables--exceptvariables) 来完成：

```yaml
test:
  only: [merge_requests]
  except:
    variables:
      - $CI_COMMIT_REF_NAME =~ /^docs-/
```

## 在父项目中为来自派生项目的合并请求运行流水线 **(PREMIUM)**

> - 引入于 13.3 版本
> - 专业版移除于 13.9 版本

默认情况下，在派生中工作的外部贡献者无法在父项目中创建流水线。 当来自派生的合并请求触发流水线时：

- 流水线在派生（源）项目中创建和运行，而不是在父（目标）项目中。
- 流水线使用派生项目的 CI/CD 配置和资源。

如果流水线在派生中运行，则会在合并请求中为流水线显示 **派生** 标志。

![Pipeline ran in fork](img/pipeline-fork_v13_7.png)

有时，父项目成员希望流水线在父项目中运行。他们可能希望确保合并后流水线在父项目中传递。
例如，派生项目可能会尝试使用损坏的，无法正确执行测试脚本，但会报告已通过的流水线的 runner。父项目中的审核者可能会错误地信任合并请求，因为它通过了一个假流水线。

至少具有开发者角色的父项目成员，可以在父项目中为来自派生项目的合并请求创建流水线。在合并请求中，转到 **流水线** 选项卡并选择 **运行流水线**。

WARNING:
派生的合并请求可能包含恶意代码，这些代码会在流水线运行时甚至在合并之前尝试窃取父项目中的机密。作为审核者，您必须在触发流水线之前仔细检查合并请求中的更改。系统会显示一条警告，您必须先接受该警告，然后才能触发流水线。

## 可用于合并请求流水线的预定义变量

当您将流水线用于合并请求时，<!--[附加预定义变量](../variables/predefined_variables.md#predefined-variables-for-merge-request-pipelines)-->附加预定义变量可用于 CI/CD 作业。
这些变量包含来自关联合并请求的信息，以便您可以将您的作业与 GitLab 合并请求 API<!--[GitLab 合并请求 API](../../api/merge_requests.md)--> 集成。

## 故障排查

### 推送到合并请求时创建的两个流水线

如果您在使用 `rules` 时遇到重复的流水线，请查看 <!--[`rules` 和 `only`/`except` 之间的重要区别](../jobs/job_control.md#avoid-duplicate-pipelines)-->`rules` 和 `only`/`except` 之间的重要区别，这有助于您正确设置起始配置。

如果您在使用 `only/except` 时看到两个流水线，请参阅上面与使用 `only/except` 相关的注意事项（或者，考虑转向 `rules`）。

在 13.7 及更高版本中，您可以添加 `workflow:rules`，[从分支流水线切换到合并请求流水线](../yaml/index.md#在分支流水线和合并请求流水线之间切换)。
在分支上打开合并请求后，流水线切换到合并请求流水线。

### 推送无效 CI 配置文件时创建的两个流水线

使用无效的 CI 配置文件推送到分支可能会触发创建两种类型的失败流水线。一个流水线是失败的合并请求流水线，另一个是失败的分支流水线，但两者都是由相同的无效配置引起的。

<!--
## Related topics

- [Pipelines for merged results](pipelines_for_merged_results.md).
- [Merge trains](merge_trains.md).
-->