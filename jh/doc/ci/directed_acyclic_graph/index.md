---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 有向无环图 **(FREE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/47063) in GitLab 12.2.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/206902) in GitLab 12.10.
-->

[有向无环图](https://www.techopedia.com/definition/5739/directed-acyclic-graph-dag) 可以在 CI/CD 流水线的上下文中，用于在作业之间建立关系，以便以最快的方式执行，无论阶段如何设置。

例如，您可能拥有作为主要项目的一部分而构建的特定工具或单独的网站。使用 DAG，您可以指定这些作业之间的关系，系统会尽快执行作业，而不是等待每个阶段完成。

与 CI/CD 的其他 DAG 解决方案不同，系统不需要您选择其中之一。您可以在单个流水线中实现 DAG 和传统的基于阶段的操作的混合组合。配置非常简单，只需一个关键字即可为任何作业启用该功能。

考虑一个 monorepo 如下：

```plaintext
./service_a
./service_b
./service_c
./service_d
```

它有一个如下所示的流水线：

| build | test | deploy |
| ----- | ---- | ------ |
| build_a | test_a | deploy_a |
| build_b | test_b | deploy_b |
| build_c | test_c | deploy_c |
| build_d | test_d | deploy_d |

使用 DAG，你可以将 `_a` 作业与 `_b` 作业分别关联起来，即使服务 `a` 需要很长时间来构建，服务 `b` 也不会等待它并尽可能快完成。在这个完全相同的流水线中，`_c` 和 `_d` 可以单独放置，并像任何普通的 GitLab 流水线一样，以分阶段的顺序一起运行。

## 用例

DAG 可以帮助解决 CI/CD 流水线内作业之间的几种不同类型的关系。<!--最典型的是，这将涵盖作业需要 fan in 或 fan out，和/或重新合并在一起（菱形依赖项）的情况。-->当您处理多平台构建或复杂的依赖网络时，可能会发生这种情况，例如操作系统构建或可独立部署但相关的微服务的复杂部署图。

此外，DAG 可以帮助提高流水线的总体速度并帮助提供快速反馈。通过创建不会不必要地相互阻塞的依赖关系，无论流水线阶段如何，您的流水线都会尽可能快地运行，从而确保开发人员尽快获得输出（包括错误）。

## 使用

使用 [`needs` 关键字](../yaml/index.md#needs) 定义作业之间的关系。

请注意，`needs` 也可以与 [parallel](../yaml/index.md#parallel) 关键字一起使用，为您的流水线中的并行化提供强大的选项。

## 限制

有向无环图是一个复杂的功能，从最初的 MVC 开始，您可能需要解决某些用例。想要查询更多的信息：

- [`needs` 要求和限制](../yaml/index.md#要求和限制)。
- 相关的史诗跟踪计划改进。

## Needs 可视化

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/215517) in GitLab 13.1 as a [Beta feature](https://about.gitlab.com/handbook/product/#beta).
> - It became a [standard feature](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38517) in 13.3.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/52208) in GitLab 13.9.
-->

需求可视化可以更轻松地可视化 DAG 中依赖作业之间的关系。此图显示流水线中需要或其他作业需要的所有作业。 没有关系的作业不会显示在此视图中。

要查看需求可视化，请在查看使用 `needs` 关键字的流水线时单击 **Needs** 选项卡。

![Needs visualization example](img/dag_graph_example_v13_1.png)

单击一个节点会突出显示它所依赖的所有作业路径。

![Needs visualization with path highlight](img/dag_graph_example_clicked_v13_1.png)

您还可以在[完整流水线图](../pipelines/index.md#查看完整的流水线图)中查看 `needs` 关系。
