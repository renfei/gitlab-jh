---
stage: Verify
group: Runner
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# Using Redis **(FREE)**

由于许多应用程序依赖 Redis 作为它们的键值存储，因此您最终需要它来运行您的测试。下面将指导您如何使用 GitLab Runner 的 Docker 和 Shell executor 执行此操作。

## 将 Redis 与 Docker executor 一起使用

如果您使用 [GitLab Runner](../runners/index.md) 和 Docker executor，基本上已经完成设置。

首先，在 `.gitlab-ci.yml` 中添加：

```yaml
services:
  - redis:latest
```

然后您需要配置您的应用程序以使用 Redis 数据库，例如：

```yaml
Host: redis
```

就是这样。 Redis 现在可以在您的测试框架中使用。

您还可以使用 [Docker Hub](https://hub.docker.com/_/redis) 上可用的任何其他 Docker 镜像。
例如，要使用 Redis 6.0，服务将变为 `redis:6.0`。

## 将 Redis 与 Shell executor 一起使用

Redis 也可用于手动配置的服务器，这些服务器使用 GitLab Runner 和 Shell executor。

在你的构建机器上安装 Redis 服务器：

```shell
sudo apt-get install redis-server
```

验证您是否可以使用 `gitlab-runner` 用户连接到服务器：

```shell
# Try connecting the Redis server
sudo -u gitlab-runner -H redis-cli

# Quit the session
127.0.0.1:6379> quit
```

最后，配置您的应用程序以使用数据库，例如：

```yaml
Host: localhost
```

<!--
## Example project

We have set up an [Example Redis Project](https://gitlab.com/gitlab-examples/redis) for your convenience
that runs on [GitLab.com](https://gitlab.com) using our publicly available
[shared runners](../runners/index.md).

Want to hack on it? Simply fork it, commit and push your changes. Within a few
moments the changes are picked by a public runner and the job begins.
-->