---
stage: Verify
group: Runner
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 使用 PostgreSQL **(FREE)**

由于许多应用程序依赖 PostgreSQL 作为它们的数据库，因此您最终需要它来运行您的测试。下面将指导您如何使用 GitLab Runner 的 Docker 和 Shell executor 执行此操作。

## 将 PostgreSQL 与 Docker executor 一起使用

如果你使用 [GitLab Runner](../runners/index.md) 和 Docker executor，基本上已经设置完成。

NOTE:
GitLab UI 中设置的变量不会传递到服务容器。[了解更多](../variables/index.md)。

首先，在 `.gitlab-ci.yml` 中添加：

```yaml
services:
  - postgres:12.2-alpine

variables:
  POSTGRES_DB: $POSTGRES_DB
  POSTGRES_USER: $POSTGRES_USER
  POSTGRES_PASSWORD: $POSTGRES_PASSWORD
  POSTGRES_HOST_AUTH_METHOD: trust
```

然后配置您的应用程序使用数据库，例如：

```yaml
Host: postgres
User: $POSTGRES_USER
Password: $POSTGRES_PASSWORD
Database: $POSTGRES_DB
```

如果您想知道为什么我们使用 `postgres` 作为 `Host`，请阅读服务如何链接到作业<!--[服务如何链接到作业](../services/index.md#服务如何链接到作业)-->。

您还可以使用 [Docker Hub](https://hub.docker.com/_/postgres) 上提供的任何其他 Docker 镜像。
例如，要使用 PostgreSQL 9.3，该服务将变为 `postgres:9.3`。

`postgres` 镜像可以接受一些环境变量。有关更多详细信息，请参阅 [Docker Hub](https://hub.docker.com/_/postgres) 上的文档。

## 将 PostgreSQL 与 Shell executor 一起使用

您还可以在手动配置的服务器上使用 PostgreSQL，这些服务器将 GitLab Runner 与 Shell executor 一起使用。

首先安装 PostgreSQL 服务器：

```shell
sudo apt-get install -y postgresql postgresql-client libpq-dev
```

下一步是创建一个用户，登录 PostgreSQL：

```shell
sudo -u postgres psql -d template1
```

然后创建一个由您的应用程序使用的用户（在我们的例子中为 `runner`）。将下面命令中的 `$password` 更改为真正的强密码。

NOTE:
确保不要在以下命令中输入 `template1=#`，因为这是 PostgreSQL 提示的一部分。

```shell
template1=# CREATE USER runner WITH PASSWORD '$password' CREATEDB;
```

创建的用户有权创建数据库（`CREATEDB`）。以下步骤描述了如何为该用户显式创建数据库，但如果在您的测试框架中，有删除和创建数据库的工具，拥有该权限会很有用。

创建数据库并为用户 `runner` 授予所有权限：

```shell
template1=# CREATE DATABASE nice_marmot OWNER runner;
```

如果一切顺利，您现在可以退出数据库会话：

```shell
template1=# \q
```

现在，尝试使用用户 `runner` 连接到新创建的数据库，以检查一切是否就绪。

```shell
psql -U runner -h localhost -d nice_marmot -W
```

此命令明确指示 `psql` 连接到 localhost 以使用 md5 身份验证。如果您省略此步骤，您将被拒绝访问。

最后，配置您的应用程序以使用数据库，例如：

```yaml
Host: localhost
User: runner
Password: $password
Database: nice_marmot
```

<!--
## Example project

We have set up an [Example PostgreSQL Project](https://gitlab.com/gitlab-examples/postgres) for your
convenience that runs on [GitLab.com](https://gitlab.com) using our publicly
available [shared runners](../runners/index.md).

Want to hack on it? Fork it, commit, and push your changes. Within a few
moments the changes are picked by a public runner and the job begins.
-->