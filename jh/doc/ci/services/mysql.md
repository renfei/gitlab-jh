---
stage: Verify
group: Runner
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 使用 MySQL **(FREE)**

许多应用程序依赖 MySQL 作为它们的数据库，您可能需要它来运行您的测试。

## 将 MySQL 与 Docker executor 一起使用

如果要使用 MySQL 容器，可以将 GitLab Runner<!--[GitLab Runner](../runners/index.md)--> 与 Docker executor 一起使用。

此示例向您展示如何设置 GitLab 用于访问 MySQL 容器的用户名和密码。如果不设置用户名和密码，则必须使用 `root`。

NOTE:
GitLab UI 中设置的变量不会传递到服务容器。[了解更多](../variables/index.md)。

1. 要指定 MySQL 镜像，请将以下内容添加到您的 `.gitlab-ci.yml` 文件中：

   ```yaml
   services:
     - mysql:latest
   ```

   - 您可以使用 [Docker Hub](https://hub.docker.com/_/mysql/) 上可用的任何 Docker 镜像。例如，要使用 MySQL 5.5，请使用 `mysql:5.5`。
   - `mysql` 镜像可以接受环境变量。更多信息请查看 [Docker Hub 文档](https://hub.docker.com/_/mysql/)。

1. 要包含数据库名称和密码，请将以下内容添加到您的 `.gitlab-ci.yml` 文件中：

   ```yaml
   variables:
     # Configure mysql environment variables (https://hub.docker.com/_/mysql/)
     MYSQL_DATABASE: $MYSQL_DATABASE
     MYSQL_ROOT_PASSWORD: $MYSQL_ROOT_PASSWORD
   ```

   MySQL 容器使用 `MYSQL_DATABASE` 和 `MYSQL_ROOT_PASSWORD` 来连接数据库。通过使用变量（`$MYSQL_DB` 和 `$MYSQL_PASS`）传递这些值，而不是直接调用它们<!--[而不是直接调用它们](https://gitlab.com/gitlab-org/gitlab/-/issues/30178)-->。

1. 配置您的应用程序以使用数据库，例如：

   ```yaml
   Host: mysql
   User: runner
   Password: <your_mysql_password>
   Database: <your_mysql_database>
   ```

   在这个例子中，用户是 `runner`。您应该使用有权访问您的数据库的用户。

## 将 MySQL 与 Shell executor 一起使用

您还可以在手动配置的服务器上使用 MySQL，这些服务器使用 GitLab Runner 和 Shell executor。

1. 安装 MySQL 服务器：

   ```shell
   sudo apt-get install -y mysql-server mysql-client libmysqlclient-dev
   ```

1. 选择一个 MySQL 根密码并在询问时输入两次。

   NOTE:
   作为安全措施，您可以运行 `mysql_secure_installation` 来删除匿名用户、删除测试数据库并禁用 root 用户的远程登录。

1. 通过以 root 身份登录 MySQL 创建一个用户：

   ```shell
   mysql -u root -p
   ```

1. 创建一个由您的应用程序使用的用户（在本例中为 `runner`）。将命令中的 `$password` 更改为强密码。在 `mysql>` 提示符下，键入：

   ```sql
   CREATE USER 'runner'@'localhost' IDENTIFIED BY '$password';
   ```

1. 创建数据库：

   ```sql
   CREATE DATABASE IF NOT EXISTS `<your_mysql_database>` DEFAULT CHARACTER SET `utf8` \
   COLLATE `utf8_unicode_ci`;
   ```

1. 授予对数据库的必要权限：

   ```sql
   GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, CREATE TEMPORARY TABLES, DROP, INDEX, ALTER, LOCK TABLES ON `<your_mysql_database>`.* TO 'runner'@'localhost';
   ```

1. 如果一切顺利，您可以退出数据库会话：

   ```shell
   \q
   ```

1. 连接到新创建的数据库以检查一切是否就绪：

   ```shell
   mysql -u runner -p -D <your_mysql_database>
   ```

1. 配置您的应用程序以使用数据库，例如：

   ```shell
   Host: localhost
   User: runner
   Password: $password
   Database: <your_mysql_database>
   ```

<!--
## Example project

To view a MySQL example, create a fork of this [sample project](https://gitlab.com/gitlab-examples/mysql).
This project uses publicly-available [shared runners](../runners/index.md) on [GitLab.com](https://gitlab.com).
Update the README.md file, commit your changes, and view the CI/CD pipeline to see it in action.
-->