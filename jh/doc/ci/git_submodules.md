---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 在 GitLab CI/CD 中使用 Git 子模块 **(FREE)**

使用 [Git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules) 将 Git 仓库作为另一个 Git 仓库的子目录。您可以将另一个仓库克隆到您的项目中，并将您的提交分开。

## 配置 `.gitmodules` 文件

当使用 Git 子模块时，您的项目应该有一个名为 `.gitmodules` 的文件。
您可能需要修改它才能在 GitLab CI/CD 作业中工作。

例如，您的 `.gitmodules` 配置可能如下所示：

- 你的项目位于 `https://gitlab.cn/secret-group/my-project`。
- 您的项目依赖于 `https://gitlab.cn/group/project`，您希望将其作为子模块包含在内。
- 您可以使用 SSH 地址查看源代码，例如 `git@gitlab.com:secret-group/my-project.git`。

```ini
[submodule "project"]
  path = project
  url = ../../group/project.git
```

当您的子模块在同一个 GitLab 服务器上时，您应该在 `.gitmodules` 文件中使用相对 URL。然后，您可以在所有 CI/CD 作业中使用 HTTPS 进行克隆。您还可以将 SSH 用于所有本地检出。

上面的配置指示 Git 在克隆源时自动推导要使用的 URL。Git 对 HTTPS 和 SSH 使用相同的配置。
GitLab CI/CD 使用 HTTPS 来克隆您的源代码，您可以继续使用 SSH 在本地进行克隆。

对于不在同一 GitLab 服务器上的子模块，请使用完整 URL：

```ini
[submodule "project-x"]
  path = project-x
  url = https://gitserver.com/group/project-x.git
```

## 在 CI/CD 作业中使用 Git 子模块

要使子模块在 CI/CD 作业中正常工作：

1. 确保对位于同一 GitLab 服务器中的子模块使用[相对 URL](#配置-gitmodules-文件)。
1. 您可以将 `GIT_SUBMODULE_STRATEGY` 变量设置为 `normal` 或 `recursive`，告诉 runner 在作业之前获取您的子模块<!--[在作业之前获取您的子模块](runners/configure_runners.md#git-submodule-strategy)-->：

   ```yaml
   variables:
     GIT_SUBMODULE_STRATEGY: recursive
   ```
   
如果您使用 [`CI_JOB_TOKEN`](jobs/ci_job_token.md) 克隆流水线作业中的子模块，则必须为执行作业的用户，分配有权在上游子模块项目中触发流水线的角色。
