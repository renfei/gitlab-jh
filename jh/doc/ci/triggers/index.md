---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: tutorial
---

# 通过 API 触发流水线 **(FREE)**

触发器可用于通过 API 调用强制重新运行特定 `ref`（分支或标签）的流水线。

## 身份验证令牌

支持以下身份验证方法：

- 触发令牌：[添加新触发器](#添加新触发器)时可以获得唯一的触发令牌。
- CI 作业令牌<!--[CI 作业令牌](../jobs/ci_job_token.md)-->。

如果使用 `$CI_PIPELINE_SOURCE` <!--[预定义 CI/CD 变量](../variables/predefined_variables.md)-->来限制哪些作业在流水线中运行，则该值可以是 `pipeline` 或 `trigger`，具体取决于使用的触发方式。

| `$CI_PIPELINE_SOURCE` 值 | 触发方式 |
|-----------------------------|----------------|
| `pipeline`                  | 在 CI/CD 配置文件中使用 `trigger` 关键字，或使用带有 `$CI_JOB_TOKEN` 的触发器 API。 |
| `trigger`                   | 使用生成的触发器令牌使用触发器 API |

这也适用于使用带有旧的 [`only/except` 基本语法](../yaml/index.md#only--except)的 `pipelines` 或 `triggers` 关键字。

## 添加新触发器

转至 **设置 > CI/CD** 在 **触发器** 下添加新触发器。**添加触发器** 按钮会创建一个新令牌，然后您可以使用它来触发此特定项目流水线的重新运行。

您创建的每个新触发器都会分配一个不同的令牌，然后您可以在脚本或 `.gitlab-ci.yml` 中使用该令牌。您还可以很好地了解上次使用触发器的时间。

![Triggers page overview](img/triggers_page.png)

WARNING:
在公共项目中传递纯文本令牌是一个安全问题。潜在的攻击者可以冒充在其 `.gitlab-ci.yml` 文件中公开公开其触发令牌的用户。使用 <!--[CI/CD 变量](../variables/index.md)-->CI/CD 变量来保护触发器令牌。

## 撤销触发器

您可以随时通过进入项目的 **设置 > CI/CD** 并在 **触发器** 下点击 **撤销** 按钮来撤销触发器。
操作是不可逆的。

## 触发流水线

要触发管道，您需要向 GitLab API 端点发送 `POST` 请求：

```plaintext
POST /projects/:id/trigger/pipeline
```

所需的参数是[触发器 `token`](#身份验证令牌) 和执行触发器的 Git `ref`。有效的 ref 是分支或标签。可以通过查询 API<!--[查询 API](../../api/projects.md)--> 或访问提供示例的 **CI/CD** 设置页面，来找到项目的 `:id`。

当触发流水线重新运行时，作业在 **CI/CD > 作业** 中标记为 `triggered`。

您可以通过访问单个作业页面来查看哪个触发器导致作业运行。
触发器令牌的一部分显示在 UI 中，如下图所示。

![Marked as triggered on a single job page](img/trigger_single_job.png)

通过使用 cURL，您可以轻松触发流水线重新运行，例如：

```shell
curl --request POST \
     --form token=TOKEN \
     --form ref=main \
     "https://gitlab.example.com/api/v4/projects/9/trigger/pipeline"
```

在这种情况下，ID 为 9 的项目的流水线在 main 分支上运行。

或者，您可以在查询字符串中传递 `token` 和 `ref` 参数：

```shell
curl --request POST \
    "https://gitlab.example.com/api/v4/projects/9/trigger/pipeline?token=TOKEN&ref=main"
```

您还可以通过在您的 `.gitlab-ci.yml` 中使用触发器来受益。假设您有两个项目 A 和 B，并且您想在项目 A 上创建标签时触发项目 B 的 `main` 分支上的流水线。这是您需要在项目 A 的 `.gitlab-ci.yml` 中添加的作业：

```yaml
trigger_pipeline:
  stage: deploy
  script:
    - 'curl --request POST --form token=TOKEN --form ref=main "https://gitlab.example.com/api/v4/projects/9/trigger/pipeline"'
  rules:
    - if: $CI_COMMIT_TAG
```

这意味着每当在项目 A 上推送新标签时，该作业都会运行并执行 `trigger_pipeline` 作业，从而触发项目 B 的流水线。`stage:deploy` 确保此作业仅在所有带有 `stage: test` 的作业成功完成后运行。

NOTE:
您不能使用 API 来启动 `when:manual` 触发器作业。

## 从 webhook 触发流水线

要从另一个项目的 webhook 触发作业，您需要为 Push 和 Tag 事件添加以下 webhook URL（更改项目 ID、引用和令牌）：

```plaintext
https://gitlab.example.com/api/v4/projects/9/ref/main/trigger/pipeline?token=TOKEN
```

您应该将 `ref` 作为 URL 的一部分传递，以优先于 webhook 主体中的 `ref`，该主体指定在源仓库中触发触发器的分支 ref。如果包含斜杠，请确保对 `ref` 进行 URL 编码。

### Using webhook payload in the triggered pipeline

> - 引入于 13.9 版本。
> - 功能标志移除于 13.11 版本。

如果您使用 webhook 触发流水线，则可以使用 `TRIGGER_PAYLOAD` <!--[预定义 CI/CD 变量](../variables/predefined_variables.md)-->预定义 CI/CD 变量访问 webhook 负载。
负载作为文件类型变量<!--[文件类型变量](../variables/index.md#cicd-variable-types)-->公开，因此您可以使用 `cat $TRIGGER_PAYLOAD` 或类似命令访问数据。

## 使用触发变量

你可以在触发器 API 调用中传递任意数量的任意变量，它们在 GitLab CI/CD 中可用，以便它们可以在您的 `.gitlab-ci.yml` 文件中使用。参数的格式为：

```plaintext
variables[key]=value
```

此信息也显示在 UI 中。*Values* 只能由拥有所有者和维护者角色的用户查看。

![Job variables in UI](img/trigger_variables.png)

由于多种原因，可以证明使用触发变量很有用：

- 可识别的工作。由于该变量在 UI 中公开，如果您传递一个解释目的的变量，您就可以知道为什么触发流水线。
- 有条件的作业处理。只要存在某个变量，就可以运行有条件的作业。

考虑下面的 `.gitlab-ci.yml`，我们设置了三个 [stages](../yaml/index.md#stages) 并且 `upload_package` 作业仅在测试和构建阶段的所有作业都通过时运行。当 `UPLOAD_TO_S3` 变量非零时，运行 `make upload`。

```yaml
stages:
  - test
  - build
  - package

run_tests:
  stage: test
  script:
    - make test

build_package:
  stage: build
  script:
    - make build

upload_package:
  stage: package
  script:
    - if [ -n "${UPLOAD_TO_S3}" ]; then make upload; fi
```

然后，您可以在传递 `UPLOAD_TO_S3` 变量并运行 `upload_package` 作业的脚本时触发流水线：

```shell
curl --request POST \
  --form token=TOKEN \
  --form ref=main \
  --form "variables[UPLOAD_TO_S3]=true" \
  "https://gitlab.example.com/api/v4/projects/9/trigger/pipeline"
```

触发器变量具有所有类型变量的最高优先级<!--[最高优先级](../variables/index.md#cicd-variable-precedence)-->。

## 使用 cron 触发夜间流水线

无论您是编写脚本还是直接运行 cURL，您都可以与 cron 一起触发作业。下面的示例在每天晚上 00:30 触发 ID 为 9 的项目的 main 分支上的作业：

```shell
30 0 * * * curl --request POST --form token=TOKEN --form ref=main "https://gitlab.example.com/api/v4/projects/9/trigger/pipeline"
```

这种操作也可以通过 GitLab UI 使用[流水线计划](../pipelines/schedules.md)来实现。

<!--
## 旧式触发器

Old triggers, created before GitLab 9.0 are marked as legacy.

Triggers with the legacy label do not have an associated user and only have
access to the current project. They are considered deprecated and might be
removed with one of the future versions of GitLab.
-->

## 故障排查

### 当触发流水线时出现 '404 not found' 

触发流水线时的 `{"message":"404 Not Found"}` 响应可能是由于使用个人访问令牌而不是触发器令牌引起的。[添加新触发器](#添加新触发器)并在触发流水线时使用该令牌进行身份验证。
