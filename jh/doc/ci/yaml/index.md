---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# `.gitlab-ci.yml` 文件的关键字参考 **(FREE)**

本文档列出了 GitLab `.gitlab-ci.yml` 文件的配置选项。

- 有关 GitLab CI/CD 的快速介绍，请遵循[快速入门指南](../quick_start/index.md)。
- 有关示例集合，请参阅 [GitLab CI/CD 示例](../examples/index.md)。
- 要查看企业使用的大型 `.gitlab-ci.yml` 文件，请参阅[`gitlab`的`.gitlab-ci.yml`文件](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/.gitlab-ci.yml)。

当在编辑您的 `.gitlab-ci.yml` 文件时，可以使用 [CI Lint](../lint.md) 工具来验证它。

## 关键字

GitLab CI/CD 流水线配置包括：

- 配置流水线行为的[全局关键字](#全局关键字)：

  | 关键字                  | 描述 |
  |:-------------------------|:------------|
  | [`default`](#default)   | 作业关键字的自定义默认值。 |
  | [`stages`](#stages)     | 流水线阶段的名称和顺序。 |
  | [`workflow`](#workflow) | 控制运行的流水线类型。 |
  | [`include`](#include)   | 从其他 YAML 文件导入配置。 |

- [作业](../jobs/index.md)由[作业关键字](#作业关键字)配置：

| 关键字                                     | 描述 |
| :-------------------------------------------|:------------|
| [`after_script`](#after_script)             | 覆盖作业后执行的一组命令。 |
| [`allow_failure`](#allow_failure)           | 允许作业失败。失败的作业不会导致流水线失败。 |
| [`artifacts`](#artifacts)                   | 成功时附加到作业的文件和目录列表。 |
| [`before_script`](#before_script)           | 覆盖在作业之前执行的一组命令。 |
| [`cache`](#cache)                           | 应在后续运行之间缓存的文件列表。 |
| [`coverage`](#coverage)                     | 给定作业的代码覆盖率设置。 |
| [`dast_configuration`](#dast_configuration) | 在作业级别使用来自 DAST 配置文件的配置。 |
| [`dependencies`](#dependencies)             | 通过提供要从中获取产物的作业列表，来限制将哪些产物传递给特定作业。 |
| [`environment`](#environment)               | 作业部署到的环境的名称。 |
| [`except`](#only--except)                   | 控制何时不创建作业。 |
| [`extends`](#extends)                       | 此作业继承自的配置条目。 |
| [`image`](#image)                           | 使用 Docker 镜像。 |
| [`inherit`](#inherit)                       | 选择所有作业继承的全局默认值。 |
| [`interruptible`](#interruptible)           | 定义当新运行使作业变得多余时，是否可以取消作业。 |
| [`needs`](#needs)                           | 在 stage 顺序之前执行的作业。 |
| [`only`](#only--except)                     | 控制何时创建作业。 |
| [`pages`](#pages)                           | 上传作业的结果，与 GitLab Pages 一起使用。 |
| [`parallel`](#parallel)                     | 应该并行运行多少个作业实例。 |
| [`release`](#release)                       | 指示运行器生成 release<!--[release](../../user/project/releases/index.md)--> 对象。 |
| [`resource_group`](#resource_group)         | 限制作业并发。 |
| [`retry`](#retry)                           | 在失败的情况下可以自动重试作业的时间和次数。 |
| [`rules`](#rules)                           | 用于评估和确定作业的选定属性以及它是否已创建的条件列表。 |
| [`script`](#script)                         | 由 runner 执行的 Shell 脚本。 |
| [`secrets`](#secrets)                       | 作业所需的 CI/CD secret 信息。 |
| [`services`](#services)                     | 使用 Docker 服务镜像。 |
| [`stage`](#stage)                           | 定义作业阶段。 |
| [`tags`](#tags)                             | 用于选择 runner 的标签列表。 |
| [`timeout`](#timeout)                       | 定义优先于项目范围设置的自定义作业级别超时。 |
| [`trigger`](#trigger)                       | 定义下游流水线触发器。 |
| [`variables`](#variables)                   | 在作业级别定义作业变量。 |
| [`when`](#when)                             | 何时运行作业。 |

## 全局关键字

某些关键字未在作业中定义。这些关键字控制流水线行为或导入额外的流水线配置。

### `default`

您可以为某些关键字设置全局默认值。 未定义一个或多个所列关键字的作业使用在 `default:` 部分中定义的值。

**关键字类型**：全局关键字。

**可能的输入**：以下关键字可以具有自定义默认值：

- [`after_script`](#after_script)
- [`artifacts`](#artifacts)
- [`before_script`](#before_script)
- [`cache`](#cache)
- [`image`](#image)
- [`interruptible`](#interruptible)
- [`retry`](#retry)
- [`services`](#services)
- [`tags`](#tags)
- [`timeout`](#timeout)

```yaml
default:
  image: ruby:3.0

rspec:
  script: bundle exec rspec

rspec 2.7:
  image: ruby:2.7
  script: bundle exec rspec
```

示例将 `ruby:3.0` 镜像设置为流水线中所有作业的默认镜像。
`rspec 2.7` 作业不使用默认值，因为它使用特定于作业的 `image:` 部分覆盖了默认值：

**额外细节**：

- 创建流水线时，每个默认值都会复制到所有未定义该关键字的作业。
- 如果作业已经配置了其中一个关键字，则作业中的配置优先，不会被默认替换。
- 使用 [`inherit:default`](#inheritdefault) 控制作业中默认关键字的继承。

### `stages`

使用 `stages` 来定义包含作业组的阶段。`stages` 是为流水线全局定义的。在作业中使用 [`stage`](#stage) 来定义作业属于哪个阶段。

如果 `.gitlab-ci.yml` 文件中没有定义 `stages`，那么默认的流水线阶段是：

- [`.pre`](#stage-pre)
- `build`
- `test`
- `deploy`
- [`.post`](#stage-post)

`stages` 项的顺序定义了作业的执行顺序：

- 同一阶段的作业并行运行。
- 下一阶段的作业在上一阶段的作业成功完成后运行。

例如：

```yaml
stages:
  - build
  - test
  - deploy
```

1. `build` 中的所有作业并行执行。
1. 如果 `build` 中的所有作业都成功，`test` 作业将并行执行。
1. 如果 `test` 中的所有作业都成功，`deploy` 作业将并行执行。
1. 如果 `deploy` 中的所有作业都成功，则流水线被标记为 `passed`。

如果任何作业失败，流水线将被标记为 `failed` 并且后续阶段的作业不会启动。当前阶段的作业不会停止并继续运行。

如果作业未指定 [`stage`](#stage)，则作业被分配到 `test` 阶段。

如果定义了一个阶段，但没有作业使用它，则该阶段在流水线中不可见。 这对合规流水线配置<!--[合规流水线配置](../../user/project/settings/index.md#compliance-pipeline-configuration)-->很有用，因为：

- 阶段可以在合规性配置中定义，但如果不使用则保持隐藏。
- 当开发人员在作业定义中使用它们时，定义的阶段变得可见。

要使作业更早开始并忽略阶段顺序，请使用 [`needs`](#needs) 关键字。

### `workflow`

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/29654) in GitLab 12.5
-->

使用 `workflow:` 来确定是否创建流水线。
在顶层定义此关键字，使用单个 `rules:` 关键字，类似于在作业中定义的 [`rules:`](#rules)。

#### `workflow:rules`

您可以使用 [`workflow:rules` 模板](#workflowrules-模板) 导入预先配置的 `workflow:rules` 条目。

`workflow: rules` 接受这些关键字：

- [`if`](#rulesif)：检查此规则以确定何时运行流水线。
- [`when`](#when)：指定当 `if` 规则为 true 时要做什么。
   - 要运行流水线，请设置为 `always`。
   - 要阻止流水线运行，请设置为 `never`。
- [`variables`](#workflowrulesvariables)：如果未定义，则使用[在别处定义的变量](#variables)。

当没有规则为 true 时，流水线不会运行。

`workflow: rules` 的一些示例 `if` 子句如下：

| 示例规则                                        | 详细信息                                                  |
|------------------------------------------------------|-----------------------------------------------------------|
| `if: '$CI_PIPELINE_SOURCE == "merge_request_event"'` | 控制合并请求流水线何时运行。                 |
| `if: '$CI_PIPELINE_SOURCE == "push"'`                | 控制分支流水线和标签流水线何时运行。 |
| `if: $CI_COMMIT_TAG`                                 | 控制标签流水线何时运行。                          |
| `if: $CI_COMMIT_BRANCH`                              | 控制分支流水线何时运行。                       |

<!--
See the [common `if` clauses for `rules`](../jobs/job_control.md#common-if-clauses-for-rules) for more examples.
-->

在以下示例中，流水线针对所有 `push` 事件（对分支和新标签的更改）运行。提交消息中带有 `-draft` 的推送事件的流水线不会运行，因为它们被设置为 `when: never`。计划或合并请求的流水线也不会运行，因为没有规则对它们评估为 true：

```yaml
workflow:
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /-draft$/
      when: never
    - if: '$CI_PIPELINE_SOURCE == "push"'
```

这个例子有严格的规则，流水线在任何其他情况下都**不**运行。

或者，所有规则都可以是 `when: never`，最后是 `when:always` 规则。匹配 `when: never` 规则的流水线不会运行。
所有其他流水线类型运行：

```yaml
workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "push"'
      when: never
    - when: always
```

此示例阻止了计划或 `push`（分支和标签）的流水线。
最后的 `when: always` 规则运行所有其他流水线类型，**包括**合并请求流水线。

如果您的规则同时匹配分支流水线和合并请求流水线，则可能会出现重复流水线<!--[重复流水线](../jobs/job_control.md#avoid-duplicate-pipelines)-->。

#### `workflow:rules:variables`

> - 引入于 13.11 版本
> - 功能标志移除于 14.1 版本。

您可以在 `workflow:rules:` 中使用 [`variables`](#variables) 来定义特定流水线条件的变量。

当条件匹配时，将创建该变量并可供流水线中的所有作业使用。如果该变量已在全局级别定义，则 `workflow` 变量优先并覆盖全局变量。

**关键字类型**：全局关键字。

**可能的输入**：变量名和值对：

- 名称只能使用数字、字母和下划线 (`_`)。
- 值必须是字符串。

**`workflow:rules:variables` 示例：**

```yaml
variables:
  DEPLOY_VARIABLE: "default-deploy"

workflow:
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      variables:
        DEPLOY_VARIABLE: "deploy-production"  # Override globally-defined DEPLOY_VARIABLE
    - if: $CI_COMMIT_REF_NAME =~ /feature/
      variables:
        IS_A_FEATURE: "true"                  # Define a new variable.
    - when: always                            # Run the pipeline in other cases

job1:
  variables:
    DEPLOY_VARIABLE: "job1-default-deploy"
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      variables:                                   # Override DEPLOY_VARIABLE defined
        DEPLOY_VARIABLE: "job1-deploy-production"  # at the job level.
    - when: on_success                             # Run the job in other cases
  script:
    - echo "Run script with $DEPLOY_VARIABLE as an argument"
    - echo "Run another script if $IS_A_FEATURE exists"

job2:
  script:
    - echo "Run script with $DEPLOY_VARIABLE as an argument"
    - echo "Run another script if $IS_A_FEATURE exists"
```

当分支是默认分支时：

- job1 的 `DEPLOY_VARIABLE` 是 `job1-deploy-production`。
- job2 的 `DEPLOY_VARIABLE` 是 `deploy-production`。

当分支是 `feature` 时：

- job1 的 `DEPLOY_VARIABLE` 是 `job1-default-deploy`，而 `IS_A_FEATURE` 是`true`。
- job2 的 `DEPLOY_VARIABLE` 是 `default-deploy`，而 `IS_A_FEATURE` 是`true`。

当分支是别的东西时：

- job1 的 `DEPLOY_VARIABLE` 是 `job1-default-deploy`。
- job2 的 `DEPLOY_VARIABLE` 是 `default-deploy`。

### `include`

<!--
> [Moved](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/42861) to GitLab Free in 11.4.
-->

使用 `include` 在 CI/CD 配置中包含外部 YAML 文件。
您可以将一个长的 `.gitlab-ci.yml` 文件拆分为多个文件以提高可读性，或减少同一配置在多个位置的重复。

您还可以将模板文件存储在中央仓库中并将它们包含在项目中。

`include` 文件：

- 与 `.gitlab-ci.yml` 文件中的那些合并。
- 无论 `include` 关键字的位置如何，始终先求值，然后与 `.gitlab-ci.yml` 文件的内容合并。

您可以 [nest](includes.md#使用-nested-includes) 最多 100 个 include，但不能有重复的 include。
在 12.4 及更高版本中，解析所有文件的时间限制为 30 秒。

**关键字类型**：全局关键字。

**可能的输入**：`include` 子键：

- [`include:local`](#includelocal)
- [`include:file`](#includefile)
- [`include:remote`](#includeremote)
- [`include:template`](#includetemplate)

**额外细节**：

- 使用合并来自定义和覆盖本地包含的 CI/CD 配置。
- 您可以通过在 `.gitlab-ci.yml` 文件中使用相同的作业名称或全局关键字，来覆盖包含的配置。这两个配置合并在一起，`.gitlab-ci.yml` 文件中的配置优先于包含的配置。

<!--
**Related topics**:

- [Use variables with `include`](includes.md#use-variables-with-include).
- [Use `rules` with `include`](includes.md#use-rules-with-include).
-->

#### `include:local`

使用 `include:local` 包含与 `.gitlab-ci.yml` 文件位于同一仓库中的文件。
使用 `include:local` 代替符号链接。

**关键字类型**：全局关键字。

**可能的输入**：

- 相对于根目录 (`/`) 的完整路径。
- YAML 文件的扩展名必须是 `.yml` 或 `.yaml`。
- 您可以[在文件路径中使用 `*` 和 `**` 通配符](includes.md#将-includelocal-与通配符文件路径一起使用)。

**`include:local` 示例**：

```yaml
include:
  - local: '/templates/.gitlab-ci-template.yml'
```

您还可以使用较短的语法来定义路径：

```yaml
include: '.gitlab-ci-production.yml'
```

**额外细节**：

- `.gitlab-ci.yml` 文件和本地文件必须在同一个分支上。
- 您不能通过 Git 子模块路径包含本地文件。
- 所有的 [nested includes](includes.md#使用-nested-includes) 都在同一个项目范围内执行，所以您可以使用本地、项目、远端或模板 include。

#### `include:file`

> 包含来自同一项目的多个文件引入于 13.6 版本。功能标志移除于 13.8 版本。

要在同一个实例上包含来自另一个私有项目的文件，请使用 `include:file`。 您只能将 `include:file` 与 `include:project` 结合使用。

**关键字类型**：全局关键字。

**可能的输入**：相对于根目录 (`/`) 的完整路径。YAML 文件的扩展名必须是`.yml` 或`.yaml`。

**`include:file` 示例**：

```yaml
include:
  - project: 'my-group/my-project'
    file: '/templates/.gitlab-ci-template.yml'
```

您还可以指定一个 `ref`。 如果不指定值，则 ref 默认为项目的 `HEAD`：

```yaml
include:
  - project: 'my-group/my-project'
    ref: main
    file: '/templates/.gitlab-ci-template.yml'

  - project: 'my-group/my-project'
    ref: v1.0.0
    file: '/templates/.gitlab-ci-template.yml'

  - project: 'my-group/my-project'
    ref: 787123b47f14b552955ca2786bc9542ae66fee5b  # Git SHA
    file: '/templates/.gitlab-ci-template.yml'
```

您可以包含来自同一项目的多个文件：

```yaml
include:
  - project: 'my-group/my-project'
    ref: main
    file:
      - '/templates/.builds.yml'
      - '/templates/.tests.yml'
```

**额外细节**：

- 所有 [nested includes](includes.md#使用-nested-includes) 都在目标项目的范围内执行。您可以使用 `local`（相对于目标项目）、`project`、`remote` 或 `template` include。
- 当流水线启动时，评估所有方法 include 的 `.gitlab-ci.yml` 文件配置。配置是一个及时的快照并持久存在于数据库中。在下一个流水线启动之前，系统不会反映对引用的 `.gitlab-ci.yml` 文件配置的任何更改。
- 当您包含来自另一个私有项目的 YAML 文件时，运行流水线的用户必须是这两个项目的成员并且具有运行流水线的适当权限。如果用户无权访问任何包含的文件，则可能会显示 `not found or access denied` 错误。

#### `include:remote`

使用带有完整 URL 的 `include:remote` 来包含来自不同位置的文件。

**关键字类型**：全局关键字。

**可能的输入**：可通过 HTTP/HTTPS `GET` 请求访问的公共 URL。不支持使用远端 URL 进行身份验证。

YAML 文件的扩展名必须是 `.yml` 或 `.yaml`。

**`include:remote` 示例**：

```yaml
include:
  - remote: 'https://gitlab.com/example-project/-/raw/main/.gitlab-ci.yml'
```

**额外细节**：

- 所有 [nested includes](includes.md#使用-nested-includes) 都以公共用户身份在没有上下文的情况下执行，因此您只能包含公共项目或模板。
- 包含远端 CI/CD 配置文件时要小心。当外部 CI/CD 配置文件更改时，不会触发任何流水线或通知。从安全角度来看，这类似于拉取第三方依赖项。

#### `include:template`

使用 `include:template` 包含 [`.gitlab-ci.yml` 模板](https://gitlab.com/gitlab-jh/gitlab/-/tree/master/lib/gitlab/ci/templates)。

**关键字类型**：全局关键字。

**可能的输入**：[`.gitlab-ci.yml` 模板](https://gitlab.com/gitlab-jh/gitlab/-/tree/master/lib/gitlab/ci/templates).

**`include:template` 示例**：

```yaml
# File sourced from the GitLab template collection
include:
  - template: Auto-DevOps.gitlab-ci.yml
```

多个 `include:template` 文件：

```yaml
include:
  - template: Android-Fastlane.gitlab-ci.yml
  - template: Auto-DevOps.gitlab-ci.yml
```

**额外细节**：

- 所有 [nested includes](includes.md#使用-nested-includes) 仅在用户许可的情况下执行，因此可以使用 `project`、`remote` 或 `template` include。

## 作业关键字

以下主题说明如何使用关键字来配置 CI/CD 流水线。

### `image`

使用 `image` 指定运行作业的 Docker 镜像。

**关键字类型**：作业关键字。您只能将其用作作业的一部分或在 [`default:` 部分](#default) 中使用。

**可能的输入**：镜像的名称，包括镜像库路径（如果需要），采用以下格式之一：

- `<image-name>`（与使用带有 `latest` 标签的 `<image-name>` 相同）
- `<image-name>:<tag>`
- `<image-name>@<digest>`

**`image` 示例**：

```yaml
default:
  image: ruby:3.0

rspec:
  script: bundle exec rspec

rspec 2.7:
  image: registry.example.com/my-group/my-project/ruby:2.7
  script: bundle exec rspec
```

在这个例子中，`ruby:3.0` 镜像是流水线中所有作业的默认镜像。
`rspec 2.7` 作业不使用默认值，因为它使用特定于作业的 `image:` 部分覆盖了默认值。

<!--
**Related topics**:

- [Run your CI/CD jobs in Docker containers](../docker/using_docker_images.md).
-->

#### `image:name`

作业运行所在的 Docker 镜像的名称。与自身使用的 [`image:`](#image) 类似。

**关键字类型**：作业关键字。您只能将其用作作业的一部分或在 [`default:` 部分](#default) 中使用。

**可能的输入**：镜像的名称，包括镜像库路径（如果需要），采用以下格式之一：

- `<image-name>`（与使用带有 `latest` 标签的 `<image-name>` 相同）
- `<image-name>:<tag>`
- `<image-name>@<digest>`

**`image:name` 示例**：

```yaml
image:
  name: "registry.example.com/my/image:latest"
```

<!--
**Related topics**:

- [Run your CI/CD jobs in Docker containers](../docker/using_docker_images.md).
-->

#### `image:entrypoint`

作为容器入口点执行的命令或脚本。

创建 Docker 容器时，`entrypoint` 被转换为 Docker 的 `--entrypoint` 选项。
语法类似于 [Dockerfile `ENTRYPOINT` 指令](https://docs.docker.com/engine/reference/builder/#entrypoint)，其中每个 shell 令牌都是数组中的一个单独字符串。

**关键字类型**：作业关键字。您只能将其用作作业的一部分或在 [`default:` 部分](#default) 中使用。

**可能的输入**：一个字符串。

**`image:entrypoint` 示例**：

```yaml
image:
  name: super/sql:experimental
  entrypoint: [""]
```
<!--
**Related topics**:

- [Override the entrypoint of an image](../docker/using_docker_images.md#override-the-entrypoint-of-an-image).
-->

#### `services`

使用 `services` 指定一个额外的 Docker 镜像来运行脚本。<!--[`services` 镜像](../services/index.md)-->`services` 镜像链接到 [`image`](#image) 关键字中指定的镜像。

**关键字类型**：作业关键字。您只能将其用作作业的一部分或在 [`default:` 部分](#default) 中使用。

**可能的输入**：服务镜像的名称，包括镜像库路径（如果需要），采用以下格式之一：

- `<image-name>`（与使用带有 `latest` 标签的 `<image-name>` 相同）
- `<image-name>:<tag>`
- `<image-name>@<digest>`

**`services` 示例**：

```yaml
default:
  image:
    name: ruby:2.6
    entrypoint: ["/bin/bash"]

  services:
    - name: my-postgres:11.7
      alias: db-postgres
      entrypoint: ["/usr/local/bin/db-postgres"]
      command: ["start"]

  before_script:
    - bundle install

test:
  script:
    - bundle exec rake spec
```

在此示例中，作业启动一个 Ruby 容器。然后，该作业从该容器启动另一个运行 PostgreSQL 的容器。然后该作业在该容器中运行脚本。

<!--
**Related topics**:

- [Available settings for `services`](../services/index.md#available-settings-for-services).
- [Define `services` in the `.gitlab-ci.yml` file](../services/index.md#define-services-in-the-gitlab-ciyml-file).
- [Run your CI/CD jobs in Docker containers](../docker/using_docker_images.md).
- [Use Docker to build Docker images](../docker/using_docker_build.md).
-->

### `script`

使用 `script` 指定 runner 要执行的命令。

除了 [trigger jobs](#trigger) 之外的所有作业都需要一个 `script` 关键字。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：一个数组，包括：

- 单行命令。
- 长命令[拆分多行](script.md#拆分长命令)。
- [YAML 锚点](yaml_optimization.md#脚本的-yaml-锚点)。

**`script` 示例：**

```yaml
job1:
  script: "bundle exec rspec"

job2:
  script:
    - uname -a
    - bundle exec rspec
```

**额外细节**：

- 当您使用 [`script` 中的这些特殊字符](script.md#在-script-中使用特殊字符)时，必须使用单引号 (`'`) 或双引号 (`"`)。

<!--
**Related topics**:

- You can [ignore non-zero exit codes](script.md#ignore-non-zero-exit-codes).
- [Use color codes with `script`](script.md#add-color-codes-to-script-output)
  to make job logs easier to review.
- [Create custom collapsible sections](../jobs/index.md#custom-collapsible-sections)
  to simplify job log output.
-->

#### `before_script`

使用 `before_script` 来定义一系列命令，这些命令应该在每个作业的 `script` 命令之前运行，但在 [artifacts](#artifacts) 恢复之后。

**关键字类型**：作业关键字。您只能将其用作作业的一部分或在 [`default:` 部分](#default) 中使用。

**可能的输入**：一个数组，包括：

- 单行命令。
- 长命令[拆分多行](script.md#拆分长命令)。
- [YAML 锚点](yaml_optimization.md#脚本的-yaml-锚点)。

**`before_script` 示例：**

```yaml
job:
  before_script:
    - echo "Execute this command before any 'script:' commands."
  script:
    - echo "This command executes after the job's 'before_script' commands."
```

**额外细节**：

- 您在 `before_script` 中指定的脚本与您在主 [`script`](#script) 中指定的任何脚本连接在一起。组合脚本在单个 shell 中一起执行。

<!--
**Related topics**:

- [Use `before_script` with `default`](script.md#set-a-default-before_script-or-after_script-for-all-jobs)
  to define a default array of commands that should run before the `script` commands in all jobs.
- You can [ignore non-zero exit codes](script.md#ignore-non-zero-exit-codes).
- [Use color codes with `before_script`](script.md#add-color-codes-to-script-output)
  to make job logs easier to review.
- [Create custom collapsible sections](../jobs/index.md#custom-collapsible-sections)
  to simplify job log output.
-->

#### `after_script`

使用 `after_script` 定义在每个作业之后运行的命令数组，包括失败的作业。

**关键字类型**：作业关键字。您只能将其用作作业的一部分或在 [`default:` 部分](#default) 中使用。

**可能的输入**：一个数组，包括：

- 单行命令。
- 长命令[拆分多行](script.md#拆分长命令)。
- [YAML 锚点](yaml_optimization.md#脚本的-yaml-锚点)。

**Example of `after_script`:**

```yaml
job:
  script:
    - echo "An example script section."
  after_script:
    - echo "Execute this command after the `script` section completes."
```

**额外细节**：

您在 `after_script` 中指定的脚本在一个新的 shell 中执行，与任何 `before_script` 或 `script` 命令分开。结果：

- 将当前工作目录设置回默认值（根据定义运行程序如何处理 Git 请求的变量<!--[定义运行程序如何处理 Git 请求的变量](../runners/configure_runners.md#configure-runner-behavior-with-variables)-->）。
- 无权访问由 `before_script` 或 `script` 中定义的命令完成的更改，包括：
  - 在`script` 脚本中导出的命令别名和变量。
  - 作业树之外的更改（取决于 runner），例如由 `before_script` 或 `script` 脚本安装的软件。
- 有一个单独的超时，它被硬编码为 5 分钟。
- 不要影响作业的退出代码。如果 `script` 部分成功并且 `after_script` 超时或失败，作业将退出，代码为 `0`（`Job Succeeded`）。

如果作业超时或被取消，则不会执行 `after_script` 命令。
存在一个问题，即为超时或取消的作业添加对执行 `after_script` 命令的支持。

<!--
**Related topics**:

- [Use `after_script` with `default`](script.md#set-a-default-before_script-or-after_script-for-all-jobs)
  to define a default array of commands that should run after all jobs.
- You can [ignore non-zero exit codes](script.md#ignore-non-zero-exit-codes).
- [Use color codes with `after_script`](script.md#add-color-codes-to-script-output)
  to make job logs easier to review.
- [Create custom collapsible sections](../jobs/index.md#custom-collapsible-sections)
  to simplify job log output.
-->

### `stage`

使用 `stage` 定义作业在哪个 [stage](#stages) 中运行。同一个 `stage` 中的作业可以并行执行（参见 **额外细节**）。

如果没有定义 `stage`，则作业默认使用 `test` 阶段。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：包含任意数量阶段名称的数组。 阶段名称可以是：

- [默认阶段](#stages)。
- 用户定义的阶段。

**`stage` 示例**：

```yaml
stages:
  - build
  - test
  - deploy

job1:
  stage: build
  script:
    - echo "This job compiles code."

job2:
  stage: test
  script:
    - echo "This job tests the compiled code. It runs when the build stage completes."

job3:
  script:
    - echo "This job also runs in the test stage".

job4:
  stage: deploy
  script:
    - echo "This job deploys the code. It runs when the test stage completes."
```

**额外细节**：

- 如果作业在不同的 runner 上运行，则它们可以并行运行。
- 如果您只有一个 runner，如果 runner 的 `concurrent` 设置大于 `1`，作业可以并行运行。

#### `stage: .pre`

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/31441) in GitLab 12.4.
-->

使用 `.pre` 阶段在流水线开始时运行作业。`.pre` 始终是流水线的第一阶段。用户定义的阶段在 `.pre` 之后执行。
您不必在 [`stages`](#stages) 中定义 `.pre`。

除了 `.pre` 或 `.post`，你必须至少有一个阶段的作业。

**关键字类型**：您只能将它与作业的 `stage` 关键字一起使用。

**`stage: .pre` 示例**：

```yaml
stages:
  - build
  - test

job1:
  stage: build
  script:
    - echo "This job runs in the build stage."

first-job:
  stage: .pre
  script:
    - echo "This job runs in the .pre stage, before all other stages."

job2:
  stage: test
  script:
    - echo "This job runs in the test stage."
```

#### `stage: .post`

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/31441) in GitLab 12.4.
-->

使用 `.post` 阶段使作业在流水线的末尾运行。`.post` 始终是流水线的最后阶段。用户定义的阶段在 `.post` 之前执行。
你不必在 [`stages`](#stages) 中定义 `.post`。

除了 `.pre` 或 `.post`，你必须至少有一个阶段的作业。

**关键字类型**：您只能将它与作业的 `stage` 关键字一起使用。

**`stage: .post` 示例**：

```yaml
stages:
  - build
  - test

job1:
  stage: build
  script:
    - echo "This job runs in the build stage."

last-job:
  stage: .post
  script:
    - echo "This job runs in the .post stage, after all other stages."

job2:
  stage: test
  script:
    - echo "This job runs in the test stage."
```

### `extends`

使用 `extends` 来重用配置 section。它是 [YAML 锚点](yaml_optimization.md#锚点) 的替代方案，并且更加灵活和可读。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：

- 流水线中另一个作业的名称。
- 流水线中其他作业的名称列表（数组）。

**`extends` 示例：**

```yaml
.tests:
  script: rake test
  stage: test
  only:
    refs:
      - branches

rspec:
  extends: .tests
  script: rake rspec
  only:
    variables:
      - $RSPEC
```

在此示例中，`rspec` 作业使用来自`.tests` 模板作业的配置。
创建流水线时：

- 根据键执行反向深度合并。
- 将 `.tests` 内容与 `rspec` 作业合并。
- 不合并键的值。

结果是这个 `rspec` 作业：

```yaml
rspec:
  script: rake rspec
  stage: test
  only:
    refs:
      - branches
    variables:
      - $RSPEC
```

**额外示例：**

- 在 12.0 及更高版本中，您可以为 `extends` 使用多个父项。
- `extends` 关键字最多支持 11 级继承，但应避免使用超过 3 级。
- 在上面的例子中，`.tests` 是一个[隐藏作业](../jobs/index.md#隐藏作业)，但也可以从常规作业扩展配置。

<!--
**Related topics:**

- [Reuse configuration sections by using `extends`](yaml_optimization.md#use-extends-to-reuse-configuration-sections).
- Use `extends` to reuse configuration from [included configuration files](yaml_optimization.md#use-extends-and-include-together).
-->

### `rules`

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/27863) in GitLab 12.3.
-->

使用 `rules` 来包含或排除流水线中的作业。

创建流水线时会评估规则，并*按顺序*评估，直到第一次匹配。找到匹配项后，该作业将包含在流水线中或从流水线中排除，具体取决于配置。

您不能在规则中使用作业脚本中创建的 dotenv 变量，因为在任何作业运行之前都会评估规则。

`rules` 替换了 [`only/except`](#only--except)，并且它们不能在同一个作业中一起使用。如果您将一个作业配置为使用两个关键字，则系统会返回一个 `key may not used with rules` 错误。

`rules` 接受以下规则：

- `if`
- `changes`
- `exists`
- `allow_failure`
- `variables`
- `when`

您可以为复杂规则<!--[复杂规则](../jobs/job_control.md#complex-rules)-->，将多个关键字组合在一起。

作业被添加到流水线中：

- 如果 `if`、`changes` 或 `exists` 规则匹配并且还具有 `when: on_success`（默认）、`when: delay` 或 `when: always`。
- 如果达到的规则只有 `when: on_success`、`when: delay` 或 `when: always`。

作业未添加到流水线中：

- 如果没有规则匹配。
- 如果规则匹配并且有 `when: never`。

您可以在不同的工作中使用 [`!reference` 标签](yaml_optimization.md#reference-标签) 来重用 `rules` 配置<!--[重用 `rules` 配置](../jobs/job_control.md#reuse-rules-in-different-jobs)-->。

#### `rules:if`

使用 `rules:if` 子句指定何时向流水线添加作业：

- 如果 `if` 语句为 true，则将作业添加到管流水线中。
- 如果 `if` 语句为 true，但它与 `when: never` 结合使用，则不要将作业添加到流水线中。
- 如果没有 `if` 语句为 true，则不要将作业添加到流水线中。

`if:` 子句根据预定义 CI/CD 变量<!--[预定义 CI/CD 变量](../variables/predefined_variables.md)-->或自定义 CI/CD 变量<!--[自定义 CI/CD 变量](../variables/index.md#custom-cicd-variables)-->。

**关键字类型**：特定于作业和特定于流水线。您可以将其用作作业的一部分来配置作业行为，或者使用 [`workflow`](#workflow) 来配置流水线行为。

**可能的输入**：CI/CD 变量表达式<!--[CI/CD 变量表达式](../jobs/job_control.md#cicd-variable-expressions)-->。

**`rules:if` 示例**：

```yaml
job:
  script: echo "Hello, Rules!"
  rules:
    - if: '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^feature/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME != $CI_DEFAULT_BRANCH'
      when: never
    - if: '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^feature/'
      when: manual
      allow_failure: true
    - if: '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME'
```

**额外细节**：

- 如果规则匹配并且没有定义 `when`，则规则使用为作业定义的 `when`，如果未定义，则默认为 `on_success`。
- 您可以为每个规则定义一次 `when`，或在适用于所有规则的作业级别定义一次。 您不能将作业级别的 `when` 与规则中的 `when` 混用。
- 与 `script`<!--[`script`](../variables/index.md#use-cicd-variables-in-job-scripts)--> 部分中的变量不同，规则表达式中的变量始终格式为 `$VARIABLE`。
   - 您可以使用 `rules:if` 和 `include` 来有条件地包含其他配置文件<!--[有条件地包含其他配置文件](includes.md#use-rules-with-include)-->。

<!--
**Related topics**:

- [Common `if` expressions for `rules`](../jobs/job_control.md#common-if-clauses-for-rules).
- [Avoid duplicate pipelines](../jobs/job_control.md#avoid-duplicate-pipelines).
-->

#### `rules:changes`

使用 `rules:changes` 通过检查对特定文件的更改来指定何时将作业添加到流水线。

WARNING:
您应该仅将 `rules: changes` 用于 **分支流水线** 或 **合并请求流水线**。
您可以将 `rules:changes` 与其他管道类型一起使用，但是当没有 Git `push` 事件时，`rules:changes` 总是评估为 true。 标记管道、计划管道等**没有**与它们关联的 Git `push` 事件。 如果没有将作业限制为分支或合并请求管道的 `if:`，则 `rules: changes` 作业**总是** 添加到这些管道中。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**： 文件路径数组。在 13.6 及更高版本中，<!--[文件路径可以包含变量](../jobs/job_control.md#variables-in-ruleschanges)-->。

**`rules:changes` 示例**：

```yaml
docker build:
  script: docker build -t my-image:$CI_COMMIT_REF_SLUG .
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - Dockerfile
      when: manual
      allow_failure: true
```

- 如果流水线是合并请求流水线，请检查 `Dockerfile` 是否有更改。
- 如果 `Dockerfile` 已更改，则将作业作为手动作业添加到流水线中，即使作业未触发，流水线也会继续运行（`allow_failure: true`）。
- 如果 `Dockerfile` 没有改变，不要将作业添加到任何流水线（与 `when: never` 相同）。

**额外细节**：

- `rules: changes` 的工作方式与 [`only: changes` 和 `except: changes`](#onlychanges--exceptchanges) 相同。
- 您可以使用 `when: never` 来实现类似于 [`except:changes`](#onlychanges--exceptchanges) 的规则。
- 如果任何匹配的文件发生更改（`OR` 操作），`changes` 将解析为 `true`。

#### `rules:exists`

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/24021) in GitLab 12.4.
-->

当仓库中存在某些文件时，使用 `exists` 来运行作业。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：文件路径数组。路径相对于项目目录 (`$CI_PROJECT_DIR`)，不能直接链接到项目目录之外。文件路径可以使用 glob 模式。

**`rules:exists` 示例**：

```yaml
job:
  script: docker build -t my-image:$CI_COMMIT_REF_SLUG .
  rules:
    - exists:
        - Dockerfile
```

`job` runs if a `Dockerfile` exists anywhere in the repository.

**额外细节**：

- Glob 模式使用 Ruby [`File.fnmatch`](https://docs.ruby-lang.org/en/2.7.0/File.html#method-c-fnmatch) 和标志 `File::FNM_PATHNAME | File::FNM_DOTMATCH | File::FNM_EXTGLOB`。
- 出于性能原因，系统最多匹配 10,000 个 `exists` pattern 或文件路径。在第 10,000 次检查之后，有 patterned globs的规则始终匹配。也就是说，`exists` 规则总是假设在超过 10,000 个文件的项目中匹配。
- 如果找到任何列出的文件，`exists` 解析为 `true`（`OR` 操作）。

#### `rules:allow_failure`

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/30235) in GitLab 12.8.
-->

在 `rules:` 中使用 [`allow_failure: true`](#allow_failure) 允许作业在不停止流水线的情况下失败。

您还可以在手动作业中使用 `allow_failure: true`。流水线继续运行，无需等待手动作业的结果。`allow_failure: false` 与规则中的 `when: manual` 结合导致流水线在继续之前等待手动作业运行。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：`true` 或 `false`。如果未定义，则默认为 `false`。

**`rules:allow_failure` 示例**：

```yaml
job:
  script: echo "Hello, Rules!"
  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH'
      when: manual
      allow_failure: true
```

如果规则匹配，则该作业是带有 `allow_failure: true` 的手动作业。

**额外细节**：

- 规则级别 `rules:allow_failure` 覆盖作业级别 [`allow_failure`](#allow_failure)，并且仅在特定规则触发作业时适用。

#### `rules:variables`

> - 引入于 13.7 版本。
> - 功能标志移除于 13.10 版本。

在 `rules:` 中使用 [`variables`](#variables) 来定义特定条件的变量。

**关键字类型**：特定于作业。您只能将其用作作业的一部分。

**可能的输入**：格式为 `VARIABLE-NAME: value` 的变量哈希。

**`rules:variables` 示例**：

```yaml
job:
  variables:
    DEPLOY_VARIABLE: "default-deploy"
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      variables:                              # Override DEPLOY_VARIABLE defined
        DEPLOY_VARIABLE: "deploy-production"  # at the job level.
    - if: $CI_COMMIT_REF_NAME =~ /feature/
      variables:
        IS_A_FEATURE: "true"                  # Define a new variable.
  script:
    - echo "Run script with $DEPLOY_VARIABLE as an argument"
    - echo "Run another script if $IS_A_FEATURE exists"
```

### `only` / `except`

NOTE:
`only` 和 `except` 没有被积极开发。[`rules`](#rules) 是控制何时向流水线添加作业的首选关键字。

您可以使用 `only` 和 `except` 来控制何时向流水线添加作业。

- 使用 `only` 来定义作业何时运行。
- 使用 `except` 定义作业 ** 不** 运行的时间。

四个关键字可以与 `only` 和 `except` 一起使用：

- [`refs`](#onlyrefs--exceptrefs)
- [`variables`](#onlyvariables--exceptvariables)
- [`changes`](#onlychanges--exceptchanges)
- [`kubernetes`](#onlykubernetes--exceptkubernetes)

<!--
See [specify when jobs run with `only` and `except`](../jobs/job_control.md#specify-when-jobs-run-with-only-and-except)
for more details and examples.
-->

#### `only:refs` / `except:refs`

使用 `only:refs` 和 `except:refs` 关键字来控制何时根据分支名称或流水线类型将作业添加到流水线中。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：一个包含任意数量的数组：

- 分支名称，例如 `main` 或 `my-feature-branch`。
- <!--[正则表达式](../jobs/job_control.md#only--except-regex-syntax)-->正则表达式匹配分支名称，例如 `/^feature-.*/`。
- 以下关键词：

  | **值**                | **描述** |
  | -------------------------|-----------------|
  | `api`                    | 对于由 pipelines API<!--[pipelines API](../../api/pipelines.md#create-a-new-pipeline)--> 触发的流水线。 |
  | `branches`               | 当流水线的 Git 引用是一个分支时。 |
  | `chat`                   | 对于使用 GitLab ChatOps<!--[GitLab ChatOps](../chatops/index.md)--> 命令创建的流水线。 |
  | `external`               | 当您使用极狐GitLab 以外的 CI 服务时。 |
  | `external_pull_requests` | 在 GitHub 上创建或更新外部拉取请求时<!--(See [Pipelines for external pull requests](../ci_cd_for_external_repos/index.md#pipelines-for-external-pull-requests))-->。 |
  | `merge_requests`         | 对于在创建或更新合并请求时创建的流水线。启用[合并请求流水线](../pipelines/merge_request_pipelines.md)、[合并结果流水线](../pipelines/pipelines_for_merged_results.md)和[合并队列](../pipelines/merge_trains.md)。 |
  | `pipelines`              | 对于通过使用带有 `CI_JOB_TOKEN` 的 API<!--[使用带有 `CI_JOB_TOKEN` 的 API](../pipelines/multi_project_pipelines.md#create-multi-project-pipelines-by-using-the-api)--> 创建的多项目流水线<!--[多项目流水线](../pipelines/multi_project_pipelines.md)-->或 [`trigger`](#trigger) 关键字。 |
  | `pushes`                 | 对于由 `git push` 事件触发的流水线，包括分支和标签。 |
  | `schedules`              | 对于计划流水线<!--[计划流水线](../pipelines/schedules.md)-->。 |
  | `tags`                   | 当流水线的 Git 引用是标签时。 |
  | `triggers`               | 对于使用 trigger token<!--[trigger token](../triggers/index.md#authentication-tokens)--> 创建的流水线。 |  
  | `web`                    | 对于通过在 GitLab UI 中，从项目的 **CI/CD > 流水线** 部分选择 **运行流水线** 创建的流水线。 |

**`only:refs` 和 `except:refs` 示例**：

```yaml
job1:
  script: echo
  only:
    - main
    - /^issue-.*$/
    - merge_requests

job2:
  script: echo
  except:
    - main
    - /^stable-branch.*$/
    - schedules
```

**额外细节：**

- 预定流水线在特定分支上运行，因此配置了 `only: branch` 的作业也可以在预定流水线上运行。添加 `except: schedules` 以防止带有 `only: branch` 的作业在预定流水线上运行。
- `only` 或 `except` 不使用任何其他关键字等价于 `only: refs` 或 `except: refs`。例如，以下两个作业配置具有相同的行为：

  ```yaml
  job1:
    script: echo
    only:
      - branches

  job2:
    script: echo
    only:
      refs:
        - branches
  ```

- 如果作业不使用 `only`、`except` 或 [`rules`](#rules)，则 `only` 默认设置为 `branches` 和 `tags`。

   例如，`job1` 和 `job2` 是等价的：

  ```yaml
  job1:
    script: echo 'test'

  job2:
    script: echo 'test'
    only:
    - branches
    - tags
  ```

#### `only:variables` / `except:variables`

根据 CI/CD 变量<!--[CI/CD 变量](../variables/index.md)--> 的状态，使用 `only:variables` 或 `except:variables` 关键字来控制何时将作业添加到流水线中。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：<!--[CI/CD 变量表达式](../jobs/job_control.md#cicd-variable-expressions)-->CI/CD 变量表达式的数组。

**`only:variables` 示例**：

```yaml
deploy:
  script: cap staging deploy
  only:
    variables:
      - $RELEASE == "staging"
      - $STAGING
```

<!--
**Related topics**:

- [`only:variables` and `except:variables` examples](../jobs/job_control.md#only-variables--except-variables-examples).
-->

#### `only:changes` / `except:changes`

当 Git 推送事件修改文件时，使用 `changes` 关键字和 `only` 来运行作业，或使用 `except` 来跳过作业。

在具有以下引用的流水线中使用 `changes`：

- `branches`
- `external_pull_requests`
- `merge_requests`<!--（请参阅有关[使用`only:changes` 与管道合并请求](../jobs/job_control.md#use-onlychanges-with-pipelines-for-merge-requests)的其他详细信息）-->

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：一个包含任意数量的数组：

- 文件路径。
- 单个目录的通配符路径，例如 `path/to/directory/*`，或一个目录及其所有子目录，例如 `path/to/directory/**/*`。
- 具有相同扩展名或多个扩展名的所有文件的通配符 (glob) 路径，例如 `*.md` 或 `path/to/directory/*.{rb,py,sh}`。
- 根目录或所有目录中文件的通配符路径，用双引号括起来。例如`"*.json"` 或`"**/*.json"`。

**`only:changes` 示例**：

```yaml
docker build:
  script: docker build -t my-image:$CI_COMMIT_REF_SLUG .
  only:
    refs:
      - branches
    changes:
      - Dockerfile
      - docker/scripts/*
      - dockerfiles/**/*
      - more_scripts/*.{rb,py,sh}
      - "**/*.json"
```

**额外细节**：

- 如果任何匹配的文件发生更改（`OR` 操作），`changes` 将解析为 `true`。
- 如果使用除 `branches`、`external_pull_requests` 或 `merge_requests` 以外的引用，`changes` 无法确定给定文件是新文件还是旧文件，并且总是返回 `true`。
- 如果您将 `only: changes` 与其他引用一起使用，作业将忽略更改并始终运行。
- 如果您将 `except: changes` 与其他引用一起使用，作业将忽略更改并且永远不会运行。

<!--
**Related topics**:

- [`only: changes` and `except: changes` examples](../jobs/job_control.md#onlychanges--exceptchanges-examples).
- If you use `changes` with [only allow merge requests to be merged if the pipeline succeeds](../../user/project/merge_requests/merge_when_pipeline_succeeds.md#only-allow-merge-requests-to-be-merged-if-the-pipeline-succeeds),
  you should [also use `only:merge_requests`](../jobs/job_control.md#use-onlychanges-with-pipelines-for-merge-requests).
- Use `changes` with [new branches or tags *without* pipelines for merge requests](../jobs/job_control.md#use-onlychanges-without-pipelines-for-merge-requests).
- Use `changes` with [scheduled pipelines](../jobs/job_control.md#use-onlychanges-with-scheduled-pipelines).
-->

#### `only:kubernetes` / `except:kubernetes`

使用 `only:kubernetes` 或 `except:kubernetes` 来控制当 Kubernetes 服务在项目中处于活动状态时是否将作业添加到流水线中。

**关键字类型**：特定于作业。您只能将其用作作业的一部分。

**可能的输入**：`kubernetes` 策略只接受 `active` 关键字。

**`only:kubernetes` 示例**：

```yaml
deploy:
  only:
    kubernetes: active
```

在此示例中，`deploy` 作业仅在项目中的 Kubernetes 服务处于活动状态时运行。

### `needs`

> - 引入于 12.2 版本。
> - 于 12.3 版本，`needs` 数组中的最大作业数从 5 增加到 50。
> - 于 12.8 版本，`needs: []` 让作业立即开始。
> - 于 14.2 版本，您可以参考与您正在配置的作业处于同一阶段的作业。

使用 `needs:` 来不按顺序执行作业。使用 `needs` 的作业之间的关系可以可视化为有向无环图<!--[有向无环图](../directed_acyclic_graph/index.md)-->。

您可以忽略阶段排序并运行一些作业，而无需等待其他作业完成。
多个阶段的作业可以同时运行。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：

- 一个数组的作业。
- 一个空数组 (`[]`)，用于将作业设置为在创建流水线后立即启动。

**`needs` 示例**：

```yaml
linux:build:
  stage: build
  script: echo "Building linux..."

mac:build:
  stage: build
  script: echo "Building mac..."

lint:
  stage: test
  needs: []
  script: echo "Linting..."

linux:rspec:
  stage: test
  needs: ["linux:build"]
  script: echo "Running rspec on linux..."

mac:rspec:
  stage: test
  needs: ["mac:build"]
  script: echo "Running rspec on mac..."

production:
  stage: deploy
  script: echo "Running production..."
```

示例创建四个执行路径：

- Linter：`lint` 作业立即运行，无需等待 `build` 阶段完成，因为它没有需求（`needs: []`）。
- Linux 路径：`linux:rspec` 和 `linux:rubocop` 作业在 `linux:build` 作业完成后立即运行，无需等待 `mac:build` 完成。
- macOS 路径：`mac:rspec` 和 `mac:rubocop` 作业在 `mac:build` 作业完成后立即运行，无需等待 `linux:build` 完成。
- `production` 作业在所有先前的作业完成后立即运行；在这种情况下：`linux:build`、`linux:rspec`、`linux:rubocop`、`mac:build`、`mac:rspec`、`mac:rubocop`。

**额外细节**：

- `needs:` 数组中单个作业可以需要的最大作业数是有限的：
  <!--- For GitLab.com, the limit is 50. For more information, see our
    [infrastructure issue](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/7541).-->
  - 对于自助管理实例，默认限制为 50。此限制可以更改<!--[可以更改](#更改-needs-作业限制)-->。
- 如果 `needs:` 指的是使用 [`parallel`](#parallel) 关键字的作业，它取决于并行创建的所有作业，而不仅仅是一个作业。 默认情况下，它还从所有并行作业下载产物。如果产物具有相同的名称，它们会相互覆盖，并且只保存最后下载的产物。
- 在 14.1 及更高版本中，您可以引用与您正在配置的作业处于同一阶段的作业。<!--此功能已在 GitLab.com 上启用并可供生产使用。-->在自助管理的  14.2 及更高版本上，此功能默认可用。
- 在 14.0 及更早版本中，您只能引用早期阶段的作业。必须为所有使用 `needs:` 关键字或在作业的 `needs:` 部分中引用的作业明确定义阶段。
- 在 13.9 及更早版本中，如果 `needs:` 指的是由于 `only`、`except` 或 `rules` 可能无法添加到流水线中的作业，则流水线可能无法创建。

#### `needs:artifacts`

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/14311) in GitLab 12.6.
-->

当作业使用 `needs` 时，默认情况下它不再下载前一阶段的所有产物，因为带有 `needs` 的作业可以在早期阶段完成之前开始。使用 `needs`，您只能从 `needs:` 配置中列出的作业中下载产物。

使用 `artifacts: true`（默认）或 `artifacts: false` 来控制何时在使用 `needs` 的作业中下载产物。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。 Must be used with `needs:job`.

**可能的输入**：

- `true`（默认）或 `false`.

**`needs:artifacts` 示例**：

```yaml
test-job1:
  stage: test
  needs:
    - job: build_job1
      artifacts: true

test-job2:
  stage: test
  needs:
    - job: build_job2
      artifacts: false

test-job3:
  needs:
    - job: build_job1
      artifacts: true
    - job: build_job2
    - build_job3
```

在这个例子中：

- `test-job1` 作业下载 `build_job1` 产物
- `test-job2` 作业不会下载 `build_job2` 产物。
- `test-job3` 作业从所有三个 `build_jobs` 下载产物，因为对于所有三个需要的作业，`artifacts:` 是 `true`，或默认为 `true`。

**额外细节**：

- 在 12.6 及更高版本中，您不能将 [`dependencies`](#dependencies) 关键字与 `needs` 结合使用。

#### `needs:project` **(PREMIUM)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/14311) in GitLab 12.7.
-->

使用 `needs:project` 从其他流水线中最多五个作业下载产物。
从指定引用的最新成功流水线下载产物。

如果为指定的 ref 运行流水线，则带有 `needs:project` 的作业不会等待流水线完成。相反，该作业从成功完成的最新流水线下载产物。

`needs:project` 必须与 `job:`、`ref:` 和 `artifacts:` 一起使用。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：

- `needs:project`：完整的项目路径，包括命名空间和群组。
- `job`：从中下载产物的作业。
- `ref`：从中下载产物的引用。
- `artifacts`：必须为 `true` 才能下载产物。

**`needs:project` 示例**：

```yaml
build_job:
  stage: build
  script:
    - ls -lhR
  needs:
    - project: namespace/group/project-name
      job: build-1
      ref: main
      artifacts: true
```

在此示例中，`build_job` 从 `group/project-name` 项目的 `main` 分支上最新成功的 `build-1` 作业下载产物。

在 13.3 及更高版本中，您可以在 `needs:project` 中使用 CI/CD 变量<!--[CI/CD variables](../variables/index.md)-->，例如：

```yaml
build_job:
  stage: build
  script:
    - ls -lhR
  needs:
    - project: $CI_PROJECT_PATH
      job: $DEPENDENCY_JOB_NAME
      ref: $ARTIFACTS_DOWNLOAD_REF
      artifacts: true
```

**额外细节**：

- 要从当前项目中的不同流水线下载产物，请将 `project:` 设置为与当前项目相同，但使用与当前流水线不同的引用。在同一个 ref 上运行的并发流水线可能会覆盖产物。
- 运行流水线的用户必须至少具有组或项目的报告者角色，或者群组/项目必须具有公开可见性。
- 你不能在与 [`trigger`](#trigger) 相同的作业中使用 `needs:project`。
- 当使用 `needs:project` 从另一个流水线下载产物时，作业不会等待所需的作业完成。[有向无环图](../directed_acyclic_graph/index.md)仅限于同一流水线中的作业。确保其他流水线中所需的作业在需要它的作业尝试下载产物之前完成。
- 您无法从在 [`parallel:`](#parallel) 中运行的作业下载产物。
- 13.3 中引入了对 `project`、`job` 和 `ref` 中的 <!--[CI/CD 变量](../variables/index.md)-->CI/CD 变量的支持。 13.4 中删除了功能标志。

<!--
**Related topics**:

- To download artifacts between [parent-child pipelines](../pipelines/parent_child_pipelines.md),
  use [`needs:pipeline:job`](#needspipelinejob).
-->

#### `needs:pipeline:job`

> 引入于 13.7 版本。

[子流水线](../pipelines/parent_child_pipelines.md) 可以从其父流水线或同一父子流水线层次结构中的另一个子流水线中的作业下载产物。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：

- `needs:pipeline`：流水线 ID。必须是存在于同一父子流水线层次结构中的流水线。
- `job:`：从中下载产物的作业。

**`needs:pipeline:job` 示例**：

- 父流水线 (`.gitlab-ci.yml`)：

  ```yaml
  create-artifact:
    stage: build
    script: echo 'sample artifact' > artifact.txt
    artifacts:
      paths: [artifact.txt]

  child-pipeline:
    stage: test
    trigger:
      include: child.yml
      strategy: depend
    variables:
      PARENT_PIPELINE_ID: $CI_PIPELINE_ID
  ```

- 子流水线 (`child.yml`)：

  ```yaml
  use-artifact:
    script: cat artifact.txt
    needs:
      - pipeline: $PARENT_PIPELINE_ID
        job: create-artifact
  ```

在此示例中，父流水线中的 `create-artifact` 作业创建了一些产物。`child-pipeline` 作业触发子流水线，并将 `CI_PIPELINE_ID` 变量作为新的 `PARENT_PIPELINE_ID` 变量传递给子流水线。子流水线可以使用 `needs:pipeline` 中的变量从父流水线下载产物。

**额外细节**：

- `pipeline` 属性不接受当前的流水线 ID (`$CI_PIPELINE_ID`)。要从当前流水线中的作业下载产物，请使用 [`needs`](#needsartifacts)。

#### `needs:optional`

> - 引入于 13.10 版本。
> - 功能标志移除于 14.0 版本。

要需要流水线中有时不存在的作业，请将 `optional: true` 添加到 `needs` 配置中。如果未定义，`optional: false` 是默认值。

使用 [`rules`](#rules)、[`only` 或 `except`](#only--except) 的作业可能并不总是存在于流水线中。创建流水线后，极狐GitLab 在启动之前会检查 `needs` 关系。如果没有`optional: true`，需要指向不存在的作业的关系会阻止流水线启动并导致类似于以下的流水线错误：

- `'job1' job needs 'job2' job, but it was not added to the pipeline`

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：

- `job:`: The job to make optional.
- `true` or `false` (default).

**`needs:optional` 示例**：

```yaml
build:
  stage: build
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

rspec:
  stage: test
  needs:
    - job: build
      optional: true
```

在这个例子中：

- 当分支为默认分支时，`build` 作业存在于流水线中，`rspec` 作业在启动之前等待它完成。
- 当分支不是默认分支时，流水线中不存在 `build` 作业。`rspec` 作业立即运行（类似于 `needs: []`），因为它与 `build` 作业的 `needs` 关系是可选的。

#### `needs:pipeline`

您可以使用 `needs:pipeline` 关键字将流水线状态从上游流水线镜像到桥接作业。来自默认分支的最新流水线状态被复制到桥接作业。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：

- 完整的项目路径，包括命名空间和群组。如果项目在同一个组或命名空间中，你可以从 `project:` 关键字中省略它们。例如：`project: group/project-name` 或 `project: project-name`。

**`needs:pipeline` 示例**：

```yaml
upstream_bridge:
  stage: test
  needs:
    pipeline: other/project
```

**额外细节**：

- 如果您将 `job` 关键字添加到 `needs:pipeline`，该作业将不再反映流水线状态。更改为 [`needs:pipeline:job`](#needspipelinejob)。

### `tags`

> - 于 14.3 版本，在自助管理版上启用的每个作业限制为 50 个标签。

使用 `tags` 从项目可用的所有 runner 列表中选择一个特定的 runner。

注册 runner 时，您可以指定 runner 的标签，例如 `ruby`、`postgres` 或 `development`。要获取并运行作业，必须为 runner 分配作业中列出的每个标签。

**关键字类型**：作业关键字。您只能将其用作作业的一部分或在 [`default:` 部分](#default) 中使用。

**可能的输入**：

- 标签名称数组。
- 14.1 及更高版本中的 CI/CD 变量<!--[CI/CD 变量](../runners/configure_runners.md#use-cicd-variables-in-tags)-->。

**`tags` 示例**：

```yaml
job:
  tags:
    - ruby
    - postgres
```

在此示例中，只有同时具有 `ruby` 和 `postgres` 标签的 runner 才能运行该作业。

**额外细节**：

- 在 14.3 及更高版本中，标签数量必须小于 `50`。

<!--
**Related topics**:

- [Use tags to control which jobs a runner can run](../runners/configure_runners.md#use-tags-to-control-which-jobs-a-runner-can-run).
-->

### `allow_failure`

使用 `allow_failure` 来确定当作业失败时，流水线是否应该继续运行。

- 要让流水线继续运行后续作业，请使用 `allow_failure: true`。
- 要停止流水线运行后续作业，请使用 `allow_failure: false`。

当允许作业失败 (`allow_failure: true`) 时，橙色警告 (**{status_warning}**) 表示作业失败。但是，流水线是成功的，并且关联的提交被标记为已通过且没有警告。

在以下情况下会显示相同的警告：

- 阶段中的所有其他工作均成功。
- 流水线中的所有其他作业均成功。

`allow_failure` 的默认值为：

- <!--[手动作业](../jobs/job_control.md#create-a-job-that-must-be-run-manually)-->手动作业为 `true`。
- 对于也使用 [`rules`](#rules) 的手动作业，`false`。
- 在所有其他情况下为 `false`。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：`true` 或 `false`。

**`allow_failure` 示例**：

```yaml
job1:
  stage: test
  script:
    - execute_script_1

job2:
  stage: test
  script:
    - execute_script_2
  allow_failure: true

job3:
  stage: deploy
  script:
    - deploy_to_staging
```

在这个例子中，`job1` 和 `job2` 并行运行：

- 如果 `job1` 失败，`deploy` 阶段的作业不会启动。
- 如果 `job2` 失败，`deploy` 阶段的作业仍然可以启动。

**额外细节**：

- 您可以使用 `allow_failure` 作为 [`rules:`](#rulesallow_failure) 的子键。
- 您可以在手动作业中使用 `allow_failure: false` 来创建阻塞的手动作业<!--[阻塞手动作业](../jobs/job_control.md#types-of-manual-jobs)-->。在手动作业启动并成功完成之前，阻塞的流水线不会在后续阶段运行任何作业。

#### `allow_failure:exit_codes`

> - 引入于 13.8 版本。
> - 功能标志移除于 13.9 版本。

使用 `allow_failure:exit_codes` 来控制何时允许作业失败。对于任何列出的退出代码，作业是 `allow_failure: true`，对于任何其他退出代码，`allow_failure` 为 false。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：

- 单个退出代码。
- 退出代码数组。

**`allow_failure` 示例**：

```yaml
test_job_1:
  script:
    - echo "Run a script that results in exit code 1. This job fails."
    - exit 1
  allow_failure:
    exit_codes: 137

test_job_2:
  script:
    - echo "Run a script that results in exit code 137. This job is allowed to fail."
    - exit 137
  allow_failure:
    exit_codes:
      - 137
      - 255
```

### `when`

使用 `when` 配置作业运行的条件。如果未在作业中定义，则默认值为 `when: on_success`。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：

- `on_success`（默认）：仅当早期阶段的所有作业都成功或具有 `allow_failure: true` 时才运行作业。
- `manual`：仅在手动触发<!--[手动触发](../jobs/job_control.md#create-a-job-that-must-be-run-manually)-->时运行作业。
- `always`：无论早期阶段的作业状态如何，都运行作业。
- `on_failure`：只有在早期阶段至少有一个作业失败时才运行作业。
- `delayed`：<!--[作业的执行延迟](../jobs/job_control.md#run-a-job-after-a-delay)-->作业的执行延迟指定的持续时间。
- `never`：不要运行作业。

**`when` 示例**：

```yaml
stages:
  - build
  - cleanup_build
  - test
  - deploy
  - cleanup

build_job:
  stage: build
  script:
    - make build

cleanup_build_job:
  stage: cleanup_build
  script:
    - cleanup build when failed
  when: on_failure

test_job:
  stage: test
  script:
    - make test

deploy_job:
  stage: deploy
  script:
    - make deploy
  when: manual

cleanup_job:
  stage: cleanup
  script:
    - cleanup after jobs
  when: always
```

在这个例子中，脚本：

1. 只有当 `build_job` 失败时才执行 `cleanup_build_job`。
1. 无论成功或失败，始终将 `cleanup_job` 作为流水线的最后一步执行。
1. 在 GitLab UI 中手动运行时执行 `deploy_job`。

**额外细节**：

- 在  13.5 及更高版本中，您可以在与 [`trigger`](#trigger) 相同的作业中使用 `when:manual`。在 13.4 及更早版本中，将它们一起使用会导致错误 `jobs:#{job-name} when should be on_success, on_failure or always`。
- `allow_failure` 的默认行为通过 `when: manual` 更改为 `true`。但是，如果您将 `when: manual` 与 [`rules`](#rules) 一起使用，`allow_failure` 默认为 `false`。

<!--
**Related topics**:

- `when` can be used with [`rules`](#rules) for more dynamic job control.
- `when` can be used with [`workflow`](#workflow) to control when a pipeline can start.
-->

### `environment`

使用 `environment` 定义作业部署到的[环境](../environments/index.md)。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：作业部署到的环境的名称，采用以下格式之一：

- 纯文本，包括字母、数字、空格和以下字符：`-`、`_`、`/`、`$`、`{`、`}`。
- CI/CD 变量，包括在`.gitlab-ci.yml` 文件中定义的预定义、安全或变量。您不能使用在 `script` 部分中定义的变量。

**`environment` 示例**：

```yaml
deploy to production:
  stage: deploy
  script: git push production HEAD:main
  environment: production
```

**额外细节**：

- 如果您指定了一个 `environment` 并且不存在具有该名称的环境，则会创建一个环境。

#### `environment:name`

为[环境](../environments/index.md)设置名称。

常见的环境名称是 `qa`、`staging` 和 `production`，但您可以使用任何名称。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：作业部署到的环境的名称，采用以下格式之一：

- 纯文本，包括字母、数字、空格和以下字符：`-`、`_`、`/`、`$`、`{`、`}`。
- CI/CD 变量，包括在 `.gitlab-ci.yml` 文件中定义的预定义、安全或变量。 您不能使用在 `script` 部分中定义的变量。

**`environment:name` 示例**：

```yaml
deploy to production:
  stage: deploy
  script: git push production HEAD:main
  environment:
    name: production
```

#### `environment:url`

为[环境](../environments/index.md)设置 URL。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：单个 URL，采用以下格式之一：

- 纯文本，如 `https://prod.example.com`。
- CI/CD 变量，包括在 `.gitlab-ci.yml` 文件中定义的预定义、安全或变量。 您不能使用在 `script` 部分中定义的变量。

**`environment:url` 示例**：

```yaml
deploy to production:
  stage: deploy
  script: git push production HEAD:main
  environment:
    name: production
    url: https://prod.example.com
```

**额外细节**：

- 作业完成后，您可以通过选择合并请求、环境或部署页面中的按钮来访问 URL。

#### `environment:on_stop`

关闭（停止）环境可以通过在 `environment` 下定义的 `on_stop` 关键字来实现。 它声明了一个为了关闭环境而运行的不同作业。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**额外细节**：

- 有关更多详细信息和示例，请参阅 [`environment:action`](#environmentaction)。

#### `environment:action`

使用 `action` 关键字来指定准备、启动或停止环境的作业。

| **值** | **描述**                                                                                                                                               |
|-----------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `start`     | 默认值。 表示作业启动环境。部署是在作业启动后创建的。                                                          |
| `prepare`   | 表示作业只准备环境。它不会触发部署。<!--[Read more about preparing environments](../environments/index.md#prepare-an-environment-without-creating-a-deployment).--> |
| `stop`      | 表示作业停止部署。请参阅下面的示例。                                                                                                   |

举例：

```yaml
review_app:
  stage: deploy
  script: make deploy-app
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: https://$CI_ENVIRONMENT_SLUG.example.com
    on_stop: stop_review_app

stop_review_app:
  stage: deploy
  variables:
    GIT_STRATEGY: none
  script: make delete-app
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
```

在上面的例子中，`review_app` 作业部署到 `review` 环境。在 `on_stop` 下列出了一个新的 `stop_review_app` 作业。在 `review_app` 作业完成后，它会根据 `when` 下定义的内容触发 `stop_review_app` 作业。在这种情况下，它被设置为 `manual`，因此它需要来自 GitLab UI 的<!--[手动操作](../jobs/job_control.md#create-a-job-that-must-be-run-manually)-->手动操作运行。

同样在示例中，`GIT_STRATEGY` 设置为 `none`。如果 `stop_review_app` 作业自动触发<!--[自动触发](../environments/index.md#stop-an-environment)-->，则 runner 在删除分支后不会尝试检出代码。

该示例还覆盖了全局变量。如果您的 `stop` `environment` 作业依赖于全局变量，请在设置 `GIT_STRATEGY` 且在不覆盖全局变量的情况下，更改作业时使用锚点变量<!--[锚点变量](yaml_optimization.md#yaml-anchors-for-variables)-->。

`stop_review_app` 作业**需要**定义以下关键字：

- `when`，定义于：
   - [作业级别](#when)。
   - [在规则子句中](#rules)。如果使用 `rules:` 和 `when: manual`，您还应该设置 [`allow_failure: true`](#allow_failure)，这样即使作业没有运行，可实现也可以完成。
- `environment:name`
- `environment:action`

此外，两个作业都应该具有匹配的 [`rules`](#only--except) 或 [`only/except`](#only--except) 配置。

在上面的示例中，如果配置不相同：

- `stop_review_app` 作业可能不会包含在包含 `review_app` 作业的所有流水线中。
- 无法触发 `action: stop` 来自动停止环境。

#### `environment:auto_stop_in`

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/20956) in GitLab 12.8.
-->

`auto_stop_in` 关键字指定环境的生命周期。当环境过期时，系统会自动停止它。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：用自然语言编写的一段时间。 例如，这些都是等价的：

- `168 hours`
- `7 days`
- `one week`

**`environment:auto_stop_in` 示例**：

```yaml
review_app:
  script: deploy-review-app
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    auto_stop_in: 1 day
```

当为 `review_app` 创建环境时，环境的生命周期设置为 `1 day`。
每次部署 review app 时，该生命周期也会重置为 `1 day`。

<!--
**Related topics**:

- [Environments auto-stop documentation](../environments/index.md#stop-an-environment-after-a-certain-time-period).
-->

#### `environment:kubernetes`

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/27630) in GitLab 12.6.
-->

使用 `kubernetes` 关键字将部署配置到与您的项目关联的 Kubernetes 集群<!--[Kubernetes 集群](../../user/infrastructure/clusters/index.md)-->。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**`environment:kubernetes` 示例**：

```yaml
deploy:
  stage: deploy
  script: make deploy-app
  environment:
    name: production
    kubernetes:
      namespace: production
```

此配置使用 `production` [Kubernetes 命名空间](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/)。

**额外细节**：

- [由极狐GitLab 管理](../../user/project/clusters/gitlab_managed_clusters.md) 的 Kubernetes 集群不支持 Kubernetes 配置。
  
  <!--To follow progress on support for GitLab-managed clusters, see the
  [relevant issue](https://gitlab.com/gitlab-org/gitlab/-/issues/38054).

**Related topics**:

- [Available settings for `kubernetes`](../environments/index.md#configure-kubernetes-deployments-deprecated).
-->

#### `environment:deployment_tier`

> 引入于 13.10 版本。

使用 `deployment_tier` 关键字指定部署环境的层级。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：以下之一：

- `production`
- `staging`
- `testing`
- `development`
- `other`

**`environment:deployment_tier` 示例**：

```yaml
deploy:
  script: echo
  environment:
    name: customer-portal
    deployment_tier: production
```

<!--
**Related topics**:

- [Deployment tier of environments](../environments/index.md#deployment-tier-of-environments).
-->

#### 动态环境

使用 CI/CD <!--[变量](../variables/index.md)-->变量动态命名环境。

例如：

```yaml
deploy as review app:
  stage: deploy
  script: make deploy
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: https://$CI_ENVIRONMENT_SLUG.example.com/
```

`deploy as review app` 作业被标记为动态创建 `review/$CI_COMMIT_REF_SLUG` 环境的部署。`$CI_COMMIT_REF_SLUG` 是由 runner 设置的 [CI/CD 变量](../variables/index.md)。`$CI_ENVIRONMENT_SLUG` 变量基于环境名称，但适合包含在 URL 中。如果 `deploy as review app` 作业在名为 `pow` 的分支中运行，则可以使用类似 `https://review-pow.example.com/` 的 URL 访问该环境。

常见的用例是为分支机构创建动态环境并将它们用作审核应用程序。<!--您可以在 <https://gitlab.com/gitlab-examples/review-apps-nginx/> 上看到一个使用 Review Apps 的示例。-->

### `cache`

使用 `cache` 指定要在作业之间缓存的文件和目录列表。您只能使用本地工作副本中的路径。

缓存在流水线和作业之间共享。缓存在[产物](#artifacts)之前恢复。

<!--Learn more about caches in [Caching in GitLab CI/CD](../caching/index.md).-->

#### `cache:paths`

使用 `cache:paths` 关键字来选择要缓存的文件或目录。

**关键字类型**：作业关键字。您只能将其用作作业的一部分或在 [`default:` 部分](#default) 中使用。

**可能的输入**：相对于项目目录的路径数组（`$CI_PROJECT_DIR`）。您可以使用使用 glob 模式的通配符：

- 在 GitLab Runner 13.0 及更高版本中，[`doublestar.Glob`](https://pkg.go.dev/github.com/bmatcuk/doublestar@v1.2.2?tab=doc#Match)。
- 在 GitLab Runner 12.10 及更早版本中，[`filepath.Match`](https://pkg.go.dev/path/filepath#Match)。

**`cache:paths` 示例**：

缓存 `binaries` 中以 `.apk` 和 `.config` 文件结尾的所有文件：

```yaml
rspec:
  script:
    - echo "This job uses a cache."
  cache:
    key: binaries-cache
    paths:
      - binaries/*.apk
      - .config
```

<!--
**Related topics**:

- See the [common `cache` use cases](../caching/index.md#common-use-cases-for-caches) for more
  `cache:paths` examples.
-->

#### `cache:key`

使用 `cache:key` 关键字为每个缓存提供唯一的标识键。使用相同缓存键的所有作业都使用相同的缓存，包括在不同的流水线中。

如果未设置，则默认键为 `default`。所有带有 `cache:` 关键字但没有 `cache:key` 的作业共享 `default` 缓存。

**关键字类型**：作业关键字。您只能将其用作作业的一部分或在 [`default:` 部分](#default) 中使用。

**可能的输入**：

- 一个字符串。
- [预定义变量](../variables/index.md)。
- 两者的结合。

**`cache:key` 示例**：

```yaml
cache-job:
  script:
    - echo "This job uses a cache."
  cache:
    key: binaries-cache-$CI_COMMIT_REF_SLUG
    paths:
      - binaries/
```

**额外细节**：

- 如果你使用 **Windows Batch** 运行你的 shell 脚本，你需要用 `%` 替换 `$`。 例如：`密钥：%CI_COMMIT_REF_SLUG%`
- `cache:key` 值不能包含：

   - `/` 字符，或等效的 URI 编码的 `%2F`。
   - 只有 `.` 字符（任何数字），或等效的 URI 编码的 `%2E`。

- 缓存在作业之间共享，因此如果您为不同的作业使用不同的路径，您还应该设置不同的 `cache:key`。 否则缓存内容可能会被覆盖。

<!--
**Related topics**:

- You can specify a [fallback cache key](../caching/index.md#use-a-fallback-cache-key)
  to use if the specified `cache:key` is not found.
- You can [use multiple cache keys](../caching/index.md#use-multiple-caches) in a single job.
- See the [common `cache` use cases](../caching/index.md#common-use-cases-for-caches) for more
  `cache:key` examples.
-->

##### `cache:key:files`

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/18986) in GitLab 12.5.
-->

使用 `cache:key:files` 关键字在一两个特定文件更改时生成新密钥。`cache:key:files` 可让您重用一些缓存，并减少重建它们的频率，从而加快后续流水线运行的速度。

**关键字类型**：作业关键字。您只能将其用作作业的一部分或在 [`default:` 部分](#default) 中使用。

**可能的输入**：一个或两个文件路径的数组。

**`cache:key:files` 示例**：

```yaml
cache-job:
  script:
    - echo "This job uses a cache."
  cache:
    key:
      files:
        - Gemfile.lock
        - package.json
    paths:
      - vendor/ruby
      - node_modules
```

此示例为 Ruby 和 Node.js 依赖项创建缓存。缓存绑定到当前版本的 `Gemfile.lock` 和 `package.json` 文件。当这些文件之一发生变化时，将计算一个新的缓存键并创建一个新的缓存。任何使用相同的 `Gemfile.lock` 和 `package.json` 以及 `cache:key:files` 的未来作业都会使用新的缓存，而不是重建依赖项。

**额外信息**：缓存 `key` 是根据最近更改了每个列出的文件的提交计算得出的 SHA。如果在任何提交中都没有更改任何文件，则回退键是 `default`。

##### `cache:key:prefix`

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/18986) in GitLab 12.5.
-->

使用 `cache:key:prefix` 将前缀与为 [`cache:key:files`](#cachekeyfiles) 计算的 SHA 结合起来。

**关键字类型**：作业关键字。您只能将其用作作业的一部分或在 [`default:` 部分](#default) 中使用。

**可能的输入**：

- 一个字符串
- [预定义变量](../variables/index.md)
- 两者的结合。

**`cache:key:prefix` 示例**：

```yaml
rspec:
  script:
    - echo "This rspec job uses a cache."
  cache:
    key:
      files:
        - Gemfile.lock
      prefix: $CI_JOB_NAME
    paths:
      - vendor/ruby
```

例如，添加 `$CI_JOB_NAME` 的 `prefix` 会使密钥看起来像 `rspec-feef9576d21ee9b6a32e30c5c79d0a0ceb68d1e5`。
如果分支更改了 `Gemfile.lock`，则该分支会为 `cache:key:files` 提供一个新的 SHA 校验和。
生成一个新的缓存键，并为该键创建一个新的缓存。如果没有找到 `Gemfile.lock`，前缀会被添加到 `default`，因此示例中的键是 `rspec-default`。

**额外细节**：如果在任何提交中都没有更改 `cache:key:files` 中的文件，则会将前缀添加到 `default` 键。

#### `cache:untracked`

使用 `untracked: true` 来缓存 Git 仓库中所有未跟踪的文件：

**关键字类型**：作业关键字。您只能将其用作作业的一部分或在 [`default:` 部分](#default) 中使用。

**可能的输入**：`true` 或 `false`（默认）。

**`cache:untracked` 示例**：

```yaml
rspec:
  script: test
  cache:
    untracked: true
```

**额外细节**：

- 您可以将 `cache:untracked` 与 `cache:paths` 结合使用来缓存所有未跟踪的文件以及配置路径中的文件。例如：

  ```yaml
  rspec:
    script: test
    cache:
      untracked: true
      paths:
        - binaries/
  ```

#### `cache:when`

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/18969) in GitLab 13.5 and GitLab Runner v13.5.0.
-->

使用 `cache:when` 定义何时根据作业的状态保存缓存。

**关键字类型**：作业关键字。您只能将其用作作业的一部分或在 [`default:` 部分](#default) 中使用。

**可能的输入**：

- `on_success`（默认）：仅在作业成功时保存缓存。
- `on_failure`：仅在作业失败时保存缓存。
- `always`：始终保存缓存。

**`cache:when` 示例**：

```yaml
rspec:
  script: rspec
  cache:
    paths:
      - rspec/
    when: 'always'
```

无论作业是失败还是成功，此示例都会存储缓存。

#### `cache:policy`

要更改缓存的上传和下载行为，请使用 `cache:policy` 关键字。
默认情况下，作业在作业开始时下载缓存，并在作业结束时将更改上传到缓存。 这是 `pull-push` 策略（默认）。

要将作业设置为仅在作业开始时下载缓存，但在作业完成时从不上传更改，请使用 `cache:policy:pull`。

要将作业设置为仅在作业完成时上传缓存，但在作业开始时从不下载缓存，请使用 `cache:policy:push`。

当您有许多使用相同缓存并行执行的作业时，请使用 `pull` 策略。
此策略可加快作业执行速度并减少缓存服务器上的负载。 您可以使用带有 `push` 策略的作业来构建缓存。

**关键字类型**：作业关键字。您只能将其用作作业的一部分或在 [`default:` 部分](#default) 中使用。

**可能的输入**：

- `pull`
- `push`
- `pull-push`（默认）

**`cache:policy` 示例**：

```yaml
prepare-dependencies-job:
  stage: build
  cache:
    key: gems
    paths:
      - vendor/bundle
    policy: push
  script:
    - echo "This job only downloads dependencies and builds the cache."
    - echo "Downloading dependencies..."

faster-test-job:
  stage: test
  cache:
    key: gems
    paths:
      - vendor/bundle
    policy: pull
  script:
    - echo "This job script uses the cache, but does not update it."
    - echo "Running tests..."
```

### `dependencies`

使用 `dependencies` 关键字定义要从中获取[产物](#artifacts)的作业列表。
您还可以设置一个作业以完全不下载任何产物。

如果您不使用 `dependencies`，则前一阶段的所有产物都会传递给每个作业。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：

- 从中获取产物的作业名称。
- 一个空数组 (`[]`)，用于将作业配置为不下载任何产物。

**`dependencies` 示例**：

```yaml
build osx:
  stage: build
  script: make build:osx
  artifacts:
    paths:
      - binaries/

build linux:
  stage: build
  script: make build:linux
  artifacts:
    paths:
      - binaries/

test osx:
  stage: test
  script: make test:osx
  dependencies:
    - build:osx

test linux:
  stage: test
  script: make test:linux
  dependencies:
    - build:linux

deploy:
  stage: deploy
  script: make deploy
```

在这个例子中，两个作业有产物：`build osx` 和 `build linux`。当执行 `test osx` 时，`build osx` 的产物被下载并在构建的上下文中提取。
同样的事情发生在 `test linux` 和 `build linux` 的产物上。

由于 [stage](#stages) 优先级，`deploy` 作业从所有以前的作业下载产物。

**额外细节**：

- 作业状态无关紧要。如果作业失败或者是未触发的手动作业，则不会发生错误。
- 如果依赖作业的产物是[已过期](#artifactsexpire_in)或[已删除](../pipelines/job_artifacts.md#删除作业产物)，则作业失败。

### `artifacts`

使用 `artifacts` 指定在作业 [succeeds, fails, 或 always](#artifactswhen) 时附加到作业的文件和目录列表。

作业完成后，产物将发送到 GitLab。如果大小不大于最大产物大小<!--[最大工件大小](../../user/gitlab_com/index.md#gitlab-cicd)-->，它们可以在 GitLab UI 中下载。

默认情况下，后期的作业会自动下载早期作业创建的所有产物。您可以使用 [`dependencies`](#dependencies) 控制作业中的产物下载行为。

使用 [`needs`](#artifact-downloads-with-needs) 关键字时，作业只能从 `needs` 配置中定义的作业下载产物。

默认只收集成功作业的作业产物，产物在[缓存](#cache)后恢复。

[阅读有关产物的更多信息](../pipelines/job_artifacts.md)。

#### `artifacts:exclude`

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/15122) in GitLab 13.1
> - Requires GitLab Runner 13.1
-->

`exclude` 可以防止将文件添加到产物存档中。

类似于 [`artifacts:paths`](#artifactspaths)，`exclude` 路径是相对于项目目录的。您可以使用使用 glob 或 [`doublestar.PathMatch`](https://pkg.go.dev/github.com/bmatcuk/doublestar@v1.2.2?tab=doc#PathMatch) 模式的通配符。

例如，要将所有文件存储在 `binaries/` 中，但不存储位于 `binaries/` 子目录中的 `*.o` 文件：

```yaml
artifacts:
  paths:
    - binaries/
  exclude:
    - binaries/**/*.o
```

与 [`artifacts:paths`](#artifactspaths) 不同，`exclude` 路径不是递归的。要排除目录的所有内容，您可以显式匹配它们而不是匹配目录本身。

例如，要将所有文件存储在 `binaries/` 中，但不存储在 `temp/` 子目录中：

```yaml
artifacts:
  paths:
    - binaries/
  exclude:
    - binaries/temp/**/*
```

与 [`artifacts:untracked`](#artifactsuntracked) 匹配的文件也可以使用 `artifacts:exclude` 排除。

#### `artifacts:expire_in`

> - 引入于 13.0 版本，在功能标志后默认禁用，无论到期时间如何，都会保留最新的作业产物。
> - 默认启用于 13.4.
> - 引入于 13.8 版本，可以在项目级别禁用保持最新的作业产物。
> - 引入于 13.9 版本，可以在实例范围内禁用保持最新的作业产物。
> - 引入于 13.12 版本，无论到期时间如何，都会保留最新的流水线产物。

使用 `expire_in` 指定[作业产物](../pipelines/job_artifacts.md)在它们过期和被删除之前存储多长时间。`expire_in` 设置不会影响：

- 来自最新作业的产物，除非保留最新的作业产物：
   - 在项目级别禁用<!--[在项目级别禁用](../pipelines/job_artifacts.md#keep-artifacts-from-most-recent-successful-jobs)-->。
   - 在实例范围禁用<!--[在实例范围禁用](../../user/admin_area/settings/continuous_integration.md#keep-the-latest-artifacts-for-all-jobs-in-the-latest-successful-pipelines)-->。
- [流水线产物](../pipelines/pipeline_artifacts.md)。无法为这些指定到期日期：
   - 来自最新流水线的流水线产物将永远保留。
   - 一周后删除其他流水线产物。

`expire_in` 的值是以秒为单位的经过时间，除非提供了单位。有效值包括：

- `'42'`
- `42 seconds`
- `3 mins 4 sec`
- `2 hrs 20 min`
- `2h20min`
- `6 mos 1 day`
- `47 yrs 6 mos and 4d`
- `3 weeks and 2 days`
- `never`

要在上传后一周使产物过期：

```yaml
job:
  artifacts:
    expire_in: 1 week
```

当产物上传并存储在极狐GitLab 上时，到期时间段开始。如果未定义到期时间，则默认为实例范围设置<!--[实例范围设置](../../user/admin_area/settings/continuous_integration.md#default-artifacts-expiration)-->（默认为 30 天）。

要覆盖到期日期并保护产物不被自动删除：

- 使用作业页面上的**保留**按钮。
- 在 13.3 及更高版本中，将 `expire_in` 的值设置为 `never`。

过期后，产物默认每小时删除一次（使用 cron 作业），并且不再可访问。

#### `artifacts:expose_as`

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/15018) in GitLab 12.5.
-->

使用 `expose_as` 关键字在[合并请求](../../user/project/merge_requests/index.md) UI 中公开[作业产物](../pipelines/job_artifacts.md)。

例如，要匹配单个文件：

```yaml
test:
  script: ["echo 'test' > file.txt"]
  artifacts:
    expose_as: 'artifact 1'
    paths: ['file.txt']
```

通过此配置，极狐GitLab 添加了一个链接 **artifact 1** 到指向 `file1.txt` 的相关合并请求。要访问该链接，请选择合并请求概览中流水线图下方的 **查看已展示的产物**。

匹配整个目录的示例：

```yaml
test:
  script: ["mkdir test && echo 'test' > test/file.txt"]
  artifacts:
    expose_as: 'artifact 1'
    paths: ['test/']
```

请注意以下事项：

- 使用变量定义 `artifacts:paths` 时，不会在合并请求 UI 中显示产物。
- 每个合并请求最多可以公开 10 个作业产物。
- 不支持全局模式。
- 如果指定了目录，如果目录中有多个文件，则链接指向作业产物浏览器<!--[产物浏览器](../pipelines/job_artifacts.md#download-job-artifacts)-->。
- 对于带有 `.html`、`.htm`、`.txt`、`.json`、`.xml` 和 `.log` 扩展名的公开单个文件产物，如果 GitLab Pages<!--[GitLab Pages](../../administration/pages/index.md)-->：
   - 启用，系统自动呈现产物。
   - 未启用，文件显示在产物浏览器中。

#### `artifacts:name`

使用 `name` 指令来定义创建的产物存档的名称。您可以为每个存档指定唯一的名称。`artifacts:name` 变量可以使用任何[预定义变量](../variables/index.md)。
默认名称是 `artifacts`，下载后会变成 `artifacts.zip`。

要使用当前作业的名称创建存档：

```yaml
job:
  artifacts:
    name: "$CI_JOB_NAME"
    paths:
      - binaries/
```

要使用当前分支或标记的名称创建档案，仅包括二进制目录：

```yaml
job:
  artifacts:
    name: "$CI_COMMIT_REF_NAME"
    paths:
      - binaries/
```

如果您的分支名称包含正斜杠（例如`feature/my-feature`），建议使用 `$CI_COMMIT_REF_SLUG` 而不是 `$CI_COMMIT_REF_NAME` 来正确命名产物。

要使用当前作业的名称和当前的分支或标记创建仅包含二进制文件目录的存档：

```yaml
job:
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    paths:
      - binaries/
```

要使用当前 [stage](#stages) 和分支名称的名称创建存档：

```yaml
job:
  artifacts:
    name: "$CI_JOB_STAGE-$CI_COMMIT_REF_NAME"
    paths:
      - binaries/
```

---

如果您使用 **Windows Batch** 运行您的 shell 脚本，你需要用 `%` 替换 `$`：

```yaml
job:
  artifacts:
    name: "%CI_JOB_STAGE%-%CI_COMMIT_REF_NAME%"
    paths:
      - binaries/
```

如果您使用 **Windows PowerShell** 运行您的 shell 脚本，你需要用 `$env:` 替换 `$`：

```yaml
job:
  artifacts:
    name: "$env:CI_JOB_STAGE-$env:CI_COMMIT_REF_NAME"
    paths:
      - binaries/
```

#### `artifacts:paths`

路径相对于项目目录 (`$CI_PROJECT_DIR`)，不能直接链接到项目目录之外。您可以使用 glob 模式的通配符和：

- 在 GitLab Runner 13.0 和更高版本，[`doublestar.Glob`](https://pkg.go.dev/github.com/bmatcuk/doublestar@v1.2.2?tab=doc#Match)。
- 在 GitLab Runner 12.10 和更早版本，[`filepath.Match`](https://pkg.go.dev/path/filepath#Match)。

要限制特定作业从哪些作业获取工件，请参阅 [dependencies](#dependencies)。

发送 `binaries` 和 `.config` 中的所有文件：

```yaml
artifacts:
  paths:
    - binaries/
    - .config
```

要禁用产物传递，请使用空 [dependencies](#dependencies) 定义作业：

```yaml
job:
  stage: build
  script: make build
  dependencies: []
```

您可能只想为标签版本创建产物，以避免使用临时构建产物填充构建服务器存储。

仅为标签创建产物（`default-job` 不创建产物）：

```yaml
default-job:
  script:
    - mvn test -U
  rules:
    - if: $CI_COMMIT_BRANCH

release-job:
  script:
    - mvn package -U
  artifacts:
    paths:
      - target/*.war
  rules:
    - if: $CI_COMMIT_TAG
```

您也可以对目录使用通配符。例如，如果您想获取以 `xyz` 结尾的目录中的所有文件：

```yaml
job:
  artifacts:
    paths:
      - path/*xyz/*
```

#### `artifacts:public`

> - 引入于 13.8 版本
> - 部署在功能标志后默认禁用
> - 推荐用于生产用途

使用 `artifacts:public` 来确定作业制品是否应该公开可用。

`artifacts:public` 的默认值为 `true`，这意味着匿名和访客用户可以下载公共流水线中的产物：

```yaml
artifacts:
  public: true
```

要拒绝匿名用户和来宾用户对公共流水线中的产物的读取访问权限，请将 `artifacts:public` 设置为 `false`：

```yaml
artifacts:
  public: false
```

#### `artifacts:reports`

使用 [`artifacts:reports`](#artifactsreports)：

- 从作业中收集测试报告、代码质量报告和安全报告。
- 在合并请求、流水线视图和安全仪表板中公开这些报告。

无论作业结果如何（成功或失败），都会收集测试报告。
您可以使用 [`artifacts:expire_in`](#artifactsexpire_in) 为其产物设置到期日期。

某些 `artifacts:reports` 类型可以由同一流水线中的多个作业生成，并由每个作业的合并请求或流水线功能使用。

为了能够浏览报告输出文件，请包含 [`artifacts:paths`](#artifactspaths) 关键字。请注意，将上传和存储产物两次。

NOTE:
不支持使用[来自子流水线的产物](#needspipelinejob)的父流水线的组合报告。<!--Track progress on adding support in [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/215725).-->

##### `artifacts:reports:accessibility` **(FREE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/39425) in GitLab 12.8.
-->

`accessibility` 报告使用 [pa11y](https://pa11y.org/) 报告合并请求中引入的更改对可访问性的影响。

极狐GitLab 可以在合并请求<!--[可访问性部件](../../user/project/merge_requests/accessibility_testing.md#accessibility-merge-request-widget)-->可访问性部件中显示一个或多个报告的结果。

<!--
For more information, see [Accessibility testing](../../user/project/merge_requests/accessibility_testing.md).
-->

##### `artifacts:reports:api_fuzzing` **(ULTIMATE)**

<!--
> - Introduced in GitLab 13.4.
> - Requires GitLab Runner 13.4 or later.
-->

`api_fuzzing` 报告收集了 [API Fuzzing bug](../../user/application_security/api_fuzzing/index.md) 作为产物。

<!--
GitLab can display the results of one or more reports in:

- The merge request [security widget](../../user/application_security/api_fuzzing/index.md#view-details-of-an-api-fuzzing-vulnerability).
- The [Project Vulnerability report](../../user/application_security/vulnerability_report/index.md).
- The pipeline [**Security** tab](../../user/application_security/security_dashboard/index.md#pipeline-security).
- The [security dashboard](../../user/application_security/api_fuzzing/index.md#security-dashboard).
-->

##### `artifacts:reports:browser_performance` **(PREMIUM)**

> - 名称从 `artifacts:reports:performance` 改为现名称于 14.0 版本。

`browser_performance` 报告收集浏览器性能测试指标<!--[浏览器性能测试指标](../../user/project/merge_requests/browser_performance_testing.md) -->作为产物。

极狐GitLab 可以在合并请求中显示一份报告的结果<!--[浏览器性能测试小部件](../../user/project/merge_requests/browser_performance_testing.md#how-browser-performance-testing-works)-->。

极狐GitLab 无法显示多个 `browser_performance` 报告的组合结果。

##### `artifacts:reports:cluster_image_scanning` **(ULTIMATE)**

> - 引入于 14.1 版本。
> - 需要 GitLab Runner 14.1 和更高版本。

`cluster_image_scanning` 报告收集 `CLUSTER_IMAGE_SCANNING` 漏洞作为产物。

<!--
GitLab can display the results of one or more reports in:

- The [security dashboard](../../user/application_security/security_dashboard/index.md).
- The [Project Vulnerability report](../../user/application_security/vulnerability_report/index.md).
-->

##### `artifacts:reports:cobertura`

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/3708) in GitLab 12.9.
-->

`cobertura` 报告收集 Cobertura 覆盖 XML 文件<!--[Cobertura 覆盖 XML 文件](../../user/project/merge_requests/test_coverage_visualization.md)-->。
收集的 Cobertura 覆盖率报告作为产物上传到极狐GitLab 并显示在合并请求中。

Cobertura 最初是为 Java 开发的，但有许多第三方移植用于其他语言，如 JavaScript、Python、Ruby 等。

<!--
GitLab can display the results of one or more reports in the merge request
[diff annotations](../../user/project/merge_requests/test_coverage_visualization.md).
-->

##### `artifacts:reports:codequality`

<!--
> - Introduced in GitLab 11.5.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/212499) to GitLab Free in 13.2.
> - Requires GitLab Runner 11.5 and above.
-->

`codequality` 报告收集代码质量问题<!--[代码质量问题](../../user/project/merge_requests/code_quality.md)-->作为产物。

收集的代码质量报告作为产物上传到极狐GitLab，并在合并请求中汇总。

<!--
GitLab can display the results of:

- One or more reports in the merge request [code quality widget](../../user/project/merge_requests/code_quality.md#code-quality-widget).
- Only one report in:
  - The merge request [diff annotations](../../user/project/merge_requests/code_quality.md#code-quality-in-diff-view).
    Track progress on adding support for multiple reports in [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/328257).
  - The [full report](../metrics_reports.md). Track progress on adding support for multiple reports in
    [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/9014).
-->

##### `artifacts:reports:container_scanning` **(ULTIMATE)**

`container_scanning` 报告收集容器扫描漏洞<!--[容器扫描漏洞](../../user/application_security/container_scanning/index.md)-->作为产物。

收集的容器扫描报告作为产物上传到极狐GitLab，并在合并请求和流水线视图中汇总。它还用于为安全仪表板提供数据。

<!--
GitLab can display the results of one or more reports in:

- The merge request [container scanning widget](../../user/application_security/container_scanning/index.md).
- The pipeline [**Security** tab](../../user/application_security/security_dashboard/index.md#pipeline-security).
- The [security dashboard](../../user/application_security/security_dashboard/index.md).
- The [Project Vulnerability report](../../user/application_security/vulnerability_report/index.md).
-->

##### `artifacts:reports:coverage_fuzzing` **(ULTIMATE)**

`coverage_fuzzing` 报告收集 coverage fuzzing bug<!--[coverage fuzzing bug](../../user/application_security/coverage_fuzzing/index.md)-->。
收集到的覆盖模糊报告作为产物上传到极狐GitLab。

<!--
GitLab can display the results of one or more reports in:

- The merge request [coverage fuzzing widget](../../user/application_security/coverage_fuzzing/index.md#interacting-with-the-vulnerabilities).
- The pipeline [**Security** tab](../../user/application_security/security_dashboard/index.md#pipeline-security).
- The [Project Vulnerability report](../../user/application_security/vulnerability_report/index.md).
- The [security dashboard](../../user/application_security/security_dashboard/index.md).
-->

##### `artifacts:reports:dast` **(ULTIMATE)**

`dast` 报告收集 DAST 漏洞<!--[DAST 漏洞](../../user/application_security/dast/index.md)-->。收集的 DAST 报告作为产物上传到极狐GitLab。

<!--
GitLab can display the results of one or more reports in:

- The merge request [security widget](../../user/application_security/dast/index.md#view-details-of-a-vulnerability-detected-by-dast).
- The pipeline [**Security** tab](../../user/application_security/security_dashboard/index.md#pipeline-security).
- The [Project Vulnerability report](../../user/application_security/vulnerability_report/index.md).
- The [security dashboard](../../user/application_security/security_dashboard/index.md).
-->

##### `artifacts:reports:dependency_scanning` **(ULTIMATE)**

dependency_scanning` 报告收集依赖扫描漏洞<!--[依赖扫描漏洞](../../user/application_security/dependency_scanning/index.md)-->。
收集到的依赖扫描报告作为产物上传到极狐GitLab。

<!--
GitLab can display the results of one or more reports in:

- The merge request [dependency scanning widget](../../user/application_security/dependency_scanning/index.md#overview).
- The pipeline [**Security** tab](../../user/application_security/security_dashboard/index.md#pipeline-security).
- The [security dashboard](../../user/application_security/security_dashboard/index.md).
- The [Project Vulnerability report](../../user/application_security/vulnerability_report/index.md).
- The [dependency list](../../user/application_security/dependency_list/).
-->

##### `artifacts:reports:dotenv`

收集的变量被注册为作业的运行时创建的变量，这对于作业完成后设置动态环境 URL<!--[作业完成后设置动态环境 URL](../environments/index.md#set-dynamic-environment-urls-after-a-job-finishes)--> 很有用。

[原始 dotenv 规则](https://github.com/motdotla/dotenv#rules) 有几个例外：

- 变量键只能包含字母、数字和下划线 (`_`)。
- `.env` 文件的最大大小为 5 KB。
- 在 13.5 及更早版本中，最大继承变量数为 10。
- 在 13.6 及更高版本中，最大继承变量数为 20。
- 不支持`.env` 文件中的变量替换。
- `.env` 文件不能有空行或注释（以 `#` 开头）。
- `env` 文件中的键值不能有空格或换行符 (`\n`)，包括使用单引号或双引号时。
- 不支持在解析过程中引用转义 (`key = 'value'` -> `{key: "value"}`)。

##### `artifacts:reports:junit`

`junit` 报告收集[JUnit 报告格式 XML 文件](https://www.ibm.com/docs/en/adfz/developer-for-zos/14.1.0?topic=formats-junit-xml-format)。
收集到的单元测试报告作为产物上传到极狐GitLab。尽管 JUnit 最初是用 Java 开发的，但也有许多其他语言（如 JavaScript、Python 和 Ruby）的第三方端口。

<!--有关更多详细信息和示例，请参阅 [单元测试报告](../unit_test_reports.md)。-->
下面是从 Ruby 的 RSpec 测试工具收集 JUnit 报告格式 XML 文件的示例：

```yaml
rspec:
  stage: test
  script:
    - bundle install
    - rspec --format RspecJunitFormatter --out rspec.xml
  artifacts:
    reports:
      junit: rspec.xml
```

<!--
GitLab can display the results of one or more reports in:

- The merge request [code quality widget](../../ci/unit_test_reports.md#how-it-works).
- The [full report](../../ci/unit_test_reports.md#viewing-unit-test-reports-on-gitlab).
-->

一些 JUnit 工具导出到多个 XML 文件。您可以在单个作业中指定多个测试报告路径以将它们连接到一个文件中。使用：

- 文件名模式（`junit: rspec-*.xml`）。
- 文件名数组（`junit: [rspec-1.xml, rspec-2.xml, rspec-3.xml]`）。
- 两者的组合（`junit: [rspec.xml, test-results/TEST-*.xml]`）。

##### `artifacts:reports:license_scanning` **(ULTIMATE)**

许可证合规报告收集许可证<!--[许可证](../../user/compliance/license_compliance/index.md)-->。许可证合规报告作为产物上传到极狐GitLab。

<!--
GitLab can display the results of one or more reports in:

- The merge request [license compliance widget](../../user/compliance/license_compliance/index.md).
- The [license list](../../user/compliance/license_compliance/index.md#license-list).
-->

##### `artifacts:reports:load_performance` **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35260) in GitLab 13.2.
> - Requires GitLab Runner 11.5 and above.
-->

`load_performance` 报告收集负载性能测试指标<!--[负载性能测试指标](../../user/project/merge_requests/load_performance_testing.md)-->作为产物。

该报告作为产物上传到极狐GitLab。

<!--
GitLab can display the results of only one report in the merge request
[load testing widget](../../user/project/merge_requests/load_performance_testing.md#how-load-performance-testing-works).

GitLab cannot display the combined results of multiple `load_performance` reports.
-->

##### `artifacts:reports:metrics` **(PREMIUM)**

`metrics` 报告收集 Metrics<!--[Metrics](../metrics_reports.md)--> 作为产物。

收集的指标报告作为产物上传到极狐GitLab。

<!--
GitLab can display the results of one or more reports in the merge request
[metrics reports widget](../../ci/metrics_reports.md#metrics-reports).
-->

##### `artifacts:reports:requirements` **(ULTIMATE)**

`requirements` 报告收集 `requirements.json` 文件作为产物。

收集到的需求报告作为产物上传到极狐GitLab，现有的[需求](../../user/project/requirements/index.md)被标记为 Satisfied。

<!--
GitLab can display the results of one or more reports in the
[project requirements](../../user/project/requirements/index.md#view-a-requirement).
-->

##### `artifacts:reports:sast`

<!--
> - Introduced in GitLab 11.5.
> - [Moved](https://gitlab.com/groups/gitlab-org/-/epics/2098) from GitLab Ultimate to GitLab Free in 13.3.
> - Requires GitLab Runner 11.5 and above.
-->

`sast` 报告收集 [SAST 漏洞](../../user/application_security/sast/index.md) 作为产物。

收集的 SAST 报告作为产物上传到极狐GitLab，并在合并请求和流水线视图中汇总。它还用于为安全仪表板提供数据。

<!--
GitLab can display the results of one or more reports in:

- The merge request [SAST widget](../../user/application_security/sast/index.md#static-application-security-testing-sast).
- The [security dashboard](../../user/application_security/security_dashboard/index.md).
-->

##### `artifacts:reports:secret_detection`

<!--
> - Introduced in GitLab 13.1.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/222788) to GitLab Free in 13.3.
> - Requires GitLab Runner 11.5 and above.
-->

`secret-detection` 报告收集检测到的 secret<!--[检测到的 secret](../../user/application_security/secret_detection/index.md)--> 作为产物。

收集到的 secret 检测报告作为产物上传到极狐GitLab，并在合并请求和流水线视图中汇总。它还用于为安全仪表板提供数据。

<!--
GitLab can display the results of one or more reports in:

- The merge request [secret scanning widget](../../user/application_security/secret_detection/index.md).
- The [pipeline **Security** tab](../../user/application_security/index.md#view-security-scan-information-in-the-pipeline-security-tab).
- The [security dashboard](../../user/application_security/security_dashboard/index.md).
-->

##### `artifacts:reports:terraform`

> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/207528) in GitLab 13.0.
> - Requires [GitLab Runner](https://docs.gitlab.com/runner/) 11.5 and above.

`terraform` 报告获取 Terraform `tfplan.json` 文件。<!--[删除凭据所需的 JQ 流程](../../user/infrastructure/iac/mr_integration.md#configure-terraform-report-artifacts)-->删除凭据所需的 JQ 流程。收集的 Terraform 计划报告作为产物上传到极狐GitLab。

<!--
GitLab can display the results of one or more reports in the merge request
[terraform widget](../../user/infrastructure/iac/mr_integration.md#output-terraform-plan-information-into-a-merge-request).

For more information, see [Output `terraform plan` information into a merge request](../../user/infrastructure/iac/mr_integration.md).
-->

#### `artifacts:untracked`

使用 `artifacts:untracked` 将所有 Git 未跟踪文件添加为产物（以及在 `artifacts:paths` 中定义的路径）。`artifacts:untracked` 忽略仓库的 `.gitignore` 文件中的配置。

发送所有 Git 未跟踪文件：

```yaml
artifacts:
  untracked: true
```

发送所有 Git 未跟踪文件和 `binaries` 中的文件：

```yaml
artifacts:
  untracked: true
  paths:
    - binaries/
```

发送所有未跟踪的文件，但[排除](#artifactsexclude) `*.txt`：

```yaml
artifacts:
  untracked: true
  exclude:
    - "*.txt"
```

#### `artifacts:when`

使用 `artifacts:when` 在作业失败时上传产物。

`artifacts:when` 可以设置为以下值之一：

1. `on_success`（默认）：仅在作业成功时上传产物。
1. `on_failure`：仅在作业失败时上传产物。
1. `always`：始终上传产物。例如，当需要对失败的测试进行故障排除时，<!--[上传产物](../unit_test_reports.md#viewing-junit-screenshots-on-gitlab)-->很有用。

例如，仅在作业失败时上传产物：

```yaml
job:
  artifacts:
    when: on_failure
```

### `coverage`

使用带有自定义正则表达式的 `coverage` 来配置如何从作业输出中提取代码覆盖率。如果作业输出中至少有一行与正则表达式匹配，则覆盖率会显示在 UI 中。

为了提取匹配行中的代码覆盖率值，极狐GitLab 使用以下正则表达式：`\d+(\.\d+)?`。

**可能的输入**：正则表达式。必须以`/`开头和结尾。

**`coverage` 示例**：

```yaml
job1:
  script: rspec
  coverage: '/Code coverage: \d+\.\d+/'
```

在这个例子中：

1. 系统检查作业日志中是否有与正则表达式匹配的行。像 `Code coverage: 67.89` 这样的行会匹配。
1. 然后检查该行以找到与 `\d+(\.\d+)?` 的匹配项。上面的示例匹配行给出了 `67.89` 的代码覆盖率。

**额外细节**：

- 如果作业输出中有多个匹配的行，则使用最后一行。
- 删除前导零。
- [子流水线](../pipelines/parent_child_pipelines.md) 的覆盖输出未记录或显示。<!--Check [the related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/280818)
  for more details.-->

### `dast_configuration` **(ULTIMATE)**

> 引入于 14.1 版本。

使用 `dast_configuration` 关键字指定要在 CI/CD 配置中使用的站点配置文件和扫描程序配置文件。必须首先在项目中创建这两个配置文件。作业的阶段必须是 `dast`。

**关键字类型**：作业关键字。只能作为作业的一部分使用。

**可能的输入**：`site_profile` 和 `scanner_profile` 各一个。

- 使用 `site_profile` 指定要在作业中使用的站点配置文件。
- 使用 `scanner_profile` 指定要在作业中使用的扫描仪配置文件。

**`dast_configuration`** 示例：

```yaml
stages:
  - build
  - dast

include:
  - template: DAST.gitlab-ci.yml

dast:
  dast_configuration:
    site_profile: "Example Co"
    scanner_profile: "Quick Passive Test"
```

在此示例中，`dast` 作业扩展了添加了 `include:` 关键字的 `dast` 配置，以选择特定的站点配置文件和扫描仪配置文件。

**额外细节**：

- 站点配置文件或扫描仪配置文件中包含的设置优先于 DAST 模板中包含的设置。

<!--
**Related topics**:

- [Site profile](../../user/application_security/dast/index.md#site-profile).
- [Scanner profile](../../user/application_security/dast/index.md#scanner-profile).
-->

### `retry`

使用 `retry` 配置作业失败时重试的次数。如果未定义，则默认为 `0` 并且作业不会重试。

当作业失败时，该作业最多再处理两次，直到成功或达到最大重试次数。

默认情况下，所有失败类型都会导致重试作业。使用 [`retry:when`](#retrywhen) 选择要重试的失败。

**关键字类型**：作业关键字。您只能将其用作作业的一部分或在 [`default:` 部分](#default) 中使用。

**可能的输入**：`0`（默认）、`1` 或`2`。

**`retry` 示例**：

```yaml
test:
  script: rspec
  retry: 2
```

#### `retry:when`

使用 `retry:when` 和 `retry:max` 仅针对特定的失败情况重试作业。`retry:max` 是最大重试次数，如 [`retry`](#retry)，可以是 `0`、`1` 或 `2`。

**关键字类型**：作业关键字。您只能将其用作作业的一部分或在 [`default:` 部分](#default) 中使用。

**可能的输入**：单一故障类型，或一个或多个故障类型的数组：

<!--
  If you change any of the values below, make sure to update the `RETRY_WHEN_IN_DOCUMENTATION`
  array in `spec/lib/gitlab/ci/config/entry/retry_spec.rb`.
  The test there makes sure that all documented
  values are valid as a configuration option and therefore should always
  stay in sync with this documentation.
-->

- `always`：任何失败重试（默认）。
- `unknown_failure`：当失败原因未知时重试。
- `script_failure`：脚本失败时重试。
- `api_failure`：在 API 失败时重试。
- `stuck_or_timeout_failure`：当作业卡住或超时时重试。
- `runner_system_failure`：如果 runner 系统出现故障（例如，作业设置失败），请重试。
- `runner_unsupported`：如果 runner 不受支持，请重试。
- `stale_schedule`：如果无法执行延迟的作业，请重试。
- `job_execution_timeout`：如果脚本超过为作业设置的最大执行时间，请重试。
- `archived_failure`：如果作业已存档且无法运行，请重试。
- `unmet_prerequisites`：如果作业未能完成先决任务，请重试。
- `scheduler_failure`：如果 scheduler 未能将作业分配给 runner，请重试。
- `data_integrity_failure`：如果检测到结构完整性问题，请重试。

**`retry:when` 示例**（单一故障型）:

```yaml
test:
  script: rspec
  retry:
    max: 2
    when: runner_system_failure
```

果存在除 runner 系统故障以外的故障，则不会重试作业。

**`retry:when` 示例** （故障类型数组）:

```yaml
test:
  script: rspec
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
```

<!--
**Related topics**:

You can specify the number of [retry attempts for certain stages of job execution](../runners/configure_runners.md#job-stages-attempts)
using variables.
-->

### `timeout`

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/14887) in GitLab 12.3.
-->

使用 `timeout` 为特定作业配置超时。如果作业运行的时间超过超时时间，作业将失败。

作业级超时可以长于项目级超时<!--[项目级超时](../pipelines/settings.md#set-a-limit-for-how-long-jobs-can-run)-->。但不能超过 runner 的超时<!--[runner 的超时](../runners/configure_runners.md#set-maximum-job-timeout-for-a-runner)-->。

**关键字类型**：作业关键字。您只能将其用作作业的一部分或在 [`default:` 部分](#default)中使用。

**可能的输入**：用自然语言编写的一段时间。例如，以下都是等价的：

- `3600 seconds`
- `60 minutes`
- `one hour`

**`timeout` 示例**：

```yaml
build:
  script: build.sh
  timeout: 3 hours 30 minutes

test:
  script: rspec
  timeout: 3h 30m
```

### `parallel`

使用 `parallel` 配置并行运行的作业实例数。
该值可以是 2 到 50。

`parallel` 关键字创建并行运行的同一作业的 N 个实例。
它们从 `job_name 1/N` 到 `job_name N/N` 按顺序命名：

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：从 `2` 到 `50` 的数值。

**`parallel` 示例**：

```yaml
test:
  script: rspec
  parallel: 5
```

此示例创建了 5 个并行运行的作业，名为 `test 1/5` 到 `test 5/5`。

**额外细节**：

- 每个并行作业都有一个 `CI_NODE_INDEX` 和 `CI_NODE_TOTAL` 预定义 CI/CD 变量<!--[预定义 CI/CD 变量](../variables/index.md#predefined-cicd-variables)-->集。

<!--
**Related topics**:

- [Parallelize large jobs](../jobs/job_control.md#parallelize-large-jobs).
-->

#### `parallel:matrix`

> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/15356) in GitLab 13.3.
> - The job naming style was [improved in GitLab 13.4](https://gitlab.com/gitlab-org/gitlab/-/issues/230452).

使用 `parallel:matrix` 在单个流水线中并行运行作业多次，但对于作业的每个实例使用不同的变量值。

必须存在多个 runner，或者必须将单个 runner 配置为同时运行多个作业。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：从 `2` 到 `50` 的数值。

**`parallel:matrix` 示例**：

```yaml
deploystacks:
  stage: deploy
  script:
    - bin/deploy
  parallel:
    matrix:
      - PROVIDER: aws
        STACK:
          - monitoring
          - app1
          - app2
      - PROVIDER: ovh
        STACK: [monitoring, backup, app]
      - PROVIDER: [gcp, vultr]
        STACK: [data, processing]
```

该示例生成 10 个并行的 `deploystacks` 作业，每个作业具有不同的 `PROVIDER` 和 `STACK` 值：

```plaintext
deploystacks: [aws, monitoring]
deploystacks: [aws, app1]
deploystacks: [aws, app2]
deploystacks: [ovh, monitoring]
deploystacks: [ovh, backup]
deploystacks: [ovh, app]
deploystacks: [gcp, data]
deploystacks: [gcp, processing]
deploystacks: [vultr, data]
deploystacks: [vultr, processing]
```

<!--
**Related topics**:

- [Run a one-dimensional matrix of parallel jobs](../jobs/job_control.md#run-a-one-dimensional-matrix-of-parallel-jobs).
- [Run a matrix of triggered parallel jobs](../jobs/job_control.md#run-a-matrix-of-parallel-trigger-jobs).
-->

### `trigger`

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/8997) in GitLab Premium 11.8.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/199224) to GitLab Free in 12.8.
-->

使用 `trigger` 启动下游流水线：

- [多项目流水线](../pipelines/multi_project_pipelines.md)。
- [子流水线](../pipelines/parent_child_pipelines.md)。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**： 

- 对于多项目流水线，指向下游项目的路径。
- 对于子流水线，子流水线 CI/CD 配置文件的路径。

**多项目流水线的 `trigger` 示例**：

```yaml
rspec:
  stage: test
  script: bundle exec rspec

staging:
  stage: deploy
  trigger: my/deployment
```

**子流水线的 `trigger` 示例**：

```yaml
trigger_job:
  trigger:
    include: path/to/child-pipeline.yml
```

**额外细节**：

**额外信息**：

- 带有 `trigger` 的作业只能使用有限的关键字集<!--[有限的关键字集](../pipelines/multi_project_pipelines.md#define-multi-project-pipelines-in-your-gitlab-ciyml-file)-->。例如，您不能使用 [`script`](#script)、[`before_script`](#before_script) 或 [`after_script`](#after_script) 运行命令。
- 在 13.5 及更高版本中，您可以在与 `trigger` 相同的作业中使用 [`when:manual`](#when)。 在 13.4 及更早版本中，将它们一起使用会导致错误 `jobs:#{job-name} when should be on_success, on_failure or always`。
- 在 13.2 及更高版本中，您可以在流水线图<!--[流水线图](../pipelines/index.md#visualize-pipelines)-->中查看哪个作业触发了下游流水线。

<!--
**Related topics**:

- [Multi-project pipeline configuration examples](../pipelines/multi_project_pipelines.md#define-multi-project-pipelines-in-your-gitlab-ciyml-file).
- [Child pipeline configuration examples](../pipelines/parent_child_pipelines.md#examples).
- To force a rebuild of a specific branch, tag, or commit, you can
  [use an API call with a trigger token](../triggers/index.md).
  The trigger token is different than the `trigger` keyword.
-->

#### `trigger:strategy`

使用 `trigger:strategy` 强制 `trigger` 作业在标记为 **success** 之前等待下游流水线完成。

此行为与默认行为不同，默认行为是在创建下游流水线后立即将 `trigger` 作业标记为 **success**。

此设置使您的流水线执行线性而不是并行。

**`trigger:strategy` 示例**：

```yaml
trigger_job:
  trigger:
    include: path/to/child-pipeline.yml
    strategy: depend
```

在此示例中，后续阶段的作业在开始之前等待触发的流水线成功完成。

### `interruptible`

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/32022) in GitLab 12.3.
-->

如果在作业完成之前新流水线启动时应取消作业，请使用 `interruptible`。

此关键字与自动取消冗余管道<!--[自动取消冗余管道](../pipelines/settings.md#auto-cancel-redundant-pipelines)-->功能一起使用。启用后，当新流水线在同一分支上启动时，可以取消具有 `interruptible: true` 的正在运行的作业。

在带有 `interruptible: false` 的作业开始后，您无法取消后续作业。

**关键字类型**：作业关键字。您只能将其用作作业的一部分或在 [`default:` 部分](#default) 中使用。

**可能的输入**：`true` 或 `false`（默认）。

**`interruptible` 示例**：

```yaml
stages:
  - stage1
  - stage2
  - stage3

step-1:
  stage: stage1
  script:
    - echo "Can be canceled."
  interruptible: true

step-2:
  stage: stage2
  script:
    - echo "Can not be canceled."

step-3:
  stage: stage3
  script:
    - echo "Because step-2 can not be canceled, this step can never be canceled, even though it's set as interruptible."
  interruptible: true
```

在这个例子中，一个新的流水线导致一个正在运行的流水线：

- 取消，如果只有 `step-1` 正在运行或挂起。
- 未取消，在 `step-2` 开始后。

**额外细节**：

- 如果作业在开始后可以安全取消，则仅设置 `interruptible: true`，例如构建作业。通常不应取消部署作业，以防止部分部署。
- 要完全取消正在运行的流水线，所有作业必须具有 `interruptible: true`，或 `interruptible: false` 作业必须尚未启动。

### `resource_group`

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/15536) in GitLab 12.7.
-->

使用 `resource_group` 创建一个资源组<!--[资源组](../resource_groups/index.md)-->，以确保同一项目的不同流水线之间的作业是互斥的。

例如，如果属于同一资源组的多个作业同时排队，则只有其中一个作业启动。其他作业一直等到 `resource_group` 空闲。

资源组的行为类似于其他编程语言中的信号量。

您可以为每个环境定义多个资源组。例如，在部署到物理设备时，您可能有多个物理设备。 每个设备都可以部署到，但在任何给定时间每个设备只能进行一次部署。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：只有字母、数字、`-`、`_`、`/`、`$`、`{`、`}`、`.` 和空格。不能以`/`开头或结尾。

**`resource_group` 示例**：

```yaml
deploy-to-production:
  script: deploy
  resource_group: production
```

在这个例子中，两个独立流水线中的两个 `deploy-to-production` 作业永远不能同时运行。因此，您可以确保并发部署永远不会发生在生产环境中。

<!--
**Related topics**:

- [Pipeline-level concurrency control with cross-project/parent-child pipelines](../resource_groups/index.md#pipeline-level-concurrency-control-with-cross-projectparent-child-pipelines).
-->

### `release`

> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/19298) in GitLab 13.2.

使用 `release` 创建一个[发布](../../user/project/releases/index.md)。

发布作业必须有权访问 `release-cli`<!--[`release-cli`](https://gitlab.com/gitlab-org/release-cli/-/tree/master/docs)-->，其必须在 `$PATH` 中。

<!--
如果您使用 Docker executor<!--[Docker executor](https://docs.gitlab.com/runner/executors/docker.html)可以使用 GitLab Container Registry 中的这个镜像：`registry.gitlab.com/gitlab-org/ 发布-cli：最新`
-->

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：`release:` 子键：

- [`tag_name`](#releasetag_name)
- [`description`](#releasedescription)
- [`name`](#releasename)（可选）
- [`ref`](#releaseref)（可选）
- [`milestones`](#releasemilestones)（可选）
- [`released_at`](#releasereleased_at)（可选）
- [`assets:links`](#releaseassetslinks)（可选）

**`release` 关键字示例**：

  ```yaml
  release_job:
    stage: release
    image: registry.gitlab.com/gitlab-org/release-cli:latest
    rules:
      - if: $CI_COMMIT_TAG                  # Run this job when a tag is created manually
    script:
      - echo 'Running the release job.'
    release:
      name: 'Release $CI_COMMIT_TAG'
      description: 'Release created using the release-cli.'
  ```

此示例创建一个版本：

- 当你推送一个 Git 标签时。
- 当您在 **仓库 > 标签** 的 UI 中添加 Git 标签时。

**额外细节**：

- 除 [trigger](#trigger) 作业外，所有发布作业都必须包含 `script` 关键字。发布作业可以使用脚本命令的输出。如果不需要脚本，可以使用占位符：

  ```yaml
  script:
    - echo 'release job'
  ```

  <!--An [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/223856) exists to remove this requirement.-->

- `release` 部分在 `script` 关键字之后和 `after_script` 之前执行。
- 仅当作业的主脚本成功时才会创建发布。
- 如果发布已经存在，则不会更新并且带有 `release` 关键字的作业失败。
- 如果你使用 Shell executor<!--[Shell executor](https://docs.gitlab.com/runner/executors/shell.html)--> 或在注册 runner 的服务器上安装 `release-cli`<!--[安装 `release-cli`](../../user/project/releases/release_cli.md)-->。

<!--
**Related topics**:

- [CI/CD example of the `release` keyword](../../user/project/releases/index.md#cicd-example-of-the-release-keyword).
- [Create multiple releases in a single pipeline](../../user/project/releases/index.md#create-multiple-releases-in-a-single-pipeline).
- [Use a custom SSL CA certificate authority](../../user/project/releases/index.md#use-a-custom-ssl-ca-certificate-authority).
-->

#### `release:tag_name`

必需的。发布的 Git 标签。

如果项目中尚不存在该标签，则在发布的同时创建该标签。
新标签使用与流水线关联的 SHA。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：标签名称。可以使用 CI/CD 流水线<!--[CI/CD 变量](../variables/index.md)-->。

**`release:tag_name` 示例**：

在项目中添加新标签时创建发布：

- 使用 `$CI_COMMIT_TAG` CI/CD 变量作为 `tag_name`。
- 使用 [`rules:if`](#rulesif) 或 [`only: tags`](#onlyrefs--exceptrefs) 将作业配置为仅针对新标签运行。

```yaml
job:
  script: echo 'Running the release job for the new tag.'
  release:
    tag_name: $CI_COMMIT_TAG
    description: 'Release description'
  rules:
    - if: $CI_COMMIT_TAG
```

要同时创建发布和新标签，您的 [`rules`](#rules) 或 [`only`](#only--except) 应该**不**应将作业配置为仅针对新标签运行。语义版本控制示例：

```yaml
job:
  script: echo 'Running the release job and creating a new tag.'
  release:
    tag_name: ${MAJOR}_${MINOR}_${REVISION}
    description: 'Release description'
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
```

#### `release:name`

版本名称。如果省略，则使用 `release: tag_name` 的值填充。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：一个文本字符串。

**`release:name` 示例**：

```yaml
  release_job:
    stage: release
    release:
      name: 'Release $CI_COMMIT_TAG'
```

#### `release:description`

发布的详细说明。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：

- 带有详细描述的字符串。
- 包含描述的文件的路径。在 13.7 中引入。
   - 文件位置必须相对于项目目录 (`$CI_PROJECT_DIR`)。
   - 如果文件是一个符号链接，它必须在 `$CI_PROJECT_DIR` 中。
   - `./path/to/file` 和文件名不能包含空格。

**`release:description` 示例**：

```yaml
job:
  release:
    tag_name: ${MAJOR}_${MINOR}_${REVISION}
    description: './path/to/CHANGELOG.md'
```

#### `release:ref`

如果 `release: tag_name` 还不存在，作为发布的 `ref`。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：

- 提交 SHA、另一个标签名称或分支名称。

#### `release:milestones`

与发布相关的每个里程碑的标题。

#### `release:released_at`

发布准备就绪的日期和时间。

**可能的输入**：

- 用引号括起来并以 ISO 8601 格式表示的日期。

**`release:released_at` 示例**：

```yaml
released_at: '2021-03-15T08:00:00Z'
```

**额外细节**：

- 如果未定义，则使用当前日期和时间。

#### `release:assets:links`

> 引入于 13.12 版本。

使用 `release:assets:links` 在发布中包含 [assets 链接](../../user/project/releases/index.md#发布-assets)。

需要 `release-cli` 版本 v0.4.0 或更高版本。

**`release:assets:links` 示例**：

```yaml
assets:
  links:
    - name: 'asset1'
      url: 'https://example.com/assets/1'
    - name: 'asset2'
      url: 'https://example.com/assets/2'
      filepath: '/pretty/url/1' # optional
      link_type: 'other' # optional
```

### `secrets` **(PREMIUM)**

> 引入于 13.4 版本。

使用 `secrets` 将 [CI/CD secret](../secrets/index.md) 指定为：

- 从外部 secret 提供商处检索。
- 在作业中作为 [CI/CD 变量](../variables/index.md)（[`file` 类型](../variables/index.md#cicd-variable-types) 默认情况下提供）。

此关键字必须与 `secrets:vault` 一起使用。

#### `secrets:vault`

> - 引入于 13.4 版本和 GitLab Runner 13.4。

使用 `secrets:vault` 指定由 [HashiCorp Vault](https://www.vaultproject.io/) 提供的 secret。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：

- `engine:name`：secret engine 的名称。
- `engine:path`：secret engine 的路径。
- `path`：secret 的路径。
- `field`：存储密码的字段的名称。

**`secrets:vault` 示例**：

要明确指定所有详细信息并使用 [KV-V2](https://www.vaultproject.io/docs/secrets/kv/kv-v2) secret engine：

```yaml
job:
  secrets:
    DATABASE_PASSWORD:  # Store the path to the secret in this CI/CD variable
      vault:  # Translates to secret: `ops/data/production/db`, field: `password`
        engine:
          name: kv-v2
          path: ops
        path: production/db
        field: password
```

您可以缩短此语法。 使用简短的语法，`engine:name` 和 `engine:path` 都默认为 `kv-v2`：

```yaml
job:
  secrets:
    DATABASE_PASSWORD:  # Store the path to the secret in this CI/CD variable
      vault: production/db/password  # Translates to secret: `kv-v2/data/production/db`, field: `password`
```

要以简短的语法指定自定义 secret engine 路径，请添加以 `@` 开头的后缀：

```yaml
job:
  secrets:
    DATABASE_PASSWORD:  # Store the path to the secret in this CI/CD variable
      vault: production/db/password@ops  # Translates to secret: `ops/data/production/db`, field: `password`
```

#### `secrets:file`

> 引入于 14.1 版本和 GitLab Runner 14.1.

使用 `secrets:file` 配置 secret 存储为 `file` 或 `variable` 类型 CI/CD 变量<!--[`file` 或 `variable` 类型 CI/CD 变量](../variables/index.md#cicd-variable-types)-->。

默认情况下，secret 作为 `file` 类型的 CI/CD 变量传递给作业。Secret 的值存储在文件中，变量包含文件的路径。

如果您的软件不能使用 `file` 类型的 CI/CD 变量，设置 `file: false` 将 secret 值直接存储在变量中。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：`true`（默认）或 `false`。

**`secrets:file` 示例**：

```yaml
job:
  secrets:
    DATABASE_PASSWORD:
      vault: production/db/password@ops
      file: false
```

**额外细节**：

- `file` 关键字是 CI/CD 变量的设置，必须嵌套在 CI/CD 变量名称下，而不是在 `vault` 部分。

### `pages`

使用 `pages` 定义一个 GitLab Pages<!--[GitLab Pages](../../user/project/pages/index.md)--> 作业，将静态内容上传到极狐GitLab，然后将内容发布为网站。

**关键字类型**：作业名称。

**`pages` 示例**：

```yaml
pages:
  stage: deploy
  script:
    - mkdir .public
    - cp -r * .public
    - mv .public public
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

此示例将所有文件从项目的根目录移动到 `public/` 目录。
`.public` 的解决方法是，`cp` 不会在无限循环中将 `public/` 复制到自身。

**额外细节**：

你必须：

- 将任何静态内容放在 `public/` 目录中。
- 定义 [`artifacts`](#artifacts) 和 `public/` 目录的路径。

### `inherit`

> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/207484) in GitLab 12.9.

Use `inherit:` to [control inheritance of globally-defined defaults and variables](../jobs/index.md#control-the-inheritance-of-default-keywords-and-global-variables).

#### `inherit:default`

使用`inherit:default`来控制[默认关键字](#default)的继承。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：

- `true`（默认）或 `false` 启用或禁用所有默认关键字的继承。
- 要继承的特定默认关键字列表。

**`inherit:default` 示例：**

```yaml
default:
  retry: 2
  image: ruby:3.0
  interruptible: true

job1:
  script: echo "This job does not inherit any default keywords."
  inherit:
    default: false

job2:
  script: echo "This job inherits only the two listed default keywords. It does not inherit 'interruptible'."
  inherit:
    default:
      - retry
      - image
```

**额外细节：**

- 您还可以在一行中列出要继承的默认关键字：`default: [keyword1, keyword2]`。

#### `inherit:variables`

使用`inherit:variables`来控制[全局变量](#variables)关键字的继承。

**关键字类型**：作业关键字。您只能将其用作作业的一部分。

**可能的输入**：

- `true`（默认）或 `false` 来启用或禁用所有全局变量的继承。
- 要继承的特定变量的列表。

**`inherit:variables` 示例：**

```yaml
variables:
  VARIABLE1: "This is variable 1"
  VARIABLE2: "This is variable 2"
  VARIABLE3: "This is variable 3"

job1:
  script: echo "This job does not inherit any global variables."
  inherit:
    variables: false

job2:
  script: echo "This job inherits only the two listed global variables. It does not inherit 'VARIABLE3'."
  inherit:
    variables:
      - VARIABLE1
      - VARIABLE2
```

**额外细节：**

- 您还可以在一行中列出要继承的全局变量：`variables: [VARIABLE1, VARIABLE2]`

## `variables`

<!--[CI/CD 变量](../variables/index.md)-->CI/CD 变量是传递给作业的可配置值。
使用 `variables` 创建自定义变量<!--[自定义变量](../variables/index.md#custom-cicd-variables)-->。

变量在 `script`、`before_script` 和 `after_script` 命令中始终可用。
您还可以在某些作业关键字中使用变量作为输入。

**关键字类型**：全局和工作关键字。您可以在全局级别使用它，也可以在作业级别使用它。

如果您在全局级别定义 `variables`，则在创建流水线时每个变量都会被复制到每个作业配置中。如果作业已经定义了该变量，则作业级变量优先<!--[作业级变量优先](../variables/index.md#cicd-variable-precedence)-->。

**可能的输入**：变量名和值对：

- 名称只能使用数字、字母和下划线 (`_`)。
- 值必须是字符串。

**`variables` 示例：**

```yaml
variables:
  DEPLOY_SITE: "https://example.com/"

deploy_job:
  stage: deploy
  script:
    - deploy-script --url $DEPLOY_SITE --path "/"

deploy_review_job:
  stage: deploy
  variables:
    REVIEW_PATH: "/review"
  script:
    - deploy-review-script --url $DEPLOY_SITE --path $REVIEW_PATH
```

**额外细节**：

- 所有 YAML 定义的变量也设置为任何链接的 Docker 服务容器<!--[Docker 服务容器](../services/index.md)-->。
- YAML 定义的变量用于非敏感项目配置。将敏感信息存储在受保护变量<!--[受保护变量](../variables/index.md#protect-a-cicd-variable)-->或 CI/CD secrets<!--[CI/CD secrets](../secrets/index.md)--> 中。

<!--
**Related topics**:

- You can use [YAML anchors for variables](yaml_optimization.md#yaml-anchors-for-variables).
- [Predefined variables](../variables/predefined_variables.md) are variables the runner
  automatically creates and makes available in the job.
- You can [configure runner behavior with variables](../runners/configure_runners.md#configure-runner-behavior-with-variables).
-->

### `variables:description`

> 引入于 13.7 版本。

当手动运行流水线时，使用 `description` 关键字定义预填的流水线级别（全局）变量<!--[预填的流水线级别（全局）变量](../pipelines/index.md#prefill-variables-in-manual-pipelines)-->。

必须与 `value` 一起使用，用于变量值。

**关键字类型**：全局关键字。 当您手动运行流水线时，您无法将作业级变量设置为预填充。

**可能的输入**：一个字符串。

**`variables:description` 示例**：

```yaml
variables:
  DEPLOY_ENVIRONMENT:
    value: "staging"
    description: "The deployment target. Change this variable to 'canary' or 'production' if needed."
```

## 废弃的关键字

以下关键字已弃用。

### 全局定义的 `types`

WARNING:
`types` 已弃用，可能会在未来版本中删除。
使用 [`stages`](#stages) 代替。

### 作业定义的 `type`

WARNING:
`type` 已弃用，可能会在未来版本之一中删除。
使用 [`stage`](#stage) 代替。

### 全局定义的 `image`, `services`, `cache`, `before_script`, `after_script`

不推荐在全局范围内定义 `image`、`services`、`cache`、`before_script` 和 `after_script`。可能会从未来的版本中删除支持。

使用 [`default:`](#default) 代替。 例如：

```yaml
default:
  image: ruby:3.0
  services:
    - docker:dind
  cache:
    paths: [vendor/]
  before_script:
    - bundle config set path vendor/bundle
    - bundle install
  after_script:
    - rm -rf tmp/
```

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
