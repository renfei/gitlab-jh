---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: howto
---

# 如何启用或禁用 GitLab CI/CD **(FREE)**

要使用 GitLab CI/CD，您需要：

- 项目根目录中存在有效的 [`.gitlab-ci.yml`](yaml/index.md) 文件。
- 准备运行作业的 [runner](runners/index.md)。

您可以阅读我们的[快速入门指南](quick_start/index.md)以开始使用。

如果您使用外部 CI/CD 服务器，如 Jenkins 或 Drone CI，您可以禁用 GitLab CI/CD 以避免与提交状态 API 发生冲突。

GitLab CI/CD 在所有新项目中默认启用。 你可以：

- [在每个项目的设置下](#在项目中启用-cicd)禁用 GitLab CI/CD 。
- 将 GitLab CI/CD 设置为在实例上的所有新项目中禁用<!--[在实例上的所有新项目中禁用](../administration/cicd.md)-->。

如果您在项目中禁用 GitLab CI/CD：

- 删除了左侧边栏中的 **CI/CD** 项目。
- `/pipelines` 和 `/jobs` 页面不再可用。
- 不会删除现有作业和流水线。重新启用 CI/CD 可以再次访问它们。

项目或实例设置不会启用或禁用在外部集成<!--[外部集成](../user/project/integrations/overview.md#integrations-listing)-->中运行的流水线。

## 在项目中启用 CI/CD

要在项目中启用或禁用 GitLab CI/CD 流水线：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 展开 **可见性、项目功能、权限**。
1. 在 **仓库** 部分，根据需要打开或关闭 **CI/CD**。

**项目可见性** 也会影响流水线可见性。如果设置为：

- **私有**：只有项目成员可以访问流水线。
- **内部** 或 **公共**：可以使用下拉框将流水线设置为 **仅项目成员** 或 **具有访问权限的任何人**。

按 **保存更改** 以使设置生效。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
