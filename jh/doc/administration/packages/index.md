---
stage: Package
group: Package
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# GitLab 软件包仓库管理**(FREE SELF)**

GitLab Packages 允许组织将极狐GitLab 用作各种常见软件包管理器的私有仓库。用户可以构建和发布软件包，这些软件包可以很容易地作为下游项目的依赖项使用。

软件包功能允许 GitLab 充当以下包类型的仓库，支持以下格式：

支持以下格式：

<div class="row">
<div class="col-md-9">
<table align="left" style="width:50%">
<tr style="background:#dfdfdf"><th>Package type</th><th>GitLab version</th></tr>
<tr><td><a href="https://docs.gitlab.cn/jh/user/packages/composer_repository/index.html">Composer</a></td><td>13.2+</td></tr>
<tr><td><a href="https://docs.gitlab.cn/jh/user/packages/conan_repository/index.html">Conan</a></td><td>12.6+</td></tr>
<tr><td><a href="https://docs.gitlab.cn/jh/user/packages/go_proxy/index.html">Go</a></td><td>13.1+</td></tr>
<tr><td><a href="https://docs.gitlab.cn/jh/user/packages/maven_repository/index.html">Maven</a></td><td>11.3+</td></tr>
<tr><td><a href="https://docs.gitlab.cn/jh/user/packages/npm_registry/index.html">npm</a></td><td>11.7+</td></tr>
<tr><td><a href="https://docs.gitlab.cn/jh/user/packages/nuget_repository/index.html">NuGet</a></td><td>12.8+</td></tr>
<tr><td><a href="https://docs.gitlab.cn/jh/user/packages/pypi_repository/index.html">PyPI</a></td><td>12.10+</td></tr>
<tr><td><a href="https://docs.gitlab.cn/jh/user/packages/generic_packages/index.html">Generic packages</a></td><td>13.5+</td></tr>
<tr><td><a href="https://docs.gitlab.cn/jh/user/packages/helm_repository/index.html">Helm Charts</a></td><td>14.1+</td></tr>
</table>
</div>
</div>

<!--
## Accepting contributions

The below table lists formats that are not supported, but are accepting Community contributions for. Consider contributing to GitLab. This [development documentation](../../development/packages.md)
guides you through the process.
-->

<!-- vale gitlab.Spelling = NO -->

<!--
| Format | Status |
| ------ | ------ |
| Chef      | [#36889](https://gitlab.com/gitlab-org/gitlab/-/issues/36889) |
| CocoaPods | [#36890](https://gitlab.com/gitlab-org/gitlab/-/issues/36890) |
| Conda     | [#36891](https://gitlab.com/gitlab-org/gitlab/-/issues/36891) |
| CRAN      | [#36892](https://gitlab.com/gitlab-org/gitlab/-/issues/36892) |
| Debian    | [Draft: Merge Request](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/50438) |
| Opkg      | [#36894](https://gitlab.com/gitlab-org/gitlab/-/issues/36894) |
| P2        | [#36895](https://gitlab.com/gitlab-org/gitlab/-/issues/36895) |
| Puppet    | [#36897](https://gitlab.com/gitlab-org/gitlab/-/issues/36897) |
| RPM       | [#5932](https://gitlab.com/gitlab-org/gitlab/-/issues/5932) |
| RubyGems  | [#803](https://gitlab.com/gitlab-org/gitlab/-/issues/803) |
| SBT       | [#36898](https://gitlab.com/gitlab-org/gitlab/-/issues/36898) |
| Terraform | [Draft: Merge Request](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/18834) |
| Vagrant   | [#36899](https://gitlab.com/gitlab-org/gitlab/-/issues/36899) |
-->

<!-- vale gitlab.Spelling = YES -->

## 启用软件包特性

NOTE:
启用软件包功能后，默认情况下，所有新项目都可以使用仓库。要为现有项目启用软件包特性，用户可以在项目设置中进行明确设置。

要启用包功能：

**Omnibus 包装**

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下内容：

   ```ruby
   gitlab_rails['packages_enabled'] = true
   ```

1. 保存文件并[重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure) 以使更改生效。

**源安装**

1. 安装完成后，在 `config/gitlab.yml` 中配置 `packages` 部分。设置为 `true` 以启用它：

   ```yaml
   packages:
     enabled: true
   ```

1. [重新启动极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure) 以使更改生效。

**Helm Chart 安装**

1. 安装完成后，在 `global.appConfig.packages` 中配置 `packages` 部分。 设置为 `true` 以启用它：

   ```yaml
   packages:
     enabled: true
   ```

1. [重新启动极狐GitLab](../restart_gitlab.md#helm-chart-安装实例) 以使更改生效。

## 速率限制

在下游项目中，将软件包作为依赖项下载时，许多请求是通过包 API 发出的。因此，您可能会达到强制的用户和 IP 速率限制。要解决此问题，您可以为 Packages API 定义特定的速率限制。<!--更多详细信息，请参见[软件包仓库速率限制](../../user/admin_area/settings/package_registry_rate_limits.md)。-->

## 更改存储路径

默认情况下，软件包存储在本地，但您可以更改默认本地位置，甚至使用对象存储。

### 更改本地存储路径

Omnibus 安装实例的软件包存储在 `/var/opt/gitlab/gitlab-rails/shared/packages/` 下，源安装实例的软件包存储在 `shared/packages/` 下（相对于 Git 主目录）。

更改本地存储路径：

**Omnibus 安装**

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下内容：

   ```ruby
   gitlab_rails['packages_storage_path'] = "/mnt/packages"
   ```

1. 保存文件并[重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure) 以使更改生效。

**源安装**

1. 在 `config/gitlab.yml` 中编辑 `packages` 区域：

   ```yaml
   packages:
     enabled: true
     storage_path: shared/packages
   ```

1. 保存文件并[重新启动 GitLab](../restart_gitlab.md#源安装实例) 以使更改生效。

### 使用对象存储

您可以使用对象存储来存储软件包，而不是依赖于本地存储。

<!--[阅读有关在极狐GitLab 中使用对象存储的更多信息](../object_storage.md)。

NOTE:
我们建议使用 [整合对象存储设置](../object_storage.md#consolidated-object-storage-configuration)。以下说明适用于原始配置格式。
-->

**Omnibus 安装**

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下行（必要时取消注释）：

   ```ruby
   gitlab_rails['packages_enabled'] = true
   gitlab_rails['packages_object_store_enabled'] = true
   gitlab_rails['packages_object_store_remote_directory'] = "packages" # The bucket name.
   gitlab_rails['packages_object_store_direct_upload'] = false         # Use Object Storage directly for uploads instead of background uploads if enabled (Default: false).
   gitlab_rails['packages_object_store_background_upload'] = true      # Temporary option to limit automatic upload (Default: true).
   gitlab_rails['packages_object_store_proxy_download'] = false        # Passthrough all downloads via GitLab instead of using Redirects to Object Storage.
   gitlab_rails['packages_object_store_connection'] = {
     ##
     ## If the provider is AWS S3, uncomment the following
     ##
     #'provider' => 'AWS',
     #'region' => 'eu-west-1',
     #'aws_access_key_id' => 'AWS_ACCESS_KEY_ID',
     #'aws_secret_access_key' => 'AWS_SECRET_ACCESS_KEY',
     ## If an IAM profile is being used with AWS, omit the aws_access_key_id and aws_secret_access_key and uncomment
     #'use_iam_profile' => true,
     ##
     ## If the provider is other than AWS (an S3-compatible one), uncomment the following
     ##
     #'host' => 's3.amazonaws.com',
     #'aws_signature_version' => 4             # For creation of signed URLs. Set to 2 if provider does not support v4.
     #'endpoint' => 'https://s3.amazonaws.com' # Useful for S3-compliant services such as DigitalOcean Spaces.
     #'path_style' => false                    # If true, use 'host/bucket_name/object' instead of 'bucket_name.host/object'.
   }
   ```

1. 保存文件并[重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure) 以使更改生效。

**源安装**

1. 编辑 `config/gitlab.yml` 中的 `packages` 部分（必要时取消注释）：

   ```yaml
   packages:
     enabled: true
     ##
     ## The location where build packages are stored (default: shared/packages).
     ##
     # storage_path: shared/packages
     object_store:
       enabled: false
       remote_directory: packages  # The bucket name.
       # direct_upload: false      # Use Object Storage directly for uploads instead of background uploads if enabled (Default: false).
       # background_upload: true   # Temporary option to limit automatic upload (Default: true).
       # proxy_download: false     # Passthrough all downloads via GitLab instead of using Redirects to Object Storage.
       connection:
       ##
       ## If the provider is AWS S3, use the following:
       ##
         provider: AWS
         region: us-east-1
         aws_access_key_id: AWS_ACCESS_KEY_ID
         aws_secret_access_key: AWS_SECRET_ACCESS_KEY
         ##
         ## If the provider is other than AWS (an S3-compatible one), comment out the previous 4 lines and use the following instead:
         ##
         #  host: 's3.amazonaws.com'             # default: s3.amazonaws.com.
         #  aws_signature_version: 4             # For creation of signed URLs. Set to 2 if provider does not support v4.
         #  endpoint: 'https://s3.amazonaws.com' # Useful for S3-compliant services such as DigitalOcean Spaces.
         #  path_style: false                    # If true, use 'host/bucket_name/object' instead of 'bucket_name.host/object'.
   ```

1. 保存文件并[重启极狐GitLab](../restart_gitlab.md#源安装实例) 以使更改生效。

### 将本地软件包迁移到对象存储

[配置对象存储](#使用对象存储)后，您可以使用以下任务将现有软件包从本地存储迁移到远程存储。
进程在后台 worker 中完成，**不需要停机时间**。

在 Omnibus 安装实例中：

```shell
sudo gitlab-rake "gitlab:packages:migrate"
```

在源安装实例中：

```shell
RAILS_ENV=production sudo -u git -H bundle exec rake gitlab:packages:migrate
```

您可以选择跟踪进度并验证所有包是否已成功迁移。

从 [PostgreSQL 控制台](https://docs.gitlab.cn/omnibus/settings/database.html#连接到捆绑的-postgresql-数据库)（Omnibus GitLab 使用 `sudo gitlab-psql -d gitlabhq_production`），验证下面的`objectstg`（其中`file_store=2`）具有所有包的数量：

```shell
gitlabhq_production=# SELECT count(*) AS total, sum(case when file_store = '1' then 1 else 0 end) AS filesystem, sum(case when file_store = '2' then 1 else 0 end) AS objectstg FROM packages_package_files;

total | filesystem | objectstg
------+------------+-----------
 34   |          0 |        34
```
