---
type: reference
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 通用 LDAP 设置 **(FREE SELF)**

<!--
GitLab integrates with [LDAP](https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol)
to support user authentication.

This integration works with most LDAP-compliant directory servers, including:

- Microsoft Active Directory
  - [Microsoft Active Directory Trusts](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-R2-and-2008/cc771568(v=ws.10)) are not supported.
- Apple Open Directory
- Open LDAP
- 389 Server

Users added through LDAP take a [licensed seat](../../../subscriptions/self_managed/index.md#billable-users).

GitLab Enterprise Editions (EE) include enhanced integration,
including group membership syncing and multiple LDAP server support.
-->

极狐GitLab 与 LDAP 集成，以支持用户身份验证。

此集成适用于大多数 LDAP 兼容目录服务器，包括：

- Microsoft Active Directory。
  不支持 [Microsoft Active Directory Trusts](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-R2-and-2008/cc771568(v=ws.10))。
- Apple Open Directory。
- Open LDAP。
- 389 Server。

通过 LDAP 添加的用户：

- 获得许可席位<!--[许可席位](../../../subscriptions/self_managed/index.md#billable-users)-->。
- 可以使用他们的极狐GitLab 用户名或电子邮件和 LDAP 密码，对 Git 进行身份验证，即使 Git 的密码身份验证已禁用<!-- [已禁用](../../../user/admin_area/settings/sign_in_restrictions.md#password-authentication-enabled)-->。

在以下情况下，LDAP DN 与现有的极狐GitLab 用户相关联：

- 现有用户首次使用 LDAP 登录极狐GitLab。
- LDAP 电子邮件地址是现有极狐GitLab 用户的主要电子邮件地址。如果在极狐GitLab 用户数据库中找不到 LDAP 电子邮件属性，则会创建一个新用户。

如果现有的极狐GitLab 用户想要为自己启用 LDAP 登录，他们应该：

1. 检查他们的极狐GitLab 电子邮件地址是否与他们的 LDAP 电子邮件地址匹配。
1. 使用他们的 LDAP 凭据登录极狐GitLab。

## 安全

极狐GitLab 有多种机制来验证用户在 LDAP 中是否仍然处于活动状态。如果用户在 LDAP 中不再处于活动状态，他们将处于 `ldap_blocked` 状态并被注销。在 LDAP 中重新激活之前，他们无法使用任何身份验证提供程序登录。

用户在以下情况下被视为在 LDAP 中处于非活动状态：

- 从目录中完全删除。
- 驻留在配置的 `base` DN 或 `user_filter` 搜索之外。
- 通过用户帐户控制属性在 Active Directory 中标记为禁用或停用。这意味着属性 `userAccountControl:1.2.840.113556.1.4.803` 设置了第 2 位。

出现以下情况，检查所有 LDAP 用户的状态：

- 使用任何身份验证提供程序登录时。在 14.4 及更早版本中，仅在直接使用 LDAP 登录时检查状态。
- 每小时一次，用于使用令牌或 SSH 密钥的活动 Web 会话或 Git 请求。
- 使用 LDAP 用户名和密码通过 HTTP 请求执行 Git 时。
- <!--[用户同步](ldap_synchronization.md#user-sync)-->用户同步期间每天一次。

### 安全风险

您应该只使用 LDAP 集成，如果您的 LDAP 用户不能：

- 更改他们在 LDAP 服务器上的 `mail`、`email` 或 `userPrincipalName` 属性。这些用户可能会接管您的极狐GitLab 服务器上的任何帐户。
- 共享电子邮件地址。具有相同电子邮件地址的 LDAP 用户可以共享相同的极狐GitLab 帐户。

## 配置

<!--
To enable LDAP integration you need to add your LDAP server settings in
`/etc/gitlab/gitlab.rb` or `/home/git/gitlab/config/gitlab.yml` for Omnibus
GitLab and installations from source respectively.

There is a Rake task to check LDAP configuration. After configuring LDAP
using the documentation below, see [LDAP check Rake task](../../raketasks/check.md#ldap-check)
for information on the LDAP check Rake task.

NOTE:
The `encryption` value `simple_tls` corresponds to 'Simple TLS' in the LDAP
library. `start_tls` corresponds to StartTLS, not to be confused with regular TLS.
Normally, if you specify `simple_tls` it is on port 636, while `start_tls` (StartTLS)
would be on port 389. `plain` also operates on port 389. Removed values: `tls` was replaced
with `start_tls` and `ssl` was replaced with `simple_tls`.

LDAP users must have a set email address, regardless of whether or not it's used
to sign in.
-->

要启用 LDAP 集成，当使用 Omnibus GitLab 时，您必须在 `/etc/gitlab/gitlab.rb` 中添加 LDAP 服务器设置；当使用源安装时，您需要在 `/home/git/gitlab/config/gitlab.yml` 中添加 LDAP 服务器设置。

有一个 Rake 任务可用于检查 LDAP 配置。使用下方文档配置 LDAP 后，请参阅 [LDAP 检查 Rake 任务](../../raketasks/check.md#ldap-检查)获取更多信息。

NOTE:
`encryption` 的值 `simple_tls` 对应于 LDAP 库中的“Simple TLS”。`start_tls` 对应于 StartTLS，不要与常规 TLS 混淆。通常，如果您指定 `simple_tls` 在端口 636 上，而 `start_tls`（StartTLS）将在端口 389 上。`plain` 也在端口 389 上运行。删除的值：`tls` 被替换为 `start_tls`，`ssl` 被替换为 `simple_tls`。

LDAP 用户必须设置一个电子邮件地址，无论是否用于登录。

### 配置案例

**Omnibus 配置**

```ruby
gitlab_rails['ldap_enabled'] = true
gitlab_rails['prevent_ldap_sign_in'] = false
gitlab_rails['ldap_servers'] = {
'main' => {
  'label' => 'LDAP',
  'host' =>  'ldap.mydomain.com',
  'port' => 389,
  'uid' => 'sAMAccountName',
  'encryption' => 'simple_tls',
  'verify_certificates' => true,
  'bind_dn' => '_the_full_dn_of_the_user_you_will_bind_with',
  'password' => '_the_password_of_the_bind_user',
  'tls_options' => {
    'ca_file' => '',
    'ssl_version' => '',
    'ciphers' => '',
    'cert' => '',
    'key' => ''
  },
  'timeout' => 10,
  'active_directory' => true,
  'allow_username_or_email_login' => false,
  'block_auto_created_users' => false,
  'base' => 'dc=example,dc=com',
  'user_filter' => '',
  'attributes' => {
    'username' => ['uid', 'userid', 'sAMAccountName'],
    'email' => ['mail', 'email', 'userPrincipalName'],
    'name' => 'cn',
    'first_name' => 'givenName',
    'last_name' => 'sn'
  },
  'lowercase_usernames' => false,

  # EE Only
  'group_base' => '',
  'admin_group' => '',
  'external_groups' => [],
  'sync_ssh_keys' => false
  }
}
```

**源安装配置**

```yaml
production:
  # snip...
  ldap:
    enabled: false
    prevent_ldap_sign_in: false
    servers:
      main:
        label: 'LDAP'
        ...
```

### 基本配置

<!--
| Setting            | Description | Required | Examples |
|--------------------|-------------|----------|----------|
| `label`            | A human-friendly name for your LDAP server. It is displayed on your sign-in page. | **{check-circle}** Yes | `'Paris'` or `'Acme, Ltd.'` |
| `host`             | IP address or domain name of your LDAP server. | **{check-circle}** Yes | `'ldap.mydomain.com'` |
| `port`             | The port to connect with on your LDAP server. Always an integer, not a string. | **{check-circle}** Yes | `389` or `636` (for SSL) |
| `uid`              | LDAP attribute for username. Should be the attribute, not the value that maps to the `uid`. | **{check-circle}** Yes | `'sAMAccountName'` or `'uid'` or `'userPrincipalName'` |
| `bind_dn`          | The full DN of the user you bind with. | **{dotted-circle}** No | `'america\momo'` or `'CN=Gitlab,OU=Users,DC=domain,DC=com'` |
| `password`         | The password of the bind user. | **{dotted-circle}** No | `'your_great_password'` |
| `encryption`       | Encryption method. The `method` key is deprecated in favor of `encryption`. | **{check-circle}** Yes | `'start_tls'` or `'simple_tls'` or `'plain'` |
| `verify_certificates` | Enables SSL certificate verification if encryption method is `start_tls` or `simple_tls`. If set to false, no validation of the LDAP server's SSL certificate is performed. Defaults to true. | **{dotted-circle}** No | boolean |
| `timeout`          | Set a timeout, in seconds, for LDAP queries. This helps avoid blocking a request if the LDAP server becomes unresponsive. A value of `0` means there is no timeout. (default: `10`) | **{dotted-circle}** No | `10` or `30` |
| `active_directory` | This setting specifies if LDAP server is Active Directory LDAP server. For non-AD servers it skips the AD specific queries. If your LDAP server is not AD, set this to false. | **{dotted-circle}** No | boolean |
| `allow_username_or_email_login` | If enabled, GitLab ignores everything after the first `@` in the LDAP username submitted by the user on sign-in. If you are using `uid: 'userPrincipalName'` on ActiveDirectory you need to disable this setting, because the userPrincipalName contains an `@`. | **{dotted-circle}** No | boolean |
| `block_auto_created_users` | To maintain tight control over the number of billable users on your GitLab installation, enable this setting to keep new users blocked until they have been cleared by an administrator (default: false). | **{dotted-circle}** No | boolean |
| `base` | Base where we can search for users. | **{check-circle}** Yes | `'ou=people,dc=gitlab,dc=example'` or `'DC=mydomain,DC=com'` |
| `user_filter`      | Filter LDAP users. Format: [RFC 4515](https://tools.ietf.org/search/rfc4515) Note: GitLab does not support `omniauth-ldap`'s custom filter syntax. | **{dotted-circle}** No | For examples, read [Examples of user filters](#examples-of-user-filters). |
| `lowercase_usernames` | If enabled, GitLab converts the name to lower case. | **{dotted-circle}** No | boolean |
-->

| 配置项           | 说明 | 是否必须配置 | 举例 |
|--------------------|-------------|----------|----------|
| `label`            | LDAP 服务器的人性化名称。在登录页面展示。 | **{check-circle}** Yes | `'Paris'` or `'Acme, Ltd.'` |
| `host`             | LDAP 服务器的 IP 地址或域名。 | **{check-circle}** Yes | `'ldap.mydomain.com'` |
| `port`             | 与 LDAP 服务器连接的端口。类型为整数而非字符串。 | **{check-circle}** Yes | `389` 或 `636` (用于 SSL) |
| `uid`              | 用户名的 LDAP 属性。应该是属性，而不是映射到 `uid` 的值。 | **{check-circle}** Yes | `'sAMAccountName'`、`'uid'` 或 `'userPrincipalName'` |
| `bind_dn`          | 绑定用户的完整 DN。 | **{dotted-circle}** No | `'america\momo'` 或 `'CN=Gitlab,OU=Users,DC=domain,DC=com'` |
| `password`         | 绑定用户的密码。 | **{dotted-circle}** No | `'your_great_password'` |
| `encryption`       | 加密方法。`method` 已废弃。 | **{check-circle}** Yes | `'start_tls'`、`'simple_tls'` 或 `'plain'` |
| `verify_certificates` | 如果加密方法为 `start_tls` 或 `simple_tls`，则启用 SSL 证书验证。如果设置为 false，则不会对 LDAP 服务器的 SSL 证书进行验证。默认为 true。 | **{dotted-circle}** No | boolean |
| `timeout`          | 为 LDAP 查询设置超时时间（以秒为单位）。当 LDAP 服务器无法响应时，有助于阻止请求。`0` 值表示没有超时（默认值：`10`） | **{dotted-circle}** No | `10` 或 `30` |
| `active_directory` | 该设置指定 LDAP 服务器是否为 Active Directory LDAP 服务器。对于非 AD 服务器，会跳过 AD 特定查询。如果您的 LDAP 服务器不是 AD，请将其设置为 false。 | **{dotted-circle}** No | boolean |
| `allow_username_or_email_login` | 如果启用，系统将忽略用户在登录时提交的 LDAP 用户名中第一个 `@` 之后的所有内容。如果您在 ActiveDirectory 上使用 `uid: 'userPrincipalName'`，您必须禁用这个设置，因为 userPrincipalName 包含一个 `@`。 | **{dotted-circle}** No | boolean |
| `block_auto_created_users` | 为了严格控制计费用户数量，请启用此设置阻止新用户直到他们被管理员清除（默认值：false）。 | **{dotted-circle}** No | boolean |
| `base` | 搜索用户的基础路径。 | **{check-circle}** Yes | `'ou=people,dc=gitlab,dc=example'` or `'DC=mydomain,DC=com'` |
| `user_filter`      | 过滤 LDAP 用户。格式：[RFC 4515](https://tools.ietf.org/search/rfc4515) 注意： 不支持 `omniauth-ldap` 的自定义过滤器语法。 | **{dotted-circle}** No | 查阅[用户过滤器的案例](#用户过滤器样例)。 |
| `lowercase_usernames` | 如果启用，系统转换用户名称为小写。 | **{dotted-circle}** No | boolean |
| `retry_empty_result_with_codes` | 如果结果/内容为空，将尝试重试操作时的 LDAP 查询响应代码数组。 | **{dotted-circle}** No | `[80]` |

#### 用户过滤器样例

`user_filter` 字段语法的一些示例：

- `'(employeeType=developer)'`
- `'(&(objectclass=user)(|(samaccountname=momo)(samaccountname=toto)))'`

### SSL 配置

<!--
| Setting       | Description | Required | Examples |
|---------------|-------------|----------|----------|
| `ca_file`     | Specifies the path to a file containing a PEM-format CA certificate, for example, if you need to use an internal CA. | **{dotted-circle}** No | `'/etc/ca.pem'` |
| `ssl_version` | Specifies the SSL version for OpenSSL to use, if the OpenSSL default is not appropriate. | **{dotted-circle}** No | `'TLSv1_1'` |
| `ciphers`     | Specific SSL ciphers to use in communication with LDAP servers. | **{dotted-circle}** No | `'ALL:!EXPORT:!LOW:!aNULL:!eNULL:!SSLv2'` |
| `cert`        | Client certificate. | **{dotted-circle}** No | `'-----BEGIN CERTIFICATE----- <REDACTED> -----END CERTIFICATE -----'` |
| `key`         | Client private key. | **{dotted-circle}** No | `'-----BEGIN PRIVATE KEY----- <REDACTED> -----END PRIVATE KEY -----'` |
-->

| 配置项       | 说明 | 是否必须配置 | 举例 |
|---------------|-------------|----------|----------|
| `ca_file`     | 例如，当您需要使用内部 CA 时，指定 PEM 格式的 CA 证书文件的路径。 | **{dotted-circle}** No | `'/etc/ca.pem'` |
| `ssl_version` | 如果 OpenSSL 默认值不合适，指定 OpenSSL 的 SSL 版本。 | **{dotted-circle}** No | `'TLSv1_1'` |
| `ciphers`     | 用于与 LDAP 服务器通信的特定 SSL 密码。 | **{dotted-circle}** No | `'ALL:!EXPORT:!LOW:!aNULL:!eNULL:!SSLv2'` |
| `cert`        | 客户凭证。 | **{dotted-circle}** No | `'-----BEGIN CERTIFICATE----- <REDACTED> -----END CERTIFICATE -----'` |
| `key`         | 客户私钥。 | **{dotted-circle}** No | `'-----BEGIN PRIVATE KEY----- <REDACTED> -----END PRIVATE KEY -----'` |


### 属性配置

<!--
LDAP attributes that GitLab uses to create an account for the LDAP user. The specified
attribute can either be the attribute name as a string (for example, `'mail'`), or an
array of attribute names to try in order (for example, `['mail', 'email']`). Note that
the user's LDAP sign-in is the attribute specified as `uid` above.

| Setting      | Description | Required | Examples |
|--------------|-------------|----------|----------|
| `username`   | The username is used in paths for the user's own projects (like `gitlab.example.com/username/project`) and when mentioning them in issues, merge request and comments (like `@username`). If the attribute specified for `username` contains an email address, the GitLab username is part of the email address before the `@`. | **{dotted-circle}** No | `['uid', 'userid', 'sAMAccountName']` |
| `email`      | LDAP attribute for user email. | **{dotted-circle}** No | `['mail', 'email', 'userPrincipalName']` |
| `name`       | LDAP attribute for user display name. If `name` is blank, the full name is taken from the `first_name` and `last_name`. | **{dotted-circle}** No | Attributes `'cn'`, or `'displayName'` commonly carry full names. Alternatively, you can force the use of `first_name` and `last_name` by specifying an absent attribute such as `'somethingNonExistent'`. |
| `first_name` | LDAP attribute for user first name. Used when the attribute configured for `name` does not exist. | **{dotted-circle}** No | `'givenName'` |
| `last_name`  | LDAP attribute for user last name. Used when the attribute configured for `name` does not exist. | **{dotted-circle}** No | `'sn'` |
-->

系统为 LDAP 用户创建账户时，使用的 LDAP 属性。指定的属性可以是字符串形式的属性名称（例如，`'mail'`），也可以是要按顺序尝试的属性名称数组（例如，`['mail', 'email']`）。用户的 LDAP 登录时指定为 `uid` 的属性。

| 配置项       | 说明 | 是否必须配置 | 举例 |
|--------------|-------------|----------|----------|
| `username`   | 用户名用于用户自己项目的路径 (例如 `gitlab.example.com/username/project`) ，以及在议题、合并请求和评论中提及时 (例如 `@username`)。如果 `username` 包含电子邮件地址, 则地址中 `@` 之前的部分作为极狐GitLab 用户名。 | **{dotted-circle}** No | `['uid', 'userid', 'sAMAccountName']` |
| `email`      | 用户电子邮件的 LDAP 属性。 | **{dotted-circle}** No | `['mail', 'email', 'userPrincipalName']` |
| `name`       | 用户显示名称的 LDAP 属性。如果 `name` 为空, 完整的显示名称从 `first_name` 和 `last_name` 获取。 | **{dotted-circle}** No |  `'cn'` 或 `'displayName'` 属性通常带有全名。或者，您可以通过指定不存在的属性，例如  `'somethingNonExistent'`，来强制使用 `first_name` 和 `last_name`。|
| `first_name` | 用户 first name 的 LDAP 属性。当 `name` 属性不存在时使用。 | **{dotted-circle}** No | `'givenName'` |
| `last_name`  | 用户 last name 的 LDAP 属性。当 `name` 属性不存在时使用。 | **{dotted-circle}** No | `'sn'` |


### LDAP 同步配置 **(PREMIUM SELF)**

<!--
| Setting           | Description | Required | Examples |
|-------------------|-------------|----------|----------|
| `group_base`      | Base used to search for groups. | **{dotted-circle}** No | `'ou=groups,dc=gitlab,dc=example'` |
| `admin_group`     | The CN of a group containing GitLab administrators. Note: Not `cn=administrators` or the full DN. | **{dotted-circle}** No | `'administrators'` |
| `external_groups` | An array of CNs of groups containing users that should be considered external. Note: Not `cn=interns` or the full DN. | **{dotted-circle}** No | `['interns', 'contractors']` |
| `sync_ssh_keys`   | The LDAP attribute containing a user's public SSH key. | **{dotted-circle}** No | `'sshPublicKey'` or false if not set |
-->

| 配置项       | 说明 | 是否必须配置 | 举例 |
|-------------------|-------------|----------|----------|
| `group_base`      | 搜索用户组的基础路径。 | **{dotted-circle}** No | `'ou=groups,dc=gitlab,dc=example'` |
| `admin_group`     | 包含 GitLab 管理员的组的 CN。注意：不是`cn=administrators` 或完整的DN。 | **{dotted-circle}** No | `'administrators'` |
| `external_groups` | 包含应被视为外部用户的组 CN 数组。 注意：不是 `cn=interns` 或完整的 DN。 | **{dotted-circle}** No | `['interns', 'contractors']` |
| `sync_ssh_keys`   | 包含用户的公共 SSH 密钥的 LDAP 属性。 | **{dotted-circle}** No | `'sshPublicKey'`  或者 false（如果未设置）。 |

### 使用多 LDAP 服务器 **(PREMIUM SELF)**

如果您在多个 LDAP 服务器上有用户，您可以配置极狐GitLab 使用它们。要添加额外的 LDAP 服务器：

1. 复制 [`main` LDAP 配置](#配置-ldap)。
1. 使用附加服务器的详细信息编辑每个重复配置。
    - 对于每个额外的服务器，选择不同的提供者 ID，如 `main`、`secondary` 或 `tertiary`。使用小写字母数字字符。极狐GitLab 使用提供者 ID 将每个用户与特定的 LDAP 服务器相关联。
    - 对于每个条目，使用唯一的 `label` 值。这些值用于登录页面上的选项卡名称。

#### 多 LDAP 服务器示例

下面的例子展示了如何在 `gitlab.rb` 中配置三个 LDAP 服务器：

```ruby
gitlab_rails['ldap_enabled'] = true
gitlab_rails['ldap_servers'] = {
'main' => {
  'label' => 'GitLab AD',
  'host' =>  'ad.example.org',
  'port' => 636,
  ...
  },

'secondary' => {
  'label' => 'GitLab Secondary AD',
  'host' =>  'ad-secondary.example.net',
  'port' => 636,
  ...
  },

'tertiary' => {
  'label' => 'GitLab Tertiary AD',
  'host' =>  'ad-tertiary.example.net',
  'port' => 636,
  ...
  }

}
```

此示例生成以下登录页面：

![Multiple LDAP servers sign in](img/multi_login.gif)

### 设置 LDAP 用户过滤器

<!--
If you want to limit all GitLab access to a subset of the LDAP users on your
LDAP server, the first step should be to narrow the configured `base`. However,
it's sometimes necessary to further filter users. In this case, you can set
up an LDAP user filter. The filter must comply with
[RFC 4515](https://tools.ietf.org/search/rfc4515).
-->

如果您想要 LDAP 服务器上的一部分 LDAP 用户具有访问极狐GitLab 的权限，第一步应该缩小配置的 `base` 范围。但是，有时必须进一步过滤用户。在这种情况下，您可以设置 LDAP 用户过滤器。过滤器必须符合 [RFC 4515](https://tools.ietf.org/search/rfc4515)。

**Omnibus 配置**

```ruby
gitlab_rails['ldap_servers'] = {
'main' => {
  # snip...
  'user_filter' => '(employeeType=developer)'
  }
}
```

**源安装配置**

```yaml
production:
  ldap:
    servers:
      main:
        # snip...
        user_filter: '(employeeType=developer)'
```
<!--
If you want to limit access to the nested members of an Active Directory
group, use the following syntax:

```plaintext
(memberOf:1.2.840.113556.1.4.1941:=CN=My Group,DC=Example,DC=com)
```

For more information about this "LDAP_MATCHING_RULE_IN_CHAIN" filter, see the following
[Microsoft Search Filter Syntax](https://docs.microsoft.com/en-us/windows/win32/adsi/search-filter-syntax) document.
Support for nested members in the user filter shouldn't be confused with
[group sync nested groups support](#supported-ldap-group-typesattributes). **(PREMIUM SELF)**

GitLab does not support the custom filter syntax used by OmniAuth LDAP.
-->

如果要限制对 Active Directory 组的嵌套成员的访问，请使用以下语法：
```plaintext
(memberOf:1.2.840.113556.1.4.1941:=CN=My Group,DC=Example,DC=com)
```
获取 `LDAP_MATCHING_RULE_IN_CHAIN` 过滤器的更多信息，请参阅 [Microsoft 搜索过滤器语法](https://docs.microsoft.com/en-us/windows/win32/adsi/search-filter-syntax) 文档。<!--用户过滤器中对嵌套成员的支持不应与[组同步嵌套组支持](#supported-ldap-group-typesattributes) 混淆。-->**(PREMIUM SELF)**

不支持 OmniAuth LDAP 使用的自定义过滤器语法。

#### 转义特殊字符

<!--
The `user_filter` DN can contain special characters. For example:

- A comma:

  ```plaintext
  OU=GitLab, Inc,DC=gitlab,DC=com
  ```

- Open and close brackets:

  ```plaintext
  OU=Gitlab (Inc),DC=gitlab,DC=com
  ```

  These characters must be escaped as documented in
  [RFC 4515](https://tools.ietf.org/search/rfc4515).

- Escape commas with `\2C`. For example:

  ```plaintext
  OU=GitLab\2C Inc,DC=gitlab,DC=com
  ```

- Escape open and close brackets with `\28` and `\29`, respectively. For example:

  ```plaintext
  OU=Gitlab \28Inc\29,DC=gitlab,DC=com
  ```
-->

`user_filter` DN 可以包含特殊字符。 例如：

- 逗号：

  ```plaintext
  OU=GitLab, Inc,DC=gitlab,DC=com
  ```

- 括号：

  ```plaintext
  OU=Gitlab (Inc),DC=gitlab,DC=com
  ```


  这些字符必须按照 [RFC 4515](https://tools.ietf.org/search/rfc4515) 文档所述进行转义。

- 使用 `\2C` 转义逗号。例如：

  ```plaintext
  OU=GitLab\2C Inc,DC=gitlab,DC=com
  ```

- 同样地，使用 `\28` 和 `\29` 转义括号。例如：

  ```plaintext
  OU=Gitlab \28Inc\29,DC=gitlab,DC=com
  ```

### 启用 LDAP 用户名小写
<!--
Some LDAP servers, depending on their configurations, can return uppercase usernames.
This can lead to several confusing issues such as creating links or namespaces with uppercase names.

GitLab can automatically lowercase usernames provided by the LDAP server by enabling
the configuration option `lowercase_usernames`. By default, this configuration option is `false`.
-->
某些 LDAP 服务器，根据其配置可以返回大写的用户名。这可能导致一些令人困惑的问题，比如使用大写名称创建链接或命名空间。

极狐GitLab 可以通过启用 `lowercase_usernames` 配置项，自动将 LDAP 服务器提供的用户名小写。默认情况下，该配置选项为 `false`。

**Omnibus 配置**

1. 编辑 `/etc/gitlab/gitlab.rb`:

   ```ruby
   gitlab_rails['ldap_servers'] = {
   'main' => {
     # snip...
     'lowercase_usernames' => true
     }
   }
   ```

1. [重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure) 使更改生效。

**Source configuration**

1. 编辑 `config/gitlab.yaml`:

   ```yaml
   production:
     ldap:
       servers:
         main:
           # snip...
           lowercase_usernames: true
   ```

1. [重启极狐GitLab](../../restart_gitlab.md#源安装实例) 使更改生效。

### 禁用 LDAP web 登录
<!--
It can be useful to prevent using LDAP credentials through the web UI when
an alternative such as SAML is preferred. This allows LDAP to be used for group
sync, while also allowing your SAML identity provider to handle additional
checks like custom 2FA.

When LDAP web sign in is disabled, users don't see an **LDAP** tab on the sign-in page.
This does not disable [using LDAP credentials for Git access](#git-password-authentication).
-->
当首选 SAML 等替代方法时，阻止通过 Web UI 使用 LDAP 凭据会很有用。 这允许 LDAP 用于组同步，同时还允许您的 SAML 身份提供商处理额外的检查，如自定义 2FA。

禁用 LDAP Web 登录后，用户在登录页面上看不到 **LDAP** 选项。这不会禁用使用 LDAP 凭据进行 Git 访问。

**Omnibus 配置**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['prevent_ldap_sign_in'] = true
   ```

1. [重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure) 使配置生效。

**源安装配置**

1. 编辑 `config/gitlab.yaml`：

   ```yaml
   production:
     ldap:
       prevent_ldap_sign_in: true
   ```

1. [重启极狐GitLab](../../restart_gitlab.md#源安装实例) 使配置生效。

### 使用加密凭证
<!--
Instead of having the LDAP integration credentials stored in plaintext in the configuration files, you can optionally
use an encrypted file for the LDAP credentials. To use this feature, you first need to enable
[GitLab encrypted configuration](../../encrypted_configuration.md).

The encrypted configuration for LDAP exists in an encrypted YAML file. By default the file will be created at
`shared/encrypted_configuration/ldap.yaml.enc`. This location is configurable in the GitLab configuration.

The unencrypted contents of the file should be a subset of the secret settings from your `servers` block in the LDAP
configuration.

The supported configuration items for the encrypted file are:

- `bind_dn`
- `password`

The encrypted contents can be configured with the [LDAP secret edit Rake command](../../raketasks/ldap.md#edit-secret).
-->

除了将 LDAP 集成凭证以明文形式存储在配置文件中，您可以选择为 LDAP 凭证使用加密文件。要使用该功能，首先必须启用 GitLab 加密配置<!--[GitLab 加密配置](../../encrypted_configuration.md)-->。

LDAP 的加密配置存放于加密的 YAML 文件中。默认情况下，该文件将在 `shared/encrypted_configuration/ldap.yaml.enc` 中创建，位置可以配置。

文件中的未加密内容应该是 LDAP 配置中  `servers` 的密钥配置。

加密文件的配置项有：

- `bind_dn`
- `password`

可以使用 [LDAP 密钥编辑 Rake 命令](../../raketasks/ldap.md#编辑-secret)配置加密内容。

**Omnibus 配置**

如果最初 LDAP 配置如下：

1. 在 `/etc/gitlab/gitlab.rb` 中：

  ```ruby
    gitlab_rails['ldap_servers'] = {
    'main' => {
      # snip...
      'bind_dn' => 'admin',
      'password' => '123'
      }
    }
  ```

1. 编辑加密密钥：

   ```shell
   sudo gitlab-rake gitlab:ldap:secret:edit EDITOR=vim
   ```

1. LDAP 密钥的未加密内容应按以下案例配置：

   ```yaml
   main:
     bind_dn: admin
     password: '123'
   ```

1. 编辑 `/etc/gitlab/gitlab.rb` 并移除 `user_bn` 和 `password` 设置。

1. [重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure) 使更改生效。

**源安装配置**

如果最初 LDAP 配置如下：

1. 在 `config/gitlab.yaml` 中：

   ```yaml
   production:
     ldap:
       servers:
         main:
           # snip...
           bind_dn: admin
           password: '123'
   ```

1. 编辑加密密钥：

   ```shell
   bundle exec rake gitlab:ldap:secret:edit EDITOR=vim RAILS_ENVIRONMENT=production
   ```

1. LDAP 密钥的未加密内容应按以下案例配置：

   ```yaml
   main:
    bind_dn: admin
    password: '123'
   ```

1. 编辑 `config/gitlab.yaml` 并移除 `user_bn` 和 `password` 设置。

1. [重启极狐GitLab](../../restart_gitlab.md#源安装实例) 使更改生效。



## 禁用匿名 LDAP 身份验证

极狐GitLab 不支持 TLS 客户端身份验证。在您的 LDAP 服务器上完成这些步骤。

1. 禁用匿名身份验证。
1. 启用以下身份验证类型之一：
    - 简单的身份验证。
    - 简单身份验证和安全层 (SASL) 身份验证。

LDAP 服务器中的 TLS 客户端身份验证设置不能是强制性的，并且客户端无法使用 TLS 协议进行身份验证。

## 删除用户

用户从 LDAP 服务器被删除：

- 立即被阻止登录极狐GitLab。
- 不再使用许可证<!--[不再使用许可证](../../../user/admin_area/moderate_users.md)-->。

但是，这些用户可以继续通过 SSH 使用 Git，直到下一次 LDAP 检查缓存运行<!--[LDAP 检查缓存运行](ldap_synchronization.md#adjust-ldap-user-sync-schedule)-->。

要立即删除帐户，您可以手动阻止用户<!--[阻止用户](../../../user/admin_area/moderate_users.md#block-a-user)-->。

<!--
## Google Secure LDAP

> Introduced in GitLab 11.9.

[Google Cloud Identity](https://cloud.google.com/identity/) provides a Secure
LDAP service that can be configured with GitLab for authentication and group sync.
See [Google Secure LDAP](google_secure_ldap.md) for detailed configuration instructions.

## Synchronize users and groups

For more information on synchronizing users and groups between LDAP and GitLab, see
[LDAP synchronization](ldap_synchronization.md).
-->

<!--
## 故障排查

查看 [LDAP 故障排查管理员指南](ldap-troubleshooting.md).
-->