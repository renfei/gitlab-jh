---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 如何设置 Consul **(PREMIUM SELF)**

Consul 集群由[服务器和客户端代理](https://www.consul.io/docs/agent)组成。
服务器在它们自己的节点上运行，而客户端在其它节点上运行，而这些节点又与服务器进行通信。

专业版包含捆绑版本的 [Consul](https://www.consul.io/)，这是一种服务网络解决方案，您可以使用 `/etc/gitlab/gitlab.rb` 进行管理。

## 必要条件

在配置 Consul 之前：

1. <!--查看[参考架构](reference_architectures/index.md#可用的参考架构)文档以-->确定您应该拥有的 Consul 服务器节点的数量。
1. 如有必要，请确保防火墙中的[适当的端口已打开](https://docs.gitlab.cn/omnibus/package-information/defaults.html#端口)。

## 配置 Consul 节点

在*每个* Consul 服务器节点上：

1. 按照说明[安装](https://about.gitlab.cn/install/)极狐GitLab，选择您喜欢的平台，但不要在询问时提供 `EXTERNAL_URL` 值。
1. 编辑 `/etc/gitlab/gitlab.rb`，并通过替换 `retry_join` 部分中注明的值来添加以下内容。在下面的示例中，有三个节点，两个用它们的 IP 表示，一个用它的 FQDN，您可以使用任一表示法：

   ```ruby
   # Disable all components except Consul
   roles ['consul_role']

   # Consul nodes: can be FQDN or IP, separated by a whitespace
   consul['configuration'] = {
     server: true,
     retry_join: %w(10.10.10.1 consul1.gitlab.example.com 10.10.10.2)
   }

   # Disable auto migrations
   gitlab_rails['auto_migrate'] = false
   ```

1. [重新配置极狐GitLab](restart_gitlab.md#omnibus-gitlab-reconfigure) 使更改生效。
1. 运行以下命令以确保 Consul 配置正确并验证所有服务器节点都在通信：

   ```shell
   sudo /opt/gitlab/embedded/bin/consul members
   ```

   输出应类似于：

   ```plaintext
   Node                 Address               Status  Type    Build  Protocol  DC
   CONSUL_NODE_ONE      XXX.XXX.XXX.YYY:8301  alive   server  0.9.2  2         gitlab_consul
   CONSUL_NODE_TWO      XXX.XXX.XXX.YYY:8301  alive   server  0.9.2  2         gitlab_consul
   CONSUL_NODE_THREE    XXX.XXX.XXX.YYY:8301  alive   server  0.9.2  2         gitlab_consul
   ```

   如果结果显示任何节点的状态不是 `alive`，或者三个节点中的任何一个缺失，请参阅[故障排查部分](#故障排查)。

## 升级 Consul 节点

要升级您的 Consul 节点，请升级 GitLab 包。

节点应该是：

- 升级 Omnibus GitLab 包之前健康集群的成员。
- 一次升级一个节点。

通过在每个节点中运行以下命令来识别集群中的任何现有健康问题。如果集群健康，该命令将返回一个空数组：

```shell
curl "http://127.0.0.1:8500/v1/health/state/critical"
```

如果 Consul 版本发生了变化，你会在 `gitlab-ctl reconfigure` 的末尾看到一条通知，通知你需要重新启动 Consul 才能使用新版本。

一次重启一个节点：

```shell
sudo gitlab-ctl restart consul
```

Consul 节点使用 raft 协议进行通信。如果当前 leader 下线，则需要进行 leader 选举。必须存在 leader 节点以促进跨集群的同步。如果同时下线的节点过多，集群将失去法定人数，并且由于[破坏共识](https://www.consul.io/docs/architecture/consensus)而无法选举 leader。

如果集群在升级后无法恢复，请参阅[故障排查部分](#故障排查)。

GitLab 使用 Consul 仅存储易于重新生成的瞬态数据。如果捆绑的 Consul 没有被 GitLab 本身以外的任何进程使用，您可以[从头开始重建集群](#从头开始重新创建)。

## 故障排查

如果您需要调试任何问题，以下是一些有用的操作。您可以通过运行来查看任何错误日志：

```shell
sudo gitlab-ctl tail consul
```

### 检查集群成员

要确定哪些节点是集群的一部分，请在集群中的任何成员上运行以下命令：

```shell
sudo /opt/gitlab/embedded/bin/consul members
```

输出应类似于：

```plaintext
Node            Address               Status  Type    Build  Protocol  DC
consul-b        XX.XX.X.Y:8301        alive   server  0.9.0  2         gitlab_consul
consul-c        XX.XX.X.Y:8301        alive   server  0.9.0  2         gitlab_consul
consul-c        XX.XX.X.Y:8301        alive   server  0.9.0  2         gitlab_consul
db-a            XX.XX.X.Y:8301        alive   client  0.9.0  2         gitlab_consul
db-b            XX.XX.X.Y:8301        alive   client  0.9.0  2         gitlab_consul
```

理想情况下，所有节点的 `Status` 都是 `alive`。

### 重启 Consul

如果有必要重新启动 Consul，那么以受控的方式执行此操作以保持法定人数是很重要的。如果仲裁丢失，要恢复集群，请遵循 Consul [中断恢复](#中断恢复)流程。

为安全起见，建议您一次仅在一个节点中重新启动 Consul，以确保集群保持完整。对于较大的集群，可以一次重新启动多个节点。其可容忍的失败次数见 [Consul 共识文档](https://www.consul.io/docs/architecture/consensus#deployment-table)。这将是它可以维持的同时重启次数。

要重启 Consul：

```shell
sudo gitlab-ctl restart consul
```

### Consul 节点无法通信

默认情况下，Consul 尝试 [bind](https://www.consul.io/docs/agent/options#_bind) 到 `0.0.0.0`，但它会为其他 Consul 节点与之交流而通告该节点上的第一个私有 IP 地址。如果其它节点无法与此地址上的节点通信，则集群处于失败状态。

如果您遇到此问题，则在 `gitlab-ctl tail consul` 中会输出如下消息：

```plaintext
2017-09-25_19:53:39.90821     2017/09/25 19:53:39 [WARN] raft: no known peers, aborting election
2017-09-25_19:53:41.74356     2017/09/25 19:53:41 [ERR] agent: failed to sync remote state: No cluster leader
```

要解决此问题：

1. 在每个节点上选择一个地址，所有其他节点都可以通过该地址到达该节点。
1. 更新您的 `/etc/gitlab/gitlab.rb`

   ```ruby
   consul['configuration'] = {
     ...
     bind_addr: 'IP ADDRESS'
   }
   ```

1. 重新配置极狐GitLab

   ```shell
   gitlab-ctl reconfigure
   ```

如果您仍然看到错误，您可能需要在受影响的节点上[擦除 Consul 数据库并重新初始化](#从头开始重新创建)。

### Consul 未启动 - 多个私有 IP

如果一个节点有多个私有 IP，Consul 不知道要通告哪个私有地址，然后它在启动时立即退出。

`gitlab-ctl tail consul` 中输出如下消息：

```plaintext
2017-11-09_17:41:45.52876 ==> Starting Consul agent...
2017-11-09_17:41:45.53057 ==> Error creating agent: Failed to get advertise address: Multiple private IPs found. Please configure one.
```

要解决此问题：

1. 在节点上选择一个地址，所有其它节点都可以通过该地址到达该节点。
1. 更新您的 `/etc/gitlab/gitlab.rb`

   ```ruby
   consul['configuration'] = {
     ...
     bind_addr: 'IP ADDRESS'
   }
   ```

1. 重新配置极狐GitLab

   ```shell
   gitlab-ctl reconfigure
   ```

### 中断恢复

如果在集群中丢失了足够多的 Consul 节点来破坏仲裁，那么集群就被认为已经失败，没有人工干预就无法运行。在这种情况下，您可以从头开始重新创建节点或尝试恢复。

#### 从头开始重新创建

默认情况下，GitLab 不会在 Consul 节点中存储任何无法重新创建的内容。擦除 Consul 数据库并重新初始化：

```shell
sudo gitlab-ctl stop consul
sudo rm -rf /var/opt/gitlab/consul/data
sudo gitlab-ctl start consul
```

在此之后，节点应该开始备份，其余的服务器代理重新加入。不久之后，客户端代理也应该重新加入。

#### 恢复故障节点

如果您已经利用 Consul 存储其他数据并希望恢复故障节点，请按照 [Consul 指南](https://learn.hashicorp.com/tutorials/consul/recovery-outage)来恢复故障集群。
