---
stage: Release
group: Release
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
description: 'Learn how to administer GitLab Pages.'
---

# GitLab Pages 管理 **(FREE SELF)**
<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/80) in GitLab EE 8.3.
> - Custom CNAMEs with TLS support were [introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173) in GitLab EE 8.5.
> - GitLab Pages [was ported](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/14605) to Community Edition in GitLab 8.17.
> - Support for subgroup project's websites was
>   [introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/30548) in GitLab 11.8.
-->


GitLab Pages 允许托管静态站点，它必须有管理员配置。<!--您还可以另行查看[用户文档](../../user/project/pages/index.md)。-->

NOTE:
本文适用于 Omnibus GitLab 安装实例。

<!--
If you have installed
GitLab from source, see
[GitLab Pages administration for source installations](source.md).
-->

## 概览

<!--
You are encouraged to read its [README](https://gitlab.com/gitlab-org/gitlab-pages/blob/master/README.md) to fully understand how
it works.
-->

GitLab Pages 利用 GitLab Pages daemon，它是一个用 Go 编写的简易 HTTP 服务器，可以侦听外部 IP 地址，并提供对自定义域名和自定义证书的支持。 它可以通过 SNI 支持动态证书并默认使用 HTTP2 对外暴露 GitLab Pages。

对于[自定义域名](#自定义域名)（不包括[通配符域名](#通配符域名))，GitLab Pages daemon 需要监听端口 `80` 和/或 `443`。因此，您对齐进行设置时，可以使用一些灵活的方法：

- 在运行 GitLab 实例的服务器上，同时运行 Pages daemon，监听 **secondary IP**。
- 在[单独的服务器](#在单独的服务器上运行-gitlab-pages)中运行 Pages daemon。在这种情况下，[Pages 路径](#更改存储路径)也必须存在于安装了 Pages daemon 的服务器中，这样您必须在网络中进行共享。
- 在运行 GitLab 实例的服务器上，同时运行 Pages daemon，侦听相同 IP 的不同端口。在这种情况下，您必须使用负载均衡器代理流量。如果您选择路由，请注意您应该针对 HTTPS 使用 TCP 负载均衡。如果您使用 TLS 终止（HTTPS 负载均衡），Pages 无法使用用户提供的证书对外提供服务。对于 HTTP，使用 HTTP 或 TCP 负载均衡皆可。

在本文中，我们假定使用第一种方法。如果您不支持自定义域名，则不需要 **secondary IP**。

## 前提条件

在继续 Pages 配置之前，您必须：

1. 准备 Pages 域名，不可以是极狐GitLab 实例域名的子域名。

   | GitLab 实例域名 | Pages 域名 | 是否生效？ |
   | :---: | :---: | :---: |
   | `example.com` | `example.io` | **{check-circle}** Yes |
   | `example.com` | `pages.example.com` | **{dotted-circle}** No |
   | `gitlab.example.com` | `pages.example.com` | **{check-circle}** Yes |

1. 配置一个 **通配符 DNS 记录**.
1. （可选）如果您决定在 HTTPS 下提供 Pages 服务，为 Pages 域名准备一个 **通配符证书**。
1. （可选但推荐）启用 Shared runners<!--[Shared runners](../../ci/runners/index.md)-->，这样用户不必准备自己的 runner。
1. （仅适用于自定义域名）准备一个 **secondary IP**。

NOTE:
如果您的 GitLab 实例和 Pages daemon 部署在专用网络中或在防火墙之后，则您的 GitLab Pages 网站只能由有权访问专用网络的设备/用户访问。

### 添加域名到公共后缀列表

浏览器使用[公共后缀列表](https://publicsuffix.org)来决定如何处理子域名。如果您的 GitLab 实例允许公众成员创建 GitLab Pages 站点，那么它还允许这些用户在 Pages 域名（`example.io`）上创建子域名。此外，添加 Pages 域名到公共后缀列表可防止浏览器接受 supercookies。

按照[这些说明](https://publicsuffix.org/submit/)提交您的 GitLab Pages 子域名。例如，如果您的域名是 `example.io`，您应该请求将 `example.io` 添加到公共后缀列表中。

### DNS 配置

GitLab Pages 期望在它们自己的虚拟主机上运行。在您的 DNS 服务器/提供商中，您需要添加一个通配符 DNS A 记录指向极狐GitLab 运行的主机。示例如下：

```plaintext
*.example.io. 1800 IN A    192.0.2.1
*.example.io. 1800 IN AAAA 2001:db8::1
```

其中 `example.io` 是 GitLab Pages 提供服务的域名，`192.0.2.1` 是 GitLab 实例的 IPv4 地址，`2001:db8::1` 是 IPv6 地址。如果您没有 IPv6，则可以省略 AAAA 记录。

#### 自定义域名

如果需要支持自定义域名，Pages 根域名和它的子域名应指向 secondary IP（专用于 Pages daemon) 。`<namespace>.<pages root domain>` 应该直接指向 Pages。如果未这样设置，用户就不能使用 `CNAME` 记录将他们的自定义域名指向 GitLab Pages。

示例如下：

```plaintext
example.com   1800 IN A    192.0.2.1
*.example.io. 1800 IN A    192.0.2.2
```

示例参数说明：

- `example.com`：GitLab 域名
- `example.io`：GitLab Pages 提供服务的域名。
- `192.0.2.1`：GitLab 实例的主 IP。
- `192.0.2.2`：secondary IP，专用于 GitLab Pages，必须和主 IP 不同。

NOTE:
您不应使用 GitLab 域名来为用户提供 Pages 服务。有关更多信息请查看[安全部分](#安全)。

## 配置

根据您的需要，您可以通过 4 种不同的方式设置 GitLab pages。

以下示例按从最简单到最高级的顺序列出。绝对最低要求是设置通配符 DNS，因为这在所有配置中都是必需的。

### 通配符域名

**要求：**

- [通配符 DNS 设置](#dns-配置)

---

URL scheme：`http://<namespace>.example.io/<project_slug>`

这种方法是您可以使用的最简单设置方法。它是所有其它设置的基础，如下所述。NGINX 将所有请求代理到 daemon。Pages daemon 不监听外界。

1. 在 `/etc/gitlab/gitlab.rb`中，为 GitLab Pages 设置外部 URL：

   ```ruby
   external_url "http://gitlab.example.com" # external_url here is only for reference
   pages_external_url "http://pages.example.com" # not a subdomain of external_url
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)。

<!--
Watch the [video tutorial](https://youtu.be/dD8c7WNcc6s) for this configuration.
-->

### 支持 TLS 的通配符域名

**要求：**

- [通配符 DNS 设置](#dns-配置)
- 通配符 TLS 证书

---

URL scheme：`https://<namespace>.example.io/<project_slug>`

NGINX 将所有请求代理到 daemon。Pages daemon 不监听外界。

1. 将 `example.io` 证书和密钥放在 `/etc/gitlab/ssl` 中。
1. 在 `/etc/gitlab/gitlab.rb` 中指定如下配置：

   ```ruby
   external_url "https://gitlab.example.com" # external_url here is only for reference
   pages_external_url "https://pages.example.com" # not a subdomain of external_url

   pages_nginx['redirect_http_to_https'] = true
   ```

1. 如果您还没有命名您的证书和密钥 `example.io.crt` 和 `example.io.key`，您需要添加如下所示的完整路径：

   ```ruby
   pages_nginx['ssl_certificate'] = "/etc/gitlab/ssl/pages-nginx.crt"
   pages_nginx['ssl_certificate_key'] = "/etc/gitlab/ssl/pages-nginx.key"
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)。
1. 如果您使用 [Pages 访问控制](#访问控制)，更新 GitLab Pages System OAuth application<!--[System OAuth application](../../integration/oauth_provider.md#instance-wide-applications)--> 中的重定向 URI，去使用 HTTPS 协议。

WARNING:
一个实例不支持多个通配符。每个实例只能分配一个通配符。

### 全局设置

下表列出了 Omnibus GitLab 中 Pages 已知的所有配置设置，以及它们的作用。这些选项可以在`/etc/gitlab/gitlab.rb`中进行调整，并在您[重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure) 后生效。大多数设置不需要手动配置，除非您需要更精细地控制 Pages daemon 如何在您的环境中运行和提供服务。

| 设置                                | 说明 |
|-----------------------------------------|-------------|
| `pages_external_url`                    | 访问 GitLab Pages 的 URL，包括协议（HTTP / HTTPS）。如果使用 `https://`，需要额外配置。查看 [支持 TLS 的通配符域名](#支持-tls-的通配符域名) 和 [支持 TLS 的自定义域名](#支持-tls-的自定义域名)，获取详细信息。 |
| **`gitlab_pages[]`**                    |  |
| `access_control`                        | 是否启用[访问控制](#访问控制)。 |
| `api_secret_key`                        | 带有用于通过 GitLab API 进行身份验证的密钥的文件的完整路径。未设置时自动生成。 |
| `artifacts_server`                      | 在 GitLab Pages 中启用查看 artifacts<!--[artifacts](../job_artifacts.md)-->。 |
| `artifacts_server_timeout`              | 服务器的代理请求的超时时间（以秒为单位）。 |
| `artifacts_server_url`                  | 将 artifact 请求代理到的 API URL。默认为 GitLab 的 `external URL` + `/api/v4`，例如 `https://gitlab.com/api/v4`。运行[单独的 Pages 服务器](#在单独的服务器上运行-gitlab-pages)时，该 URL 必须指向主 GitLab 服务器的 API。 |
| `auth_redirect_uri`                     | 对于使用 GitLab 进行身份验证的回调 URL。默认为 `pages_external_url` + `/auth` 的项目子域名。 |
| `auth_secret`                           | 用于签署身份验证请求的密钥。 在 OAuth 注册期间留空以从 GitLab 自动提取。 |
| `dir`                                   | 配置和 secrets 文件的工作目录。 |
| `enable`                                | 在当前系统中启用或禁用 GitLab Pages。 |
| `external_http`                         | 配置 Pages 以绑定到一个或多个 secondary IP 地址，为 HTTP 请求提供服务。多个地址连同确切的端口可以作为一个数组，例如 `['1.2.3.4', '1.2.3.5:8063']`。设置 `listen_http` 的值。 |
| `external_https`                        | 配置 Pages 以绑定到一个或多个 secondary IP 地址，为 HTTP 请求提供服务。多个地址连同确切的端口可以作为一个数组，例如 `['1.2.3.4', '1.2.3.5:8063']`。设置 `listen_https` 的值。 |
| `gitlab_client_http_timeout`            | GitLab API HTTP 客户端连接超时秒数（默认值：10s）。 |
| `gitlab_client_jwt_expiry`              | JWT 令牌到期时间，以秒为单位（默认值：30 秒）。 |
| `gitlab_cache_expiry`                   | 域名配置存储在缓存中的最长时间（默认值：600 秒）。 |
| `gitlab_cache_refresh`                  | 域名配置设置刷新的时间间隔（默认值：60 秒）。 |
| `gitlab_cache_cleanup`                  | 从缓存中删除过期项目的时间间隔（默认值：60 秒）。 |
| `gitlab_retrieval_timeout`              | 每个请求等待 GitLab API 响应的最长时间（默认值：30 秒）。 |
| `gitlab_retrieval_interval`             | 在通过 GitLab API 重试解析域名配置之前等待的时间间隔（默认值：1s）。 |
| `gitlab_retrieval_retries`              | 通过 API 重试解析域名配置的最大次数（默认值：3）。 |
| `domain_config_source`                  | 该参数从 14.0 版本起被移除。 |
| `gitlab_id`                             | OAuth 应用程序公共 ID。留空以在 Pages 使用 GitLab 进行身份验证时自动填充。 |
| `gitlab_secret`                         | OAuth 应用程序 secret。留空以在 Pages 使用 GitLab 进行身份验证时自动填充。 |
| `auth_scope`                            | 用于身份验证的 OAuth 应用程序范围。 必须匹配 GitLab Pages OAuth 应用程序设置。 默认情况下留空以使用 `api` 范围。 |
| `gitlab_server`                         | 启用访问控制时，用于身份认证的服务器；默认为 GitLab 的 `external_url`。 |
| `headers`                               | 指定应随每个响应发送到客户端的任何其他 http header。多个 header 可以作为一个数组，header 和值作为一个字符串，例如`['my-header: myvalue', 'my-other-header: my-other-value']` |
| `inplace_chroot`                        | 在不支持 bind-mounts 的系统上<!--[不支持 bind-mounts 的系统上](index.md#gitlab-pages-fails-to-start-in-docker-container)-->，于 14.3 版本删除，使得 GitLab Pages 将 `chroot` 放到它的 `pages_path` 目录中。使用本地 `chroot` 时存在一些警告<!--获取更多信息，参考 GitLab Pages [README](https://gitlab.com/gitlab-org/gitlab-pages/blob/master/README.md#caveats)。--> |
| `enable_disk`                           | 允许 GitLab Pages daemon 为磁盘的内容提供服务。如果共享磁盘存储不可用，则应禁用。 |
| `insecure_ciphers`                      | 使用默认的密码套件列表，可能包含不安全的密码套件，如 3DES 和 RC4。 |
| `internal_gitlab_server`                | 专门用于 API 请求的内部 GitLab 服务器地址。 如果您想通过内部负载均衡器发送该流量，则很有用。 默认为 GitLab 的 `external_url`。 |
| `listen_proxy`                          | 侦听反向代理请求的地址。Pages 绑定到这些地址的网络套接字并接收来自它们的传入请求。 在 `$nginx-dir/conf/gitlab-pages.conf` 中设置 `proxy_pass` 的值。 |
| `log_directory`                         | 日志目录的绝对路径。 |
| `log_format`                            | 日志输出格式：`text` 或 `json`。 |
| `log_verbose`                           | 详细日志记录，true 或 false。 |
| `propagate_correlation_id`              | 设置为 true（默认为 false）以重新使用传入请求 header `X-Request-ID`（如果存在）中的现有关联 ID。 如果反向代理设置了此标头，则该值将在请求链中传播。 |
| `max_connections`                       | 限制与 HTTP、HTTPS 或代理侦听器的并发连接数。 |
| `max_uri_length`                        | GitLab Pages 接受的 URI 的最大长度。 设置为 0 表示无限长度。引入于 14.5 版本。 |
| `metrics_address`                       | 监听指标请求的地址。 |
| `redirect_http`                         | 将页面从 HTTP 重定向到 HTTPS，true 或 false。 |
| `sentry_dsn`                            | 发送 Sentry 崩溃报告的地址。 |
| `sentry_enabled`                        | 启用 Sentry 报告和日志记录，true 或 false。 |
| `sentry_environment`                    | Sentry 崩溃报告的环境。 |
| `status_uri`                            | 状态页面的 URL 路径，例如 `/@status`。 |
| `tls_max_version`                       | 指定最大 SSL/TLS 版本（“tls1.2”或“tls1.3”）。 |
| `tls_min_version`                       | 指定最小 SSL/TLS 版本（“tls1.2”或“tls1.3”）。 |
| `use_http2`                             | 启用 HTTP2 支持。 |
| **`gitlab_pages['env'][]`**             |  |
| `http_proxy`                            | 配置 GitLab Pages 以使用 HTTP 代理来调解 Pages 和 GitLab 之间的流量。 启动 Pages daemon 时设置环境变量 `http_proxy`。 |
| **`gitlab_rails[]`**                    |  |
| `pages_domain_verification_cron_worker` | 验证自定义 GitLab Pages 域名的时间计划。 |
| `pages_domain_ssl_renewal_cron_worker`  | 通过 Let's Encrypt 为 GitLab Pages 域名获取和更新 SSL 证书的时间计划。 |
| `pages_domain_removal_cron_worker`      | 删除未经验证的自定义 GitLab Pages 域名的时间计划。 |
| `pages_path`                            | 要存储页面的磁盘目，默认为 `GITLAB-RAILS/shared/pages`。 |
| **`pages_nginx[]`**                     |  |
| `enable`                                | 在 NGINX 中为 Pages 配置一个虚拟主机 `server{}` 块。NGINX 需要将流量代理到 Pages daemon。如果 Pages daemon 应直接接收所有请求，则设置为 `false`，例如在使用[自定义域名](#自定义域名)时。 |
| `FF_ENABLE_REDIRECTS`                   | 启用/禁用重定向的功能标志（默认启用）。 <!--阅读[重定向文档](../../user/project/pages/redirects.md#disable-redirects) 了解更多信息。--> |
| `FF_ENABLE_PLACEHOLDERS`                | 启用/禁用重写的功能标志（默认禁用）。<!--Read the [redirects documentation](../../user/project/pages/redirects.md#feature-flag-for-rewrites) for more information.-->  |
| `use_legacy_storage`                    | 允许使用旧域名配置源和存储的临时引入参数。在 14.3 中删除。 |
| `rate_limit_source_ip`                  | 每个源 IP 的速率限制（以每秒请求数表示）。设置为 `0` 以禁用此功能。 |
| `rate_limit_source_ip_burst`            | 每秒允许的每个源 IP 最大突发的速率限制。 |

## 高级配置

除了通配符域名之外，您还可以选择配置 GitLab Pages 以使用自定义域名。 同样，这里有两个选项：支持带有和不带有 TLS 证书的自定义域名。最简单的设置是没有 TLS 证书。在任何一种情况下，您都需要一个 **secondary IP**。 如果您有 IPv6 和 IPv4 地址，则可以同时使用它们。

### 自定义域名

**要求：**

- [通配符 DNS 设置](#dns-配置)
- Secondary IP

---

URL scheme：`http://<namespace>.example.io/<project_slug>` and `http://custom-domain.com`

在这种情况下，Pages daemon 正在运行，NGINX 仍然代理对 daemon 的请求，但守护进程也能够接收来自外部的请求。支持自定义域名，但不支持 TLS。

1. 在 `/etc/gitlab/gitlab.rb` 中指定以下配置：

   ```ruby
   external_url "http://gitlab.example.com" # external_url here is only for reference
   pages_external_url "http://pages.example.com" # not a subdomain of external_url
   nginx['listen_addresses'] = ['192.0.2.1'] # The primary IP of the GitLab instance
   pages_nginx['enable'] = false
   gitlab_pages['external_http'] = ['192.0.2.2:80', '[2001:db8::2]:80'] # The secondary IPs for the GitLab Pages daemon
   ```

   如果您没有 IPv6，您可以忽略 IPv6 地址。

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)。

### 支持 TLS 的自定义域名

**Requirements:**

- [通配符 DNS 设置](#dns-配置)
- Wildcard TLS certificate
- Secondary IP

---

URL scheme：`https://<namespace>.example.io/<project_slug>` 和 `https://custom-domain.com`

在这种情况下，Pages daemon 正在运行，NGINX 仍然代理对 daemon 的请求，但 daemon 也能够接收来自外部的请求。支持自定义域和 TLS。

1. 将 `example.io` 证书和密钥放在 `/etc/gitlab/ssl` 中。
1. 在 `/etc/gitlab/gitlab.rb` 中指定以下配置：

   ```ruby
   external_url "https://gitlab.example.com" # external_url here is only for reference
   pages_external_url "https://pages.example.com" # not a subdomain of external_url
   nginx['listen_addresses'] = ['192.0.2.1'] # The primary IP of the GitLab instance
   pages_nginx['enable'] = false
   gitlab_pages['external_http'] = ['192.0.2.2:80', '[2001:db8::2]:80'] # The secondary IPs for the GitLab Pages daemon
   gitlab_pages['external_https'] = ['192.0.2.2:443', '[2001:db8::2]:443'] # The secondary IPs for the GitLab Pages daemon
   # Redirect pages from HTTP to HTTPS
   gitlab_pages['redirect_http'] = true
   ```

   如果您没有 IPv6，您可以忽略 IPv6 地址。

1. 如果您还没有分别命名您的证书和密钥 `example.io.crt` 和 `example.io.key`，然后您还需要添加完整路径，如下所示：

   ```ruby
   gitlab_pages['cert'] = "/etc/gitlab/ssl/example.io.crt"
   gitlab_pages['cert_key'] = "/etc/gitlab/ssl/example.io.key"
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)。
1. 如果您使用[Pages 访问控制](#访问控制), 更新 GitLab Pages System OAuth application<!--[System OAuth application](../../integration/oauth_provider.md#instance-wide-applications)--> 中的重定向 URI，去使用 HTTPS 协议。

<!--
### 自定义域名验证

为了防止恶意用户劫持不属于他们的域名，极狐GitLab 支持自定义域名验证[自定义域名验证](../../user/project/pages/custom_domains_ssl_tls_certification/index.md#steps)。
添加自定义域名时，用户需要通过向该域名的 DNS 记录添加极狐GitLab 控制的验证码来证明他们拥有该域名。

如果您的用户群是私密的或以其他方式受信任，您可以禁用验证要求：

1. 在顶部导航栏，选择 **菜单 > 管理**。
2. 在左侧导航栏，选择 **设置 > 偏好设置**。
3. 展开 **Pages**。
4. 取消勾选 **要求用户证明自定义域名的所有权** 复选框。该设置是默认启用的。
-->

### Let's Encrypt 集成

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/28996) in GitLab 12.1.
-->

<!--[GitLab Pages 的 Let's Encrypt 集成](../../user/project/pages/custom_domains_ssl_tls_certification/lets_encrypt_integration.md)-->GitLab Pages 的 Let's Encrypt 集成允许用户为 GitLab Pages 站点，向其自定义域名添加 Let's Encrypt SSL 证书。


要启用该功能：

1. 选择您希望接收有关域名过期通知的电子邮件地址。
1. 在顶部导航栏，选择 **菜单 > **管理**。
2. 在左侧导航栏，选择 **设置 > 偏好设置**。
3. 展开 **Pages**。
1. 输入接收通知的电子邮件地址并接受 Let's Encrypt 的服务条款。
1. 选择 **保存更改**。

### 访问控制

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/33422) in GitLab 11.5.
-->

GitLab Pages 访问控制可以针对每个项目进行配置，并允许根据项目的成员身份来控制对 Pages 站点的访问。

访问控制的工作原理是将 Pages daemon 注册为极狐GitLab 的 OAuth 应用程序。每当未经身份验证的用户发出访问私人 Pages 站点的请求时，Pages daemon 会将用户重定向到 GitLab。如果身份验证成功，用户将被重定向到带有令牌的 Pages，该令牌保存在 cookie 中。cookie 使用密钥签名，因此可以检测到篡改。

每个查看私有站点中资源的请求都由 Pages 使用该令牌进行身份验证。 对于它收到的每个请求，它都会向 GitLab API 发出请求，以检查用户是否有权阅读该站点。

默认情况下禁用页面访问控制。 要启用它：

1. 在 `/etc/gitlab/gitlab.rb` 中启用：

   ```ruby
   gitlab_pages['access_control'] = true
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)。
1. 用户可以在项目设置<!--[项目设置](../../user/project/pages/pages_access_control.md)-->中进行配置。

NOTE:
要使此设置对多节点设置有效，它必须应用于所有 App 节点和 Sidekiq 节点。

#### 使用 Pages 时缩小身份验证范围

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-pages/-/merge_requests/423) in GitLab 13.10.
-->

默认情况下，Pages daemon 使用 `api` 范围进行身份验证。 您可以对此进行配置。 例如，这将范围缩小到 `/etc/gitlab/gitlab.rb` 中的 `read_api`：

```ruby
gitlab_pages['auth_scope'] = 'read_api'
```

用于身份验证的范围必须与 GitLab Pages OAuth 应用程序设置相匹配。 现有应用程序的用户必须修改 GitLab Pages OAuth 应用程序。 请按照以下步骤执行此操作：

1. 在顶部导航栏，选择 **菜单 > **管理**。
2. 在左侧导航栏，选择 **设置 > 偏好设置**。
3. 展开 **Pages**。
1. 清除 `api` 范围的复选框并选择所需范围的复选框（例如，`read_api`）。
1. 选择 **保存更改**。

#### 禁止公开访问所有 Pages 网站

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/32095) in GitLab 12.7.
-->

您可以对托管在您的 GitLab 实例上的所有 GitLab Pages 网站实施 [访问控制](#访问控制)。 通过这样做，只有登录用户才能访问它们。此设置会覆盖用户在各个项目中设置的访问控制。

这对于将随 Pages 网站发布的信息仅保留给您实例的用户很有用。要做到这一点：

1. 在顶部导航栏，选择 **菜单 > **管理**。
2. 在左侧导航栏，选择 **设置 > 偏好设置**。
3. 展开 **Pages**。
1. 选择 **Disable public access to Pages sites** 复选框。
1. 单击 **保存更改**。

WARNING:
对于自助管理实例，所有公共网站在重新部署之前都保持私密状态。

<!--
Resolve this issue by
[sourcing domain configuration from the GitLab API](https://gitlab.com/gitlab-org/gitlab/-/issues/218357).
-->

### 在代理后运行

与 GitLab 的其余部分一样，Pages 可用于外部 Internet 连接由代理控制的环境。要为 GitLab Pages 使用代理：

1. 在 `/etc/gitlab/gitlab.rb` 中配置：

   ```ruby
   gitlab_pages['env']['http_proxy'] = 'http://example:8080'
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure) 使更改生效

### 使用自定义证书颁发机构（CA）

使用自定义 CA 颁发的证书时，如果无法识别自定义 CA，<!--[访问控制](../../user/project/pages/pages_access_control.md#gitlab-pages-access-control)-->访问控制和 HTML job artifacts 的在线视图<!--[HTML job artifacts 的在线视图](../../ci/pipelines/job_artifacts.md#download-job-artifacts)-->将无法工作。

这通常会导致此错误：`Post /oauth/token: x509: certificate signed by unknown authority`。

对于源安装实例，通过在系统证书存储中安装自定义 CA 可以解决。

对于 Omnibus 安装实例，通过[在 Omnibus GitLab 中安装自定义 CA](https://docs.gitlab.cn/omnibus/settings/ssl.html#安装自定义公共证书) 可以解决。

### Zip 服务和缓存配置

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-pages/-/merge_requests/392) in GitLab 13.7.
-->

WARNING:
以下为高级设置，已在 GitLab Pages 中设置了推荐的默认值。仅在绝对必要时才应更改这些设置。建议谨慎使用。

GitLab Pages 可以通过对象存储，提供来自 zip archives 的内容<!--（[issue](https://gitlab.com/gitlab-org/gitlab-pages/-/issues/485)也存在用于支持磁盘存储)-->。使用内存缓存来提高从 zip archives 提供内容时的性能。您可以通过更改以下配置标志来修改缓存。

| 设置 | 说明 |
| ------- | ----------- |
| `zip_cache_expiration` | zip archives 的缓存过期间隔。 必须大于零以避免提供陈旧的内容。 默认为 60 秒。 |
| `zip_cache_cleanup` | 如果 archives 已过期，则从内存中清除 archives 的时间间隔。 默认为 30 秒。 |
| `zip_cache_refresh` | 如果在 `zip_cache_expiration` 之前访问，则 archives 在内存中扩展的时间间隔。与 `zip_cache_expiration` 一起确定存档是否在内存中扩展。有关重要详细信息，请参阅[下面的示例](#zip-缓存刷新示例)。 默认为 30 秒。 |
| `zip_open_timeout` | 允许打开 zip archives 的最长时间。 对于大的 archive 或慢速网络连接，增加此时间，这样做可能会影响 Pages 的延迟。 默认为 30 秒。 |

#### Zip 缓存刷新示例

如果 archives 在 `zip_cache_expiration` 之前被访问，并且在过期前的剩余时间小于或等于 `zip_cache_refresh`，则会在缓存中刷新（延长它们在内存中的时间）。 例如，如果 `archive.zip` 在时间 0 被访问，它会在 60 秒后过期（`zip_cache_expiration` 的默认值）。 在下面的示例中，如果 archives 在 15 秒后再次打开，则**不**刷新，因为到期的剩余时间（45 秒）大于“zip_cache_refresh”（默认为 30 秒）。 但是，如果 archives 在 45 秒后（从第一次打开开始）再次被访问，它会被刷新。 这将 archives 保留在内存中的时间从 45 秒 + zip_cache_expiration (60 秒) 延长，总共 105 秒。

在存档到达 `zip_cache_expiration` 后，它会被标记为已过期并在下一个 `zip_cache_cleanup` 时间间隔内被删除。

![Zip cache configuration](img/zip_cache_configuration.png)

## 激活 daemon 详细日志记录

<!--
Verbose logging was [introduced](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/2533) in
Omnibus GitLab 11.1.
-->

按照以下步骤配置 GitLab Pages daemon 的详细日志记录。

1. 默认情况下，守护进程只记录 `INFO` 级别。如果你想让它以 `DEBUG` 级别记录事件，你必须在 `/etc/gitlab/gitlab.rb` 中配置它：

   ```ruby
   gitlab_pages['log_verbose'] = true
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)。

## 传播关联 ID

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-pages/-/merge_requests/438) in GitLab 13.10.
-->

将 `propagate_correlation_id` 设置为 true，允许反向代理后面的实例，为发送到 GitLab Pages 的请求生成和设置相关 ID。当反向代理设置 header 值“X-Request-ID”时，该值在请求链中传播。用户可以在日志中找到相关 ID<!--[可以在日志中找到相关 ID](../troubleshooting/tracing_correlation_id.md#identify-the-correlation-id-for-a-request)-->。

要启用关联 ID 的传播：

1. 在 `/etc/gitlab/gitlab.rb` 中，将参数设置为 true：

   ```ruby
   gitlab_pages['propagate_correlation_id'] = true
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)。

## 更改存储路径

按照以下步骤更改存储 GitLab 页面内容的默认路径。

1. 页面默认存储在 `/var/opt/gitlab/gitlab-rails/shared/pages` 中。 如果您希望将它们存储在另一个位置，您必须在 `/etc/gitlab/gitlab.rb` 中进行设置：

   ```ruby
   gitlab_rails['pages_path'] = "/mnt/storage/pages"
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure).

或者，如果您已经部署了 Pages，您可以按照以下步骤，进行无停机时间转移到新的存储位置。

1. 通过在 `/etc/gitlab/gitlab.rb` 中设置以下内容来暂停 Pages：

   ```ruby
   sidekiq['queue_selector'] = true
   sidekiq['queue_groups'] = [
     "feature_category!=pages"
   ]
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure).
1. `rsync` 内容从当前存储位置到新的存储位置：`sudo rsync -avzh --progress /var/opt/gitlab/gitlab-rails/shared/pages/ /mnt/storage/pages`。
1. 在`/etc/gitlab/gitlab.rb` 中设置新的存储位置：

   ```ruby
   gitlab_rails['pages_path'] = "/mnt/storage/pages"
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)。
1. 验证 Pages 仍按预期提供服务。
1. 通过从 `/etc/gitlab/gitlab.rb` 中删除上面设置的 `sidekiq` 设置来恢复 Pages 部署。
1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)。
1. 触发新的 Pages 部署并验证它是否按预期工作。
1. 删除旧的 Pages 存储位置：`sudo rm -rf /var/opt/gitlab/gitlab-rails/shared/pages`
1. 验证 Pages 仍按预期提供。

## 为反向代理请求配置侦听器

<!--
> [Introduced](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/2533) in Omnibus GitLab 11.1.
-->

请按照以下步骤配置 GitLab Pages 的代理侦听器。

1. 默认情况下，侦听器配置为侦听 `localhost:8090` 上的请求。

   如果你想禁用它，你必须在 `/etc/gitlab/gitlab.rb` 中进行如下配置：

   ```ruby
   gitlab_pages['listen_proxy'] = nil
   ```

   如果你想让它监听不同的端口，你也必须在 `/etc/gitlab/gitlab.rb` 中配置：
   
   ```ruby
   gitlab_pages['listen_proxy'] = "localhost:10080"
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure).


## 覆盖每个项目或组的最大 Pages 大小 **(PREMIUM SELF)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/16610) in GitLab 12.7.
-->

NOTE:
只有 GitLab 管理员才能查看和覆盖 **最大 Pages 大小** 设置。


要覆盖特定项目的全局最大 Pages 大小：

1. 在顶部导航栏，选择 **菜单> 项目** 并找到您的项目。
1. 在左侧导航栏，选择 **设置 > Pages**。
1. 在 **pages 最大大小** 下方输入值，单位为 MB。
1. 选择 **保存更改**.

要覆盖特定群组的全局最大 Pages 大小：

1. 在顶部导航栏，选择 **菜单> 群组** 并找到您的群组。
2. 在左侧导航栏，选择 **设置 > 通用**。
3. 展开 **Pages**。
1. 在 **pages 最大大小** 下方输入值，单位为 MB。
1. 选择 **保存更改**。

## 在单独的服务器上运行 GitLab Pages

您可以在单独的服务器上运行 GitLab Pages daemon 以减少主应用程序服务器上的负载。此配置不支持双向 TLS (mTLS)。<!--See the [corresponding feature proposal](https://gitlab.com/gitlab-org/gitlab-pages/-/issues/548) for more information.-->


在单独的服务器上配置 GitLab Pages：

WARNING:
以下过程包括备份和编辑 `gitlab-secrets.json` 文件的步骤。此文件包含控制数据库加密的 secrets，建议谨慎操作。

1. 在 **GitLab 服务器** 上创建 secret 文件的备份：

   ```shell
   cp /etc/gitlab/gitlab-secrets.json /etc/gitlab/gitlab-secrets.json.bak
   ```

1. 在 **GitLab 服务器** 上，要启用 Pages，添加以下内容到 `/etc/gitlab/gitlab.rb`：

   ```ruby
   pages_external_url "http://<pages_server_URL>"
   ```

1. 作为可选项，要启用[访问控制](#访问控制)，添加以下内容到 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_pages['access_control'] = true
   ```

1. 配置[对象存储并迁移 pages 数据到其上](#使用对象存储)。

1. [重新配置 **GitLab 服务器**](../restart_gitlab.md#omnibus-gitlab-reconfigure)使更改生效。`gitlab-secrets.json` 文件更新了最新的配置。

1. 设置一个新的服务器，作为 **Pages 服务器**.

1. 在 **Pages 服务器**上, 安装 Omnibus GitLab 并修改 `/etc/gitlab/gitlab.rb` 以包含：

   ```ruby
   roles ['pages_role']

   pages_external_url "http://<pages_server_URL>"

   gitlab_pages['gitlab_server'] = 'http://<gitlab_server_IP_or_URL>'
   ```

1. 如果您在 **GitLab 服务器** 上有自定义 UID/GID 设置，请将它们也添加到 **Pages 服务器** `/etc/gitlab/gitlab.rb`，否则在 **GitLab 服务器** 上运行 `gitlab-ctl reconfigure`，可以更改文件所有权并导致 Pages 请求失败。

1. 在 **Pages 服务器** 上创建 secret 文件的备份：

   ```shell
   cp /etc/gitlab/gitlab-secrets.json /etc/gitlab/gitlab-secrets.json.bak
   ```

1. 将 `/etc/gitlab/gitlab-secrets.json` 文件从 **GitLab 服务器** 复制到 **Pages 服务器**。

   ```shell
   # On the GitLab server
   cp /etc/gitlab/gitlab-secrets.json /mnt/pages/gitlab-secrets.json

   # On the Pages server
   mv /var/opt/gitlab/gitlab-rails/shared/pages/gitlab-secrets.json /etc/gitlab/gitlab-secrets.json
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure) 使更改生效。

1. 在 **GitLab 服务器** 上，对 `/etc/gitlab/gitlab.rb` 进行以下更改：

   ```ruby
   pages_external_url "http://<pages_server_URL>"
   gitlab_pages['enable'] = false
   pages_nginx['enable'] = false
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure) 使更改生效。

如果您希望分布负载，可以在多台服务器上运行 GitLab Pages。 您可以通过标准的负载均衡实践来做到这一点，例如配置您的 DNS 服务器为您的 Pages 服务器返回多个 IP，配置负载均衡器以在 IP 级别工作，等等。 如果您希望在多个服务器上设置 GitLab Pages，请为每个 Pages 服务器执行上述过程。

## 域名源配置

当 GitLab Pages daemon 为 pages 请求提供服务时，它首先需要确定应该使用哪个项目来为请求的 URL 提供服务以及其内容的存储方式。

<!--
Before GitLab 13.3, all pages content was extracted to the special shared directory,
and each project had a special configuration file.
The Pages daemon was reading these configuration files and storing their content in memory.
-->

这种方法有几个缺点，并且每次请求新域名时都会使用内部 GitLab API 替换为 GitLab Pages。域名信息也由 Pages daemon 缓存，以加快后续请求。

<!--
From [GitLab 13.3 to GitLab 13.12](#domain-source-configuration-before-140) GitLab Pages supported both ways of obtaining domain information.
-->

从 14.0 版本开始，GitLab Pages 默认使用 API，如果无法连接则无法启动。
<!--关于常见问题请查看[故障排查部分](#failed-to-connect-to-the-internal-gitlab-api).-->

<!--
For more details see this [blog post](https://about.gitlab.com/blog/2020/08/03/how-gitlab-pages-uses-the-gitlab-api-to-serve-content/).
-->

<!--
### Domain source configuration before 14.0

> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/217912) in GitLab 13.3.

WARNING:
`domain_config_source` parameter is removed and has no effect starting from [GitLab 14.0](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/5993)

From [GitLab 13.3](https://gitlab.com/gitlab-org/gitlab/-/issues/217912) to [GitLab 13.12](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/5993) GitLab Pages can either use `disk` or `gitlab` domain configuration source.

We highly advise you to use `gitlab` configuration source as it will make transition to newer versions easier.

To explicitly enable API source:

1. Add the following to your `/etc/gitlab/gitlab.rb` file:

   ```ruby
   gitlab_pages['domain_config_source'] = "gitlab"
   ```

1. [Reconfigure GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure) for the changes to take effect.

Or if you want to use legacy confiration source you can:

1. Add the following to your `/etc/gitlab/gitlab.rb` file:

   ```ruby
   gitlab_pages['domain_config_source'] = "disk"
   ```

1. [Reconfigure GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure) for the changes to take effect.
-->

### GitLab API 缓存配置

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-pages/-/issues/520) in GitLab 13.10.
-->

基于 API 的配置使用缓存机制来提高服务 Pages 的性能和可靠性。可以通过更改缓存设置来修改缓存行为，但是，建议值是为您设置的，仅应在需要时进行修改。这些值的不正确配置可能会导致间歇性或持续性错误，或者 Pages daemon 提供旧内容。

NOTE:
到期、间隔和超时标志使用 [Golang 的持续时间格式](https://golang.org/pkg/time/#ParseDuration)。
持续时间字符串是一个可能有符号的十进制数字序列，每个数字都有可选的小数和单位后缀，例如 `300ms`、`1.5h` 或 `2h45m`。 有效的时间单位是 `ns`、`us`（或 `µs`）、`ms`、`s`、`m`、`h`。

示例：

- 增加 `gitlab_cache_expiry`，允许项目在缓存中存在更长时间。 如果 GitLab Pages 和 GitLab Rails 之间的通信不稳定，此设置可能很有用。

- 增加 `gitlab_cache_refresh` 会降低 GitLab Pages 从 GitLab Rails 请求域配置的频率。 GitLab Pages 向 GitLab API 生成太多请求时，此设置可能很有用，并且内容不会经常更改。

- 减小 `gitlab_cache_cleanup` 会更频繁地从缓存中删除过期项目，从而减少 Pages 节点的内存使用量。

- 减小 `gitlab_retrieval_timeout` 可以让您更快地停止对 GitLab Rails 的请求。增加它可以有更多时间从 API 接收响应，这在慢速网络环境中很有用。

- 减小 `gitlab_retrieval_interval` 会使对 API 的请求更加频繁，仅当 API 有错误响应时，例如连接超时。

- 减小 `gitlab_retrieval_retries` 会减少在报告错误之前尝试自动解析域配置的次数。

<!--
## 使用对象存储

> [Introduced](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/5577) in GitLab 13.6.

[阅读更多关于使用对象存储的文档](../object_storage.md).
-->

### 对象存储设置

设置如下：

- 在源安装实例的 `object_store:` 中，嵌套在 `pages:` 下。
- 在 Omnibus GitLab 安装实例中，以 `pages_object_store_` 为前缀。

| 设置 | 描述 | 默认值 |
|---------|-------------|---------|
| `enabled` | 是否启用对象存储。 | `false` |
| `remote_directory` | 存储 Pages 站点内容的存储桶的名称。 | |
| `connection` | 下文描述各种连接选项。 | |

<!--
NOTE:
If you want to stop using and disconnect the NFS server, you need to [explicitly disable
local storage](#disable-pages-local-storage), and it's only possible after upgrading to GitLab 13.11.
-->

#### S3 兼容的连接设置

<!--查看[不同提供商的可用连接设置](../object_storage.md#connection-settings)。-->

在 Omnibus 安装实例中：

1. 将以下内容添加到 `/etc/gitlab/gitlab.rb` 并将值替换为您想要的值：

   ```ruby
   gitlab_rails['pages_object_store_enabled'] = true
   gitlab_rails['pages_object_store_remote_directory'] = "pages"
   gitlab_rails['pages_object_store_connection'] = {
     'provider' => 'AWS',
     'region' => 'eu-central-1',
     'aws_access_key_id' => 'AWS_ACCESS_KEY_ID',
     'aws_secret_access_key' => 'AWS_SECRET_ACCESS_KEY'
   }
   ```

   如果您使用 AWS IAM 配置文件，请确保省略 AWS 访问密钥和 secret 访问键值对：

   ```ruby
   gitlab_rails['pages_object_store_connection'] = {
     'provider' => 'AWS',
     'region' => 'eu-central-1',
     'use_iam_profile' => true
   }
   ```

1. 保存文件并[重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

1. [将现有的 Pages 部署迁移到对象存储。](#将-pages-部署迁移到对象存储)

在源安装实例中：

1. 编辑 `/home/git/gitlab/config/gitlab.yml` 并添加或修改以下内容：

   ```yaml
   pages:
     object_store:
       enabled: true
       remote_directory: "pages" # The bucket name
       connection:
         provider: AWS # Only AWS supported at the moment
         aws_access_key_id: AWS_ACCESS_KEY_ID
         aws_secret_access_key: AWS_SECRET_ACCESS_KEY
         region: eu-central-1
   ```

1. 保存文件并[重启极狐GitLab](../restart_gitlab.md#源安装实例)，使更改生效。

1. [将现有的 Pages 部署迁移到对象存储。](#将-pages-部署迁移到对象存储)

## ZIP 存储

在 GitLab 14.0 中，GitLab Pages 的底层存储格式从直接存储在磁盘中的文件更改为每个项目的单个 ZIP archives。

这些 ZIP archives 可以本地存储在磁盘存储上，也可以存储在对象存储<!--[对象存储](#使用对象存储)-->上（如果已配置）。

每次更新 Pages 站点时，都会存储 ZIP archives.

### 将 legacy 存储迁移到 ZIP 存储

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/59003) in GitLab 13.11.
-->

当您升级到 13.11 或更高版本时，系统会尝试自动迁移 legacy 存储格式到新的基于 ZIP 的存储格式。
但是，某些项目可能由于不同原因无法迁移。 要验证所有项目都已成功迁移，您可以手动运行迁移：

```shell
gitlab-rake gitlab:pages:migrate_legacy_storage
```

中断此任务并多次运行它是安全的。

此任务可能报告的两个最常见的问题如下：

- `Missing public directory` 错误：

  ```txt
  E, [2021-04-09T13:11:52.534768 #911919] ERROR -- : project_id: 1 /home/vlad/gdk/gitlab/shared/pages/gitlab-org/gitlab-test failed to be migrated in 0.07 seconds: Archive not created. Missing public directory in /home/vlad/gdk/gitlab/shared/pages/gitlab-org/gitlab-test
  ```

  在这种情况下，您应该验证这些项目没有部署页面，并使用附加标志重新运行迁移以将这些项目标记为未使用 GitLab Pages 部署：

  ```shell
  sudo PAGES_MIGRATION_MARK_PROJECTS_AS_NOT_DEPLOYED=true gitlab-rake gitlab:pages:migrate_legacy_storage
  ```

- 文件 `is invalid` 错误：

  ```txt
  E, [2021-04-09T14:43:05.821767 #923322] ERROR -- : project_id: 1 /home/vlad/gdk/gitlab/shared/pages/gitlab-org/gitlab-test failed to be migrated: /home/vlad/gdk/gitlab/shared/pages/gitlab-org/gitlab-test/public/link is invalid, input_dir: /home/vlad/gdk/gitlab/shared/pages/gitlab-org/gitlab-test
  ```

此错误表示磁盘存储上的文件无效，最常见的情况时 symlinks 位于 `public` 目录之外。 您可以手动删除这些文件，或者在迁移过程中忽略它们：

  ```shell
  sudo PAGES_MIGRATION_IGNORE_INVALID_ENTRIES=true gitlab-rake gitlab:pages:migrate_legacy_storage
  ```

### 回滚 ZIP 迁移

如果发现迁移的数据无效，可以运行以下命令删除所有迁移的数据：

```shell
sudo gitlab-rake gitlab:pages:clean_migrated_zip_storage
```

这不会从旧磁盘存储中删除任何数据，并且 GitLab Pages daemon 会自动回退到使用它。

### 将 Pages 部署迁移到对象存储

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/325285) in GitLab 13.11.
-->

现有的 Pages 部署对象（存储[ZIP archive](#zip-存储)）可以类似地迁移到[对象存储](#使用对象存储)。

将您现有的 Pages 部署从本地存储迁移到对象存储：

```shell
sudo gitlab-rake gitlab:pages:deployments:migrate_to_object_storage
```

### 回滚 Pages 部署到本地存储

迁移到对象存储后，您可以选择将 Pages 部署移动到本地存储：

```shell
sudo gitlab-rake gitlab:pages:deployments:migrate_to_local
```

### 禁用 Pages 本地存储

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/301159) in GitLab 13.11.
-->

如果您使用[对象存储](#使用对象存储)，您可以禁用本地存储：

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['pages_local_store_enabled'] = false
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure) 使更改生效。

从 13.12 版本开始，该设置还禁用了 [legacy 存储](#将-legacy-存储迁移到-zip-存储)，因此如果您使用 NFS 来提供页面服务，则可以完全断开与它的连接。

## 为 14.0 版本准备 GitLab Pages


在 14.0 版本中引入了许多重大更改，可能需要一些用户干预。
以下步骤描述了在不导致 GitLab 实例停机的情况下进行迁移的最佳方式。

如果您在单个服务器上运行 GitLab 实例，那么升级到 14.0 后您很可能不会注意到任何问题，但无论如何按照这些步骤操作可能会更安全。如果您在任何时候遇到问题，请参阅 [故障排查部分](#故障排查)。

如果当前的版本低于 13.12，那么首先需要升级到13.12。在完成以下步骤之前，直接升级到 14.0 可能会导致托管在 GitLab Pages 上的某些网站停机。要将 GitLab Pages 迁移到 GitLab 14.0：

1. 设置 [`domain_config_source`为`gitlab`](#域名源配置)，这是 14.0 开始的默认设置。如果您已经在运行 14.0 或更高版本，请跳过此步骤。
1. 如果要将 Pages 内容存储在[对象存储](#使用对象存储)中，请务必进行配置。如果要在本地存储 Pages 内容或继续使用 NFS 服务器，请跳过此步骤。
1. [将 legacy 存储迁移到 ZIP 存储](#将-legacy-存储迁移到-zip-存储)。
1. 升级 GitLab 版本到 14.0。

<!--
## 备份

GitLab Pages 是[常规备份](../../raketasks/backup_restore.md)的一部分，因此无需配置单独的备份。
-->

## 安全

您应该积极考虑在于 GitLab 实例不同的主机名下运行 GitLab Pages，以防止 XSS 攻击。

### 速率限制

> 引入于 14.5 版本。

您可以强制执行源 IP 速率限制，以帮助最大程度地降低拒绝服务 (DoS) 攻击的风险。GitLab Pages 使用令牌桶算法来强制执行速率限制。 默认情况下，超过指定限制的请求会被报告但不会被拒绝。

源 IP 速率限制使用以下设置强制执行：

- `rate_limit_source_ip`：设置每秒请求数的最大阈值。设置为 0 以禁用此功能。
- `rate_limit_source_ip_burst`：设置初始请求爆发中允许的请求数量的最大阈值。
   例如，当您加载同时加载多个资源的网页时。

#### 启用源 IP 速率限制

1. 在 `/etc/gitlab/gitlab.rb` 中设置速率限制：

   ```ruby
   gitlab_pages['rate_limit_source_ip'] = 20.0
   gitlab_pages['rate_limit_source_ip_burst'] = 600
   ```

1. 要拒绝超过指定限制的请求，请在 `/etc/gitlab/gitlab.rb` 中启用 `FF_ENABLE_RATE_LIMITER` 功能标志：

   ```ruby
   gitlab_pages['env'] = {'FF_ENABLE_RATE_LIMITER' => 'true'}
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->

## 故障排查

### 如何查看 GitLab Pages 日志

运行以下命令查看 Pages daemon 日志：

```shell
sudo gitlab-ctl tail gitlab-pages
```

您也可以在 `/var/log/gitlab/gitlab-pages/current` 路径下找到日志文件。

### `open /etc/ssl/ca-bundle.pem: permission denied`

WARNING:
此问题在 14.3 及更高版本中已修复，首先尝试升级。

GitLab Pages 在 `chroot` jail 中运行，通常在一个唯一编号的目录中，例如 `/tmp/gitlab-pages-*`。

`/etc/ssl/ca-bundle.pem` 提供一系列可信证书。作为启动 Pages 的一部分，从 `/opt/gitlab/embedded/ssl/certs/cacert.pem` 复制证书。

如果源文件的权限不正确（它们应该是 `0644`），那么 `chroot` jail 中的文件也是错误的。

Pages 在 `/var/log/gitlab/gitlab-pages/current` 中记录错误，例如：

```plaintext
x509: failed to load system roots and no roots provided
open /etc/ssl/ca-bundle.pem: permission denied
```

使用 `chroot` jail 会使这个错误产生误导，因为它不是指根文件系统上的 `/etc/ssl`。

修复方法是更正源文件权限并重新启动 Pages：

```shell
sudo chmod 644 /opt/gitlab/embedded/ssl/certs/cacert.pem
sudo gitlab-ctl restart gitlab-pages
```

### `dial tcp: lookup gitlab.example.com` 和 `x509: certificate signed by unknown authority`

WARNING:
此问题在 14.3 及更高版本中已修复，首先尝试升级。

当同时设置 `inplace_chroot` 和 `access_control` 为 `true`，您可能会遇到以下错误：

```plaintext
dial tcp: lookup gitlab.example.com on [::1]:53: dial udp [::1]:53: connect: cannot assign requested address
```

或者：

```plaintext
open /opt/gitlab/embedded/ssl/certs/cacert.pem: no such file or directory
x509: certificate signed by unknown authority
```

出现这些错误的原因是 `chroot` 中缺少文件 `resolv.conf`、`/etc/hosts/`、`/etc/nsswitch.conf` 和 `ca-bundle.pem`。 修复方法是在 `chroot` 内复制这些文件：

```shell
sudo mkdir -p /var/opt/gitlab/gitlab-rails/shared/pages/etc/ssl
sudo cp /etc/resolv.conf /var/opt/gitlab/gitlab-rails/shared/pages/etc/
sudo cp /etc/hosts /var/opt/gitlab/gitlab-rails/shared/pages/etc/
sudo cp /etc/nsswitch.conf /var/opt/gitlab/gitlab-rails/shared/pages/etc/
sudo cp /etc/resolv.conf /var/opt/gitlab/gitlab-rails/shared/pages/etc
sudo cp /opt/gitlab/embedded/ssl/certs/cacert.pem /var/opt/gitlab/gitlab-rails/shared/pages/opt/gitlab/embedded/ssl/certs/
sudo cp /opt/gitlab/embedded/ssl/certs/cacert.pem /var/opt/gitlab/gitlab-rails/shared/pages/etc/ssl/ca-bundle.pem
```

### `unsupported protocol scheme \"\""`

如果您查看到类似以下错误：

```plaintext
{"error":"failed to connect to internal Pages API: Get \"/api/v4/internal/pages/status\": unsupported protocol scheme \"\"","level":"warning","msg":"attempted to connect to the API","time":"2021-06-23T20:03:30Z"}
```

这意味着您没有在 Pages 服务器设置中，设置 HTTP(S) 协议方案。
要修复它：

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_pages['gitlab_server'] = "https://<your_pages_domain_name>"
   gitlab_pages['internal_gitlab_server'] = "https://<your_pages_domain_name>"
   ```

1. 重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

### 当服务器不通过 IPv6 侦听时，连接到 GitLab Pages 代理时出现 502 错误

在某些情况下，即使服务器不通过 IPv6 侦听，NGINX 也可能默认使用 IPv6 连接到 GitLab Pages 服务。 如果您在“gitlab_pages_error.log”中看到类似于以下日志条目的内容，则可以确定何时发生这种情况：

```plaintext
2020/02/24 16:32:05 [error] 112654#0: *4982804 connect() failed (111: Connection refused) while connecting to upstream, client: 123.123.123.123, server: ~^(?<group>.*)\.pages\.example\.com$, request: "GET /-/group/project/-/jobs/1234/artifacts/artifact.txt HTTP/1.1", upstream: "http://[::1]:8090//-/group/project/-/jobs/1234/artifacts/artifact.txt", host: "group.example.com"
```

要解决此问题，请为 GitLab Pages `listen_proxy` 设置显式 IP 和端口，以定义 GitLab Pages daemon 应侦听的显式地址：

```ruby
gitlab_pages['listen_proxy'] = '127.0.0.1:8090'
```

### Intermittent 502 errors or after a few days

如果您在使用 `systemd` 和 [`tmpfiles.d`](https://www.freedesktop.org/software/systemd/man/tmpfiles.d.html) 的系统上运行 Pages，您可能会遇到类似以下的间歇性 502 错误：

```plaintext
dial tcp: lookup gitlab.example.com on [::1]:53: dial udp [::1]:53: connect: no route to host"
```

GitLab Pages 在 `/tmp/gitlab-pages-*` 内创建了一个 [bind mount](https://man7.org/linux/man-pages/man8/mount.8.html)，其中包含像 `/etc/hosts` 的文件。 然而，`systemd` 可能会定期清理 `/tmp/` 目录，因此 DNS 配置可能会丢失。

要阻止 `systemd` 清理页面相关内容：

1. 使 `tmpfiles.d` 不要移除 Pages 的 `/tmp` 目录：

   ```shell
   echo 'x /tmp/gitlab-pages-*' >> /etc/tmpfiles.d/gitlab-pages-jail.conf
   ```

1. 重启 GitLab Pages：

   ```shell
   sudo gitlab-ctl restart gitlab-pages
   ```

### 将项目转移到不同的组或用户，或更改项目路径后出现 404 错误

如果您在将项目转移到另一个组或用户，或更改项目路径后，在 Pages 站点上遇到 `404 Not Found` 错误，则必须触发 Pages 的域名配置更新。为此，请在 `.update` 文件中写入一些内容。Pages daemon 监视此文件的更改，并在发生更改时重新加载配置。

使用此示例修复传输项目或使用 Pages 更改项目路径后的 `404 Not Found` 错误：

```shell
date > /var/opt/gitlab/gitlab-rails/shared/pages/.update
```

如果您自定义了 Pages 存储路径，请调整上面的命令以使用您的自定义路径。

### 将 Geo 辅助节点提升为主节点后出现 404 错误

这是因为 Pages 文件不在支持的数据类型<!--[支持的数据类型](../geo/replication/datatypes.md#limitations-on-replicationverification)-->之内。

可以将 [Pages 路径](#更改存储路径)中的子文件夹和文件复制到新的主节点来解决此问题。
<!--例如，您可以调整[移动存储库文档](../operations/moving_repositories.md) 中的 `rsync` 策略。-->
或者，再次运行那些包含 `页面` 作业的项目的 CI 流水线。

## 404 or 500 error when accessing GitLab Pages in a Geo setup

Pages 站点仅在主 Geo 站点上可用，而项目的代码库在所有站点上可用。

如果您尝试访问次要站点上的 Pages 页面，您将得到 404 或 500 HTTP 代码，具体取决于访问控制。

<!--
Read more which [features don't support Geo replication/verification](../geo/replication/datatypes.md#limitations-on-replicationverification).
-->

### 无法连接到内部 GitLab API

如果您查看到以下错误：

```plaintext
ERRO[0010] Failed to connect to the internal GitLab API after 0.50s  error="failed to connect to internal Pages API: HTTP status: 401"
```

如果您[在单独的服务器上运行 GitLab Pages](#在单独的服务器上运行-gitlab-pages)，您必须按文档所述，从 **GitLab 服务器** 复制 `/etc/gitlab/gitlab-secrets.json` 文件到 **Pages 服务器**。

其它原因可能包括您的 **GitLab 服务器** 和 **Pages 服务器** 之间的网络连接问题，例如防火墙配置或关闭的端口。
例如存在连接超时：

```plaintext
error="failed to connect to internal Pages API: Get \"https://gitlab.example.com:3000/api/v4/internal/pages/status\": net/http: request canceled while waiting for connection (Client.Timeout exceeded while awaiting headers)"
```

### Pages 无法与 GitLab API 的实例通信

如果您使用 `domain_config_source=auto` 的默认值并运行 GitLab Pages 的多个实例，您可能会在提供 Pages 内容时看到间歇性的 502 错误响应。 您可能还会在页面日志中看到以下警告：

```plaintext
WARN[0010] Pages cannot communicate with an instance of the GitLab API. Please sync your gitlab-secrets.json file https://gitlab.com/gitlab-org/gitlab-pages/-/issues/535#workaround. error="pages endpoint unauthorized"
```

如果您的 `gitlab-secrets.json` 文件在 GitLab Rails 和 GitLab Pages 之间过期，就会发生这种情况。在所有 GitLab Pages 实例中，按照[在单独的服务器上运行 GitLab Pages](#在单独的服务器上运行-gitlab-pages) 的步骤 8-10 j进行操作。

### 500 错误 `securecookie: failed to generate random iv` 和 `Failed to save the session`

此问题可能是由过时的操作系统<!--[过时的操作系统](https://docs.gitlab.cn/omnibus/package-information/deprecated_os.html)-->引起。
Pages daemon 使用 `securecookie` 库通过 [`crypto/rand` in Go](https://golang.org/pkg/crypto/rand/#pkg-variables) 获取随机字符串。
这需要 `getrandom` 系统调用或 `/dev/urandom` 在主机操作系统上可用。建议升级到[支持的操作系统](https://about.gitlab.cn/install/)。

### 请求的范围无效、格式错误或未知

这个问题来自 GitLab Pages OAuth 应用程序的权限。要修复它：

1. 在顶部导航栏，选择 **菜单 > **管理**。
2. 在左侧导航栏，选择 **应用 > GitLab Pages**。
3. 编辑应用程序。
4. 在**范围**下方，确保选择了 `api` 范围。
5. 保存您的更改。

运行[单独的 Pages 服务器](#在单独的服务器上运行-gitlab-pages) 时，此设置需要在主 GitLab 服务器上进行配置。

### 无法设置通配符 DNS 条目的解决方法

如果无法达到通配符 DNS 的[前提条件](#前提条件)，您仍然可以以有限的方式使用 GitLab Pages。

1. 将您需要使用 Pages 的所有项目移动<!--[移动](../../user/project/settings/index.md#transferring-an-existing-project-into-another-namespace)-->到单个组命名空间中，例如 `pages`。
1. 配置不带 `*.`-通配符的 [DNS 条目](#dns-配置)，例如 `pages.example.io`。
1. 在你的 `gitlab.rb` 文件中配置 `pages_external_url http://example.io/`。 在此处省略组命名空间，因为系统会自动添加它。

### Pages daemon 因权限被拒绝错误而失败

如果 `/tmp` 与 `noexec` 一起挂载，Pages daemon 将无法启动，并显示如下错误：

```plaintext
{"error":"fork/exec /gitlab-pages: permission denied","level":"fatal","msg":"could not create pages daemon","time":"2021-02-02T21:54:34Z"}
```

在这种情况下，将 `TMPDIR` 更改为未使用 `noexec` 挂载的位置。将以下内容添加到`/etc/gitlab/gitlab.rb`：

```ruby
gitlab_pages['env'] = {'TMPDIR' => '<new_tmp_path>'}
```

添加后，使用 `sudo gitlab-ctl reconfigure` 重新配置并使用 `sudo gitlab-ctl restart` 重新启动 GitLab。

### 使用 Pages 访问控制时的 `The redirect URI included is not valid.` 错误

验证 GitLab Pages System OAuth 应用程序<!--[System OAuth 应用程序](../../integration/oauth_provider.md#instance-wide-applications)--> 中的 **Callback URL**/Redirect URI 使用的协议（HTTP 或 HTTPS），是在 `pages_external_url` 中配置使用的。

### 500 错误 `cannot serve from disk`

如果您从 Pages 收到 500 响应并遇到类似于以下内容的错误：

```plaintext
ERRO[0145] cannot serve from disk                        error="gitlab: disk access is disabled via enable-disk=false" project_id=27 source_path="file:///shared/pages/@hashed/67/06/670671cd97404156226e507973f2ab8330d3022ca96e0c93bdbdb320c41adcaf/pages_deployments/14/artifacts.zip" source_type=zip
```

这意味着 GitLab Rails 使得 GitLab Pages 从磁盘上的某个位置提供内容，但是，GitLab Pages 被配置为禁用磁盘访问。

要启用磁盘访问：

1. 在 `/etc/gitlab/gitlab.rb` 中为 GitLab Pages 启用磁盘访问：

   ```ruby
   gitlab_pages['enable_disk'] = true
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)。

### `httprange: new resource 403`

如果您查看到类似以下错误：

```plaintext
{"error":"httprange: new resource 403: \"403 Forbidden\"","host":"root.pages.example.com","level":"error","msg":"vfs.Root","path":"/pages1/","time":"2021-06-10T08:45:19Z"}
```
并且您通过 NFS 在单独的服务器同步文件上运行页面，这可能意味着共享页面目录安装在主 GitLab 服务器和 GitLab Pages 服务器上的不同路径上。

在这种情况下，强烈建议您配置[对象存储并将任何现有 pages 数据迁移到其上](#使用对象存储)。

或者，您可以将 GitLab Pages 共享目录挂载到两台服务器上的相同路径。

### 升级到 14.0 或以上版本后 GitLab Pages 不工作

14.0 对 GitLab Pages 引入了许多可能需要手动干预的更改。

1. 首先[遵循迁移指南](#为-140-版本准备-gitlab-pages)。
1. 尝试升级到 14.3 或更高版本，一些问题已在 14.1、14.2  和 14.3 版本中修复
1. 如果无法正常运行，查看 [GitLab Pages 日志](#如何查看-gitlab-pages-日志)，如果您看到任何错误，请在此页面上搜索它们。

WARNING:
在 14.0-14.2 版本中，您可以临时启用 legacy 存储和配置机制。

<!--
要做到这一点：

1. Please describe the issue you're seeing in [here](https://gitlab.com/gitlab-org/gitlab/-/issues/331699).

1. Edit `/etc/gitlab/gitlab.rb`:

   ```ruby
   gitlab_pages['use_legacy_storage'] = true
   ```

1. [Reconfigure GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure).
-->

### GitLab Pages 无法在 Docker 容器中启动

WARNING:
此问题已在 GitLab 14.3 及更高版本中修复，请先尝试升级。

GitLab Pages 守护进程在 Docker 容器中运行时没有绑定挂载的权限。为了克服这个问题，必须改变 `chroot` 行为：

1. 编辑 `/etc/gitlab/gitlab.rb`。
1. 将 GitLab Pages 的 `inplace_chroot` 设置为 `true`：

   ```ruby
   gitlab_pages['inplace_chroot'] = true
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)。

NOTE:
`inplace_chroot` 选项可能不适用于其他功能，例如 [Pages 访问控制](#访问控制)。
<!--
The [GitLab Pages README](https://gitlab.com/gitlab-org/gitlab-pages#caveats) has more information about caveats and workarounds.
-->
