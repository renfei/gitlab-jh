---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 上传文件迁移 Rake 任务 **(FREE SELF)**

有一个用于在不同存储类型之间迁移上传的 Rake 任务。

- 使用 [`gitlab:uploads:migrate:all`](#多合一的-rake-任务) 或
- 只迁移特定的上传类型，使用 [`gitlab:uploads:migrate`](#单独的-rake-任务)。

## 迁移到对象存储

为上传文件[配置对象存储](../../uploads.md#使用对象存储)后，使用此任务将现有上传文件从本地存储迁移到远程存储。

所有处理都在后台 worker 中完成，并且**无停机时间**。

<!--阅读更多关于使用[对象存储与 GitLab](../../object_storage.md)的文档。-->

### 多合一的 Rake 任务

GitLab 提供了一个包装器 Rake 任务，可以一步将所有上传的文件（例如头像、徽标、附件和网站图标）迁移到对象存储。包装器任务调用单独的 Rake 任务来逐一迁移属于这些类别中的每一个文件。

[单独的 Rake 任务](#单独的-rake-任务)文档在下一部分。

要将所有上传文件从本地存储迁移到对象存储，请运行：

**Omnibus 安装实例**

```shell
gitlab-rake "gitlab:uploads:migrate:all"
```

**源安装实例**

```shell
sudo RAILS_ENV=production -u git -H bundle exec rake gitlab:uploads:migrate:all
```

### 单独的 Rake 任务

如果您已运行[多合一的 Rake 任务](#多合一的-rake-任务)，无需运行以下单独的任务。

Rake 任务使用三个参数来查找要迁移的上传文件：

| 参数        | 类型          | 描述                                            |
|:-----------------|:--------------|:-------------------------------------------------------|
| `uploader_class` | string        | 要从中迁移的上传者的类型。                  |
| `model_class`    | string        | 要从中迁移的模型的类型。                     |
| `mount_point`    | string/symbol | 上传器所挂载的模型列的名称。|

NOTE:
这些参数主要是 GitLab 结构的内部参数，您可能需要参考下面的任务列表。

此任务还接受一个环境变量，您可以使用它来覆盖默认批量大小：

| 变量 | 类型    | 描述                                     |
|:---------|:--------|:--------------------------------------------------|
| `BATCH`  | integer | 指定批次的大小。默认为 200。 |

下面显示了如何为各种上传类型运行 `gitlab:uploads:migrate`。

**Omnibus 安装实例**

```shell
# gitlab-rake gitlab:uploads:migrate[uploader_class, model_class, mount_point]

# Avatars
gitlab-rake "gitlab:uploads:migrate[AvatarUploader, Project, :avatar]"
gitlab-rake "gitlab:uploads:migrate[AvatarUploader, Group, :avatar]"
gitlab-rake "gitlab:uploads:migrate[AvatarUploader, User, :avatar]"

# Attachments
gitlab-rake "gitlab:uploads:migrate[AttachmentUploader, Note, :attachment]"
gitlab-rake "gitlab:uploads:migrate[AttachmentUploader, Appearance, :logo]"
gitlab-rake "gitlab:uploads:migrate[AttachmentUploader, Appearance, :header_logo]"

# Favicon
gitlab-rake "gitlab:uploads:migrate[FaviconUploader, Appearance, :favicon]"

# Markdown
gitlab-rake "gitlab:uploads:migrate[FileUploader, Project]"
gitlab-rake "gitlab:uploads:migrate[PersonalFileUploader, Snippet]"
gitlab-rake "gitlab:uploads:migrate[NamespaceFileUploader, Snippet]"
gitlab-rake "gitlab:uploads:migrate[FileUploader, MergeRequest]"

# Design Management design thumbnails
gitlab-rake "gitlab:uploads:migrate[DesignManagement::DesignV432x230Uploader, DesignManagement::Action, :image_v432x230]"
```

**源安装实例**

对每个任务使用 `RAILS_ENV=production`。

```shell
# sudo -u git -H bundle exec rake gitlab:uploads:migrate

# Avatars
sudo -u git -H bundle exec rake "gitlab:uploads:migrate[AvatarUploader, Project, :avatar]"
sudo -u git -H bundle exec rake "gitlab:uploads:migrate[AvatarUploader, Group, :avatar]"
sudo -u git -H bundle exec rake "gitlab:uploads:migrate[AvatarUploader, User, :avatar]"

# Attachments
sudo -u git -H bundle exec rake "gitlab:uploads:migrate[AttachmentUploader, Note, :attachment]"
sudo -u git -H bundle exec rake "gitlab:uploads:migrate[AttachmentUploader, Appearance, :logo]"
sudo -u git -H bundle exec rake "gitlab:uploads:migrate[AttachmentUploader, Appearance, :header_logo]"

# Favicon
sudo -u git -H bundle exec rake "gitlab:uploads:migrate[FaviconUploader, Appearance, :favicon]"

# Markdown
sudo -u git -H bundle exec rake "gitlab:uploads:migrate[FileUploader, Project]"
sudo -u git -H bundle exec rake "gitlab:uploads:migrate[PersonalFileUploader, Snippet]"
sudo -u git -H bundle exec rake "gitlab:uploads:migrate[NamespaceFileUploader, Snippet]"
sudo -u git -H bundle exec rake "gitlab:uploads:migrate[FileUploader, MergeRequest]"

# Design Management design thumbnails
sudo -u git -H bundle exec rake "gitlab:uploads:migrate[DesignManagement::DesignV432x230Uploader, DesignManagement::Action]"
```

## 迁移到本地存储

如果您出于任何原因需要禁用对象存储<!--[对象存储](../../object_storage.md)-->，您必须首先将数据从对象存储迁移回本地存储。

WARNING:
**需要延长停机时间**，因此迁移期间不会在对象存储中创建新文件。
<!--A configuration setting to allow migrating
from object storage to local files with only a brief moment of downtime for configuration changes
is tracked [in this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/30979).-->

### 多合一的 Rake 任务

GitLab 提供了一个包装器 Rake 任务，可以一步将所有上传的文件（例如，头像、徽标、附件和网站图标）迁移到本地存储。包装器任务调用单独的 Rake 任务来逐一迁移属于这些类别中的每一个的文件。

有关这些 Rake 任务的详细信息，请参阅 [单独的 Rake 任务](#单独的-rake-任务)，记住本例中的任务名称是 `gitlab:uploads:migrate_to_local`。

要将上传文件从对象存储迁移到本地存储：

1. 在 `gitlab.rb` 的 `uploads` 设置下禁用 `direct_upload` 和 `background_upload`：

   ```ruby
   gitlab_rails['uploads_object_store_direct_upload'] = false
   gitlab_rails['uploads_object_store_background_upload'] = false
   ```

   保存文件并[重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure)。

1. 运行 Rake 任务：

   **Omnibus 安装实例**

   ```shell
   gitlab-rake "gitlab:uploads:migrate_to_local:all"
   ```

   **源安装实例**

   ```shell
   sudo RAILS_ENV=production -u git -H bundle exec rake gitlab:uploads:migrate_to_local:all
   ```

运行 Rake 任务后，您可以通过撤消[配置对象存储](../../uploads.md#使用对象存储)说明中描述的更改来禁用对象存储。
