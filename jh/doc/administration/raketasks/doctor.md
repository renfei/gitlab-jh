---
stage: Enablement
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Doctor Rake 任务 **(FREE SELF)**

这是一组可帮助调查和修复由数据完整性问题引起的问题的任务。

## 验证数据库值是否可以使用当前 secret 解密

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/20069) in GitLab 13.1.
-->

此任务遍历数据库中所有可能的加密值，验证它们是否可以使用当前的 secret 文件 (`gitlab-secrets.json`) 解密。

尚未实现自动解析。如果您有无法解密的值，您可以按照步骤重置它们<!--，请参阅我们的文档，了解[当 secret 文件丢失时](../../raketasks/backup_restore.md#当-secret-文件丢失时)该怎么办-->。

这可能需要很长时间，具体取决于数据库的大小，因为它会检查所有表中的所有行。

**Omnibus 安装实例**

```shell
sudo gitlab-rake gitlab:doctor:secrets
```

**源安装实例**

```shell
bundle exec rake gitlab:doctor:secrets RAILS_ENV=production
```

**示例输出**

```plaintext
I, [2020-06-11T17:17:54.951815 #27148]  INFO -- : Checking encrypted values in the database
I, [2020-06-11T17:18:12.677708 #27148]  INFO -- : - ApplicationSetting failures: 0
I, [2020-06-11T17:18:12.823692 #27148]  INFO -- : - User failures: 0
[...] other models possibly containing encrypted data
I, [2020-06-11T17:18:14.938335 #27148]  INFO -- : - Group failures: 1
I, [2020-06-11T17:18:15.559162 #27148]  INFO -- : - Operations::FeatureFlagsClient failures: 0
I, [2020-06-11T17:18:15.575533 #27148]  INFO -- : - ScimOauthAccessToken failures: 0
I, [2020-06-11T17:18:15.575678 #27148]  INFO -- : Total: 1 row(s) affected
I, [2020-06-11T17:18:15.575711 #27148]  INFO -- : Done!
```

### 详细模式

要获取有关无法解密的行和列的更多详细信息，您可以传递一个 `VERBOSE` 环境变量：

**Omnibus 安装实例**

```shell
sudo gitlab-rake gitlab:doctor:secrets VERBOSE=1
```

**源安装实例**

```shell
bundle exec rake gitlab:doctor:secrets RAILS_ENV=production VERBOSE=1
```

**示例详细输出**

<!-- vale gitlab.SentenceSpacing = NO -->

```plaintext
I, [2020-06-11T17:17:54.951815 #27148]  INFO -- : Checking encrypted values in the database
I, [2020-06-11T17:18:12.677708 #27148]  INFO -- : - ApplicationSetting failures: 0
I, [2020-06-11T17:18:12.823692 #27148]  INFO -- : - User failures: 0
[...] other models possibly containing encrypted data
D, [2020-06-11T17:19:53.224344 #27351] DEBUG -- : > Something went wrong for Group[10].runners_token: Validation failed: Route can't be blank
I, [2020-06-11T17:19:53.225178 #27351]  INFO -- : - Group failures: 1
D, [2020-06-11T17:19:53.225267 #27351] DEBUG -- :   - Group[10]: runners_token
I, [2020-06-11T17:18:15.559162 #27148]  INFO -- : - Operations::FeatureFlagsClient failures: 0
I, [2020-06-11T17:18:15.575533 #27148]  INFO -- : - ScimOauthAccessToken failures: 0
I, [2020-06-11T17:18:15.575678 #27148]  INFO -- : Total: 1 row(s) affected
I, [2020-06-11T17:18:15.575711 #27148]  INFO -- : Done!
```

<!-- vale gitlab.SentenceSpacing = YES -->
