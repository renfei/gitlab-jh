---
disqus_identifier: 'https://docs.gitlab.com/ee/workflow/todos.html'
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 待办事项列表 **(FREE)**

您的*待办事项列表*是等待您输入的按时间顺序排列的项目列表。
这些项目被称为*待办事项*。

您可以使用待办事项列表来跟踪与以下相关的[操作](#创建待办事项的操作)：

- [议题](project/issues/index.md)
- [合并请求](project/merge_requests/index.md)
- [史诗](group/epics/index.md)
- [设计](project/issues/design_management.md)

## 访问待办事项列表

要访问您的待办事项列表：

在顶部栏的右上角，选择待办事项列表 (**{task-done}**)。

## 创建待办事项的操作

许多待办事项是自动创建的。
在以下情况下，待办事项会添加到您的待办事项列表中：

- 向您分配了一个议题或合并请求。
- 您在议题、合并请求或史诗的描述或评论中被[提及](project/issues/issue_data_and_actions.md#提及)。
- 在提交或设计的评论中提到了您。
- 合并请求的 CI/CD 流水线失败。
- 由于冲突，无法合并打开的合并请求，并且存在以下情况之一：
  - 您是作者。
  - 您是将合并请求设置为在流水线成功后自动合并的用户。
- 在 13.2 及更高版本，合并请求从合并队列<!--[合并队列](../ci/pipelines/merge_trains.md)-->删除，并且您是添加它的用户。

当同一用户在同一对象上发生多个操作时，系统会将第一个操作显示为单个待办事项。

待办事项不受[通知电子邮件设置](profile/notifications.md)的影响。

## 创建待办事项

您可以手动将项目添加到您的待办事项列表。

1. 转到您的：

    - [议题](project/issues/index.md)
    - [合并请求](project/merge_requests/index.md)
    - [史诗](group/epics/index.md)
    - [设计](project/issues/design_management.md)

1. 在右侧边栏的顶部，选择 **添加待办事项**。

   ![Adding a to-do item from the issuable sidebar](img/todos_add_todo_sidebar_v14_1.png)

## 通过直接@某人来创建待办事项

您可以通过在行首直接@某人来创建待办事项。
例如，在以下评论中：

```markdown
@alice What do you think? cc: @bob

- @carol can you please have a look?

>>>
@dan what do you think?
>>>

@erin @frank thank you!
```

收到待办事项的人是 `@alice`、`@erin` 和 `@frank`。

要查看直接针对用户的待办事项，请转到待办事项列表，然后从 **操作** 过滤器中选择 **直接 @**。

多次提及用户只会创建一个待办事项。

## 将待办事项标记为已完成的操作

对议题、合并请求或史诗的任何操作都将其相应的待办事项标记为已完成。

关闭待办事项的操作包括：

- 更改指派人
- 更改里程碑
- 关闭议题或合并请求
- 添加或删除标记
- 评论议题
- 解决[设计讨论主题](project/issues/design_management.md#解决设计主题)

如果其他人关闭、合并或对议题、合并请求或史诗采取行动，您的待办事项仍处于待处理状态。

## 将待办事项标记为已完成

您可以手动将待办事项标记为已完成。

有两种方法可以做到这一点：

- 在待办事项列表的待办事项右侧，选择 **完成**。
- 在议题、合并请求或史诗的侧边栏中，选择 **标记为完成**。

  ![Mark as done from the sidebar](img/todos_mark_done_sidebar_v14_1.png)

## 将所有待办事项标记为已完成

您可以同时将所有待办事项标记为已完成。

在待办事项列表的右上角，选择**全部标记为已完成**。

## 当用户的访问权限发生变化时，用户的待办事项列表如何受到影响

出于安全原因，当用户不再有权访问相关资源时，系统会删除待办事项。
例如，如果用户不再有权访问议题、合并请求、史诗、项目或群组，系统会删除相关的待办事项。

此过程发生在他们的访问权限更改后的一小时内。删除被延迟以防止数据丢失，防止用户的访问被意外撤销。
