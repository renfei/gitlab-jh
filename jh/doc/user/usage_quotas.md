---
type: howto
stage: Fulfillment
group: Utilization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers
---

# 存储使用配额 **(FREE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/13294) in GitLab 12.0.
> - Moved to GitLab Free.
-->

项目仓库的免费存储配额为 2 GB。当项目仓库达到存储配额时会被锁定，您无法将更改推送到锁定的项目。要监控命名空间中每个仓库的大小，包括每个项目的细分，您可以[查看存储使用情况](#查看存储使用情况)。要允许项目仓库的存储超出免费配额，您必须购买额外的存储空间。更多详情，请参见[超额存储使用](#超额存储使用)。

## 查看存储使用情况

您可以查看项目或命名空间<!--[命名空间](../user/group/#namespaces)-->的存储使用情况。

1. 转到您的项目或命名空间：
    - 对于项目，在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
    - 对于命名空间，请在浏览器的工具栏中输入 URL。
1. 从左侧边栏中，选择 **设置 > 使用量配额**。
1. 选择 **存储** 选项卡。

显示统计信息。选择任何标题以查看详细信息。此页面上的信息每 90 分钟更新一次。

如果您的命名空间显示 `N/A`，请将提交推送到命名空间中的任何项目以重新计算存储。

## 存储使用统计

> - 项目级图表引入于 14.4 版本，功能标志为 `project_storage_ui`。默认禁用。
> - 于 14.5 版本支持自助管理版。
> - 功能标志移除于 14.5 版本。

<!--
> - Enabled on GitLab.com in GitLab 14.4.
-->

以下存储使用统计信息可供所有者使用：

- 已使用的命名空间总存储量：此命名空间中跨项目使用的总存储量。
- 已使用的超额存储总量：已使用的存储总量超过其分配的存储量。
- 已购买的可用存储空间：已购买但尚未使用的总存储空间。

## 超额存储使用

超额存储使用量是项目仓库超出免费存储配额的使用量。如果没有购买的存储可用，则项目将被锁定。您无法将更改推送到锁定的项目。
要解锁项目，您必须为命名空间购买更多存储空间。购买完成后，锁定的项目会自动解锁。购买的可用存储量必须始终大于零。

**使用量配额**页面的**存储**选项卡可能会警告您以下信息：

- Purchased storage available is running low.（购买的可用存储空间不足。）
- Projects that are at risk of being locked if purchased storage available is zero.（如果购买的可用存储空间为 0，项目可能有被锁定的风险。）
- Projects that are locked because purchased storage available is zero. Locked projects are marked with an information icon (**{information-o}**) beside their name.（由于购买的可用存储空间为 0，项目被锁定。对于锁定的项目，在其名称旁边显示 (**{information-o}**) 图标。）

### 超额存储示例

以下示例描述了命名空间 *Example Company* 的超额存储场景：

| 仓库 | 已使用的存储 | 超额存储 | 配额  | 状态            |
|------------|--------------|----------------|--------|-------------------|
| Red        | 2 GB        | 0 GB           | 2 GB  | 锁定 **{lock}** |
| Blue       | 1 GB         | 0 GB           | 2 GB  | 未锁定        |
| Green      | 2 GB        | 0 GB           | 2 GB  | 锁定 **{lock}** |
| Yellow     | 1 GB         | 0 GB           | 2 GB  | 未锁定        |
| **总数** | **6 GB**    | **0 GB**       | -      | -                 |

Red 和 Green 项目被锁定，因为它们的仓库存储已达到配额。在此示例中，尚未购买额外的存储空间。

要解锁 Red 和 Green 项目，需要购买 50 GB 的额外存储空间。

假设 Red 和 Green 项目的仓库增长超过 10 GB 配额，则购买存储的可用量会减少。所有项目保持解锁状态，因为 40 GB 购买的存储可用：50 GB（购买的存储）- 10 GB（使用的超额存储）。

| 仓库 | 已使用的存储 | 超额存储 | 配额  | 状态            |
|------------|--------------|----------------|---------|-------------------|
| Red        | 7 GB        | 5 GB           | 2 GB   | 未锁定        |
| Blue       | 6 GB        | 4 GB           | 2 GB   | 未锁定        |
| Green      | 3 GB        | 1 GB           | 2 GB   | 未锁定        |
| Yellow     | 2 GB         | 0 GB           | 2 GB   | 未锁定        |
| **Totals** | **18 GB**    | **10 GB**      | -       | -                 |
