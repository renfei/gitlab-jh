---
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 双重验证 **(FREE)**

双因素身份验证 (2FA) 为您的帐户提供了额外的安全级别。启用后，除了提供用于登录的用户名和密码外，系统还会提示您输入由一次性密码验证器（例如，您的一台设备上的密码管理器）生成的代码。

通过启用 2FA，除您之外的其他人可以登录您的帐户的唯一方法是知道您的用户名和密码*并且*可以访问您的一次性密码 secret。

## 概览

NOTE:
当您启用 2FA 时，请不要忘记备份您的[恢复码](#恢复码)！

除了基于时间的一次性密码 (TOTP)，还支持 U2F（universal 2nd factor）和 WebAuthn（实验性）设备作为身份验证的第二因素。启用后，除了提供您的用户名和密码进行登录外，系统还会提示您激活代表您执行安全身份验证的 U2F/WebAuthn 设备（通常通过按下设备上的按钮）。

强烈建议您使用[一次性密码验证器](#一次性密码)，或使用 [FortiAuthenticator](#fortiauthenticator-一次性密码) 和 [U2F 设备](#u2f-设备) 或 [WebAuthn 设备](#webauthn-设备)，因此如果您丢失 U2F / WebAuthn 设备，您仍然可以访问您的帐户。

## 启用 2FA

> - 帐户邮箱确认要求引入于 14.3。部署在 `ensure_verified_primary_email_for_2fa` 功能标志后，默认启用。
> - 帐户邮箱确认要求可用，功能标志 `ensure_verified_primary_email_for_2fa` 移除于 14.4。

有多种方法可以启用双因素身份验证 (2FA)：

- 使用一次性密码验证器。
- 使用 U2F / WebAuthn 设备。

在 14.3 和更高版本，必须确认您的帐户电子邮件才能启用双重身份验证。

### 一次性密码

要启用 2FA：

1. **在极狐GitLab 中：**
    1. 登录您的帐户。
    1. 转到您的[**用户设置**](../index.md#访问您的用户设置)。
    1. 进入 **帐户**。
    1. 选择 **启用双重认证**。
1. **在您的设备（通常是您的手机）上：**
   1. 安装兼容的应用程序，例如：
      - [Authy](https://authy.com/)
      - [Duo Mobile](https://duo.com/product/multi-factor-authentication-mfa/duo-mobile-app)
      - [LastPass Authenticator](https://lastpass.com/auth/)
      - [Authenticator](https://mattrubin.me/authenticator/)
      - [andOTP](https://github.com/andOTP/andOTP)
      - [Google Authenticator](https://support.google.com/accounts/answer/1066447?hl=en)
      - [Microsoft Authenticator](https://www.microsoft.com/en-us/account/authenticator)
      - [SailOTP](https://openrepos.net/content/seiichiro0185/sailotp)
   1. 在应用程序中，通过以下两种方式之一添加新条目：
      - 使用设备的相机扫描 GitLab 中显示的二维码以自动添加条目。
      - 输入提供的详细信息以手动添加条目。
1. **在极狐GitLab 中：**
   1. 在 **Pin 码** 字段中输入您设备上输入的六位数 PIN 码。
   1. 输入您的当前密码。
   1. 选择 **提交**。

如果您输入的 PIN 正确，则会显示一条消息，指示已启用双重身份验证，并且您会看到[恢复码](#恢复码)列表。请务必下载它们并将它们保存在安全的地方。

### FortiAuthenticator 一次性密码

<!--
> - Introduced in [GitLab 13.5](https://gitlab.com/gitlab-org/gitlab/-/issues/212312)
> - It's deployed behind a feature flag, disabled by default.
> - To use it in GitLab self-managed instances, ask a GitLab administrator to [enable it](#enable-fortiauthenticator-integration).
-->

您可以在使用 FortiAuthenticator 作为 OTP 提供程序。用户必须在 FortiAuthenticator 和极狐GitLab 中使用完全相同的用户名，并且用户必须在 FortiAuthenticator 中配置 FortiToken。

您需要 FortiAuthenticator 的用户名和访问令牌。下面显示的代码示例中的 “access_token” 是 FortAuthenticator 访问密钥。要获取令牌，请参阅 [`Fortinet 文档库`](https://docs.fortinet.com/document/fortiauthenticator/6.2.0/rest-api-solution-guide/158294/the-fortiauthenticator-api)中的 `REST API 解决方案指南`。<!--GitLab 13.5 已经用 FortAuthenticator 6.2.0 版进行了测试。-->

首先配置 FortiAuthenticator。在您的 GitLab 服务器上：

1. 打开配置文件：

   对于 Omnibus 实例：

   ```shell
   sudo editor /etc/gitlab/gitlab.rb
   ```

   对于源安装实例：

   ```shell
   cd /home/git/gitlab
   sudo -u git -H editor config/gitlab.yml
   ```

1. 添加 provider 配置：

   对于 Omnibus 实例：

   ```ruby
   gitlab_rails['forti_authenticator_enabled'] = true
   gitlab_rails['forti_authenticator_host'] = 'forti_authenticator.example.com'
   gitlab_rails['forti_authenticator_port'] = 443
   gitlab_rails['forti_authenticator_username'] = '<some_username>'
   gitlab_rails['forti_authenticator_access_token'] = 's3cr3t'
   ```

   对于源安装实例：

   ```yaml
   forti_authenticator:
     enabled: true
     host: forti_authenticator.example.com
     port: 443
     username: <some_username>
     access_token: s3cr3t
   ```

1. 保存配置文件。
1. 如果您通过 Omnibus 或从源安装 GitLab，[重新配置](../../../administration/restart_gitlab.md#omnibus-gitlab-reconfigure) 或 [重启极狐GitLab](../../../administration/restart_gitlab.md#源安装实例) 以使更改生效。

#### 启用 FortiAuthenticator 集成

此功能带有默认禁用的 `:forti_authenticator` 功能标志。

要启用此功能，请要求具有 <!--[Rails 控制台访问权限](../../../administration/feature_flags.md#how-to-enable-and-disable-features-behind-flags)-->Rails 控制台访问权限的管理员运行以下命令：

```ruby
Feature.enable(:forti_authenticator, User.find(<user ID>))
```

### FortiToken Cloud 一次性密码

<!--
> - Introduced in [GitLab 13.7](https://gitlab.com/gitlab-org/gitlab/-/issues/212313).
> - It's deployed behind a feature flag, disabled by default.
> - It's disabled on GitLab.com.
> - It's not recommended for production use.
> - To use it in GitLab self-managed instances, ask a GitLab administrator to [enable it](#enable-or-disable-fortitoken-cloud-integration).
-->

WARNING:
您可能无法使用此功能。查看上面的 **版本历史** 注释以了解详细信息。

您可以使用 FortiToken Cloud 作为 OTP 提供程序。用户必须使用同时存在于 FortiToken Cloud 和极狐GitLab 中完全相同的用户名，并且用户必须在 FortiToken Cloud 中配置 FortiToken。

您还需要一个 `client_id` 和 `client_secret` 来配置 FortiToken Cloud。要获得这些，请参阅“REST API 指南”，网址为
[`Fortinet 文档库`](https://docs.fortinet.com/document/fortitoken-cloud/latest/rest-api)。

首先在极狐GitLab 中配置 FortiToken Cloud。在您的 GitLab 服务器上：

1. 打开配置文件：

   对于 Omnibus 实例：

   ```shell
   sudo editor /etc/gitlab/gitlab.rb
   ```

   对于源安装实例：

   ```shell
   cd /home/git/gitlab
   sudo -u git -H editor config/gitlab.yml
   ```

1. 添加 provider 配置：

   对于 Omnibus 实例：

   ```ruby
   gitlab_rails['forti_token_cloud_enabled'] = true
   gitlab_rails['forti_token_cloud_client_id'] = '<your_fortinet_cloud_client_id>'
   gitlab_rails['forti_token_cloud_client_secret'] = '<your_fortinet_cloud_client_secret>'
   ```

   对于源安装实例：

   ```yaml
   forti_token_cloud:
     enabled: true
     client_id: YOUR_FORTI_TOKEN_CLOUD_CLIENT_ID
     client_secret: YOUR_FORTI_TOKEN_CLOUD_CLIENT_SECRET
   ```

1. 保存配置文件。
1. 如果您通过 Omnibus 或从源安装 GitLab，[重新配置](../../../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)或[重启极狐GitLab](../../../administration/restart_gitlab.md#源安装实例) 以使更改生效。

#### 启用或禁用 FortiToken Cloud 集成

FortiToken Cloud 集成正在开发中，尚未准备好用于生产。
它部署在 **默认禁用** 的功能标志后面。可以访问 Rails 控制台的管理员<!--[可以访问 Rails 控制台的管理员](../../../administration/feature_flags.md)-->可以启用它。

要启用它：

```ruby
Feature.enable(:forti_token_cloud, User.find(<user ID>))
```

要禁用它：

```ruby
Feature.disable(:forti_token_cloud, User.find(<user ID>))
```

### U2F 设备

官方只支持 [YubiKey](https://www.yubico.com/products/) U2F 设备，但用户已经成功使用过 [SoloKeys](https://solokeys.com/) 或 [Google Titan Security Key](https://cloud.google.com/titan-security-key)。

U2F 工作流由以下桌面浏览器[支持](https://caniuse.com/#search=U2F)：

- Chrome
- Edge
- Firefox 67+
- Opera

NOTE:
对于 Firefox 47-66，您可以在 [`about:config`](https://support.mozilla.org/en-US/kb/about-config-editor-firefox) 中启用 FIDO U2F API。搜索 `security.webauth.u2f` 并双击它切换到 `true`。

要使用 U2F 设备设置 2FA：

1. 登录您的帐户。
1. 转到您的[**用户设置**](../index.md#访问您的用户设置)。
1. 进入 **帐户**。
1. 单击 **启用双重认证**。
1. 连接您的 U2F 设备。
1. 点击 **设置新的 U2F 设备**。
1. 设备上的灯开始闪烁。通过按下它的按钮来激活它。

将显示一条消息，表明您的设备已成功设置。
单击**注册 U2F 设备**以完成该过程。

### WebAuthn 设备

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/22506) in GitLab 13.4.
> - It's [deployed behind a feature flag](../../feature_flags.md), disabled by default.
> - It's disabled on GitLab.com.
> - It's not recommended for production use.
> - To use it in GitLab self-managed instances, ask a GitLab administrator to [enable it](#enable-or-disable-webauthn). **(FREE SELF)**
-->

> - 引入于 13.4。
>  - 部署于功能标志后，默认禁用。
>  - 不推荐用于生产。
>  - 要在自助管理实例中使用，需要请求管理员启用它。

WebAuthn 工作流由以下桌面浏览器[支持](https://caniuse.com/#search=webauthn)：

- Chrome
- Edge
- Firefox
- Opera
- Safari

以及以下移动浏览器：

- Chrome for Android
- Firefox for Android
- iOS Safari (since iOS 13.3)

要使用 WebAuthn 兼容设备设置 2FA：

1. 登录您的帐户。
1. 转到您的[**用户设置**](../index.md#访问您的用户设置)。
1. 进入 **帐户**。
1. 选择 **启用双重认证**。
1. 插入您的 WebAuthn 设备。
1. 选择 **设置新的 WebAuthn 设备**。
1. 根据您的设备，您可能需要按下按钮或触摸传感器。

将显示一条消息，表明您的设备已成功设置。
不会为 WebAuthn 设备生成恢复码。 

## 恢复码

NOTE:
不会为 U2F / WebAuthn 设备生成恢复码。

WARNING:
每个码只能用于登录您的帐户一次。

成功启用双重身份验证后，系统会立即提示您下载一组生成的恢复码。如果您无法访问一次性密码验证器，您可以使用这些恢复码之一登录您的帐户。我们建议复制和打印它们，或使用 **下载代码** 按钮下载它们以将其存储在安全的地方。如果您选择下载它们，文件名为 `gitlab-recovery-codes.txt`。

为方便起见，用户界面现在包括 **复制代码** 和**打印代码** 按钮。引入于 13.7 版本。

如果您丢失了恢复码或想生成新码，您可以通过[双重验证帐户设置页面](#重新生成-2fa-恢复码) 或 [使用 SSH](#使用-ssh-生成新恢复码) 进行操作。

## 在启用 2FA 的情况下登录

启用 2FA 的登录与正常登录过程仅略有不同。
像往常一样输入您的用户名和密码凭据，您会看到第二个提示，具体取决于您启用的 2FA 类型。

### 使用一次性密码登录

当系统询问时，输入来自一次性密码验证器应用程序的 PIN 码或恢复码以登录。

### 使用 U2F 设备登录

要使用 U2F 设备登录：

1. 点击 **通过 U2F 设备登录**。
1. 设备上的灯开始闪烁。通过触摸/按下其按钮激活它。

将显示一条消息，表明您的设备已响应身份验证请求，并且您已自动登录。

### 使用 WebAuthn 设备登录

在支持的浏览器中，您应该在输入您的凭据后自动提示您激活您的 WebAuthn 设备（例如，通过触摸/按下其按钮）。

将显示一条消息，表明您的设备已响应身份验证请求并且您已自动登录。

## 禁用 2FA

如果您需要禁用 2FA：

1. 登录您的帐户。
1. 转到您的[**用户设置**](../index.md#访问您的用户设置)。
1. 进入 **帐户**。
1. 选择 **管理双重认证**。
1. 在 **双重身份认证**下，输入您当前的密码并选择 **禁用**。

将清除您所有的双重身份验证注册，包括移动应用程序和 U2F/WebAuthn 设备。

对禁用 2FA 的支持是有限的，具体取决于您的订阅级别。<!--有关更多信息，请参阅我们网站的 [帐户恢复](https://about.gitlab.com/support/#account-recovery) 部分。-->

## 个人访问令牌

启用 2FA 后，您不能再使用普通帐户密码在命令行上通过 HTTPS 或使用 API<!--[GitLab API](../../../api/index.md)--> 对 Git 进行身份验证。您必须改用[个人访问令牌](../personal_access_tokens.md)。

## 恢复选项

要在您的帐户上禁用双重身份验证（例如，如果您丢失了代码生成设备），您可以：

- [使用保存的恢复码](#使用保存的恢复码)。
- [使用 SSH 生成新恢复码](#使用-ssh-生成新恢复码)。
- [重新生成 2FA 恢复码](#重新生成-2fa-恢复码)。
<!--- [在您的帐户上禁用 2FA](#在您的帐户上禁用-2fa)。-->

### 使用保存的恢复码

为您的帐户启用双重身份验证会生成多个恢复码。如果您保存了这些代码，则可以使用其中之一登录。

要使用恢复代码，请在登录页面上输入您的用户名/电子邮件和密码。当提示输入双重码时，输入恢复码。

使用恢复码后，您将无法重复使用它。您仍然可以使用您保存的其他恢复码。

### 使用 SSH 生成新恢复码

用户在启用双重身份验证时经常忘记保存他们的恢复码。如果将 SSH 密钥添加到您的帐户，您可以使用 SSH 生成一组新的恢复码：

1. 运行：

   ```shell
   ssh git@gitlab.cn 2fa_recovery_codes
   ```

   NOTE:
   在自助实例上，将上面命令中的 **`gitlab.cn`** 替换为 GitLab 服务器主机名（`gitlab.example.com`）。

1. 系统会提示您确认要生成新码。继续这个过程会使之前保存的码失效：

   ```shell
   Are you sure you want to generate new two-factor recovery codes?
   Any existing recovery codes you saved will be invalidated. (yes/no)

   yes

   Your two-factor authentication recovery codes are:

   119135e5a3ebce8e
   11f6v2a498810dcd
   3924c7ab2089c902
   e79a3398bfe4f224
   34bd7b74adbc8861
   f061691d5107df1a
   169bf32a18e63e7f
   b510e7422e81c947
   20dbed24c5e74663
   df9d3b9403b9c9f0

   During sign in, use one of the codes above when prompted for your
   two-factor code. Then, visit your Profile Settings and add a new device
   so you do not lose access to your account again.
   ```

1. 转到登录页面并输入您的用户名/电子邮件和密码。当提示输入双重码时，输入从命令行输出中获得的恢复码之一。

登录后，立即访问您的 **用户设置 > 帐户** 以使用新设备设置双重身份验证。

### 重新生成 2FA 恢复码

要重新生成 2FA 恢复代码，您需要访问桌面浏览器：

1. 导航到极狐GitLab。
1. 登录您的帐户。
1. 转到您的[**用户设置**](../index.md#访问您的用户设置)。
1. 选择 **帐户 > 双重认证 (2FA)**。
1. 如果您已经配置了 2FA，请点击 **管理双重认证**。
1. 在 **注册双重认证** 窗格中，输入您当前的密码并选择 **重新生成恢复码**。

NOTE:
如果您重新生成 2FA 恢复码，请保存它们。您不能使用任何以前创建的 2FA 码。

<!--
### 在您的帐户上禁用 2FA

If you can't use a saved recovery code or generate new recovery codes, submit a [support ticket](https://support.gitlab.com/hc/en-us/requests/new) to
request a GitLab global administrator disable two-factor authentication for your account. Note that:

- Only the owner of the account can make this request.
- This service is only available for accounts that have a GitLab.com subscription. For more information, see our
  [blog post](https://about.gitlab.com/blog/2020/08/04/gitlab-support-no-longer-processing-mfa-resets-for-free-users/).
- Disabling this setting temporarily leaves your account in a less secure state. You should sign in and re-enable two-factor authentication
  as soon as possible.
-->

## 管理员注意事项

- 您需要特别注意在 <!--[恢复 GitLab 备份](../../../raketasks/backup_restore.md)-->恢复 GitLab 备份后 2FA 继续工作。
- 为确保 2FA 与 TOTP 服务器正确授权，您可能需要确保您的 GitLab 服务器的时间通过 NTP 等服务同步。否则，您可能会遇到由于时差导致授权总是失败的情况。
- 当 GitLab 实例从多个主机名或 FQDN 访问时，GitLab U2F 实现不会工作。每个 U2F 注册都链接到注册时的*当前主机名*，并且不能用于其他主机名/FQDN。这同样适用于 WebAuthn 注册。

  例如，如果用户尝试从 `first.host.xyz` 和 `second.host.xyz` 访问实例：

   - 用户使用 `first.host.xyz` 登录并注册他们的 U2F 密钥。
   - 用户注销并尝试使用 `first.host.xyz` 登录 - U2F 身份验证成功。
   - 用户注销并尝试使用 `second.host.xyz` 登录 - U2F 身份验证失败，因为 U2F 密钥仅在 `first.host.xyz` 上注册。

<!--
- To enforce 2FA at the system or group levels see [Enforce Two-factor Authentication](../../../security/two_factor_authentication.md).
-->

## 启用或禁用 WebAuthn **(FREE SELF)**

对 WebAuthn 的支持正在开发中，尚未准备好用于生产。它部署在**默认禁用**的功能标志后面。可以访问 GitLab Rails 控制台的管理员<!--[可以访问 GitLab Rails 控制台的管理员](../../../administration/feature_flags.md)-->可以启用它。

要启用它：

```ruby
Feature.enable(:webauthn)
```

要禁用它：

```ruby
Feature.disable(:webauthn)
```

## 故障排查

如果您收到 `invalid pin code` 错误，这可能表明身份验证应用程序和实例本身之间存在时间同步问题。

为避免时间同步问题，请在生成代码的设备中启用时间同步。 例如：

- 对于 Android（谷歌身份验证器）：
   1. 进入 Google Authenticator 的主菜单。
   1. 选择设置。
   1. 选择代码的时间校正。
   1. 选择立即同步。
- 对于 iOS：
   1. 进入设置。
   1. 选择常规。
   1. 选择日期和时间。
   1. 启用自动设置。如果已启用，请将其禁用，等待几秒钟，然后重新启用。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
