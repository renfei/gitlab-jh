---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 标记 **(FREE)**

随着议题、合并请求和史诗的数量不断增加，跟踪这些项目变得越来越具有挑战性。特别是当您的组织从几个人发展到数百或数千人时。这就是标记的用武之地。它们可以帮助您组织和标记您的工作，以便您可以跟踪和找到您感兴趣的工作项目。

标记是[议题看板](issue_board.md)的关键部分。使用标记，您可以：

- 使用颜色和描述性标题（如 `bug`、`feature request` 或 `docs`）对史诗、议题和合并请求进行分类。
- 动态过滤和管理史诗、议题和合并请求。
<!--- [Search lists of issues, merge requests, and epics](../search/index.md#issues-and-merge-requests),
  as well as [issue boards](../search/index.md#issue-boards).-->

## 项目标记和群组标记

有两种类型的标记：

- **项目标记**只能分配给该项目中的议题和合并请求。
- **群组标记**可以分配给所选群组或其子群组中任何项目中的议题和合并请求。
  - 它们也可以分配给所选群组或其子组中的史诗。 **(ULTIMATE)**

## 分配和取消分配标签

<!--
> Unassigning labels with the **X** button [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/216881) in GitLab 13.5.
-->

每个议题、合并请求和史诗都可以分配任意数量的标记。标记在右侧边栏中进行管理，您可以在其中根据需要分配或取消分配标签。

要分配或取消分配标签：

1. 在侧边栏的**标记** 部分，单击**编辑**。
1. 在 **指派标记** 列表中，通过键入标签名称来搜索标签。您可以重复搜索以添加更多标签。所选标签标有复选标记。
1. 单击要分配或取消分配的标记。
1. 要将更改应用于标记，请单击 **指派标记** 旁边或标记部分之外的任何位置的 **X**。

或者，要取消分配标签，请单击要取消分配的标签上的 **X**。

您还可以使用`/label` [快速操作](quick_actions.md) 分配标签，使用`/unlabel` 移除标签，并使用`/relabel` 重新分配标签（移除所有标签并分配新标签）。

## 标记管理

权限级别<!--[权限级别](../permissions.md)-->为报告者或更高级别的用户可以创建和编辑标记。

### 项目标记

<!--
> Showing all inherited labels [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/241990) in GitLab 13.5.
-->

要查看项目的可用标记，请在项目中转到 **项目信息 > 标记**。标记列表既包括在项目级别定义的标记，也包括由其所在群组定义的所有标记。对于每个标记，您都可以看到其创建位置的项目或组路径。您可以通过在 **筛选器** 字段中输入搜索查询，然后单击其搜索图标 (**{search}**) 来过滤列表。

要创建新的项目标记：

1. 在您的项目中，转到 **项目信息 > 标记**。
1. 选择 **新标记** 按钮。
1. 在 **标题** 字段中，为标记输入一个简短的描述性名称。您还可以使用此字段创建 [范围内的互斥标记](#范围标记)。
1. （可选）在 **描述** 字段中，您可以输入有关如何以及何时使用此标记的其他信息。
1. （可选）通过选择一种可用颜色或在 **背景颜色** 字段中输入十六进制颜色值来选择标记的背景颜色。
1. 选择 **创建标记**。

您还可以从议题或合并请求中创建新的项目标记。在议题或合并请求右侧边栏的标记部分：

1. 点击 **编辑**。
1. 点击 **创建项目标记**。
    - 填写名称字段。请注意，如果以这种方式创建标记，则无法指定描述。您可以稍后通过编辑标记来添加说明（见下文）。
    - （可选）通过单击可用颜色选择颜色，或输入特定颜色的十六进制颜色值。
1. 点击 **创建**。

创建后，您可以通过单击铅笔 (**{pencil}**) 来编辑标签，或者通过单击 **订阅** 按钮旁边的三个点 (**{ellipsis_v}**) 并选择 **删除**。

WARNING:
如果您删除标记，它将被永久删除。对标记的所有引用都将从系统中删除，并且您无法撤消删除操作。

#### 将项目标记提升为群组标记

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/231472) in GitLab 13.6: promoting a project label keeps that label's ID and changes it into a group label. Previously, promoting a project label created a new group label with a new ID and deleted the old label.
-->

如果您之前创建了一个项目标记，但现在想要将其提供给同一群组内的其他项目，您可以将其提升为群组标记。

如果同一群组中的其他项目具有相同标题的标记，则它们都将与新的群组标记合并。如果存在具有相同标题的群组标记，它也会被合并。

带有旧标记的所有议题、合并请求、议题看板列表、议题看板过滤器和标记订阅都分配给新的群组标记。

新的组标签与之前的项目标签具有相同的 ID。

WARNING:
提升标记是一项永久性操作，无法撤消。

要将项目标记提升为群组标记：
   
1. 导航到项目中的 **项目信息 > 标记**。
1. 单击 **订阅** 按钮旁边的三个点 (**{ellipsis_v}**) 并选择 **升级到群组标记**。

### 群组标记

要查看群组标记列表，请导航到群组并单击 **群组信息 > 标记**。
该列表包括仅在群组级别定义的所有标记。它没有列出在项目中定义的任何标记。您可以通过在顶部输入搜索查询并单击搜索 (**{search}**) 来过滤列表。

要创建 **群组标记**，请导航到群组中的 **群组信息 > 标记**，并按照与[创建项目标记](#项目标记)相同的过程进行操作。

#### 从史诗创建群组标记 **(ULTIMATE)**

您可以从史诗侧边栏创建群组标记。您创建的标记属于史诗所属的直接群组。该过程与[从议题或合并请求创建项目标记](#项目标记)相同。

### 生成默认标记

如果项目或群组没有标记，您可以从标记列表页面生成一组默认的项目或群组标记。如果列表为空，页面会显示 **生成一组默认标记** 按钮。选择按钮以将以下默认标记添加到项目：

- `bug`
- `confirmed`
- `critical`
- `discussion`
- `documentation`
- `enhancement`
- `suggestion`
- `support`

## 范围标记 **(PREMIUM)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/9175) in [GitLab Premium](https://about.gitlab.com/pricing/) 11.10.
-->

范围标记允许团队使用标记功能来注释议题、合并具有互斥标签的请求和史诗。可以通过防止将某些标记一起使用来实现更复杂的工作流程。

当标记的标题中使用特殊的双冒号 (`::`) 语法时，标签是有作用范围的，例如：

![Scoped labels](img/labels_key_value_v13_5.png)

一个议题、合并请求或史诗不能有两个范围标记，形式为 `key::value`，具有相同的 `key`。添加具有相同 `key` 但不同 `value` 的新标记会导致之前的 `key` 标记被新标记替换。

例如：

1. 一个议题被确定为低优先级，并为其添加一个 `priority::low` 项目标记。
1. 经过多次审核，议题优先级提高，并添加了一个 `priority::high` 标记。
1. 系统自动删除了 `priority::low` 标记，因为一个议题不应该同时有两个优先级标记。

### 带有范围标记的工作流

假设您想要议题中的自定义字段来跟踪您的功能所针对的操作系统平台，其中每个议题应该只针对一个平台。然后，您将创建三个标记 `platform::iOS`、`platform::Android`、`platform::Linux`。在给定的议题上应用这些标记中的任何一个都会自动删除任何其他以 `platform::` 开头的现有标记。

可以应用相同的模式来表示团队的工作流状态。假设您有标记 `workflow::development`、`workflow::review` 和 `workflow::deployed`。如果一个议题已经应用了标记 `workflow::development`，并且开发人员想要将问题推进到 `workflow::review`，他们只需应用该标记，`workflow::development` 标记将自动移除。当您在[议题看板](issue_board.md#创建工作流)中跨标记列表移动议题时，此行为已经存在，但现在，可能未直接在议题看板中工作的团队成员，仍然能够通过给他们自己议题来持续推进工作流状态。

<!--
This functionality is demonstrated in a video regarding
[using scoped labels for custom fields and workflows](https://www.youtube.com/watch?v=4BCBby6du3c).
-->

### 具有嵌套范围的范围标记

您可以在创建时使用多个双冒号 `::` 来创建具有嵌套范围的标记。在这种情况下，最后一个 `::` 之前的所有内容都是范围。

例如，`workflow::backend::review` 和 `workflow::backend::development` 是有效的范围标记，但它们**不能**同时存在于同一议题上，因为它们都共享相同的范围，`workflow::backend`。

此外，`workflow::backend::review` 和 `workflow::frontend::review` 是有效的范围标记，它们**可以**同时存在于同一议题上，因为它们都有不同的范围，`workflow::frontend` 和 `workflow::backend`。

## 订阅标记

在项目标记列表页面和群组标记列表页面，您可以点击任意标记右侧的 **订阅**，为该标记启用通知<!--[通知](../profile/notifications.md)-->。每当将标记分配给史诗、议题或合并请求时，您都会收到通知。

如果您从项目中订阅群组标记，您可以选择仅订阅项目或整个群组的标记通知。

![Labels subscriptions](img/labels_subscriptions_v13_5.png)

## 标记优先级

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/14189) in GitLab 8.9.
> - Priority sorting is based on the highest priority label only. [This discussion](https://gitlab.com/gitlab-org/gitlab/-/issues/14523) considers changing this.
-->

标记可以有相对优先级，用于**标记优先级**、**优先级**排序议题和合并请求列表页面。群组和项目标记的优先级发生在项目级别，不能从群组标记列表中完成。

在项目标记列表页面中，为标记加星标以表明它具有优先级。

![Labels prioritized](img/labels_prioritized_v13_5.png)

在列表中上下拖动带星号的标记以更改它们的优先级，列表中越高表示优先级越高。

![Drag to change label priority](img/labels_drag_priority_v12_1.gif)

在合并请求和议题列表页面（对于群组和项目），您可以按 `标记优先级` 或 `优先级` 排序。

如果按 `标记优先级` 排序，使用以下排序顺序：

1. 具有更高优先级标记的项目。
1. 没有优先标记的项目。

优先级相同时任意排序。请注意，仅检查优先级最高的标记，忽略优先级较低的标记。<!--See this [related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/14523)
for more information.-->

![Labels sort label priority](img/labels_sort_label_priority.png)

如果按 `优先级` 排序，使用以下排序顺序：

1. 具有截止日期的里程碑的项目，其中最早分配的里程碑<!--[里程碑](milestones/index.md)-->列在最前面。
1. 具有里程碑且没有截止日期的项目。
1. 具有更高优先级标记的项目。
1. 没有优先标记的项目。

优先级相同时任意排序。

![Labels sort priority](img/labels_sort_priority.png)

## 故障排查

### 一些标记标题以 `_duplicate<number>` 结尾

在特定情况下，可以在同一命名空间中创建具有重复标题的标记。

为了解决重复问题，在 13.2 及更高版本中，一些重复的标记在其标题后附加了 `_duplicate<number>`。

如果您愿意，您可以安全地更改这些标记的标题。
