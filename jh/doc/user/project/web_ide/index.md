---
stage: Create
group: Editor
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, how-to
---

# Web IDE **(FREE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/4539) in [GitLab Ultimate](https://about.gitlab.com/pricing/) 10.4.
> - [Moved](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/44157) to GitLab Free in 10.7.
-->

Web 集成开发环境 (IDE) 编辑器通过提供具有提交暂存功能的高级编辑器，使您可以更快、更轻松地将更改贡献给您的项目。

## 打开 Web IDE

使用 <kbd>.</kbd> [键盘快捷键](../../shortcuts.md) 打开 Web IDE。
您还可以在查看文件、存储库文件列表和合并请求时打开 Web IDE：

- *查看文件或仓库文件列表时* -
  1. 在页面右上角，如果可见，选择 **在 Web IDE 中编辑**。
  1. 如果 **在 Web IDE 中编辑** 不可见：
     1. 根据您的配置，选择 **编辑** 或 **Gitpod** 旁边的 **(angle-down)**。
     1. 从列表中选择 **在 Web IDE 中编辑** 将其显示为编辑选项。
     1. 选择 **在 Web IDE 中编辑** 打开编辑器。
- *查看合并请求时* -
  1. 转到您的合并请求，然后选择 **概览** 选项卡。
  1. 滚动到小部件部分，在合并请求描述之后。
  1. 如果可见，请选择 **在 Web IDE 中编辑**。
  1. 如果 **在 Web IDE 中编辑** 不可见：
     1. 选择 **在 Gitpod 中打开** 旁边的**(angle-down)**。
     1. 从列表中选择 **在 Web IDE 中打开**以将其显示为编辑选项。
     1. 选择 **在 Web IDE 中打开** 打开编辑器。

## 文件查找器

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/18323) in [GitLab Free](https://about.gitlab.com/pricing/) 10.8.
-->

文件查找器允许您通过搜索文件路径的片段来快速打开当前分支中的文件。使用键盘快捷键 <kbd>Command</kbd>+<kbd>p</kbd>、<kbd>Control</kbd>+<kbd>p</kbd> 或 <kbd>t</kbd> 启动文件查找器（当编辑器不在焦点时）。键入文件名或文件路径片段，开始查看结果。

## 命令面板

当编辑器处于焦点时，您可以通过按 <kbd>F1</kbd> 键来查看用于操作编辑器内容的所有可用命令。之后，编辑器会显示用于操作编辑器内容的可用命令的完整列表。该编辑器支持多光标编辑、代码块折叠、注释、搜索和替换、导航编辑器警告和建议等命令。

某些命令具有分配给它们的键盘快捷键。命令面板在每个命令旁边显示此快捷方式。您可以使用此快捷方式调用命令，而无需在命令面板中选择它。

![Command palette](img/command_palette_v13_6.png)

## 语法高亮

正如 IDE 所期望的那样，Web IDE 中多种语言的语法突出显示使您的直接编辑更加容易。

Web IDE 目前提供：

- 各种编程、脚本和标记语言（例如 XML、PHP、C#、C++、Markdown、Java、VB、Batch、Python、Ruby 和 Objective-C）的基本语法着色。
- 对某些语言的 IntelliSense 和验证支持（显示错误和警告，提供智能补全、格式和大纲）。例如：TypeScript、JavaScript、CSS、LESS、SCSS、JSON 和 HTML。

由于 Web IDE 是基于 [Monaco 编辑器](https://microsoft.github.io/monaco-editor/)，你可以在 [Monaco 语言](https://github.com/Microsoft/monaco-languages/)仓库中找到更完整的支持语言列表。在幕后，Monaco 使用 [Monarch](https://microsoft.github.io/monaco-editor/monarch.html) 库进行语法高亮显示。

如果您缺少对任何语言的语法突出显示支持，我们准备了一份简短指南，介绍如何[添加对缺少语言语法突出显示的支持。](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/app/assets/javascripts/ide/lib/languages/README.md)

### 主题

<!--
> - [Introduced](https://gitlab.com/groups/gitlab-org/-/epics/2389) in GitLab 13.0.
> - Full Solarized Dark Theme [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/219228) in GitLab 13.1.
> - Full [Solarized Light](https://gitlab.com/gitlab-org/gitlab/-/issues/221035) and [Monokai](https://gitlab.com/gitlab-org/gitlab/-/issues/221034) Themes introduced in GitLab 13.6.
-->

系统支持的所有语法高亮主题都应用于 Web IDE 的整个屏幕。
您可以从[个人资料偏好设置](../../profile/preferences.md)中选择一个主题。

| Solarized Dark 主题                                       | Dark 主题                              |
|-------------------------------------------------------------|-----------------------------------------|
| ![Solarized Dark Theme](img/solarized_dark_theme_v13_1.png) | ![Dark Theme](img/dark_theme_v13_0.png) |

## 高亮行

Web IDE 是使用 [Web Editor](../repository/web_editor.md) 构建的。这使 Web IDE 能够共享相同的核心功能，用于突出显示和链接到 [Web 编辑器描述](../repository/web_editor.md#高亮行)编辑过的文件中的特定行为。

## 基于 schema 的验证

> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/218472) validation based on predefined schemas in GitLab 13.2 [with a flag](../../../administration/feature_flags.md) named `schema_linting`. Disabled by default.

FLAG:
在自助管理实例上，默认情况下此功能不可用。要使其可用于您的整个实例，请要求管理员启用功能标志，名为 `schema_linting`。
不能按每个项目启用或禁用此功能。
<!--在 GitLab.com 上，此功能可用。-->
您不应将此功能用于生产环境。

Web IDE 使用基于 [JSON Schema Store](https://www.schemastore.org/json/) 的模式，为某些 JSON 和 YAML 文件提供验证支持。

### 预定义 schema

Web IDE 对某些内置文件进行了验证。此功能仅支持 `*.gitlab-ci.yml` 文件。

### 自定义 schemas **(PREMIUM)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/226982) in GitLab 13.4.
-->

Web IDE 还允许您为项目中的某些 JSON/YAML 文件定义自定义架构。
您可以通过在仓库根目录中的 `.gitlab/.gitlab-webide.yml` 文件中定义一个 `schemas` 条目来实现。这是一个示例配置：

```yaml
schemas:
  - uri: https://json.schemastore.org/package
    match:
      - package.json
  - uri: https://somewebsite.com/first/raw/url
    match:
      - data/release_posts/unreleased/*.{yml,yaml}
  - uri: https://somewebsite.com/second/raw/url
    match:
      - "*.meta.json"
```

每个架构条目支持两个属性：

- `uri`：请在此处提供架构定义文件的绝对 URL。当匹配的文件打开时，将加载来自此 URL 的架构。
- `match`：匹配路径或 glob 表达式的列表。如果模式与特定路径模式匹配，则将其应用于该文件。如果模式以星号 (`*`) 开头，请用引号将模式括起来，它将应用于该文件。如果模式以星号 (`*`) 开头，请将其括在引号中。否则，配置文件不是有效的 YAML。

## 配置 Web IDE

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/23352) in [GitLab Free](https://about.gitlab.com/pricing/) 13.1.
-->

Web IDE 支持使用 [`.editorconfig` 文件](https://editorconfig.org/) 配置某些编辑器设置。打开文件时，Web IDE 会在当前目录和所有父目录中查找名为 `.editorconfig` 的文件。如果找到配置文件并且具有与文件路径匹配的设置，则会在打开的文件上强制执行这些设置。

Web IDE 当前支持以下 `.editorconfig` 设置：

- `indent_style`
- `indent_size`
- `end_of_line`
- `trim_trailing_whitespace`
- `tab_width`
- `insert_final_newline`

## 提交变更

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/4539) in [GitLab Ultimate](https://about.gitlab.com/pricing/) 10.4.
> - [Moved](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/44157) to GitLab Free in 10.7.
> - From [GitLab 12.7 onward](https://gitlab.com/gitlab-org/gitlab/-/issues/33441), files were automatically staged.
> - From [GitLab 12.9 onward](https://gitlab.com/gitlab-org/gitlab/-/issues/196609), support for staging files was removed to prevent loss of unstaged data. All your current changes necessarily have to be committed or discarded.
-->

进行更改后，单击左下角的 **提交** 按钮以查看已更改文件的列表。

完成更改后，您可以添加提交消息、提交更改并直接创建合并请求。如果您对所选分支没有写访问权限，您会看到警告，但仍然可以创建新分支并启动合并请求。

要放弃特定文件中的更改，请单击更改选项卡中该文件上的 **放弃更改** 按钮。要放弃所有更改，请单击更改边栏右上角的垃圾桶图标。

![Commit changes](img/commit_changes_v13_11.png)

## 审核变更

在提交更改之前，您可以通过切换到审核模式，或从更改列表中选择文件来将它们与之前的提交进行比较。

当您打开合并请求时，可以使用额外的审核模式，如果您提交更改，它会向您显示合并请求差异的预览。

## 查看 CI 作业日志

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/19279) in [GitLab Free](https://about.gitlab.com/pricing/) 11.0.
-->

您可以使用 Web IDE，通过在 Web IDE 中打开分支或合并请求并打开失败作业的日志来快速修复失败的测试。您可以通过单击右上角的 **流水线** 按钮访问最新流水线的所有作业状态和当前提交的作业跟踪。

流水线状态也始终显示在左下角的状态栏中。

## 切换合并请求

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/19318) in [GitLab Free](https://about.gitlab.com/pricing/) 11.0.
-->

要在您编写的和分配的合并请求之间切换，请单击侧边栏顶部的下拉菜单以打开合并请求列表。在切换到不同的合并请求之前，您必须提交或放弃所有更改。

## 切换分支

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/20850) in [GitLab Free](https://about.gitlab.com/pricing/) 11.2.
-->

要在当前项目仓库的分支之间切换，请单击侧边栏顶部的下拉菜单以打开分支列表。
在切换到不同的分支之前，您必须提交或放弃所有更改。

## Markdown 编辑

> - Markdown 预览引入于标准版 10.7 版本。
> - 支持粘贴图片引入于标准版 13.1 版本。
> - 并排 Markdown 预览引入于标准版 14.3 版本。

在 Web IDE 中编辑 Markdown 文件：

1. 转到您的仓库，然后导航到您要编辑的 Markdown 页面。
1. 选择 **在 Web IDE 中编辑**，系统会在编辑器的选项卡中加载页面。
1. 对文件进行更改。系统支持 GitLab Flavored Markdown<!--[GitLab Flavored Markdown](../../markdown.md#gitlab-flavored-markdown)-->。
1. 更改完成后，在左侧边栏中选择 **提交**。
1. 添加提交信息，选择要提交的分支，选择 **提交**。

编辑时，可以直接将本地图片粘贴到 Markdown 文件中上传。
图片上传到同一个目录，默认命名为`image.png`。
如果已存在另一个同名文件，则会在文件名中自动添加一个数字后缀。

在 Web IDE 中预览 Markdown 内容有两种方法：

1. 在文件选项卡的顶部，选择 **预览 Markdown**，预览文件中的格式。您无法在此视图中编辑文件。
   1. 要向文件添加更多更改，请选择**编辑**。
1. 右键单击​​或使用键盘快捷键 `Command/Control + Shift + P` 并选择 **预览 Markdown** 以切换实时 Markdown 预览面板。

## 实时预览

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/19764) in [GitLab Free](https://about.gitlab.com/pricing/) 11.2.
> - [Renamed](https://gitlab.com/gitlab-org/gitlab/-/issues/213853) from _Client Side Evaluation_ to _Live Preview_ in GitLab 13.0.
-->

您可以使用 Web IDE 直接在浏览器中预览 JavaScript 项目。
此功能使用 CodeSandbox 编译和捆绑用于预览 Web 应用程序的 JavaScript。

![Web IDE Live Preview](img/live_preview_v13_0.png)

此外，对于公共项目，可以使用**在 CodeSandbox 中打开**按钮，将项目内容传输到公共 CodeSandbox 项目中，以便与他人快速共享您的项目。

### 启用实时预览

启用实时预览后，您可以在 Web IDE 中使用 `package.json` 文件和 `main` 入口点预览项目。

<!--GitLab.com 上的所有项目都启用了实时预览。-->如果您是自助管理实例的管理员，并且想要启用实时预览：

1. 在顶部栏上，选择 **菜单 > 管理员**。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 滚动到 **Web IDE** 并选择 **展开**：
   ![Administrator Live Preview setting](img/admin_live_preview_v13_0.png)
1. 选择 **启用实时预览**，然后选择 **保存更改**。

在 12.9 及更高版本中，实时预览启用时所需的第三方资产和库托管在 `https://sandbox-prod .gitlab-static.net`。 但是，某些库仍由其他第三方服务提供，这在您的环境中可能需要也可能不需要。

示例 `package.json`：

```json
{
  "main": "index.js",
  "dependencies": {
    "vue": "latest"
  }
}
```

## 用于 Web IDE 的交互式 Web 终端

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/5426) in [GitLab Ultimate](https://about.gitlab.com/pricing/) 11.6.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/211685) to GitLab Free in 13.1.
-->

WARNING:
用于 Web IDE 的交互式 Web 终端目前处于 **Beta**。
<!--GitLab.com shared runners [do not yet support Interactive Web Terminals](https://gitlab.com/gitlab-org/gitlab/-/issues/24674),
so you must use your own private runner to make use of this feature.-->

交互式 Web 终端<!--[交互式 Web 终端](../../../ci/interactive_web_terminal/index.md)--> 授予项目维护者用户访问终端权限，直接从 GitLab 与 runner 交互，包括通过 Web IDE。

### Runner 配置

必须在 runner 中配置一些东西才能让交互式 Web 终端工作：

- runner 需要 `[session_server]` 正确配置<!--[`[session_server]` 正确配置](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-session_server-section)-->。此部分至少需要一个 `session_timeout` 值（默认为 1800 秒）和一个 `listen_address` 值。如果未定义 `advertise_address`，则使用 `listen_address`。
- 如果您在实例中使用反向代理，则 Web 终端必须启用<!--[启用](../../../administration/integration/terminal.md#enabling-and-disabling-terminal-support)-->。 **(ULTIMATE SELF)**

如果您打开终端并且作业已完成其任务，则终端会在 `[session_server].session_timeout`<!--[`[session_server].session_timeout`](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-session_server-section)--> 中配置的持续时间内阻止作业完成，直到您关闭终端窗口。

NOTE:
并非所有执行程序都受支持<!--[受支持](https://docs.gitlab.com/runner/executors/#compatibility-chart)-->。
[文件同步](#文件同步到-web-终端)功能仅在 Kubernetes 运行程序上受支持。

### Web IDE 配置文件

要启用 Web IDE 终端，您必须在仓库的根目录中创建文件 `.gitlab/.gitlab-webide.yml`。此文件与 CI 配置文件<!--[CI 配置文件](../../../ci/yaml/index.md)-->语法非常相似，但有一些限制：

- 不能定义全局块（例如 `before_script` 或`after_script`）。
- 只能将一个名为 `terminal` 的作业添加到此文件中。
- 仅允许使用关键字 `image`、`services`、`tags`、`before_script`、`script` 和 `variables` 来配置作业。
- 要连接到交互式终端，`terminal` 作业必须仍处于活动状态并正在运行，否则终端无法连接到作业的会话。默认情况下，`script` 关键字的值为 `sleep 60` 以防止作业结束并为 Web IDE 提供足够的连接时间。这意味着，如果你覆盖默认的 `script` 值，你必须添加一个命令来保持作业运行，比如 `sleep`。

在下面的代码中有此配置文件的示例：

```yaml
terminal:
  # This can be any image that has the necessary runtime environment for your project.
  image: node:10-alpine
  before_script:
    - apk update
  script: sleep 60
  variables:
    RAILS_ENV: "test"
    NODE_ENV: "test"
```

终端启动后，将显示控制台，我们可以访问项目仓库文件。

当您使用 image 关键字时，将创建一个具有指定图像的容器。如果指定图像，则没有效果。当您使用 shell executor<!--[shell executor](https://docs.gitlab.com/runner/executors/shell.html)--> 时就是这种情况。

**重要**：终端作业依赖于分支。这意味着用于触发和配置终端的配置文件是 Web IDE 所选分支中的配置文件。

如果分支中没有配置文件，则会显示错误消息。

### 在 Web IDE 中运行交互式终端

如果当前用户可以使用交互式终端，则在 Web IDE 的右侧栏中可以看到 **终端** 按钮。单击此按钮可打开或关闭终端选项卡。

打开后，该选项卡会显示 **启动 Web 终端** 按钮。如果环境配置不正确，此按钮可能会被禁用。如果是这样，状态消息将描述该问题。以下是 **启动 Web 终端** 可能被禁用的一些原因：

- `.gitlab/.gitlab-webide.yml` 不存在或设置不正确。
- 该项目没有可用的活跃私人 runner。

如果处于有效状态，单击 **启动 Web 终端** 按钮会加载终端视图并开始连接到 runner 的终端。**终端** 选项卡可以随时关闭和重新打开，终端状态不受影响。

当终端启动并成功连接到 runner 时，runner 的 shell 提示就会出现在终端中。从这里，您可以输入在运行程序环境中执行的命令。这类似于在本地终端或通过 SSH 运行命令。

当终端正在运行时，可以通过单击 **停止终端** 来停止它。
这会断开终端并停止 runner 的终端工作。从这里，单击 **重启终端** 以启动新的终端会话。

### 文件同步到 Web 终端

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/5276) in [GitLab Ultimate](https://about.gitlab.com/pricing/) 12.0.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/211686) to GitLab Free in 13.1.
-->

Web IDE 中的文件更改可以同步到正在运行的 Web 终端。
这使用户能够在预配置的终端环境中测试他们的代码更改。

NOTE:
只有 Web IDE 中的文件更改会同步到终端。
在终端中所做的更改**不**同步到 Web IDE。
此功能仅适用于 Kubernetes 运行程序。

要启用文件同步到 web 终端，`.gitlab/.gitlab-webide.yml` 文件需要配置一个 `webide-file-sync` 服务。以下是使用此服务的 Node JS 项目的示例配置：

```yaml
terminal:
  # This can be any image that has the necessary runtime environment for your project.
  image:
    name: node:10-alpine
  services:
    - name: registry.gitlab.com/gitlab-org/webide-file-sync:latest
      alias: webide-file-sync
      entrypoint: ["/bin/sh"]
      command: ["-c", "sleep 5 && ./webide-file-sync -project-dir $CI_PROJECT_DIR"]
      ports:
        # The `webide-file-sync` executable defaults to port 3000.
        - number: 3000
```

- `webide-file-sync` 可执行文件必须在 **after** 项目目录可用后启动。这就是我们必须在 `command` 中添加 `sleep 5` 的原因。
  <!--See [this issue](https://gitlab.com/gitlab-org/webide-file-sync/-/issues/7) for
  more information.-->
- `$CI_PROJECT_DIR` 是 GitLab Runners 的预定义 CI/CD 变量<!--[预定义 CI/CD 变量](../../../ci/variables/predefined_variables.md)-->。这是您项目的仓库所在的位置。

为文件同步配置 Web 终端后，当 Web 终端启动时，状态栏中会显示 **Terminal** 状态。

![Web IDE Client Side Evaluation](img/terminal_status.png)

在以下情况下，通过 Web IDE 对文件所做的更改同步到正在运行的终端：

- <kbd>Control</kbd> + <kbd>S</kbd>（或 <kbd>Command</kbd> + <kbd>S</kbd> 在 Mac 上）在编辑文件时按下。
- 编辑文件后单击文件编辑器之外的任何内容。
- 创建、删除或重命名文件或文件夹。

### 限制

Web IDE 有一些限制：

- 交互式终端处于测试阶段，并将在即将发布的版本中继续改进。同时，用户一次只能拥有一个活动终端。

- LFS 文件可以渲染和显示，但不能使用 Web IDE 更新和提交。如果 LFS 文件被修改并推送到仓库，则仓库中的 LFS 指针将被修改后的 LFS 文件内容覆盖。

### 故障排查

- 如果终端的文本为灰色且无响应，则终端已停止且无法再使用。停止的终端可以通过点击 **重启终端** 重新启动。
- 如果终端显示 **连接失败**，则终端无法连接到 runner。请尝试停止并重新启动终端。如果问题仍然存在，请仔细检查您的 runner 配置。
