---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 禅道产品集成 **(PREMIUM)**

如果您的团队使用禅道作为项目管理工具，那么可以使用极狐GitLab 的禅道集成功能，来获得更加无缝切换的产品使用体验。  
启用禅道集成功能后，可以在极狐GitLab 实时同步查看禅道某个产品的所有需求、任务和 Bug 信息。

## 前提条件

1. 极狐GitLab 版本在 14.4 及以上。
1. 禅道版本在 15.4 及以上。
1. 集成的禅道实例中，必须有一个可用的账号，和对应的 API token。

## 配置禅道

集成需要禅道 API secret key。  

在禅道平台上完成以下步骤：  

1. 访问 **管理员** 页面并选择 **二次开发 > 应用**。
1. 选择 **添加应用**。
1. 在 **名称** 和 **代号** 下，输入新的 secret key 对应的应用名称和代号。
1. 在 **账号** 下，选择一个已有账号的名称。
1. 选择 **保存**。
1. 复制生成的密钥，后续在极狐GitLab 中使用。

## 配置极狐GitLab

在极狐GitLab 上完成以下步骤，在指定的极狐GitLab 项目中，展示来自单个禅道产品的议题。

1. 访问您指定的项目并选择 **设置 > 集成**。
1. 选择 **禅道**。
1. 在 **启用集成** 下，选择 **启用**。
1. 提供以下禅道配置信息：
   - **禅道 Web URL**：和极狐GitLab 集成的禅道系统实例的网页URL，例如 `example.zentao.net`。
   - **禅道 API URL**（可选）：禅道实例 API 的基础 URL，如不填则默认为与 Web URL 相同。  
   - **禅道 API 令牌**：使用您在[配置禅道](#配置禅道)时生成的密钥。

     ![ZenTao API Token](img/zentao_api_token.png)

   - **禅道产品 ID**：配置要在极狐GitLab 指定项目上展示议题的禅道产品，可以在禅道产品页面下的 **设置 > 概况** 中找到对应的产品 ID。

     ![ZenTao settings page](img/zentao_product_id.png)

1. 要验证与禅道的连接是否正常，选择 **测试设置**。
1. 选择 **保存更改**。
