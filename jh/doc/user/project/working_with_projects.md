---
stage: Manage
group: Workspace
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
---

# 使用项目进行工作 **(FREE)**

在极狐GitLab 中，大部分工作是在[项目](../../user/project/index.md)中完成的。文件和代码保存在项目中，大部分功能都在项目范围内。

## 浏览项目

您可以浏览可用的其他流行项目。要浏览项目：

1. 在顶部栏上，选择 **菜单 > 项目**。
1. 选择 **浏览项目**。

系统显示项目列表，按上次更新日期排序。要查看拥有最多[星标](#星标一个项目)的项目，请单击 **最多星标**。要查看过去一个月评论数量最多的项目，请单击 **热门**。

NOTE:
默认情况下，未经身份验证的用户可以看到 `/explore`。但是，如果**公开** 可见性级别<!--[**公开** 可见性级别](../admin_area/settings/visibility_and_access_controls.md#restrict-visibility-levels)-->受到限制，则 `/explore` 仅对登录用户可见。

## 创建一个项目

要在极狐GitLab 中创建一个项目：

1. 在您的仪表板中，单击绿色的 **新建项目** 按钮或使用导航栏中的加号图标。将打开 **新建项目** 页面。
1. 在 **新建项目** 页面，选择创建类型：
   - 创建一个[空白项目](#空白项目)。
   - 使用一个可用的项目模板<!--[项目模板](#项目模板)-->创建项目。
   - 如果在您的实例上启用了相关功能，可以从不同的库导入一个项目<!--[导入一个项目](../../user/project/import/index.md)-->，如果不可用，请联系您的管理员。
   - 为外部仓库运行 CI/CD 流水线<!--Run [CI/CD pipelines for external repositories](../../ci/ci_cd_for_external_repos/index.md)-->。 **(PREMIUM)**

NOTE:
有关不能用作项目名称的单词列表，请参阅[保留项目和群组名称](../../user/reserved_names.md)。

### 空白项目

要在 **新建项目** 页面上创建一个新的空白项目：

1. 点击 **创建空白项目**。
1. 提供以下信息：
   - 对于 **项目名称** 字段中的项目名称，您不能使用特殊字符，但可以使用空格、连字符、下划线甚至表情符号。添加名称时，**项目标识串** 会自动填充。项目标识串是实例用作项目的 URL 路径的内容。如果您想要不同的项目标识串，请先输入项目名称，然后再更改项目标识串。
   - **项目标识串** 字段中是项目路径，即实例使用的项目的 URL 路径。如果 **项目名称** 为空，则在您填写 **项目标识串** 时它会自动填写。
   - **项目描述（可选）** 字段使您能够输入项目仪表板的描述，这有助于其他人了解您的项目是关于什么的。虽然它不是必需的，但最好填写它。
   - 更改 **可见性级别** 会为用户修改项目的查看和访问权限<!--[查看和访问权限](../../public_access/public_access.md)-->。
   - 选择 **使用自述文件初始化仓库** 选项会创建一个 README 文件，以便 Git 仓库被初始化，拥有一个默认分支，并且可以被克隆。
1. 点击 **创建项目**。

<!--
### 项目模板

项目模板可以使用必要的文件预先填入新项目，让您快速开始。

有两种主要类型的项目模板：

- [内置模板](#内置模板), sourced from the following groups:
  - [`project-templates`](https://gitlab.com/gitlab-org/project-templates)
  - [`pages`](https://gitlab.com/pages)
- [Custom project templates](#custom-project-templates), for custom templates
  configured by GitLab administrators and users.

#### 内置模板

Built-in templates are project templates that are:

- Developed and maintained in the [`project-templates`](https://gitlab.com/gitlab-org/project-templates)
  and [`pages`](https://gitlab.com/pages) groups.
- Released with GitLab.
- Anyone can contribute a built-in template by following [these steps](https://about.gitlab.com/community/contribute/project-templates/).

To use a built-in template on the **New project** page:

1. Click **Create from template**
1. Select the **Built-in** tab.
1. From the list of available built-in templates, click the:
   - **Preview** button to look at the template source itself.
   - **Use template** button to start creating the project.
1. Finish creating the project by filling out the project's details. The process is
   the same as creating a [blank project](#blank-projects).

##### Enterprise templates **(ULTIMATE)**

GitLab is developing Enterprise templates to help you streamline audit management with selected regulatory standards. These templates automatically import issues that correspond to each regulatory requirement.

To create a new project with an Enterprise template, on the **New project** page:

1. Click **Create from template**
1. Select the **Built-in** tab.
1. From the list of available built-in Enterprise templates, click the:
   - **Preview** button to look at the template source itself.
   - **Use template** button to start creating the project.
1. Finish creating the project by filling out the project's details. The process is the same as creating a [blank project](#blank-projects).

Available Enterprise templates include:

- HIPAA Audit Protocol template ([introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/13756) in GitLab 12.10)

NOTE:
You can improve the existing built-in templates or contribute new ones in the
[`project-templates`](https://gitlab.com/gitlab-org/project-templates) and
[`pages`](https://gitlab.com/pages) groups by following [these steps](https://gitlab.com/gitlab-org/project-templates/contributing).

##### Custom project templates **(PREMIUM)**

> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/6860) in [GitLab Premium](https://about.gitlab.com/pricing/) 11.2.

Creating new projects based on custom project templates is a convenient option for
quickly starting projects.

Custom projects are available at the [instance-level](../../user/admin_area/custom_project_templates.md)
from the **Instance** tab, or at the [group-level](../../user/group/custom_project_templates.md)
from the **Group** tab, on the **Create from template** page.

To use a custom project template on the **New project** page:

1. Click **Create from template**
1. Select the **Instance** tab or the **Group** tab.
1. From the list of available custom templates, click the:
   - **Preview** button to look at the template source itself.
   - **Use template** button to start creating the project.
1. Finish creating the project by filling out the project's details. The process is
   the same as creating a [blank project](#blank-projects).
-->

## 推送创建一个新项目

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/26388) in GitLab 10.5.
-->

在本地新建仓库时，无需登录极狐GitLab 界面创建项目并[克隆其仓库](../../gitlab-basics/start-using-git.md#克隆一个仓库)。您可以直接将新仓库推送到极狐GitLab，在不离开终端的情况下，系统创建您的新项目。

要推送一个新项目：

1. 确定要将新项目添加到的[命名空间](../group/index.md#命名空间)，您在以后的步骤中需要此信息。要确定您是否有权在命名空间中创建新项目，请在 Web 浏览器中查看群组页面并确认该页面显示 **新建项目** 按钮。

   NOTE:
   由于项目创建权限可能有很多因素，如果您不确定，请联系您的管理员。

1. 如果您想使用 SSH 推送，请确保您已[创建 SSH 密钥](../../ssh/index.md)并[将其添加到您的帐户](../../ssh/index.md#将-ssh-密钥添加到您的极狐gitlab-帐户)。
1. 使用以下方法之一推送。将 `gitlab.example.com` 替换为托管 Git 仓库的机器的域名，将 `namespace` 替换为命名空间的名称，将 `myproject` 替换为新项目的名称：
   - 使用 SSH 推送：`git push --set-upstream git@gitlab.example.com:namespace/myproject.git master`
   - 使用 HTTPS 推送：`git push --set-upstream https://gitlab.example.com/namespace/myproject.git master`
   可选：要导出现有的仓库标签，请将 `--tags` 标志附加到您的 `git push` 命令。
1. 推送完成后会显示一条消息：

   ```plaintext
   remote: The private project namespace/myproject was created.
   ```

1. （可选）要配置远端，请更改命令 `git remote add origin https://gitlab.example.com/namespace/myproject.git` 以匹配您的命名空间和项目名称。

您可以在 `https://gitlab.example.com/namespace/myproject` 查看你的新项目。
默认情况下，您项目的可见性设置为 **私有**，但您可以在项目设置<!--[项目设置](../../public_access/public_access.md#change-project-visibility)-->中更改它。

此功能不适用于之前已使用和重命名<!--[重命名](settings/index.md#renaming-a-repository)-->的项目路径。之前的项目路径上存在重定向，导致推送尝试将请求重定向到重命名的项目位置，而不是创建新项目。要创建新项目，请使用 [Web UI](#创建一个项目) 或项目 API<!--[Projects API](../../api/projects.md#create-project)-->。

## 派生一个项目

派生（fork）是原始仓库的副本，您将其放在另一个命名空间中，您可以在其中试验和应用更改，以后可以决定是否共享，而不会影响原始项目。

<!--It takes just a few steps to [fork a project in GitLab](repository/forking_workflow.md#creating-a-fork).-->

## 星标一个项目

您可以为项目加星标，以便更轻松地找到您经常使用的项目。
一个项目的星数可以表明它的受欢迎程度。

要为项目加星标：

1. 转到您要加星的项目的主页。
1. 在页面右上角，点击 **星标**。

查看您加星标的项目：

1. 在顶部栏上，选择 **菜单 > 项目**。
1. 选择 **星标项目**。
1. 显示有关您已加星标的项目的信息，包括：

    - 项目描述，包括名称、描述和图标
    - 该项目被星标的次数
    - 该项目被派生的次数
    - 打开合并请求的数量
    - 未解决的议题数量

## 删除一个项目

要删除项目，首先导航到该项目的主页。

1. 导航到 **设置 > 通用**。
1. 展开 **高级** 部分。
1. 向下滚动到 **删除项目** 部分。
1. 点击 **删除项目**。
1. 通过输入预期文本来确认此操作。

个人命名空间中的项目会根据要求立即删除。关于群组内项目延迟删除的信息，请参见[启用延迟项目删除](../group/index.md#启用延迟项目移除)。

## 项目设置

设置项目的可见性级别和对其各个页面的访问级别，并执行诸如归档、重命名或转换项目之类的操作。

<!--Read through the documentation on [project settings](settings/index.md).-->

## 项目动态

要查看项目的动态：

1. 在左侧边栏上，选择 **项目信息 > 动态**。
1. 选择一个选项卡以查看 **全部** 动态，或按以下任一条件对其进行过滤：
    - **推送事件**
    - **合并事件**
    - **议题事件**
    - **评论**
    - **团队**
    - **Wiki**

### 离开一个项目

**离开项目** 仅在项目属于群组（在 [群组命名空间](../group/index.md#命名空间) 下）时显示在项目的仪表板上。如果您选择离开项目，您将不再是项目成员，也无法做出贡献。

## 使用您的项目作为 Go 包

任何项目都可以用作 Go 包，支持正确响应 `go get` 和 `godoc.org` 发现请求，包括 [`go-import`](https://golang.org/cmd/go/#hdr-Remote_import_paths) 和 [`go-source `](https://github.com/golang/gddo/wiki/Source-Code-Links) 元标签。

私有项目，包括子组中的项目，可以用作 Go 包，但可能需要配置才能正常工作。系统会正确响应对*不在*子组中的项目的 `go get` 发现请求，无论身份验证或授权如何。
[验证](#验证-go-请求)需要使用子组中的私有项目作为 Go 包。否则，会将子组中私有项目的路径截断到前两个段，从而导致 `go get` 失败。

<!--
GitLab implements its own Go proxy. This feature must be enabled by an
administrator and requires additional configuration. See [GitLab Go
Proxy](../packages/go_proxy/index.md).
-->

### 禁用私有项目的 Go 模块功能

在 Go 1.12 及更高版本中，Go 在获取模块<!--[获取模块](../../development/go_guide/dependencies.md#fetching)-->的过程中查询模块代理和校验和数据库。这可以通过 `GOPRIVATE`（禁用两者）、`GONOPROXY`<!--[`GONOPROXY`](../../development/go_guide/dependencies.md#proxies)-->（禁用代理查询）和 `GONOSUMDB`<!--[`GONOSUMDB`](../../development/go_guide/dependencies.md#fetching)-->（禁用校验和查询）。

`GOPRIVATE`、`GONOPROXY` 和 `GONOSUMDB` 是 Go 模块和 Go 模块前缀的逗号分隔列表。例如，`GOPRIVATE=gitlab.example.com/my/private/project` 禁用对该项目的查询，但 `GOPRIVATE=gitlab.example.com` 禁用对 *全部* 项目的查询。如果模块名称或其前缀出现在 `GOPRIVATE` 或 `GONOPROXY` 中，Go 不会查询模块代理。如果模块名称或其前缀出现在 `GONOPRIVATE` 或 `GONOSUMDB` 中，Go 不会查询校验和数据库。

### 验证 Go 请求

要验证 Go 对私有项目的请求，请在密码字段中使用 [`.netrc` 文件](https://everything.curl.dev/usingcurl/netrc) 和[个人访问令牌](../profile/personal_access_tokens.md)。 **仅在您的实例可以使用 HTTPS 访问时才有效。** `go` 命令不会通过不安全的连接传输凭据，将验证 Go 直接发出的所有 HTTPS 请求，但不会验证通过 Git 发出的请求。

例如：

```plaintext
machine gitlab.example.com
login <gitlab_user_name>
password <personal_access_token>
```

NOTE:
在 Windows 上，Go 读取 `~/_netrc` 而不是 `~/.netrc`。

### 验证 Git 获取

如果无法从代理获取模块，Go 将回退到使用 Git（对于极狐GitLab 项目）。Git 使用 `.netrc` 来验证请求。您还可以将 Git 配置为：

- 在请求 URL 中嵌入特定凭据。
- 使用 SSH 而不是 HTTPS，因为 Go 总是使用 HTTPS 来获取 Git 仓库。


```shell
# Embed credentials in any request to GitLab.com:
git config --global url."https://${user}:${personal_access_token}@gitlab.example.com".insteadOf "https://gitlab.example.com"

# Use SSH instead of HTTPS:
git config --global url."git@gitlab.example.com".insteadOf "https://gitlab.example.com"
```

### 从 Geo 次要站点获取 Go 模块

由于 Go 模块存储在 Git 仓库中，您可以使用 Geo<!--[Geo](../../administration/geo/index.md)--> 功能，该功能允许在 Geo 次要节点上访问 Git 仓库。

在下面的例子中，主站点域名是 `gitlab.example.com`，次要站点域名是`gitlab-secondary.example.com`。

`go get` 最初会向主节点生成一些 HTTP 流量，但是当模块下载开始时，`insteadOf` 配置将流量发送到次要节点。

#### 使用 SSH 访问 Geo 次要节点

要使用 SSH 从辅助节点获取 Go 模块：

1. 在客户端重新配置 Git 以将主要节点的流量发送到次要节点：

   ```plaintext
   git config --global url."git@gitlab-secondary.example.com".insteadOf "https://gitlab.example.com"
   git config --global url."git@gitlab-secondary.example.com".insteadOf "http://gitlab.example.com"
   ```

1. 确保客户端设置为通过 SSH 访问极狐GitLab 仓库。可以在主要节点上进行测试，极狐GitLab 会将公钥复制到次要节点。

#### 使用 HTTP 访问 Geo 次要节点

使用 HTTP 获取 Go 模块不适用 CI/CD 作业令牌，仅适用复制到次要节点的持久访问令牌。

要使用 HTTP 从次要节点获取 Go 模块：

1. 在客户端上放置一个 Git `insteadOf` 重定向：

   ```plaintext
   git config --global url."https://gitlab-secondary.example.com".insteadOf "https://gitlab.example.com"
   ```

1. 生成[个人访问令牌](../profile/personal_access_tokens.md)并在客户端的`~/.netrc` 文件中提供这些凭据：

   ```plaintext
   machine gitlab.example.com login USERNAME password TOKEN
   machine gitlab-secondary.example.com login USERNAME password TOKEN
   ```

## 使用项目 ID 访问项目页面

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/53671) in GitLab 11.8.
-->

要使用项目 ID 从 GitLab UI 快速访问项目，请访问浏览器中的 `/projects/:id` URL 或访问该项目的其他工具。

## 项目登录页面

项目的登录页面根据项目的可见性设置和用户权限显示不同的信息。

对于公共项目，以及具有查看项目代码的权限<!--[查看项目代码的权限](../permissions.md#project-members-permissions)-->的内部和私有项目的成员：

- 显示 <!--[`README` 或索引文件](repository/index.md#readme-and-index-files)-->`README` 或索引文件的内容（如果有），然后是项目仓库中的目录列表。
- 如果项目不包含这些文件中的任何一个，访问者将看到仓库的文件和目录列表。

对于没有权限查看项目代码的用户，显示：

- Wiki 主页，如果有的话。
- 项目中的议题列表。
