---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 管理议题 **(FREE)**

[议题](index.md)是在极狐GitLab 中就想法和计划工作进行协作的基本媒介。

议题的关键操作包括：

- [创建议题](#创建一个新议题)
- [移动议题](#移动议题)
- [关闭议题](#关闭议题)
- [删除议题](#删除议题)
- [提升议题](#将议题提升为史诗) **(PREMIUM)**

## 创建一个新议题

创建新议题时，系统会提示您输入议题的字段。
如果您知道要分配给议题的值，则可以使用 [快速操作](../quick_actions.md) 输入它们。

在创建议题时，您可以通过使用 **史诗** 下拉菜单，将其与当前群组中的现有史诗相关联。

### 访问新建议题表单

有多种方法可以从项目页面访问“新建议题”表单：

- 导航到您的 **项目面板** > **议题** > **新建议题**：

  ![New issue from the issue list view](img/new_issue_from_tracker_list.png)

- 在项目中的**打开的议题**，单击垂直省略号 (**{ellipsis_v}**) 按钮以打开下拉菜单，然后单击 **新建议题** 在同一项目中创建新问题 ：

  ![New issue from an open issue](img/new_issue_from_open_issue_v13_6.png)

- 在您的 **项目面板**，单击加号 (**+**) 以打开包含几个选项的下拉菜单。选择 **新建议题** 在该项目中创建一个议题：

  ![New issue from a project's dashboard](img/new_issue_from_projects_dashboard.png)

- 在 **议题面板**，通过单击列表顶部的加号 (**+**) 创建新议题。它为该项目打开了一个新议题，并预先标有其各自的列表。

  ![From the issue board](img/new_issue_from_issue_board.png)

### 新建议题表单的元素

> 旗舰版 13.1 中引入了将新议题添加到史诗的功能。

![New issue from the issues list](img/new_issue_v13_1.png)

创建新议题时，您可以填写以下字段：

- 标题
- 描述
- 配置议题[机密性](confidential_issues.md)的复选框
- 指派人
- 权重
- [史诗](../../group/epics/index.md)
- 截止日期
- 里程碑
- 标记

### 来自群组级议题跟踪器的新议题

要访问群组中所有项目的议题跟踪器：

1. 前往群组仪表板。
1. 在左侧边栏上，选择**问题**。
1. 在右上角，选择 **Select project to create issue** 按钮。
1. 选择您要为其创建议题的项目。该按钮现在显示了选定的项目。
1. 选择按钮在所选项目中创建议题。

![Select project to create issue](img/select_project_from_group_level_issue_tracker_v13_11.png)

您最近选择的项目将成为您下次访问的默认项目。如果您主要为同一个项目创建议题，这应该可以为您节省大量时间和点击次数。

### 通过服务台新建议题

为您的项目启用[服务台](../service_desk.md)并提供电子邮件支持。现在，当您的客户发送新电子邮件时，可以在适当的项目中创建新议题并从那里跟进。

### 通过电子邮件发送新议题

**将新议题通过电子邮件发送到该项目** 的链接显示在项目的 **问题列表** 页面底部。仅当您的实例配置了 incoming email<!--[incoming email](../../../administration/incoming_email.md)--> 并且议题列表中至少有一个议题时，才会显示该链接。

![Bottom of a project issues page](img/new_issue_from_email.png)

当您单击此链接时，将生成并显示一个电子邮件地址，该地址仅供**您**使用，以在此项目中创建议题。您可以将此地址保存为电子邮件客户端中的联系人，以便快速访问。

WARNING:
这是一个私人电子邮件地址，专为您生成。**保留给自己**，因为任何知道它的人都可以创建问题或合并请求，就好像他们是您一样。如果地址被盗用，或者您想重新生成它，请单击**将新问题发送到此项目**，然后单击**重置它**。

向此地址发送电子邮件会创建一个与您的此项目帐户相关联的新问题，其中：

- 电子邮件主题成为议题标题。
- 电子邮件正文成为议题描述。
- 支持 Markdown<!--[Markdown](../../markdown.md)--> 和[快速操作](../quick_actions.md)。

<!--
NOTE:
In GitLab 11.7, we updated the format of the generated email address. However the
older format is still supported, allowing existing aliases or contacts to continue working.
-->

### 通过带有预填充字段的 URL 新建议题

要直接链接到带有预填充字段的新议题页面，请在 URL 中使用查询字符串参数。您可以在外部 HTML 页面中嵌入 URL 以创建预先填充的某些字段的议题。

| 字段               | URL 参数名称    | 说明                                              |
|----------------------|-----------------------|-------------------------------------------------------|
| 标题                | `issue[title]`        |                                                       |
| 描述          | `issue[description]`  | 不能与 `issuable_template` 同时使用。 |
| 描述模板 | `issuable_template`   | 不能与 `issue[description]` 同时使用。  |
| 议题类型         | `issue[issue_type]`   | `incident` 或 `issue`。                         |
| 机密         | `issue[confidential]` | 参数值必须为 `true` 才能设置为机密。 |

<!--
Follow these examples to form your new issue URL with prefilled fields.

- For a new issue in the GitLab Community Edition project with a pre-filled title
  and a pre-filled description, the URL would be `https://gitlab.com/gitlab-org/gitlab-foss/-/issues/new?issue[title]=Validate%20new%20concept&issue[description]=Research%20idea`
- For a new issue in the GitLab Community Edition project with a pre-filled title
  and a pre-filled description template, the URL would be `https://gitlab.com/gitlab-org/gitlab-foss/-/issues/new?issue[title]=Validate%20new%20concept&issuable_template=Research%20proposal`
- For a new issue in the GitLab Community Edition project with a pre-filled title,
  a pre-filled description, and the confidential flag set, the URL would be `https://gitlab.com/gitlab-org/gitlab-foss/-/issues/new?issue[title]=Validate%20new%20concept&issue[description]=Research%20idea&issue[confidential]=true`
-->

## 编辑议题

您可以编辑问题的标题和描述。

要编辑议题，请选择 **编辑标题和描述** (**{pencil}**)。

## 项目级别的批量编辑问题

<!--
> - Assigning epic ([introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/210470) in GitLab 13.2.
> - Editing health status [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/218395) in GitLab 13.2.
> - Editing iteration [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/196806) in GitLab 13.9.
-->

具有报告者或更高<!--[报告者或更高](../../permissions.md)-->权限级别的用户可以管理议题。

在项目中批量编辑问题时，您可以编辑以下属性：

- 状态（开放/已关闭）
- 指派人
- [史诗](../../group/epics/index.md)
- 里程碑<!--[里程碑](../milestones/index.md)-->
- [标记](../labels.md)
- [健康状态](#健康状态)
- 通知订阅
- 迭代<!--[迭代](../../group/iterations/index.md)-->

要同时更新多个项目议题：

1. 在项目中，转到 **议题 > 列表**。
1. 单击 **编辑议题**。屏幕右侧会出现一个侧边栏，其中包含可编辑的字段。
1. 选中您要编辑的每个议题旁边的复选框。
1. 从侧栏中选择适当的字段及其值。
1. 点击 **全部更新**。

### 群组级别的批量编辑问题

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/7249) in GitLab 12.1.
> - Assigning epic ([introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/210470) in GitLab 13.2.
> - Editing health status [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/218395) in GitLab 13.2.
> - Editing iteration [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/196806) in GitLab 13.9.
-->

具有报告者或更高<!--[报告者或更高](../../permissions.md)-->权限级别的用户可以管理议题。

在群组中批量编辑议题时，您可以编辑以下属性：

- [史诗](../../group/epics/index.md)
- 里程碑<!--[里程碑](../milestones/index.md)-->
- [标记](../labels.md)
- [健康状态](#健康状态)
- 迭代<!--[迭代](../../group/iterations/index.md)-->

同时更新多个项目议题：

1. 在群组中，转到**议题 > 列表**。
1. 单击**编辑议题**。 屏幕右侧会出现一个侧边栏，其中包含可编辑的字段。
1. 选中您要编辑的每个问题旁边的复选框。
1. 从侧栏中选择适当的字段及其值。
1. 点击**全部更新**。

## 移动议题

移动议题将其复制到目标项目，并在原始项目中关闭它。原议题没有删除。系统注释被添加到两个议题中，表明它来自哪里，去哪里。

查看议题时，“移动议题”按钮位于右侧栏的底部。

![move issue - button](img/sidebar_move_issue.png)

### 批量移动议题 **(FREE SELF)**

如果您具有高级技术技能，您还可以在 Rails 控制台中将所有议题从一个项目批量移动到另一个项目。以下脚本将所有未处于 **关闭** 状态的议题从一个项目移动到另一个项目。

要访问 rails 控制台，请在 GitLab 服务器上运行 `sudo gitlab-rails console` 并运行以下脚本。请务必将 `project`、`admin_user` 和 `target_project` 更改为您的值。我们还建议在尝试在控制台中进行任何更改之前创建备份<!--[创建备份](../../../raketasks/backup_restore.md#back-up-gitlab)-->。

```ruby
project = Project.find_by_full_path('full path of the project where issues are moved from')
issues = project.issues
admin_user = User.find_by_username('username of admin user') # make sure user has permissions to move the issues
target_project = Project.find_by_full_path('full path of target project where issues moved to')

issues.each do |issue|
   if issue.state != "closed" && issue.moved_to.nil?
      Issues::MoveService.new(project, admin_user).execute(issue, target_project)
   else
      puts "issue with id: #{issue.id} and title: #{issue.title} was not moved"
   end
end; nil
```

## 关闭议题

当您决定某个议题已解决或不再需要时，您可以关闭该议题。
该议题已标记为已关闭但未删除。

要关闭议题，您可以执行以下操作：

- 选择 **关闭议题**：

  ![close issue - button](img/button_close_issue_v13_6.png)

- 在[议题看板](../issue_board.md)中，将议题卡从其列表拖到 **已关闭** 列表中。

  ![close issue from the issue board](img/close_issue_from_board.gif)

### 重新开放关闭的议题

要重新打开已关闭的议题，请选择**重新打开议题**。
重新打开的议题与任何其他未解决的议题有什么不同。

### 自动关闭议题

当提交或合并请求解决了议题，且提交到达项目的默认分支时，议题可以自动关闭。

如果提交消息或合并请求描述包含与[定义模式](#默认关闭模式)匹配的文本，则匹配文本中引用的所有议题都将关闭。当提交被推送到项目的**默认**分支<!--[**默认**分支](../repository/branches/default.md)-->时，或者当提交或合并请求合并到其中时，就会发生这种情况。

例如，如果 `Closes #4, #6, Related to #5` 包含在合并请求描述中，则在合并 MR 时会自动关闭问题 `#4` 和 `#6`，而不是 `#5`。使用 `Related to` 标志 `#5` 作为[相关议题](related_issues.md)，但不会自动关闭。

![merge request closing issue when merged](img/merge_request_closes_issue_v13_11.png)

如果议题与 MR 位于不同的仓库中，请添加议题的完整 URL：

```markdown
Closes #4, #6, and https://gitlab.com/<username>/<projectname>/issues/<xxx>
```

出于性能原因，第一次从现有仓库推送时禁用自动关闭议题。

或者，当您从议题创建合并请求 <!--[从议题创建合并请求](../merge_requests/getting_started.md#merge-requests-to-close-issues)--> 时，它会继承议题的里程碑和标签。

#### 默认关闭模式

如果未指定，则使用此默认议题关闭模式：

```shell
\b((?:[Cc]los(?:e[sd]?|ing)|\b[Ff]ix(?:e[sd]|ing)?|\b[Rr]esolv(?:e[sd]?|ing)|\b[Ii]mplement(?:s|ed|ing)?)(:?) +(?:(?:issues? +)?%{issue_ref}(?:(?: *,? +and +| *,? *)?)|([A-Z][A-Z0-9_]+-\d+))+)
```

转化为以下关键字：

- Close, Closes, Closed, Closing, close, closes, closed, closing
- Fix, Fixes, Fixed, Fixing, fix, fixes, fixed, fixing
- Resolve, Resolves, Resolved, Resolving, resolve, resolves, resolved, resolving
- Implement, Implements, Implemented, Implementing, implement, implements, implemented, implementing

请注意，`%{issue_ref}` 是一个复杂的正则表达式，定义在源代码中，可以匹配对以下内容的引用：

- 一个本地议题（`#123`）。
- 一个跨项目议题（`group/project#123`）。
- 一个议题的链接（`https://gitlab.example.com/group/project/issues/123`）。

例如以下提交消息：

```plaintext
Awesome commit message

Fix #20, Fixes #21 and Closes group/otherproject#22.
This commit is also related to #17 and fixes #18, #19
and https://gitlab.example.com/group/otherproject/issues/23.
```

关闭此提交所推送到的项目中的 `#18`、`#19`、`#20` 和 `#21`，以及 `group/otherproject`  中的 `#22` 和 `#23`。`#17` 没有关闭，因为它与模式不匹配。当从命令行使用 `git commit -m` 时，它适用于多行提交消息以及单行。

#### 禁用自动关闭议题

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/19754) in GitLab 12.7.
-->

可以在项目的仓库设置<!--[项目的仓库设置](../settings/index.md)-->中针对每个项目禁用自动议题关闭功能。引用的议题仍会显示，但不会自动关闭。

![disable issue auto close - settings](img/disable_issue_auto_close.png)

如果项目禁用了议题跟踪器，则项目中的自动关闭议题也会被禁用。如果要启用自动关闭议题，请确保启用了议题<!--[启用了议题](../settings/index.md#sharing-and-permissions)-->。

这仅适用于受新合并请求或提交影响的议题。已经关闭的议题保持原样。
如果启用了议题跟踪，则禁用自动关闭议题仅适用于尝试在同一项目中自动关闭议题的合并请求。
其他项目中的合并请求仍然可以关闭另一个项目的议题。

#### 自定义议题关闭模式 **(FREE SELF)**

为了更改默认的议题关闭模式，极狐GitLab 管理员必须编辑您安装的 `gitlab.rb` 或 `gitlab.yml` 文件 <!--[`gitlab.rb` 或 `gitlab.yml` 文件](../../../administration/issue_closure_pattern.md)-->。

## 更改议题类型

具有开发者角色<!--[开发者角色](../../permissions.md)-->的用户可以更改议题的类型。为此，请编辑议题并从**议题类型**选择器菜单中选择一个议题类型：

- [Issue](index.md)
- Incident<!--[Incident](../../../operations/incident_management/index.md)-->

![Change the issue type](img/issue_type_change_v13_12.png)

## 删除议题

具有所有者角色<!--[所有者角色](../../permissions.md)-->的用户可以通过编辑议题并选择**删除议题**。

![delete issue - button](img/delete_issue_v13_11.png)

## 将议题提升为史诗 **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/3777) in GitLab Ultimate 11.6.
> - Moved to GitLab Premium in 12.8.
> - Promoting issues to epics via the UI [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/233974) in GitLab Premium 13.6.
-->

您可以将议题提升为直接父组中的史诗。

将议题提升为史诗：

1. 在一个议题中，选择垂直省略号 (**{ellipsis_v}**) 按钮。
1. 选择**提升为史诗**。

或者，您可以使用 `/promote` [快速操作](../quick_actions.md#议题合并请求和史诗)。

在[管理史诗页面](../../group/epics/manage_epics.md#将一个议题提升为史诗)文档上阅读有关将问题提升为史诗的更多信息。

## 向迭代添加议题 **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/216158) in GitLab 13.2.
> - Moved to GitLab Premium in 13.9.
-->

向迭代<!--[迭代](../../group/iterations/index.md)-->添加议题：

1. 在议题侧边栏中，单击 **迭代** 旁边的 **编辑**。出现下拉列表。
1. 单击您希望将此议题与其关联的迭代。

您还可以在评论或描述字段中使用 `/iteration` [快速操作](../quick_actions.md#议题合并请求和史诗)。

## 复制议题引用

要引用极狐GitLab 中其他地方的议题，您可以使用其完整 URL 或简短引用，类似于 `namespace/project-name#123`，其中 `namespace` 是群组或用户名。

要将议题引用复制到剪贴板：

1. 转到议题。
1. 在右侧边栏的 **引用** 旁边，选择 **复制引用** (**{copy-to-clipboard}**)。

您现在可以将引用地址粘贴到另一个描述或评论中。

<!--
Read more about issue references in [GitLab-Flavored Markdown](../../markdown.md#gitlab-specific-references).
-->

## 复制议题电子邮件

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/18816) in GitLab 13.8.
-->

您可以通过发送电子邮件在议题中创建评论。

要复制议题的电子邮件地址：

1. 转到议题。
1. 在右侧边栏 **发邮件** 旁边，选择 **复制引用** (**{copy-to-clipboard}**)。

向此地址发送电子邮件会创建包含电子邮件正文的评论。

<!--
要了解有关通过发送电子邮件和必要配置创建评论的更多信息，请参阅[通过发送电子邮件回复评论](../../discussions/index.md#reply-to-a-comment-by-sending- 电子邮件）。
-->

## 实时侧边栏 **(FREE SELF)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/17589) in GitLab 13.3.
-->

侧边栏中的指派人实时更新。此特性功能 **默认禁用**。
要启用它，您需要启用 [Action Cable 应用内模式](https://docs.gitlab.cn/omnibus/settings/actioncable.html)。

## 指派人

一个议题可以分配给一个或[多个用户](multiple_assignees_for_issues.md)。

指派人可以根据需要随时更改。指派人是对议题负责的人。
当一个议题被分配给某人时，它会出现在他们分配的议题列表中。

如果用户不是项目的成员，则只能将议题分配给他们，前提是他们自己创建议题或其他项目成员将议题分配给他们。

要更改议题的指派人：

1. 转到您的议题。
1. 在右侧边栏的 **指派人** 部分中，选择 **编辑**。
1. 从下拉列表中选择要添加为指派人的用户。
1. 选择下拉列表之外的任何区域。

## 类似议题

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/22866) in GitLab 11.6.
-->

为防止同一主题出现重复议题，系统在创建新议题时会搜索类似议题。

当您在**新建议题**页面的标题字段中键入时，系统会搜索当前项目中所有议题的标题和描述。仅返回您有权访问的议题。
标题框下方最多显示五个类似问题，按最近更新排序。必须启用 GraphQL<!--[GraphQL](../../../api/graphql/index.md)--> 才能使用此功能。

![Similar issues](img/similar_issues.png)

## 健康状态 **(ULTIMATE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/36427) in GitLab Ultimate 12.10.
> - Health status of closed issues [can't be edited](https://gitlab.com/gitlab-org/gitlab/-/issues/220867) in GitLab Ultimate 13.4 and later.
> - Issue health status visible in issue lists [introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/45141) in GitLab Ultimate 13.6.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/213567) in GitLab 13.7.
-->

为了帮助您跟踪议题状态，您可以为每个议题分配一个状态。
这将议题标记为按计划进行或需要注意以按计划进行：

- On track（绿色）
- 需要注意 (琥珀黄)
- 有风险（红色）

议题关闭后，无法编辑其运行状况，并且在重新打开议题之前 **编辑** 按钮将变为禁用状态。

然后，您可以在议题列表和史诗树中查看议题状态。

## 公开议题 **(ULTIMATE)**

> 引入于 13.1 版本。

如果状态页面应用程序与项目相关联，您可以使用 `/publish` [快速操作](../quick_actions.md) 发布议题。

<!--
For more information, see [GitLab Status Page](../../../operations/incident_management/status_page.md).
-->