---
stage: Plan
group: Product Planning
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
---

# 设计管理 **(FREE)**

<!--
> - [Introduced](https://gitlab.com/groups/gitlab-org/-/epics/660) in [GitLab Premium](https://about.gitlab.com/pricing/) 12.2.
> - Support for SVGs was [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/12771) in [GitLab Premium](https://about.gitlab.com/pricing/) 12.4.
> - Design Management was [moved](https://gitlab.com/gitlab-org/gitlab/-/issues/212566) to GitLab Free in 13.0.
-->

设计管理允许您将设计资产（包括线框和模型）上传到议题并将它们存储在一个地方，通过议题内的设计管理页面访问，为产品设计师、产品经理和工程师提供了一种在单一事实来源上协作设计的方法。

您可以与您的团队共享设计模型，或者可以轻松查看和解决视觉回归。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview, see the video [Design Management (GitLab 12.2)](https://www.youtube.com/watch?v=CCMtCqdK_aM).
-->

## 要求

设计管理需要启用 Large File Storage (LFS)<!--[Large File Storage (LFS)](../../../topics/git/lfs/index.md)-->：

<!--- For GitLab.com, LFS is already enabled.-->
- 对于自助管理实例，管理员必须全局启用 LFS<!--[全局启用 LFS](../../../administration/lfs/index.md)-->。
- <!--对于 GitLab.com 和自管理实例：-->必须为项目本身启用 LFS。如果全局启用，默认情况下所有项目都启用 LFS。 要在项目级别启用 LFS，请导航到项目的 **设置 > 通用**，展开 **可见性，项目功能，权限** 并启用 **Git 大文件存储 (LFS) **。

设计管理还要求项目使用[哈希存储](../../../administration/raketasks/storage.md#迁移到哈希存储)。新创建的项目默认使用哈希存储。管理员可以通过导航到 **管理中心 > 项目**，然后选择有议题的项目来验证项目的存储类型。如果项目的 *Gitaly 相对路径* 包含 `@hashed`，则可以将项目标识为哈希存储。

如果不满足要求，**设计** 选项卡会向用户显示一条消息。

## 支持的文件

上传的文件必须具有 `png`、`jpg`、`jpeg`、`gif`、`bmp`、`tiff`、`ico`、`webp` 或 `svg` 的文件扩展名。

<!--
Support for PDF is tracked [in this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/32811).
-->

## 限制

- 设计上传一次限制为 10 个文件。
- 从 13.1 版本开始，设计文件名限制为 255 个字符。
- 当项目被销毁时，设计管理数据不会被删除。
- 删除议题时不会删除设计管理数据。
- 从 12.7 版本开始，设计管理数据通过 Geo 可以复制<!--[可以复制](../../../administration/geo/replication/datatypes.md#limitations-on-replicationverification)-->，但未验证。
- 只能删除最新版本的设计。
- 删除的设计无法恢复，但您可以在以前的设计版本中看到它们。

## GitLab-Figma 插件

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-figma-plugin/-/issues/2) in GitLab 13.2.-->

在无缝工作流程中将您的设计环境与源代码管理连接起来。GitLab-Figma 插件通过将产品设计师的工作作为上传的设计，直接从 Figma 带到议题，从而可以快速轻松地在极狐GitLab 中进行协作。

要使用该插件，请从 [Figma 目录](https://www.figma.com/community/plugin/860845891704482356)安装它，并通过个人访问令牌连接到 极狐GitLab。<!--The details are explained in the [plugin documentation](https://gitlab.com/gitlab-org/gitlab-figma-plugin/-/wikis/home).-->

## 设计管理部分

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/223193) in GitLab 13.2, Designs are displayed directly on the issue description rather than on a separate tab.
> - New display's feature flag [removed](https://gitlab.com/gitlab-org/gitlab/-/issues/223197) in GitLab 13.4.
-->

您可以在议题描述中的**设计管理**部分找到：

![Designs section](img/design_management_v13_2.png)

## 添加设计

要上传设计图像，请将文件从您的计算机拖放到设计管理部分，或选择 **上传** 从文件浏览器中选择图像：

![Designs empty state](img/design_management_upload_v13.3.png)

您可以将设计拖放到专用放置区以上传它们。

![Drag and drop design uploads](img/design_drag_and_drop_uploads_v13_2.png)

您还可以从文件系统复制图像并将它们作为新设计直接粘贴到极狐GitLab 设计页面上。

在 macOS 上，您可以通过同时按 <kbd>Control</kbd> + <kbd>Command</kbd> + <kbd>Shift</kbd> + <kbd>3</kbd> 来截取屏幕截图并立即将其复制到剪贴板，然后将其粘贴为设计。

复制粘贴有一些限制：

- 一次只能粘贴一张图片。复制/粘贴多个文件时，只会上传第一个文件。
- 所有图像都转换为 `png` 格式，因此当您要复制/粘贴 `gif` 文件时，会导致动画损坏。
- 如果您从剪贴板粘贴屏幕截图，它会被重命名为 `design_<timestamp>.png`。
- Internet Explorer 不支持复制/粘贴设计。

与现有上载设计具有相同文件名的设计会创建设计的新版本，并替换之前的版本。如果文件名相同，将设计放在现有上传的设计上也会创建新版本。

### 跳过的设计

与现有上传的设计具有相同文件名且内容未更改的设计将被跳过。
这意味着不会创建新版本的设计。跳过设计时，您会通过议题上的警告消息获知。

## 查看设计

设计管理页面上的图像可以通过单击放大。
您可以通过选择右上角的导航按钮或使用 <kbd>Left</kbd>/<kbd>Right</kbd> 键盘按钮来浏览设计。

关于设计的讨论数量（如果有）列在设计文件名的右侧。单击此数字会放大设计，就像单击设计上的任何其他地方一样。
添加或修改设计时，项目上会显示一个图标，以帮助汇总版本之间的更改。

| 指示符 | 示例 |
| --------- | ------- |
| 讨论 | ![Discussions Icon](img/design_comments_v12_3.png) |
| 已修改（所选版本中） | ![Design Modified](img/design_modified_v12_3.png) |
| 已添加（所选版本中） | ![Design Added](img/design_added_v12_3.png) |

### 通过缩放浏览设计

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/13217) in [GitLab Premium](https://about.gitlab.com/pricing/) 12.7.
-->

通过放大和缩小图像，可以更详细地浏览设计。
使用图像底部的 “+” 和 “-” 按钮控制缩放量。
放大后，您仍然可以在图像上[开始新讨论](#开始讨论设计)，并查看任何现有的讨论。

放大时，您可以在图像上拖动，以在其周围移动。

![Design zooming](img/design_zooming_v12_7.png)

## 删除设计

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/11089) in [GitLab Premium](https://about.gitlab.com/pricing/) 12.4.
-->

删除设计有两种方式：手动逐个删除，或者选择几个一次性删除，如下图。

要删除单个设计，请单击它以放大查看，然后单击右上角的垃圾桶图标，并通过单击模式窗口上的 **删除** 按钮确认删除：

![Confirm design deletion](img/confirm_design_deletion_v12_4.png)

要一次删除多个设计，请在设计的列表视图中，首先选择要删除的设计：

![Select designs](img/select_designs_v12_4.png)

选择 **删除所选** 按钮以确认删除：

![Delete multiple designs](img/delete_multiple_designs_v12_4.png)

NOTE:
只能删除最新版本的设计。删除的设计不会永久丢失；可以通过浏览以前的版本来查看它们。

## 重新排序设计

> 在 13.3 版本中引入。

您可以通过将设计拖到新位置来更改设计的顺序。

## 开始讨论设计

上传设计后，您可以通过单击您希望讨论重点的确切位置上的图像来开始讨论。
图像中添加了一个图钉，用于标识讨论的位置。

![Starting a new discussion on design](img/adding_note_to_design_1.png)

您可以通过在图像周围拖动图钉来调整图钉的位置。当您的设计布局在修订之间发生更改时，或者您需要移动现有图钉以在其位置添加新图钉时，这非常有用。

不同的讨论有不同的图钉数字：

![Discussions on designs](img/adding_note_to_design_2.png)

新的讨论输出到议题活动中，让所有参与的人都可以参与讨论。

## 解决设计主题

> 在 13.1 版本中引入。

讨论主题可以在设计上解决。

有两种方法可以解决/取消解决设计主题：

1. 您可以通过单击讨论第一条评论右上角的 **解决主题** 的对勾图标，将主题标记为已解决或未解决：

  ![Resolve thread icon](img/resolve_design-discussion_icon_v13_1.png)

1. 也可以使用复选框，在主题中解决或取消解决设计主题。回复评论时，您可以单击一个复选框，以便在发布后解决或取消解决主题：

  ![Resolve checkbox](img/resolve_design-discussion_checkbox_v13_1.png)


解决讨论主题还会将主题内与笔记相关的任何待办事项标记为已完成。仅适用于触发操作的用户拥有的待办事项。

请注意，您已解决的评论图钉会从设计中消失，以便为新的讨论腾出空间。
但是，如果您需要重新访问或找到已解决的讨论，您可以在右侧边栏底部的 **已解决评论** 区域中找到所有已解决的主题。

## 为设计添加待办事项

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/198439) in GitLab 13.4.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/245074) in GitLab 13.5.
-->

选择设计侧栏上的 **添加待办事项**，为设计添加待办事项：

![To-do button](img/design_todo_button_v13_5.png)

## 在 Markdown 中引用设计

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/217160) in **GitLab 13.1**.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/258662) in **GitLab 13.5**
-->

我们支持在 Markdown<!--[Markdown](../../markdown.md)--> 中引用设计，它在整个应用程序中都可用，包括合并请求和议题描述、讨论和评论以及 wiki 页面。

支持完整的 URL 引用。<!--例如，如果我们在某处引用一个设计：-->

<!--
```markdown
See https://gitlab.cn/your-group/your-project/-/issues/123/designs/homescreen.png
```

这被呈现为：

> See [#123[homescreen.png]](https://gitlab.com/your-group/your-project/-/issues/123/designs/homescreen.png)
-->

## 设计活动记录

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/33051) in GitLab 13.1.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/225205) in GitLab 13.2.
-->

设计中的用户活动事件（创建、删除和更新）由极狐GitLab 跟踪并显示在用户配置文件<!--[用户配置文件](../../profile/index.md#access-your-user-profile)-->、群组<!--[群组](../../group/index.md#view-group-activity)-->和项目<!--[项目](../working_with_projects.md#project-activity)-->活动页面。
