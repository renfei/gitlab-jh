---
stage: Manage
group: Access
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, howto
---

# 项目访问令牌

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/210181) in GitLab 13.0.
> - [Became available on GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/issues/235765) in GitLab 13.5 for paid groups only.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/235765) in GitLab 13.5.
-->

项目访问令牌类似于[个人访问令牌](../../profile/personal_access_tokens.md)，不同之处在于它们附加到项目而不是用户。它们可用于：

- 使用 GitLab API<!--[GitLab API](../../../api/index.md#personalproject-access-tokens)--> 进行身份验证。
- 使用 HTTP 基本身份验证与 Git 进行身份验证。如果在认证时要求您输入用户名，您可以使用任何非空值，因为只需要令牌。

项目访问令牌：

- 在您定义的日期（UTC 午夜）到期。
- 支持标准版及以上级别的自助管理实例。标准版自我管理实例应该：
   - 查看他们关于用户自行注册<!--[用户自行注册](../../admin_area/settings/sign_up_restrictions.md#disable-new-sign-ups)-->的安全和合规政策。
   - 考虑[禁用项目访问令牌](#启用或禁用项目访问令牌创建)以减少潜在的滥用。
- SaaS 专业版及更高版本也支持（不包括[试用许可证](https://about.gitlab.cn/free-trial/)。）

<!--
For examples of how you can use a project access token to authenticate with the API, see the
[relevant section from our API Docs](../../../api/index.md#personalproject-access-tokens).
-->

## 创建一个项目访问令牌

1. 登录极狐GitLab。
1. 导航到您要为其创建访问令牌的项目。
1. 在 **设置** 菜单中选择**访问令牌**。
1. 为令牌选择一个名称和可选的到期日期。
1. 为令牌选择一个角色。
1. 选择[所需范围](#限制项目访问令牌的范围)。
1. 点击 **创建项目访问令牌** 按钮。
1. 将项目访问令牌保存在安全的地方。离开或刷新页面后，您将无法再次访问它。

## 项目机器人用户

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/210181) in GitLab 13.0.
> - [Excluded from license seat use](https://gitlab.com/gitlab-org/gitlab/-/issues/223695) in GitLab 13.5.
-->

项目机器人用户是极狐GitLab 创建的服务帐户<!--[极狐GitLab 创建的服务帐户](../../../subscriptions/self_managed/index.md#billable-users)-->，不计为许可席位。

对于创建的每个项目访问令牌，都会创建一个机器人用户并将其添加到具有指定级别权限的项目中。

对于机器人：

- 名称设置为令牌的名称。
- 第一个访问令牌的用户名设置为 `project_{project_id}_bot`，例如 `project_123_bot`。
- 用户名设置为 `project_{project_id}_bot{bot_count}` 以获取更多访问令牌，例如 `project_123_bot1`。

使用项目访问令牌进行的 API 调用与相应的机器人用户相关联。

这些机器人用户包含在项目的 **项目信息 > 成员** 列表中，但无法修改。此外，机器人用户不能添加到任何其他项目。

当项目访问令牌被[撤销](#撤销项目访问令牌) 时，bot 用户将被删除，所有记录都将移动到用户名为 “Ghost User” 的系统范围用户。<!--有关详细信息，请参阅 [关联记录](../../profile/account/delete_account.md#related-records)。-->

## 撤销项目访问令牌

您可以随时通过单击 **设置 > 访问令牌** 中相应的 **撤销** 按钮来撤销任何项目访问令牌。

## 限制项目访问令牌的范围

可以使用一个或多个范围创建项目访问令牌，这些范围允许给定令牌可以执行的各种操作。下表描述了可用的范围。

| 范围            |  描述 |
| ------------------ |  ----------- |
| `api`              | 授予对范围项目 API 的完整读/写访问权限，包括软件包库。 |
| `read_api`         | 授予对范围项目 API 的读取访问权限，包括软件包库。 |
| `read_registry`    | 如果项目是私有的并且需要验证，则允许对容器镜像库镜像进行读取访问（拉取）。 |
| `write_registry`   | 允许对容器镜像库进行写访问（推送）。 |
| `read_repository`  | 允许对仓库进行只读访问（拉取）。 |
| `write_repository` | 允许对仓库进行读写访问（拉取、推送）。 |

## 启用或禁用项目访问令牌创建

> 引入于 13.11 版本。

您可以在 **群组 > 设置 > 通用 > 权限, LFS, 2FA > 允许创建项目访问令牌** 中，为群组中的所有项目启用或禁用项目访问令牌创建。 即使创建被禁用，您仍然可以使用和撤销现有的项目访问令牌。此设置仅适用于顶级组。

## 群组访问令牌解决方法 **(FREE SELF)**

NOTE:
本节介绍了一种解决方法，可能会发生变化。

群组访问令牌让您可以使用单个令牌：

- 在群组级别执行操作。
- 管理群组内的项目。
- 在 14.2 及更高版本, 通过 HTTPS 使用 Git 进行身份验证。

不支持 UI 中的群组访问令牌，尽管自助管理实例的管理员可以使用<!--[Rails 控制台](../../../administration/operations/rails_console.md)--> Rails 控制台创建它们。

<!--
<div class="video-fallback">
  For a demo of the group access token workaround, see <a href="https://www.youtube.com/watch?v=W2fg1P1xmU0">Demo: Group Level Access Tokens</a>.
</div>
<figure class="video-container">
  <iframe src="https://www.youtube.com/embed/W2fg1P1xmU0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
-->

### 创建一个群组访问令牌

要创建群组访问令牌，请在 Rails 控制台中运行以下命令：

```ruby
admin = User.find(1) # group admin
group = Group.find(109) # the group you want to create a token for
bot = Users::CreateService.new(admin, { name: 'group_token', username: "group_#{group.id}_bot", email: "group_#{group.id}_bot@example.com", user_type: :project_bot }).execute # create the group bot user
# for further group access tokens, the username should be group_#{group.id}_bot#{bot_count}, e.g. group_109_bot2, and their email should be group_109_bot2@example.com
bot.confirm # confirm the bot
group.add_user(bot, :maintainer) # add the bot to the group at the desired access level
token = bot.personal_access_tokens.create(scopes:[:api, :write_repository], name: 'group_token') # give it a PAT
gtoken = token.token # get the token value
```

测试生成的群组访问令牌是否有效：

1. 将 `PRIVATE-TOKEN` 标头的群组访问令牌传递给 REST API。

   <!--例如：

   - [Create an epic](../../../api/epics.md#new-epic) on the group.
   - [Create a project pipeline](../../../api/pipelines.md#create-a-new-pipeline)
     in one of the group's projects.
   - [Create an issue](../../../api/issues.md#new-issue) in one of the group's projects.
   -->

1. 使用群组令牌克隆使用[群组中使用 HTTPS 的项目](../../../gitlab-basics/start-using-git.md#用-ssh-进行克隆)。

### 撤销群组访问令牌

要撤销群组访问令牌，请在 Rails 控制台中运行以下命令：

```ruby
bot = User.find_by(username: 'group_109_bot') # the owner of the token you want to revoke
token = bot.personal_access_tokens.last # the token you want to revoke
token.revoke!
```
