---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, concepts
---

# 当流水线成功时合并 **(FREE)**

在审核看起来准备合并但仍有流水线正在运行的合并请求时，您可以将其设置为在流水线成功时自动合并。这样，您不必等待流水线完成并记住手动合并请求。

![Enable](img/merge_when_pipeline_succeeds_enable.png)

## 如何工作

当您单击 “流水线成功时合并” 时，合并请求的状态会更新以显示即将进行的合并。如果您等不及流水线成功，您可以在主按钮右侧的下拉菜单中选择 **立即合并**。

合并请求的作者和具有开发者角色的项目成员可以在流水线完成之前随时取消自动合并。

![Status](img/merge_when_pipeline_succeeds_status.png)

当流水线成功时，合并请求会自动合并。
当流水线失败时，作者有机会重试任何失败的作业，或推送新的提交以修复失败。

当作业重试并在第二次尝试成功时，合并请求将自动合并。当使用新提交更新合并请求时，自动合并将被取消以允许审核新更改。

默认情况下，所有主题必须都被解决，然后才能看到 **流水线成功时合并** 按钮。如果有人在选择按钮后添加新评论，但在 CI 流水线中的作业完成之前，合并将被阻止，直到您解决所有现有主题。

## 仅流水线成功允许合并合并请求

如果出现以下情况，您可以阻止合并请求被合并：

- 没有流水线运行。
- 流水线没有成功。

适用于：

- GitLab CI/CD 流水线
- 流水线从外部 CI 集成<!--[外部 CI 集成](../integrations/overview.md#integrations-listing)-->运行

因此，<!--[禁用 GitLab CI/CD 流水线](../../../ci/enable_or_disable_ci.md)-->禁用 GitLab CI/CD 流水线不会禁用此功能，因为可以使用来自具有此功能的外部 CI 提供商的流水线。要启用它，您必须：

1. 导航到您项目的 **设置 > 通用** 页面。
1. 展开 **合并请求** 部分。
1. 在 **合并检查** 部分，选中 **流水线必须成功** 复选框。
1. 按 **保存** 以使更改生效。

如果没有流水线，此设置还可以防止合并请求被合并。
您应该谨慎配置 CI/CD，以便为每个合并请求运行流水线。

### 限制

启用此设置后，如果没有流水线，将阻止合并请求。这可能与使用 `only/except` 或 `rules` 并且不生成任何流水线的某些用例相冲突。

您应该确保始终有一个流水线并且它是成功的。

如果单个合并请求同时触发了分支流水线和合并请求流水线，则只检查*合并请求流水线*的成功或失败。如果合并请求流水线配置的作业少于分支流水线，则可能允许合并未通过测试的代码：

```yaml
branch-pipeline-job:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"'
  script:
    - echo "Code testing scripts here, for example."

merge-request-pipeline-job:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
  script:
    - echo "No tests run, but this pipeline always succeeds and enables merge."
    - echo true
```

您应该避免这种配置, 并且在可能的情况下仅使用分支（`push`）流水线或合并请求流水线。<!--See [`rules` documentation](../../../ci/jobs/job_control.md#avoid-duplicate-pipelines)
for details on avoiding two pipelines for a single merge request.-->

### 跳过的流水线

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/211482) in GitLab 13.1.
-->

当 **流水线必须成功** 复选框被选中时，跳过的流水线<!--[跳过的流水线](../../../ci/pipelines/index.md#skip-a-pipeline)-->会阻止合并请求被合并。要更改：

1. 导航到您项目的 **设置 > 通用** 页面。
1. 展开 **合并请求** 部分。
1. 在 **合并检查** 部分，确保选中 **流水线必须成功**。
1. 在 **合并检查** 小节中，选中 **Skipped 流水线被认为成功** 复选框。
1. 按 **保存** 以使更改生效。

## 从命令行

从命令行推送时，您可以使用[推送选项](../push_options.md)在流水线成功时为合并请求启用合并。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
