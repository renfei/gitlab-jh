---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, concepts
---

# 合并请求批准设置 **(PREMIUM)**

您可以配置[合并请求批准](index.md) 的设置，以确保批准规则符合您的用例。您还可以配置[批准规则](rules.md)，它定义了在合并之前必须批准工作的用户数量和类型。合并请求批准设置定义了在合并请求接近完成时如何应用这些规则。

## 编辑合并请求批准设置

查看或编辑合并请求批准设置：

1. 转到您的项目并选择 **设置 > 通用**。
1. 展开**合并请求 (MR) 批准**。

在本节通用设置中，您可以配置以下设置：

| 设置 | 描述 |
| ------ | ------ |
| [阻止作者批准](#阻止作者批准)  | 启用后，合并请求的作者无法批准它。 |
| [阻止添加提交的用户批准](#阻止添加提交的用户批准) | 启用后，已提交合并请求的用户无法批准它。 |
| [阻止在合并请求中编辑批准规则](#阻止在合并请求中编辑批准规则) | 启用后，用户无法覆盖项目对合并请求的批准规则。  |
| [需要用户密码才能核准](#需要用户密码才能核准) | 强制潜在的核准人首先使用密码进行身份验证。 |
| [将提交添加到源分支时删除所有核准](#将提交添加到源分支时删除所有核准) | 启用后，当添加更多更改时，删除合并请求上的所有现有核准。 |

## 阻止作者批准

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/3349) in GitLab 11.3.
> - Moved to GitLab Premium in 13.9.
-->

默认情况下，合并请求的作者无法批准它。要更改此设置：

1. 转到您的项目并选择 **设置 > 通用**。
1. 展开 **合并请求 (MR) 批准**。
1. 清除 **阻止作者批准** 复选框。
1. 选择 **保存更改**。

作者可以编辑单个合并请求中的批准规则并覆盖此设置，除非您配置以下选项之一：

- 在项目级别[阻止覆盖默认批准](#阻止在合并请求中编辑批准规则)
- *（仅限自助管理实例）* 在实例级别阻止覆盖默认批准。在实例级别配置时，您无法在项目或单个合并请求级别编辑此设置。

## 阻止添加提交的用户批准

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/10441) in GitLab 11.10.
> - Moved to GitLab Premium in 13.9.
-->

默认情况下，提交合并请求的用户仍然可以批准它。在项目级别或实例级别<!--[实例级别](../../../admin_area/merge_requests_approvals.md)-->，您可以阻止提交者批准部分属于他们自己的合并请求。操作如下：

1. 转到您的项目并选择 **设置 > 通用**。
1. 展开 **合并请求 (MR) 批准**。
1. 选中 **阻止添加提交的用户批准** 复选框。如果清除此复选框，管理员在实例级别<!--[在实例级别](../../../admin_area/merge_requests_approvals.md)-->已禁用它，并且无法在项目级别进行更改。
1. 选择 **保存更改**。

根据您的极狐GitLab 版本，提交合并请求的代码所有者<!--[代码所有者](../../code_owners.md)-->可能会也可能不会批准工作：

- 在 13.10 及更早版本中，提交合并请求的代码所有者<!--[代码所有者](../../code_owners.md)-->可以核准它，即使合并请求影响了他们拥有的文件。
- 在 13.11 及更高版本中，当合并请求影响他们拥有的文件时，提交合并请求的代码所有者<!--[代码所有者](../../code_owners.md)-->无法核准它。

要了解有关[作者和提交者之间的差异](https://git-scm.com/book/en/v2/Git-Basics-Viewing-the-Commit-History)的更多信息，请阅读官方 Git 文档。

## 阻止在合并请求中编辑批准规则

默认情况下，用户可以在每个合并请求的基础上覆盖您[为项目创建](rules.md)的批准规则。如果您不希望用户更改合并请求的批准规则，您可以禁用此设置：

1. 转到您的项目并选择 **设置 > 通用**。
1. 展开 **合并请求 (MR) 批准**。
1. 选中 **阻止在合并请求中编辑批准规则** 复选框。
1. 选择 **保存更改**。

此更改会影响所有打开的合并请求。

## 需要用户密码才能核准

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/5981) in GitLab 12.0.
> - Moved to GitLab Premium in 13.9.
-->

您可以强制潜在的核准人首先使用密码进行身份验证。<!--此许可启用电子签名以供批准，例如[联邦法规 (CFR) 第 11 部分](https://www.accessdata.fda.gov/scripts/cdrh/cfdocs/cfcfr/CFRSearch.cfm) 定义的签名 ?CFRPart=11&showFR=1&subpartNode=21:1.0.1.1.8.3))-->：

1. 为 Web 界面启用密码验证<!--，如[登录限制文档](../../../admin_area/settings/sign_in_restrictions.md#password-authentication-enabled) 中所述-->。
1. 转到您的项目并选择 **设置 > 通用**。
1. 展开 **合并请求 (MR) 批准**。
1. 选中 **需要用户密码才能批准** 复选框。
1. 选择 **保存更改**。

## 将提交添加到源分支时删除所有核准

默认情况下，即使您在批准后添加更多更改，对合并请求的批准仍然存在。如果要在添加更多更改时删除合并请求上的所有现有批准：

1. 转到您的项目并选择 **设置 > 通知**。
1. 展开 **合并请求 (MR) 批准**。
1. 选中 **将提交添加到源分支时删除所有批准** 复选框。
1. 选择 **保存更改**。

当合并请求[在 UI 上变基](../fast_forward_merge.md)时，不会重置批准。但是，如果目标分支发生更改，则会重置批准。

## 合并请求的安全批准 **(ULTIMATE)**

如果合并请求可能引入漏洞，您可以要求安全团队的成员批准合并请求。

<!--
要了解更多信息，请参阅[合并请求中的安全批准](../../../application_security/index.md#security-approvals-in-merge-requests)。
-->

## 代码覆盖率检查批准

如果合并请求会导致代码测试覆盖率下降，您可以要求获得特定批准。

<!--
To learn more, see [Coverage check approval rule](../../../../ci/pipelines/settings.md#coverage-check-approval-rule).

## Merge request approval settings cascading

> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/285410) in GitLab 14.4. [Deployed behind the `group_merge_request_approval_settings_feature_flag` flag](../../../../administration/feature_flags.md), disabled by default.
> - [Enabled by default](https://gitlab.com/gitlab-org/gitlab/-/issues/285410) in GitLab 14.5.

FLAG:
On self-managed GitLab, by default this feature is available. To hide the feature per group, ask an administrator to [disable the feature flag](../../../../administration/feature_flags.md) named `group_merge_request_approval_settings_feature_flag`. On GitLab.com, this feature is available.

You can also enforce merge request approval settings:

- At the [instance level](../../../admin_area/merge_requests_approvals.md), which apply to all groups on an instance and, therefore, all
  projects.
- On a [top-level group](../../../group/index.md#group-approval-rules), which apply to all subgroups and projects.

If the settings are inherited by a group or project, they cannot be overridden by the group or project that inherited them.

## Related links

- [Instance-level merge request approval settings](../../../admin_area/merge_requests_approvals.md)
- [Compliance report](../../../compliance/compliance_report/index.md)
- [Merge request approvals API](../../../../api/merge_request_approvals.md)
-->