---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 机密议题的合并请求

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/58583) in GitLab 12.1.
-->

公共仓库中的合并请求也是公开的，即使为[机密议题](../issues/confidential_issues.md)创建合并请求也是如此。为避免在处理机密议题时泄露机密信息，请从同一命名空间中的私有分支创建合并请求。

角色是从父组继承的。如果您在与原始（公共）仓库相同的命名空间（同一组或子组）中创建私有派生项目，则开发人员在您的派生项目中获得相同的权限。这种继承确保：

- 开发人员用户拥有查看机密议题并解决它们所需的权限。
- 您不需要授予个人用户访问您的派生项目的权限。

<!--
The security practices for confidential merge requests at GitLab are available to read.
-->

## 创建机密合并请求

默认情况下，分支是公共的。为了保护您工作的机密性，您必须在同一命名空间中创建分支和合并请求，但在私有派生项目的下游。 如果您在与公共仓库相同的命名空间中创建私有派生，则您的分支将继承上游公共仓库的权限。在上游公共仓库中具有开发者角色的用户，在您的下游私有派生项目中继承这些上游权限，而无需您采取任何行动。这些用户可以立即将代码推送到您的私有派生项目中的分支，以帮助解决机密议题。

WARNING:
如果您在与上游仓库不同的命名空间中创建它，您的私有派生项目可能会暴露机密信息。两个命名空间可能不包含相同的用户。

先决条件：

- 您在公共仓库中拥有所有者或维护者角色，因为您需要这些角色之一来[创建子组](../../group/subgroups/index.md)。
- 您已经派生<!--[派生](../repository/forking_workflow.md)-->公共仓库。
- 您的派生项目的 **可见性级别** 为*私有*。

要创建机密合并请求：

1. 转到机密议题的页面。在议题描述下方滚动并选择 **创建机密合并请求**。
1. 选择符合您需求的项目：
   - *要同时创建分支和合并请求，*选择 **创建机密合并请求和分支**。您的合并请求目标是您的派生项目的默认分支，而不是公共上游项目的默认分支。
   - *要只创建一个分支，*选择 **创建分支**。
1. 选择要使用的 **项目**。这些项目启用了合并请求，并且您在其中拥有开发者角色（或更高角色）。
1. 提供 **分支名称**，然后选择 **来源（分支或标签）**。极狐GitLab 检查这些分支是否在您的私有派生项目中可用，因为这两个分支必须在您选择的分支中可用。
1. 选择 **创建**。

此合并请求目标是您的私有派生项目，而不是公共上游项目。
您的分支、合并请求和提交保留在您的私有派生项目中。这可以防止过早泄露机密信息。

在以下情况下打开从您的派生项目到上游仓库<!--[从您的派生项目到上游仓库](../repository/forking_workflow.md#merging-upstream)-->的合并请求 ：

- 您对议题在您的私人派生项目中得到解决感到满意。
- 您已准备好公开机密提交。

<!--
## Related links

- [Confidential issues](../issues/confidential_issues.md)
- [Make an epic confidential](../../group/epics/manage_epics.md#make-an-epic-confidential)
- [Mark a comment as confidential](../../discussions/index.md#mark-a-comment-as-confidential)
- [Security practices for confidential merge requests](https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/developer.md#security-releases-critical-non-critical-as-a-developer) at GitLab
-->