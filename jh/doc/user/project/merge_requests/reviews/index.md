---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: index, reference
---

# 审核合并请求 **(FREE)**

> - 引入于 13.5 版本
> - 功能标志移除于 13.9 版本

[合并请求](../index.md) 是对极狐GitLab 项目中的文件进行更改的主要方法。[创建并提交合并请求](../creating_merge_requests.md) 提出更改建议。您的团队在您的合并请求上留下评论<!--[评论](../../../discussions/index.md)-->，并提出[代码建议](suggestions.md)，您可以从用户界面接受。当您的工作被审核时，您的团队成员可以选择接受或拒绝它。

您可以从极狐GitLab 界面查看合并请求。如果您安装了 GitLab Workflow VS Code 扩展<!--[GitLab Workflow VS Code 扩展](../../repository/vscode.md)-->，您还可以在 Visual Studio Code 中查看合并请求。

## 审核合并请求

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/4213) in GitLab Premium 11.4.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/28154) to GitLab Free in 13.1.
-->

当您查看合并请求时，您可以创建仅对您可见的评论。准备就绪后，您可以在一个操作中将它们一起发布。
开始您的审核：

1. 转到您要查看的合并请求，然后选择 **变更** 选项卡。<!--要了解有关导航此选项卡中显示的差异的更多信息，请阅读[合并请求中的更改选项卡](../changes.md)。-->
1. 选择装订线中的 **{comment}** **comment** 图标以展开差异行并显示评论框。在 13.2 及更高版本中，您可以[选择多行](#多行评论)。
1. 写下您的第一条评论，然后在您的评论下方选择 **启动评审**：
   ![Starting a review](img/mr_review_start.png)
1. 继续给代码行添加评论，写完评论后选择合适的按钮：
    - **添加到评论**：将此评论保密并添加到当前评论。
      这些审核评论被标记为 **Pending** 并且只有您可以看到。
    - **立即添加评论**：将特定评论作为普通评论而不是作为审核的一部分提交。
1. （可选）您可以在审核评论中使用[快速操作](../../quick_actions.md)。评论显示发布后要执行的操作，但在您提交评论之前不会执行这些操作。
1. 审核完成后，您可以[提交审核](#提交审核)。您的评论现在可见，并且您的评论中包含的任何[快速操作](../../quick_actions.md) 都会执行。

在 13.10 及更高版本中，如果您[核准合并请求](../approvals/index.md#核准合并请求) 并显示在审核者列表中，则在您的名称旁边会出现绿色复选标记 **{check-circle-filled}**。

### 提交审核

您可以通过多种方式提交完整的评论：

- 在非审核评论的文本中使用 `/submit_review` [快速操作](../../quick_actions.md)。
- 创建审核评论时，选择 **提交评论**。
- 滚动到屏幕底部并选择 **提交评论**。

当您提交评论时，极狐GitLab：

- 在您的审核中发布评论。
- 向合并请求的每个通知用户发送一封电子邮件，并附上您的评论。回复此电子邮件会创建对合并请求的新评论。
- 执行您添加到审核评论中的任何快速操作。

### 使用评论解决主题或取消解决主题

审核评论也可以解决或取消解决可解决的主题<!--[可解决的主题](../../../discussions/index.md#resolve-a-thread)-->。
回复评论时，会显示一个复选框，用于在发布后解决或取消解决主题。

![Resolve checkbox](img/mr_review_resolve.png)

如果特定的待处理评论解决或未解决主题，会显示在待处理评论本身上。

![Resolve status](img/mr_review_resolve2.png)

![Unresolve status](img/mr_review_unresolve.png)

### 添加新评论

> 引入于 13.10 版本

如果您正在进行审核，您将看到 **启动评审** 的选项：

![New thread](img/mr_review_new_comment_v13_11.png)

### 审核者的批准规则信息 **(PREMIUM)**

> - 引入于 13.8 版本
> - 功能标志移除于 13.9 版本

在新的或现有的合并请求中编辑 **审核者** 字段时，极狐GitLab 会在每个建议的审核者姓名下方显示匹配的[批准规则](../approvals/rules.md) 的名称。<!--[代码所有者](../../code_owners.md)-->代码所有者显示为“代码所有者”，没有群组详细信息。

此示例显示创建新合并请求时的审核者和批准规则：

![Reviewer approval rules in new/edit form](img/reviewer_approval_rules_form_v13_8.png)

此示例在合并请求侧栏中显示审核者和批准规则：

![Reviewer approval rules in sidebar](img/reviewer_approval_rules_sidebar_v13_8.png)

### 请求新的审核

> 引入于 13.9 版本

在审核者完成他们的合并请求审核<!--[合并请求审核](../../../discussions/index.md)-->后，合并请求的作者可以向审核者请求新的审核：

1. 如果合并请求中的右侧边栏已折叠，请单击 **{chevron-double-lg-left}** **展开侧边栏** 图标将其展开。
1. 在 **审核者** 部分，点击审核者姓名旁边的 **重新请求审核** 图标 (**{redo}**)。

极狐GitLab 为审核者创建一个新的待办事项<!--[待办事项](../../../todos.md)-->，并向他们发送通知电子邮件。

## 半线性历史合并请求

每次合并都会创建一个合并提交，但只有在可能进行快进合并时才会合并分支。这样确保如果合并请求构建成功，则合并后目标分支构建也成功。

1. 转到您的项目并选择 **设置 > 通用**。
1. 展开 **合并请求**。
1. 在 **合并方法** 部分，选择 **合并提交与半线性历史记录**。
1. 选择 **保存更改**。

## 多行评论

> - 引入于 13.2 版本。
> - 于 13.8 版本中添加了单击和拖动功能.
> - 功能标志移除于 13.9 版本。

在评论差异时，您可以通过以下任一方式选择您的评论引用的代码行：

![Comment on any diff file line](img/comment-on-any-diff-line_v13_10.png)

- 在装订线中拖动 **{comment}** **评论** 图标以突出显示差异行，将展开差异行并显示一个评论框。
- 通过选择装订线中的 **{comment}** **评论** 图标开始评论后，在 **Commenting on lines** 选择框中选择您的评论引用的第一行号。新评论默认为单行评论，除非您选择不同的起始行。

多行评论在评论正文上方显示评论的行号：

![Multiline comment selection displayed above comment](img/multiline-comment-saved.png)

## 在项目级别批量编辑合并请求

权限级别为[开发者或更高](../../../permissions.md) 的用户可以管理合并请求。

在项目中批量编辑合并请求时，您可以编辑以下属性：

- 状态（打开/关闭）
- 指派人
- 里程碑
- 标记
- 订阅

同时更新多个项目合并请求：

1. 在项目中，转到 **合并请求**。
1. 单击 **编辑合并请求**。屏幕右侧会出现一个侧边栏，其中包含可编辑的字段。
1. 选中要编辑的每个合并请求旁边的复选框。
1. 从侧栏中选择适当的字段及其值。
1. 点击 **全部更新**。

## 在群组级别批量编辑合并请求

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/12719) in [GitLab Premium](https://about.gitlab.com/pricing/) 12.2.
-->

权限级别为开发者或更高的用户可以管理合并请求。

在群组中批量编辑合并请求时，您可以编辑以下属性：

- 里程碑
- 标记

同时更新多个群组合并请求：

1. 在群组中，转到 **合并请求**。
1. 单击 **编辑合并请求**。屏幕右侧会出现一个侧边栏，其中包含可编辑的字段。
1. 选中要编辑的每个合并请求旁边的复选框。
1. 从侧栏中选择适当的字段及其值。
1. 点击 **全部更新**。

<!--
## Associated features

These features are associated with merge requests:

- [Cherry-pick changes](../cherry_pick_changes.md):
  Cherry-pick any commit in the UI by selecting the **Cherry-pick** button in a merged merge requests or a commit.
- [Fast-forward merge requests](../fast_forward_merge.md):
  For a linear Git history and a way to accept merge requests without creating merge commits
- [Find the merge request that introduced a change](../versions.md):
  When viewing the commit details page, GitLab links to the merge request(s) containing that commit.
- [Merge requests versions](../versions.md):
  Select and compare the different versions of merge request diffs
- [Resolve conflicts](../resolve_conflicts.md):
  GitLab can provide the option to resolve certain merge request conflicts in the GitLab UI.
- [Revert changes](../revert_changes.md):
  Revert changes from any commit from a merge request.
- [Keyboard shortcuts](../../../shortcuts.md#issues-and-merge-requests):
  Access and modify specific parts of a merge request with keyboard commands.
-->

## 故障排查

有时在合并请求中事情不会按预期进行。以下是一些故障排查步骤。

### 合并请求无法获取流水线状态

如果 Sidekiq 没有足够快地获取更改，则可能会发生这种情况。

#### Sidekiq

Sidekiq 处理 CI 状态变化的速度不够快。 请等待几秒钟，状态应该会自动更新。

#### Bug

发生以下情况时，无法检索合并请求流水线状态：

1. 创建合并请求
1. 合并请求关闭
1. 在项目中进行了更改
1. 合并请求重新打开

要正确检索流水线状态，请再次关闭并重新打开合并请求。

## Tips

以下是一些提示，可帮助您更高效地处理命令行中的合并请求。

### 复制本地检出的分支名称

> 引入于 13.4 版本。

合并请求侧边栏包含用于为此合并请求贡献更改的源分支的分支引用。

要将分支引用复制到剪贴板，请选择右侧栏中的 **复制分支名称** 按钮 (**{copy-to-clipboard}**) 通过运行 `git checkout <branch-name>`，使用它从命令行在本地检出分支。

### 通过 `head` ref 在本地检出合并请求

合并请求包含来自仓库的所有历史记录，以及添加到与合并请求关联的分支的额外提交。以下是在本地检出合并请求的几种方法。

即使源项目是目标项目的派生（甚至是私有派生），您也可以在本地检出合并请求。

这依赖于可用于每个合并请求的合并请求 `head` ref (`refs/merge-requests/:iid/head`)。它允许使用其 ID 而不是其分支来检出合并请求。

引入于 13.4 版本，合并请求关闭或合并 14 天后，合并请求 `head` ref 被删除。这意味着合并请求不再可用于从合并请求 `head` 引用的本地检出。合并请求仍然可以重新打开。如果合并请求的分支存在，您仍然可以检出该分支，因为它不受影响。

#### 通过添加 Git 别名在本地检出

将以下别名添加到您的 `~/.gitconfig` 中：

```plaintext
[alias]
    mr = !sh -c 'git fetch $1 merge-requests/$2/head:mr-$1-$2 && git checkout mr-$1-$2' -
```

现在您可以从任何仓库和任何远端检查特定的合并请求。例如，要从“origin”远端检查检查GitLab 中显示的 ID 为 5 的合并请求，请执行以下操作：

```shell
git mr origin 5
```

这会将合并请求提取到本地 `mr-origin-5` 分支中并检出。

#### 通过修改给定仓库的`.git/config` 在本地检出

在`.git/config` 文件中找到您的 GitLab 远端部分。如下所示：

```plaintext
[remote "origin"]
  url = https://gitlab.com/gitlab-org/gitlab-foss.git
  fetch = +refs/heads/*:refs/remotes/origin/*
```

您可以使用以下命令打开文件：

```shell
git config -e
```

现在将以下行添加到上述部分：

```plaintext
fetch = +refs/merge-requests/*/head:refs/remotes/origin/merge-requests/*
```

最后，它应该是这样的：

```plaintext
[remote "origin"]
  url = https://gitlab.com/gitlab-org/gitlab-foss.git
  fetch = +refs/heads/*:refs/remotes/origin/*
  fetch = +refs/merge-requests/*/head:refs/remotes/origin/merge-requests/*
```

现在您可以获取所有合并请求：

```shell
git fetch origin

...
From https://gitlab.com/gitlab-org/gitlab-foss.git
 * [new ref]         refs/merge-requests/1/head -> origin/merge-requests/1
 * [new ref]         refs/merge-requests/2/head -> origin/merge-requests/2
...
```

并检出特定的合并请求：

```shell
git checkout origin/merge-requests/1
```

以上所有内容都可以通过 [`git-mr`](https://gitlab.com/glensc/git-mr) 脚本完成。

## 缓存合并请求计数

> - 引入于 13.11 版本。
> - 功能标志移除于 14.0 版本。

在一个组中，侧边栏显示开放的合并请求的总数。如果该值大于 1000，则缓存该值。缓存值四舍五入为千（或百万）并每 24 小时更新一次。
