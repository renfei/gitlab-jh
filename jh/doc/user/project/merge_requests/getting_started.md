---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: index, reference
description: "Getting started with merge requests."
---

# 合并请求入门 **(FREE)**

合并请求 (**MR**) 是 GitLab 作为代码协作和版本控制的基础。

在基于 Git 的平台上工作时，您可以使用分支策略来协作处理代码。

仓库由其*默认分支*组成，其中包含代码库的主要版本，您可以从中创建次要分支，也称为*功能分支*，以提出对代码库的更改，而不会将它们直接引入代码库的主要版本。

在与他人协作时，分支尤其重要，避免在没有事先审核、测试和批准的情况下将更改直接推送到默认分支。

当您创建新的功能分支、更改文件并将其推送到极狐GitLab 时，您可以选择创建**合并请求**，这本质上是将一个分支合并到另一个分支的请求。

将更改添加到的分支称为*源分支*，而您请求将更改合并到的分支称为*目标分支*。

目标分支可以是默认分支或任何其他分支，具体取决于您选择的分支策略。

在合并请求中，除了可视化原始内容和您提议的更改之间的差异之外，您还可以在结束工作和合并合并请求之前执行[大量任务](#您可以用合并请求做什么)。

<!--
You can watch our [GitLab Flow video](https://www.youtube.com/watch?v=InKNIvky2KE) for
a quick overview of working with merge requests.
-->

## 如何创建合并请求

了解[创建合并请求](creating_merge_requests.md)的各种方法。

## 您可以用合并请求做什么

当您开始新的合并请求时，您可以立即包括以下选项。您也可以稍后通过在右上角的合并请求页面上，选择**编辑**或使用[合并请求的键盘快捷键](../../shortcuts.md#议题和合并请求)：

- [指派](#指派人)合并请求给同事审核。使用[多个指派人](#多个指派人)，您可以一次将其分配给多个人。
- 设置里程碑<!--[里程碑](../milestones/index.md)-->来跟踪时间敏感的变化。
- 添加[标记](../labels.md)以帮助上下文理解或过滤您的合并请求。
- [需要来自您的团队的批准](approvals/index.md#必须批准)。 **(PREMIUM)**
- 合并时[自动关闭议题](#合并请求关闭议题)。
- 启用[接受合并请求时删除源分支](#删除源分支)选项，保持仓库清洁。
- 启用[接受合并请求时压缩提交](squash_and_merge.md) 选项，在合并之前将所有提交合并为一个，从而在您的仓库中保持干净的提交历史。
- 将合并请求设置为 [**Draft**](drafts.md) ，避免在准备就绪之前发生意外合并。

创建合并请求后，您还可以：

- 在合并请求主题中与您的团队讨论<!--[讨论](../../discussions/index.md)-->您的实现。
- [执行内联代码审核](reviews/index.md)。
- 添加[合并请求依赖项](merge_request_dependencies.md)，限制在其它合并请求合并后才能合并。 **(PREMIUM)**
- [在合并请求部件上](widgets.md)预览持续集成流水线。
- 使用 Review Apps<!--[Review Apps](widgets.md#live-preview-with-review-apps)--> 在已部署的应用程序上直接预览您的更改。
- [允许跨派生项目合并请求的协作](allow_collaboration.md)。
- 执行[审核](reviews/index.md)，在差异上创建多个评论并在您准备好后发布它们。
- 添加[代码建议](reviews/suggestions.md)更改合并请求的内容，进入合并请求主题，并轻松地直接从 UI 将它们应用到代码库。
- 使用时间跟踪<!--[时间跟踪](../time_tracking.md#time-tracking)-->添加时间估计和该合并请求所花费的时间。

可以设置其中许多选项：

- 从合并请求页面，使用[键盘快捷键](../../shortcuts.md#议题和合并请求)。
- 从命令行推送更改时，使用 [Git 推送选项](../push_options.md)。

<!--
See also other [features associated to merge requests](reviews/index.md#associated-features).
-->

### 指派人

选择一个指派人指定某人作为负责第一次[审核合并请求](reviews/index.md)的人。打开下拉框搜索您要分配的用户，合并请求被添加到他们的已分配合并请求列表<!--[分配的合并请求列表](../../search/index.md#issues-and-merge-requests)-->。

#### 多个指派人 **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/2004) in GitLab 11.11.
-->
> - 于 13.9 版本移动到专业版。

多人经常同时审核合并请求。极狐GitLab 允许您为合并请求设置多个指派人，以指示需要审核或对其负责的每个人。

![multiple assignees for merge requests sidebar](img/multiple_assignees_for_merge_requests_sidebar.png)

要将多个指派人分配给合并请求：

1. 从合并请求中，展开右侧边栏并找到 **指派人** 部分。
1. 单击 **编辑** 并从下拉菜单中选择您想要将合并请求分配给的任意数量的用户。

类似地，通过从同一下拉菜单中取消选择指派人来移除指派人。

也可以管理多个指派人：

- 当创建合并请求时
- 使用[快速操作](../quick_actions.md#议题合并请求和史诗)。

### 审核者

WARNING:
请求代码审核是贡献代码的重要部分。然而，决定谁应该审核你的代码并要求审核并不是一件容易的事。对作者和审核者都使用“指派人”字段会使其他人很难确定谁在合并请求中做了什么。

合并请求审核者功能使您能够请求对您的工作进行审核，并查看审核状态。审核者帮助区分合并请求中涉及的用户的角色。 与直接负责创建或合并合并请求的 **指派人** 相比，**审核者** 是可能只参与合并请求的一个方面的团队成员，例如同行审核。

要请求审核合并请求，请展开右侧边栏中的 **审核者** 选择框。搜索您要请求审核的用户。选中后，极狐GitLab 会为每个审核者创建一个[待办事项列表项](../../todos.md)。

要了解更多，查看[审核合并请求](reviews/index.md)。

### 合并请求关闭议题

要创建合并请求并在合并时关闭议题，您可以：

- 在 MR 描述中添加注释<!--[在 MR 描述中添加注释](../issues/managing_issues.md#closure-issues-automatically)-->。
- 在议题中，选择 **创建合并请求**。然后，您可以：

   - 在一个操作中创建一个新分支和[草稿合并请求](../merge_requests/drafts.md)。该分支默认命名为`issuenumber-title`，但您可以选择任何名称，系统会验证它是否尚未被使用。合并请求继承议题的里程碑和标签，并设置为在合并时自动关闭议题。
   - 仅创建一个新分支<!--[新分支](../repository/web_editor.md#create-a-new-branch-from-an-issue)-->，其名称以问题编号开头。

如果议题是[机密](../issues/confidential_issues.md)，您可能希望对[机密议题的合并请求](confidential.md)使用不同的工作流程，以防止泄露机密信息。

### 删除源分支

创建合并请求时，选择 **接受合并请求时删除源分支** 选项，合并请求合并时删除源分支。要默认为所有新合并请求启用此选项，请在项目设置<!--[项目设置](../settings/index.md#merge-request-settings)-->中启用它。

此选项在现有合并请求中的合并请求按钮旁边也可见，并且可以在合并之前选择或取消选择。它仅对源项目中具有维护者角色的用户可见。

如果查看合并请求的用户没有删除源分支的正确权限，并且源分支设置为删除，则合并请求部件会显示**删除源分支**文本。

![Delete source branch status](img/remove_source_branch_status.png)

### 合并时的分支重定向 **(FREE SELF)**

> - 引入与 13.9 版本
> - 在 13.9 版本不适用于自助管理版
> - 在 13.10 版本适用于自助管理版

在特定情况下，如果目标分支在合并请求打开时合并，极狐GitLab 可以重新定位打开合并请求的目标分支。合并请求通常以这种方式关联，一个合并请求依赖于另一个：

- **合并请求 1**：将 `feature-alpha` 合并到 `main`。
- **合并请求 2**：将 `feature-beta` 合并到 `feature-alpha`。

这些合并请求通常以下列方式之一处理：

- 合并请求 1 首先合并到 `main` 中。合并请求 2 然后被重定向到 `main`。
- 合并请求 2 合并到 `feature-alpha` 中。更新后的合并请求 1，现在包含 `feature-alpha` 和 `feature-beta` 的内容，被合并到 `main` 中。

当他们的目标分支合并到 `main` 时，极狐GitLab 最多重新定位四个合并请求，因此您不需要手动执行此操作。来自分叉的合并请求不会重新定位。

今天的功能仅适用于合并。在合并请求合并后单击 **删除源分支** 按钮不会自动重新定位合并请求。<!--这一改进[作为后续跟踪](https://gitlab.com/gitlab-org/gitlab/-/issues/321559)。-->

## 合并请求的建议和最佳实践

- 在您的分支本地工作时，添加多个提交并仅在您完成后推送，因此极狐GitLab 一次只为所有提交运行一个流水线。通过这样做，您可以节省流水线分钟。
- 在合并时或合并后删除功能分支以保持仓库清洁。
- 一次只做一件事，并尽可能进行最小的更改。这样审核速度会更快，并且您的更改更不容易出错。
- 不要在分支名称中使用大写字母或特殊字符。
