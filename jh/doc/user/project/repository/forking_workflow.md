---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
disqus_identifier: 'https://docs.gitlab.com/ee/workflow/forking_workflow.html'
---

# 项目派生工作流 **(FREE)**

只要有可能，建议在公共 Git 仓库中工作并使用[分支策略](../../../topics/gitlab_flow.md)来管理您的工作。但是，如果您对要贡献的仓库没有写入权限，则可以创建一个分支。

派生是仓库及其所有分支的个人副本，您可以在您选择的命名空间中创建它。通过这种方式，您可以在自己的派生项目中进行更改，并通过合并请求将它们提交到您无权访问的仓库。

## 创建派生

在极狐GitLab 中派生现有项目：

1. 在项目首页，右上角，点击**{fork}** **派生**。

   ![Fork button](img/forking_workflow_fork_button_v13_10.png)

1. 选择要派生到的项目：

   - 推荐方法：在 **选择一个命名空间来派生（fork）项目** 下方，确定要派生到的项目，然后单击 **选择**。仅显示您拥有开发者和更高权限的命名空间。

     ![Choose namespace](img/forking_workflow_choose_namespace_v13_10.png)

   - 实验方法：如果你的管理员已经[启用了实验 fork 项目表单](#启用或禁用派生项目表单)，阅读[使用派生项目表单创建一个派生项目](#使用派生项目表单创建一个派生项目)。仅显示您拥有开发者和更高权限的命名空间。

   NOTE:
   项目路径在命名空间中必须是唯一的。

极狐GitLab 创建您的派生项目，并将您重定向到新派生项目的项目页面。
您在命名空间中拥有的权限就是您在派生项目中的权限。

WARNING:
当仓库功能设置为 **Members Only** 的公共项目被派生时，仓库在派生项目中是公开的。派生项目的所有者必须手动更改可见性。

## 仓库镜像

您可以使用<!--[仓库镜像](mirror/index.md)-->使您的派生与原始仓库同步。您也可以使用 `git remote add upstream` 来达到同样的效果。

主要区别在于，使用仓库镜像，您的远端分支会自动保持最新。

如果没有镜像，要在本地工作，您必须使用 `git pull` 用上游项目更新您的本地仓库，然后将更改推送回您的派生仓库以更新它。

WARNING:
使用镜像，在批准合并请求之前，系统会要求您进行同步。因此，建议将其自动化。

<!--
Read more about [How to keep your fork up to date with its origin](https://about.gitlab.com/blog/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/).
-->

## 合并到上游

当您准备好将代码发送回上游项目时，[创建合并请求](../merge_requests/creating_merge_requests.md)。对于 **源分支**，选择您的派生项目的分支。对于**目标分支**，选择原始项目的分支。

NOTE:
创建合并请求时，如果派生项目的可见性比父项目的限制更多（例如派生项目是私有的，父项目是公共的），则目标分支默认为派生项目的默认分支。这可以防止潜在地暴露派生项目的私有代码。

![Selecting branches](img/forking_workflow_branch_select.png)

然后您可以添加标记、里程碑，并将合并请求分配给可以查看您的更改的人员。然后单击 **提交合并请求** 以结束该过程。成功合并后，您的更改将添加到您要合并到的仓库和分支中。

## 删除派生关系

您可以在高级设置<!--[高级设置](../settings/index.md#removing-a-fork-relationship)-->中取消派生项目与其上游项目的链接。

## 使用派生项目表单创建一个派生项目 **(FREE SELF)**

> - 引入于 13.11 版本
> - 在功能标志后默认禁用
> - 不推荐生产环境使用
> - 要在自助管理实例中使用它，请要求管理员[启用它](#启用或禁用派生项目表单)。 **(FREE SELF)**

这个实验版本的派生项目表单只有在您的管理员已[启用它](#启用或禁用派生项目表单)时才可用：

![Choose namespace](img/fork_form_v13_10.png)

使用请按照 [Creating a fork](#创建派生) 中的说明进行操作并提供：

- 项目名称。
- 项目 URL。
- 项目标识串。
- *（可选）* 项目描述。
- 派生项目的可见性级别。

### 启用或禁用派生项目表单 **(FREE SELF)**

新的[派生项目表单](#使用派生项目表单创建一个派生项目)正在开发中，尚未准备好用于生产。它部署在**默认禁用**的功能标志后面。
可以访问 GitLab Rails 控制台的 GitLab 管理员可以启用它。

要启用它：

```ruby
Feature.enable(:fork_project_form)
```

要禁用它：

```ruby
Feature.disable(:fork_project_form)
```
