---
stage: Create
group: Editor
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, how-to
description: "The static site editor enables users to edit content on static websites without prior knowledge of the underlying templating language, site architecture or Git commands."
---

# 静态站点编辑器 **(FREE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28758) in GitLab 12.10.
> - WYSIWYG editor [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/214559) in GitLab 13.0.
> - Non-Markdown content blocks not editable on the WYSIWYG mode [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/216836) in GitLab 13.3.
> - Formatting Markdown [introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/49052) in GitLab 13.7.
-->

静态站点编辑器 (SSE) 使用户能够在不了解底层模板语言、站点架构或 Git 命令的情况下编辑静态网站上的内容。项目的贡献者可以快速编辑 Markdown 页面并提交更改以供审核。

## 用例

静态站点编辑器允许协作者无缝地提交对静态站点文件的更改。例如：

- 非技术合作者可以直接从浏览器编辑页面。他们不需要了解 Git 和您的项目的详细信息即可做出贡献。
- 最近聘用的团队成员可以快速编辑内容。
- 临时协作者可以从一个项目跳到另一个项目并快速编辑页面，而不必克隆或派生他们需要提交更改的每个项目。

## 要求

- 为了使用静态站点编辑器功能，您的项目需要使用静态站点编辑器 Middleman 模板进行预配置。
- 您需要登录极狐GitLab并成为项目成员（具有开发者或更高权限级别）。

## 如何工作

静态站点编辑器处于早期开发阶段，目前仅支持 Middleman 站点。您必须使用特定的站点模板才能开始使用它。项目模板配置为使用 GitLab Pages<!--[GitLab Pages](../pages/index.md)--> 部署 [Middleman](https://middlemanapp.com/) 静态网站。

一旦您的网站启动并运行，一个 **编辑此页** 按钮会显示在其页面的左下角：

![Edit this page button](img/edit_this_page_button_v12_10.png)

当您单击它时，会打开一个编辑器窗口，从中可以直接编辑内容。准备就绪后，您可以通过单击按钮提交更改：

![Static Site Editor](img/wysiwyg_editor_v13_3.png)

当编辑器提交他们的更改时，这些是系统在后台自动执行的以下操作：

1. 创建一个新分支。
1. 提交更改。
    1. 根据 [Handbook Markdown Style Guide](https://about.gitlab.cn/handbook/markdown-guide/) 样式指南修复格式并通过另一个提交添加它们。
1. 针对默认分支打开合并请求。

然后，编辑器可以导航到合并请求，将其分配给同事进行审核。

## 设置您的项目

首先设置项目。完成后，您可以使用静态站点编辑器来[编辑您的内容](#编辑内容)。

1. 首先，从<!--[静态站点编辑器 - Middleman](https://gitlab.com/gitlab-org/project-templates/static-site-editor-middleman)-->静态站点编辑器 - Middleman 模板创建一个新项目。您可以[派生它](../repository/forking_workflow.md#创建派生)或从模板创建新项目<!--[从模板创建新项目](../working_with_projects.md#built-in-templates)-->。
1. 编辑 [`data/config.yml`](#静态站点生成器配置) 配置文件，将 `<username>` 和 `<project-name>` 替换为项目路径的正确值。
1. （可选）编辑 [`.gitlab/static-site-editor.yml`](#静态站点编辑器配置文件) 文件以自定义静态站点编辑器。
1. 当您提交更改时，会触发 CI/CD 流水线以使用 GitLab Pages 部署您的项目。
1. 流水线完成后，从项目的左侧菜单中，转到 **设置 > Pages** 以找到新网站的 URL。
1. 访问您的网站并查看屏幕左下角以查看新的 **编辑此页** 按钮。

任何满足[要求](#要求)的人都可以编辑页面内容，而无需事先了解 Git 或您网站的代码库。

## 编辑内容

<!--
> - Support for modifying the default merge request title and description [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/216861) in GitLab 13.5.
> - Support for selecting a merge request template [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/263252) in GitLab 13.6.
-->

设置项目后，您可以直接从静态站点编辑器开始编辑内容。

要编辑文件：

1. 访问您要编辑的页面。
1. 单击 **编辑此页面** 按钮。
1. 文件以 **WYSIWYG** 模式在静态站点编辑器中打开。如果您想编辑原始 Markdown，你可以在右下角切换 **Markdown** 模式。
1. 完成后，单击 **提交更改...**。
1. （可选）调整合并请求的默认标题和描述，以提交您的更改。或者，从下拉菜单中选择[合并请求模板](../../../user/project/description_templates.md#创建合并请求模板)并相应地进行编辑。
1. 单击 **提交更改**。
1. 自动创建新的合并请求，您可以指派同事进行审核。

### 文本

<!--
> Support for `*.md.erb` files [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/223171) in GitLab 13.2.
-->

静态站点编辑器支持用于编辑文本的 Markdown 文件（`.md`、`.md.erb`）。

### 图像

<!--
> - Support for adding images through the WYSIWYG editor [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/216640) in GitLab 13.1.
> - Support for uploading images via the WYSIWYG editor [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/218529) in GitLab 13.6.
-->

#### 上传图像

您可以通过 WYSIWYG 编辑器将图像文件直接上传到仓库，到默认上传目录 `source/images`。这样做：

1. 单击图像图标 (**{doc-image}**)。
1. 选择 **上传文件** 选项卡。
1. 单击 **选择文件** 从您的计算机中选择一个文件。
1. 可选：为 SEO 和可访问性添加图片描述（[ALT 文本](https://moz.com/learn/seo/alt-text)）。
1. 点击 **插入图片**。

所选文件可以是任何受支持的图像文件（`.png`、`.jpg`、`.jpeg`、`.gif`）。编辑器会呈现缩略图预览，因此您可以验证是否包含正确的图像，并且没有对丢失图像的任何引用。

#### 链接到图像

如果您愿意，也可以链接到图像：

1. 单击图像图标 (**{doc-image}**)。
1. 选择 **链接到图像** 选项卡。
1. 将图片链接添加到 **图片网址** 字段中（使用完整路径；尚不支持相对路径）。
1. 可选：为 SEO 和可访问性添加图片描述（[ALT 文本](https://moz.com/learn/seo/alt-text)）。
1. 点击 **插入图片**。

该链接可以引用已托管在您的项目中的图像、内容交付网络外部托管的资产或任何其他外部 URL。编辑器会呈现缩略图预览，因此您可以验证是否包含正确的图像，并且没有对丢失图像的任何引用。

<!--
### 视频

> - Support for embedding YouTube videos through the WYSIWYG editor [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/216642) in GitLab 13.5.

You can embed YouTube videos on the WYSIWYG mode by clicking the video icon (**{live-preview}**).
The following URL/ID formats are supported:

- **YouTube watch URLs**: `https://www.youtube.com/watch?v=0t1DgySidms`
- **YouTube embed URLs**: `https://www.youtube.com/embed/0t1DgySidms`
- **YouTube video IDs**: `0t1DgySidms`
-->

### Front matter

<!--
> - Markdown front matter hidden on the WYSIWYG editor [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/216834) in GitLab 13.1.
> - Ability to edit page front matter [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/235921) in GitLab 13.5.
-->

Front Matter 是一种灵活方便的方法，可以在旨在由静态站点生成器解析的数据文件中定义特定于页面的变量。使用它来设置页面的标题、布局模板或作者。当页面呈现为 HTML 时，您还可以将任何类型的元数据传递给生成器。包含在每个数据文件的最顶部，前端内容通常被格式化为 YAML 或 JSON，并且需要一致且准确的语法。

要从静态站点编辑器编辑前端内容，您可以使用常规文件编辑器、Web IDE，或直接从 WYSIWYG 编辑器更新数据：

1. 单击右下角的 **页面设置** 按钮以显示包含页面前端内容数据的网络表单。该表单填充了当前数据：

   ![Editing page front matter in the Static Site Editor](img/front_matter_ui_v13_4.png)

1. 根据需要更新值并关闭面板。
1. 完成后，单击 **提交更改...**。
1. 描述您的更改（添加提交消息）。
1. 单击 **提交更改**。
1. 点击 **查看合并请求**查看。

不支持从表单向页面的前端添加新属性。
要添加新属性：

- 在本地编辑文件
- 使用常规文件编辑器编辑文件。
- 使用 Web IDE 编辑文件。

添加属性后，表单会加载新字段。

## 配置文件

您可以使用以下配置文件自定义使用静态站点编辑器的项目的行为：

- [`.gitlab/static-site-editor.yml`](#静态站点编辑器配置文件)，自定义静态站点编辑器的行为。
- [静态站点生成器配置文件](#静态站点生成器配置)，例如 `data/config.yml`，用于配置静态站点生成器本身。它还控制生成站点时的 **编辑此页** 按钮。

### 静态站点编辑器配置文件

<!--
> [Introduced](https://gitlab.com/groups/gitlab-org/-/epics/4267) in GitLab 13.6.
-->

`.gitlab/static-site-editor.yml` 配置文件包含可用于自定义静态站点编辑器 (SSE) 的条目。如果该文件不存在，则使用支持默认 Middleman 项目配置的默认值。
静态站点编辑器 - Middleman 项目模板生成一个预先填充了这些默认值的文件。

要自定义 SSE 的行为，请根据项目需要编辑 `.gitlab/static-site-editor.yml` 的条目。 确保遵守 YAML 语法。

表格后，见[SSE配置文件示例](#gitlabstatic-site-editoryml-示例)。

| 条目 | 版本 | 类型 | 默认值 | 说明 |
|---|---|---|---|---|
| `image_upload_path` | 13.6 | String | `source/images` | 从 WYSIWYG 编辑器上传的图像的目录。 |

#### `.gitlab/static-site-editor.yml` 示例

```yaml
image_upload_path: 'source/images' # Relative path to the project's root. Don't include leading or trailing slashes.
```

### 静态站点生成器配置

静态站点编辑器使用 Middleman 的配置文件 `data/config.yml` 来自定义项目本身的行为。这个文件还控制了 **编辑此页** 按钮，通过文件 <!--[`layout.erb`](https://gitlab.com/gitlab-org/project-templates/static-site-editor-middleman/-/blob/master/source/layouts/layout.erb)-->`layout.erb` 渲染。

要[将项目模板配置为您自己的项目](#设置您的项目)，您必须将 `data/config.yml` 文件中的 `<username>` 和`<project-name>` 替换为项目路径的正确值。

[其它静态站点生成器](#使用其它静态站点生成器)与静态站点编辑器一起使用，可能会使用不同的配置文件或方法。

## 使用其它静态站点生成器

尽管 Middleman 是静态站点编辑器官方支持的唯一静态站点生成器，但您可以将项目的构建和部署配置为使用不同的静态站点生成器。在这种情况下，以 Middleman 布局为例，并按照类似的方法在静态站点生成器的布局中正确呈现 **编辑此页** 按钮。

<!--
## Upgrade from GitLab 12.10 to 13.0

In GitLab 13.0, we [introduced breaking changes](https://gitlab.com/gitlab-org/gitlab/-/issues/213282)
to the URL structure of the Static Site Editor. Follow the instructions in this
[snippet](https://gitlab.com/gitlab-org/project-templates/static-site-editor-middleman/snippets/1976539)
to update your project with the 13.0 changes.
-->

## 限制

- 静态站点编辑器仍然无法快速添加到现有的 Middleman 站点。
  <!--Follow this [epic](https://gitlab.com/groups/gitlab-org/-/epics/2784) for updates.-->
