---
type: reference, howto
stage: Plan
group: Product Planning
info: 要确定分配给与此页面相关的阶段/群组的技术作家，请参见 https://about.gitlab.cn/handbook/engineering/ux/technical-writing/#assignments
---


# 史诗 **(PREMIUM)**

<!--
> - 被引入于 [GitLab 旗舰版](https://about.gitlab.cn/pricing/) 10.2 版本中。
> - 单层史诗在 12.8 版本 [被移入](https://gitlab.com/gitlab-org/gitlab/-/issues/37081) [GitLab 专业版](https://about.gitlab.cn/pricing/)。
-->

当[议题](../../project/issues/index.md)跨项目和里程碑共享一个主题时，您可以用史诗来管理它们。

您还可以创建子史诗，并指定开始和结束日期，将为您创建一个可视化的路线图来查看进度。

使用史诗：

- 当您的团队在做一个大的功能时，涉及到在一个群组<!--[群组](.../index.md)-->中的不同项目中的不同问题的多个讨论。
- 跟踪该组问题的工作何时开始和结束。
- 要在高层次上讨论并协作相关功能的想法和范围。

## 史诗与议题的关系

史诗和议题之间可能存在的关系是：

- 一个史诗包含一个或多个议题。
- 一个史诗包含一个或多个子史诗。详见[多级子史诗](manage_epics.md#多级子史诗)。

```mermaid
graph TD
    Parent_epic --> Issue1
    Parent_epic --> Child_epic
    Child_epic --> Issue2
```
## 史诗的路线图 **(ULTIMATE)**

<!--
> [被引入](https://gitlab.com/gitlab-org/gitlab/-/issues/7327) 于 [GitLab 旗舰版](https://about.gitlab.cn/pricing/) 11.10 版本中。
-->

如果您的史诗包含一个或多个[子史诗](manage_epics.md#多级子史诗)，并且有一个开始或到期的日期，一个可视化的父史诗包含子史诗的[路线图](../roadmap/index.md)如下所示。

![子史诗路线图](img/epic_view_roadmap_v12_9.png)

## 权限

如果您有权限查看史诗并且添加议题到该史诗，您可以在该史诗的议题列表中查看该问题。

如果您有权限编辑史诗并且添加议题到该史诗，您可以将该议题添加到该史诗或从该史诗中删除。

对于一个特定的群组，所有项目的可见性必须与该群组相同，或者限制更少。这意味着，如果您能访问某个群组的史诗，那么您就能访问其项目的议题。

<!--您也可以参考[群组权限表](.../.../permissions.md#群组成员权限)。-->

## 相关话题

- [管理史诗](manage_epics.md)和多级子史诗。
- 用[史诗看板](epic_boards.md)创建工作流。
<!--- [打开通知](.../.../profile/notifications.md)以了解史诗事件。-->
- 为史诗或其评论[奖励表情符号](../../award_emojis.md)。
- 通过在[主题帖](../../discussions/index.md)中发表评论，在史诗中进行合作。
- 使用[健康状态](../../project/issues/managing_issues.md#健康状态)来跟踪您的进展。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
