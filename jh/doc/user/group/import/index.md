---
type: reference, howto
stage: Manage
group: Import
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 从其它实例迁移群组 **(FREE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/249160) in GitLab 13.7.
> - [Deployed behind a feature flag](../../feature_flags.md), disabled by default.
> - Enabled on GitLab.com.
-->

> - 引入于 13.7 版本。
> - 部署于功能标记之后，默认禁用。

NOTE:
导入者**仅**迁移此页面上列出的群组数据。<!--To leave feedback on this
feature, see [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/284495).-->

使用 GitLab 群组迁移，您可以从自助管理实例迁移现有的顶级群组。 群组可以作为顶级群组或任何现有顶级组的子组迁移到目标实例。

以下资源迁移到目标实例：

- 群组（引入于 13.7）
  - 描述
  - 属性
  - 子组
  - 头像（引入于 14.0）
- 群组标记（引入于 13.9）
  - 标题
  - 描述
  - 颜色
  - 创建时间（引入于 13.10）
  - 更新时间（引入于 13.10）
- 成员（引入于 13.9）
  在以下情况下，群组成员与导入的群组相关联：
   - 该用户已经存在于目标实例中并且
   - 用户在源实例中拥有与目标实例中确认的电子邮件，匹配的公共电子邮件
- 史诗（引入于 13.7）
  - 标题
  - 描述
  - 状态（开放/已关闭）
  - 开始日期
  - 截止日期
  - 看板上的史诗顺序
  - 机密性
  - 标记（引入于 13.9）
  - 作者（引入于 13.9）
  - 父史诗（引入于 13.9）
  - 使用 emoji（引入于 13.9）
  - 事件（引入于 13.10）
- 里程碑（引入于 13.10）
  - 标题
  - 描述
  - 状态（启用/已关闭）(active / closed)
  - 开始日期
  - 截止日期
  - 创建时间
  - 更新时间
  - iid（引入于 13.11）
- 迭代（引入于 13.10）
  - iid
  - 标题
  - 描述
  - 状态 （即将到来 / 已开始 / 已结束）
  - 开始日期
  - 截止日期
  - 创建时间
  - 更新时间
- 徽章（引入于 13.11）
  - 名称
  - 链接 URL
  - 图片 URL
- 看板
- 看板列表

任何其他项目**不**迁移。

## 启用或禁用群组迁移

GitLab Migration 部署在**默认启用**的功能标志后面。<!--[可以访问 GitLab Rails 控制台的管理员](../../../administration/feature_flags.md)-->可以访问 GitLab Rails 控制台的管理员可以启用它。 

要启用：

```ruby
Feature.enable(:bulk_import)
```

要禁用：

```ruby
Feature.disable(:bulk_import)
```

## 将您的群组导入

在开始之前，请确保目标实例可以通过 HTTPS（不支持 HTTP）与源通信。

NOTE:
这可能涉及重新配置您的防火墙，以防止在自助管理实例端阻止连接。

### 连接到远程实例

1. 转到新建群组页面：

   - 在顶部栏上，选择 `+`，然后选择 **新建群组**。
   - 或者，在现有群组页面的右上角，选择 **新建子组**。

   ![Navigation paths to create a new group](img/new_group_navigation_v13_8.png)

1. 在新建群组页面上，选择 **导入群组**。

   ![Fill in import details](img/import_panel_v14_1.png)

1. 输入您的实例的源 URL。
1. 在远程实例上生成或复制个人访问令牌<!--[个人访问令牌](../../../user/profile/personal_access_tokens.md)-->，需具有 `api` 和 `read_repository` 范围。
1. 为您的远程实例输入个人访问令牌<!--[个人访问令牌](../../../user/profile/personal_access_tokens.md)。
1. 选择 **连接实例**。

### 选择要导入的群组

在您授权访问实例后，您将被重定向到 GitLab Group Migration 导入器页面。列出了您具有所有者角色的远程组。

1. 默认情况下，建议的群组命名空间与远程实例中存在的名称相匹配，但根据您的权限，您可以选择在继续导入其中任何名称之前编辑这些名称。
1. 在要导入的组旁边，选择 **导入**。
1. **状态** 栏显示各群组的导入状态。如果您将页面保持打开状态，它会实时更新。
1. 导入群组后，选择其 GitLab 路径以打开其 GitLab URL。

![Group Importer page](img/bulk_imports_v14_1.png)
