---
type: reference
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 迭代 **(PREMIUM)**

> - 引入于 13.1 版本。
> - 部署在功能标志后默认禁用
> - 从 13.2 版本默认启用
> - 无法分群组启用或禁用
> - 推荐用于生产使用
> - 对于自助管理实例，管理员可以选择[禁用它](#启用或禁用迭代)
> - 移动到专业版 13.9 版本。
>  -  普遍适用于 14.6 版本。功能标志 `group_iterations` 移除。

迭代是在一段时间内跟踪议题的一种方式。这允许团队跟踪速度和波动性指标。迭代可以与[里程碑](../../project/milestones/index.md)一起用于跟踪不同时间段。

例如，您可以使用：

- 计划增量的里程碑，跨越 8-12 周。
- 跨度为 2 周的 Sprint 迭代。

迭代类似于里程碑，但有一些区别：

- 迭代仅适用于群组。
- 一个群组一次只能有一个有效迭代。
- 迭代需要开始和结束日期。
- 迭代日期范围不能重叠。

## 迭代周期

> - 引入于 14.1 版本。
> - 部署在功能标志后默认禁用
> - 不推荐生产环境使用
> - 要在自助管理实例中启用，询问管理员[启用它](#启用或禁用迭代周期).

您可能无法使用此开发中的功能。启用仍在开发中的功能时可能存在风险<!--[启用仍在开发中的功能时可能存在风险](../../../administration/feature_flags.md#risks-when-enabling-features-still-in-development)-->。
有关更多详细信息，请参阅此功能的版本历史记录。

迭代周期自动执行一些常见的迭代任务。它们可用于每 1、2、3、4 或 6 周自动创建一次迭代。还可以配置将自动将未完成的议题滚动到下一次迭代。

启用迭代周期后，您必须先[创建迭代周期](#创建迭代周期)，然后才能[创建迭代](#创建迭代)。

### 创建迭代周期

先决条件：

- 您必须至少具有群组的开发人员角色。

要创建迭代周期：

1. 在顶部栏上，选择 **菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **议题 > 迭代**。
1. 选择 **新建迭代周期**。
1. 填写必填字段，然后选择 **创建迭代周期**。周期列表页面打开。

### 删除迭代周期

先决条件：

- 您必须至少具有群组的开发人员角色。

删除迭代周期也会删除该周期内的所有迭代。

要删除迭代周期：

1. 在顶部栏上，选择 **菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **议题 > 迭代**。
1. 为要删除的节奏选择菜单 (**{ellipsis_v}**) > **删除周期**。
1. 在确认窗口中选择 **删除周期**。

## 查看迭代列表

要查看迭代列表，请转至 **{issues}** **议题 > 迭代**。
要查看某个周期中的所有迭代（按日期降序排列），请选择该迭代周期。
从那里您可以创建一个新的迭代或选择一个迭代以获得更详细的视图。

## 创建迭代

先决条件：

- 您必须至少具有组的开发人员角色。

对于手动计划的迭代周期，您自己创建和添加迭代。

创建迭代：

1. 在顶部栏上，选择 **菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **议题 > 迭代**。
1. 选择 **新建迭代**。
1. 输入标题、描述（可选）、开始日期和截止日期。
1. 选择 **创建迭代**。迭代详细信息页面打开。

## 编辑迭代

> 引入于 13.2 版本。

先决条件：

- 您必须至少具有组的开发人员角色。

要编辑迭代，请选择菜单 (**{ellipsis_v}**) > **编辑**。

## 删除迭代

> 引入于 14.3 版本。

先决条件：

- 您必须至少具有群组的开发人员角色。

要删除迭代，请选择菜单 (**{ellipsis_v}**) > **删除**。

## 添加议题到迭代

> 引入于 13.2 版本。

要了解如何向迭代添加议题，请参阅[管理议题](../../project/issues/managing_issues.md#向迭代添加议题)中的步骤。

## 查看迭代报告

> 查看项目中的迭代报告引入于 13.5 版本。

您可以通过查看迭代报告来跟踪迭代的进度。
迭代报告显示分配给迭代的所有议题及其状态的列表。

该报告还显示了迭代中所有议题的细分。
打开迭代报告显示已完成、未开始和进行中议题的摘要。
封闭迭代报告显示截止日期前完成的议题总数。

要查看迭代报告，请转到迭代列表页面并选择迭代的标题。

### 迭代燃尽和燃起图

> - 引入于 13.6 版本
> - 功能标志移除于 13.7 版本。

迭代报告包括[燃尽图和燃起图](../../project/milestones/burndown_and_burnup_charts.md)，类似于查看[里程碑](../../project/milestones/index.md)时的显示方式。

燃尽图有助于跟踪整体范围的完成进度，燃起图跟踪在给定时间框中添加和完成的议题的每日总计数和权重。

### 按标记分组议题

> 引入于 13.8 版本。

您可以按标记对议题列表进行分组。
这可以帮助您查看带有团队标记的议题，并更准确地了解每个标记的范围。

要按标记对议题进行分组：

1. 在顶部栏上，选择 **菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **议题 > 迭代**。
1. 在 **分组方式** 下拉列表中，选择 **标记**。
1. 选择 **按标记筛选** 下拉菜单。
1. 在标记下拉列表中选择要分组的标记。您还可以通过在搜索输入中，键入来搜索标记。
1. 选择标签下拉列表之外的任何区域。该页面现在按所选标签分组。

### 启用或禁用迭代周期 **(PREMIUM SELF)**

迭代周期功能正在开发中，尚未准备好用于生产。它部署在**默认禁用**的功能标志后面。可以访问 GitLab Rails 控制台的 GitLab 管理员可以启用它。

要启用它：

```ruby
Feature.enable(:iteration_cadences)
```

要禁用它：

```ruby
Feature.disable(:iteration_cadences)
```

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
