---
type: reference, howto
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 群组 **(FREE)**

在极狐GitLab 中，您可以使用群组同时管理一个或多个相关项目。

您可以使用群组来管理项目的权限。如果某人有权访问该群组，则他们可以访问该群组中的所有项目。

您还可以查看群组中项目的所有议题和合并请求，并查看显示群组活动的分析。

您可以使用群组同时与群组的所有成员进行通信。

对于较大的组织，您还可以创建[子组](subgroups/index.md)。

## 查看群组

要查看群组：

1. 在顶部栏上，选择 **菜单 > 群组**。
1. 选择 **您的群组**。显示您所属的所有群组。
1. 要查看公共群组列表，请选择 **浏览群组**。

您还可以按命名空间查看群组。

### 群组可见性

与项目一样，群组可以配置为将其可见性限制为：

- 匿名用户。
- 所有登录用户。
- 只有明确的群组成员。

应用程序设置级别上对可见性级别<!--[可见性级别](../admin_area/settings/visibility_and_access_controls.md#restrict-visibility-levels)-->的限制也适用于群组。如果设置为内部，则匿名用户的浏览页面为空。群组页面有一个可见性级别图标。

管理员用户不能创建比直接父组可见性级别更高的子组或项目。

### 命名空间

命名空间是用户、群组或子组的唯一名称和 URL。

- `http://gitlab.example.com/username`
- `http://gitlab.example.com/groupname`
- `http://gitlab.example.com/groupname/subgroup_name`

例如一个名为 Alex 的用户：

1. Alex 使用用户名 `alex` 创建一个帐户：`https://gitlab.example.com/alex`
1. Alex 为他们的团队创建了一个名为 `alex-team` 的群组。该群组及其项目可在以下网址获得：`https://gitlab.example.com/alex-team`
1. Alex 创建了一个名为 `marketing` 的属于 `alex-team`  的子组。该子组及其项目可在以下网址获得：`https://gitlab.example.com/alex-team/marketing`

## 创建一个群组

要创建一个群组：

1. 在顶部导航栏，有两种方式可选：
   - 选择**菜单 > 群组**，然后在右侧选择 **创建群组**。
   - 在搜索框的左侧，选择加号，然后选择 **新建群组**。
1. 选择 **创建群组**。
1. 对于 **群组名称**，只能使用：
   - 字母和数字字符
   - Emoji
   - 下划线
   - 破折号、点、空格和括号（但不能以任何这些字符开头）

   有关不能用作群组名的单词列表，请参阅[保留名称](../reserved_names.md)。

1. 对于用于[命名空间](#命名空间)的 **群组 URL**，仅使用：
   - 字母和数字字符
   - 下划线
   - 破折号和点（不能以破折号开头或以点结尾）
1. 选择可见性级别<!--[可见性级别](../../public_access/public_access.md)-->。
1. 通过回答以下问题来个性化您的体验：
   - 您的角色是什么？
   - 谁将使用这个群组？
   - 您会用这个群组做什么？
1. 邀请极狐GitLab 成员或其他用户加入群组。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For details about groups, watch [GitLab Namespaces (users, groups and subgroups)](https://youtu.be/r0sJgjR2f5A).
-->

## 添加用户到群组

您可以授予用户访问群组中所有项目的权限。

1. 在顶部栏上，选择 **菜单 > 群组**。
1. 选择 **您的群组**。
1. 找到您的群组并选择它。
1. 从左侧边栏中，选择 **群组信息 > 成员**。
1. 填写字段。
    - 该角色适用于群组中的所有项目。<!--[了解有关权限的更多信息](../permissions.md)。-->
    - 在**访问过期时间**，用户无法再访问组中的项目。

## 请求访问群组

作为用户，如果管理员允许，您可以请求成为群组的成员。

1. 在顶部栏上，选择 **菜单 > 群组**。
1. 选择 **您的群组**。
1. 找到群组并选择它。
1. 在群组名下，选择 **请求访问**。

多达 10 个最近活跃的群组所有者会收到一封包含您的请求的电子邮件。任何群组所有者都可以批准或拒绝请求。

如果您在请求获得批准之前改变主意，请选择 **撤销访问请求**。

## 阻止用户请求访问群组

作为群组所有者，您可以阻止非成员请求访问您的群组。

1. 在顶部栏上，选择**菜单 > 群组**。
1. 选择 **您的群组**。
1. 找到群组并选择它。
1. 从左侧菜单中，选择 **设置 > 通用**。
1. 展开 **权限, LFS, 2FA** 部分。
1. 清除 **允许用户请求访问** 复选框。
1. 选择 **保存修改**。

## 更改群组的所有者

您可以更改群组的所有者。每个群组必须始终至少有一个具有所有者角色<!--[所有者角色](../permissions.md#group-members-permissions)-->的成员。

- 作为管理员：
   1. 进入群组并从左侧菜单中选择 **群组信息 > 成员**。
   1. 给不同的成员 **所有者** 角色。
   1. 刷新页面。您现在可以删除原来的 **所有者** 角色。
- 作为当前组的所有者：
   1. 进入群组并从左侧菜单中选择 **群组信息 > 成员**。
   1. 给不同的成员 **所有者** 角色。
   1. 让新所有者登录并删除您的 **所有者** 角色。

## 从群组中删除成员

先决条件：

- 您必须具有所有者角色<!--[所有者角色](../permissions.md#group-members-permissions)-->。
- 该成员必须是该群组的直接成员。如果成员资格是从父组继承的，则只能从父组中删除该成员。

要从群组中删除成员：

1. 进入群组。
1. 从左侧菜单中，选择 **群组信息 > 成员**。
1. 在您要删除的成员旁边，选择 **删除**。
1. 可选。在 **删除成员** 确认框中，选中 **同时从相关的议题和合并请求中取消指派此用户** 复选框。
1. 选择 **移除成员**。

## 过滤和排序群组中的成员

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/21727) in GitLab 12.6.
> - [Improved](https://gitlab.com/gitlab-org/gitlab/-/issues/228675) in GitLab 13.7.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/289911) in GitLab 13.8.
-->

要查找群组中的成员，您可以排序、过滤或搜索。

### 过滤群组

过滤群组以查找成员。默认情况下，显示群组和子组中的所有成员。

1. 进入群组并选择 **群组信息 > 成员**。
1. 在成员列表上方，在 **筛选成员** 框中，输入筛选条件。
    - 要仅查看群组中的成员，请选择 **成员 = 直接**。
    - 要查看群组及其子组的成员，请选择 **成员 = 继承**。
    - 要查看启用或禁用双重身份验证的成员，请选择 **2FA = 启用** 或 **已禁用**。

### 搜索群组

您可以按姓名、用户名或电子邮件搜索成员。

1. 进入群组并选择 **群组信息 > 成员**。
1. 在成员列表上方，在 **筛选成员** 框中，输入搜索条件。
1. 在 **筛选成员** 框的右侧，选择放大镜 (**{search}**)。

### 对群组中的成员进行排序

您可以按 **帐户**、**已授予访问**、**最大角色** 或 **上次登录** 对成员进行排序。

1. 进入群组并选择 **群组信息 > 成员**。
1. 在成员列表上方的右上角，从 **帐户** 列表中，选择筛选条件。
1. 要在升序和降序之间切换排序，在 **帐户** 列表的右侧，选择箭头（**{sort-lowest}** 或 **{sort-highest}**）。

## 在议题或合并请求中提及群组

当您在评论中提及某个群组时，该群组的每个成员都会将一个待办事项添加到他们的待办事项列表中。

1. 打开 MR 或议题。
1. 在注释中，键入 `@`，后跟用户、群组或子组命名空间。 例如，`@alex`、`@alex-team` 或 `@alex-team/marketing`。
1. 选择 **评论**。

为所有群组和子组成员创建了待办事项。

## 更改群组的默认分支保护

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/7583) in GitLab 12.9.
-->

默认情况下，每个群组都会继承在全局级别设置的分支保护。

要为特定群组更改此设置：

1. 进入群组的 **设置 > 通用** 页面。
1. 展开 **权限, LFS, 2FA** 部分。
1. 在 **默认分支保护** 下拉列表中选择所需的选项。
1. 单击 **保存修改**。

<!--To change this setting globally, see [Default branch protection](../admin_area/settings/visibility_and_access_controls.md#protect-default-branches).-->

NOTE:
在[专业版或更高级别](https://about.gitlab.cn/pricing/)，管理员可以选择禁用群组所有者更新默认分支保护<!--[禁用群组所有者更新默认分支保护](../admin_area/settings/visibility_and_access_controls.md#prevent-overrides-of-default-branch-protection)-->。

## 添加项目到群组

将新项目添加到群组有两种不同的方法：

- 选择一个群组，然后单击 **新键项目**。<!--然后您可以继续创建您的项目[创建您的项目](../../user/project/working_with_projects.md#create-a-project)-->。
- 在创建项目时，从下拉菜单中选择一个群组。

  ![Select group](img/select_group_dropdown_13_10.png)

### 指定谁可以将项目添加到群组

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/2534) in GitLab Premium 10.5.
> - [Moved](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/25975) to GitLab Free in 11.10.
-->

默认情况下，开发者和维护者<!--[开发人员和维护人员](../permissions.md#group-members-permissions)-->可以在一个群组下创建项目。

要为特定群组更改此设置：

1. 进入群组的 **设置 > 通用** 页面。
1. 展开**权限, LFS, 2FA** 部分。
1. 在 **允许创建项目** 下拉列表中选择所需的选项。
1. 单击 **保存更改**。

要全局更改此设置，请参阅默认项目创建保护<!--[默认项目创建保护](../admin_area/settings/visibility_and_access_controls.md#define-which-roles-can-create-projects)-->。

## 群组动态分析 **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/207164) in GitLab 12.10 as
a [beta feature](https://about.gitlab.com/handbook/product/#beta).-->

对于群组，您可以查看在过去 90 天内创建的合并请求、议题和成员的数量。

可以使用 `group_activity_analytics` 功能标志<!--[功能标志](../../development/feature_flags/index.md#enabling-a-feature-flag-locally-in-development)-->启用群组动态分析。

![Recent Group Activity](img/group_activity_analytics_v13_10.png)

### 查看群组动态

您可以在浏览器或 RSS 提要中查看群组中最近执行的操作：

1. 在顶部栏上，选择 **菜单 > 群组**。
1. 选择 **您的群组**。
1. 找到群组并选择它。
1. 在左侧边栏上，选择 **群组信息 > 动态**。

要以 Atom 格式查看动态提要，请选择 **RSS** (**{rss}**) 图标。

## 与另一个群组共享群组

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/18328) in GitLab 12.7.
-->

NOTE:
在 13.11 版本，您可以[用模态窗口替换此表单](#共享群组模态窗口)。

和与群组共享项目<!--[与群组共享项目](../project/members/share_project_with_groups.md)-->的方式类似，您可以与另一个群组共享一个群组。成员可以直接访问共享群组，包括从父组继承群组成员身份的成员。

要与另一个组共享给定的组，例如 `Frontend`，例如，`Engineering`：

1. 进入`前端` 群组。
1. 从左侧菜单中，选择 **群组信息 > 成员**。
1. 选择 **邀请群组** 选项卡。
1. 在 **选择要邀请的群组** 列表中，选择 `Engineering`。
1. 在 **最大角色** 中，选择角色<!--[角色](../permissions.md)-->。
1. 选择 **邀请**。

`Engineering` 组的所有成员都添加到 `Frontend` 组中。

### 共享群组模态窗口

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/247208) in GitLab 13.11.
> - [Deployed behind a feature flag](../feature_flags.md), disabled by default.
> - Enabled on GitLab.com.
> - Recommended for production use.
> - Replaces the existing form with buttons to open a modal window.
> - To use in GitLab self-managed instances, ask a GitLab administrator to [enable it](../project/members/index.md#enable-or-disable-modal-window). **(FREE SELF)**
-->

WARNING:
您可能无法使用此功能。查看上面的 **版本历史** 注释以了解详细信息。

在 13.11 版本，您可以选择用模态窗口替换共享表单。
启用此功能后共享群组：

1. 前往您的群组页面。
1. 在左侧边栏，进入 **群组信息 > 成员**，然后选择 **邀请群组**。
1. 选择一个组，然后选择一个 **最大角色**。
1. （可选）选择 **访问到期日期**。
1. 选择 **邀请**。

## 通过 LDAP 管理群组成员身份 **(PREMIUM SELF)**

群组同步允许将 LDAP 组映射到群组，提供了对每个群组的用户管理的更多控制。要配置群组同步，请编辑 `group_base` **DN** (`'OU=Global Groups,OU=GitLab INT,DC=GitLab,DC=org'`)。此 **OU** 包含将与群组关联的所有 LDAP 组。

可以使用 CN 或过滤器创建群组链接。要创建这些群组链接，请转至群组的 **设置 > LDAP 同步** 页面。配置链接后，用户可能需要一个多小时才能与群组同步。

有关 LDAP 和群组同步管理的更多信息，请参阅 [LDAP 文档](../../administration/auth/ldap/index.md#群组同步)。

NOTE:
添加 LDAP 同步时，如果 LDAP 用户是群组成员并且他们不是 LDAP 组的一部分，则他们将从群组中删除。

### 通过 CN 创建群组链接 **(PREMIUM SELF)**

要通过 CN 创建群组链接：

<!-- vale gitlab.Spelling = NO -->

1. 选择 **LDAP 服务器** 作为链接。
1. **同步方式** 选择 `LDAP Group cn`。
1. 在 **LDAP Group cn** 字段中，开始输入组的 CN。在配置的 `group_base` 中有一个带有匹配 CN 的下拉菜单。从该列表中选择您的 CN。
1. 在 **LDAP Access** 部分，为在该组中同步的用户选择权限级别<!--[权限级别](../permissions.md)-->。
1. 选择 **添加同步** 按钮。

<!-- vale gitlab.Spelling = YES -->

### 通过过滤器创建群组链接 **(PREMIUM SELF)**

要通过过滤器创建群组链接：

1. 选择 **LDAP 服务器** 作为链接。
1. **同步方法** 选择 `LDAP用户过滤器`。
1. 在 **LDAP 用户过滤器** 框中输入您的过滤器。<!--遵循[用户过滤器文档](../../administration/auth/ldap/index.md#set-up-ldap-user-filter)-->。
1. 在 **LDAP Access** 部分，为在该组中同步的用户选择权限级别<!--[权限级别](../permissions.md)-->。
1. 选择 **添加同步** 按钮。

### 覆盖用户权限 **(PREMIUM SELF)**

LDAP 用户权限可以由管理员手动覆盖。要覆盖用户的权限：

1. 进入您群组的 **群组信息 > 成员** 页面。
1. 在您正在编辑的用户行中，选择铅笔 (**{pencil}**) 图标。
1. 在模态中选择棕色的 **编辑权限** 按钮。

现在您可以从 **成员** 页面编辑用户的权限。

## 转换群组

您可以通过以下方式转换群组：

- 将子组转移到新的父组。
- 通过将顶级组转移到所需组，将其转换为子组。
- 通过将子组从当前组中移出，将子组转换为顶级组。

转换群组时，请注意：

- 更改群组的父级可能会产生意想不到的副作用。<!--请参阅[存储库路径更改时会发生什么](../project/repository/index.md#what-happens-when-a-repository-path-changes)。-->
- 您只能将群组转移到您管理的群组。
- 您必须更新本地仓库以指向新位置。
- 如果直接父组的可见性低于该组的当前可见性，子组和项目的可见性级别会更改以匹配新父组的可见性。
- 仅转移明确的群组成员资格，而不是继承的成员资格。如果群组的所有者仅继承了成员资格，则该群组将没有所有者。在这种情况下，转移群组的用户将成为组的所有者。
- 如果软件包<!--[软件包](../packages/index.md)-->存在于群组中的任何项目或其任何子组中，则转换失败。

## 更改群组路径

更改群组的路径（群组 URL）可能会产生意想不到的副作用。<!--在继续之前阅读 [重定向的行为](../project/repository/index.md#what-happens-when-a-repository-path-changes)。-->

如果您正在更改路径以便其他组或用户可以声明它，您可能也需要重命名该组。名称和路径都必须是唯一的。

要保留原始命名空间的所有权并保护 URL 重定向，请创建一个新群组并将项目转移到该群组。

要更改您的群组路径（群组 URL）：

1. 前往您群组的 **设置 > 通用** 页面。
1. 展开 **路径, 转移, 删除** 部分。
1. 在 **更改群组 URL** 下，输入一个新名称。
1. 选择 **更改群组 URL**。

WARNING:
如果命名空间包含带有 Container Registry<!--[Container Registry](../packages/container_registry/index.md)--> 标记的项目，则无法重命名命名空间，因为该项目无法移动。

## 为初始分支使用自定义名称

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/43290) in GitLab 13.6.
-->

创建新项目时，会在第一次推送时创建一个默认分支。群组所有者可以为群组的项目自定义初始分支<!--[自定义初始分支](../project/repository/branches/default.md#group-level-custom-initial-branch-name)-->，满足您群组的需求。

## 删除群组

要删除群组及其内容：

1. 前往您群组的 **设置 > 通用** 页面。
1. 展开 **路径, 转移, 删除** 部分。
1. 在删除群组部分，选择 **删除群组**。
1. 确认操作。

此操作将删除该群组。它还添加了一个后台作业来删除组中的所有项目。

特别注意：

- 在 12.8 及更高版本，专业版或更高级别, 此操作添加了一个后台作业来标记要删除的群组。默认情况下，作业将删除安排在未来 7 天。 您可以通过实例设置<!--[实例设置](../admin_area/settings/visibility_and_access_controls.md#default-deletion-delay)-->修改这个等待时间。
- 在 13.6 及更高版本，如果设置删除的用户在删除之前从群组中删除，则作业将被取消，该群组不再被安排删除。

## 立即删除群组 **(PREMIUM)**

> 引入于 14.2 版本。

如果您不想等待，可以立即删除一个群组。

先决条件：

- 您必须至少具有群组的所有者角色。
- 您已[标记要删除的组](#删除群组)。

立即删除标记为删除的群组：

1. 在顶部栏上，选择 **菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 展开 **高级**。
1. 在 “永久删除群组” 部分，选择 **删除群组**。
1. 根据要求确认操作。

您的群组、其子组、项目和所有相关资源（包括议题和合并请求）都将被删除。

## 恢复群组 **(PREMIUM)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/33257) in GitLab 12.8.
-->

要恢复标记为删除的组：

1. 前往您群组的 **设置 > 通用** 页面。
1. 展开  **路径, 转移, 删除** 部分。
1. 在恢复群组部分，选择 **恢复群组**。

## 防止群组层次结构外的群组共享

此设置仅适用于顶级群组。它会影响所有子组。

选中后，顶级群组层次结构中的任何群组只能与层次结构中的其他群组共享。

例如，对于这些群组：

- **动物 > 狗**
- **动物 > 猫**
- **植物 > 树木**

如果您在 **动物** 群组中选择此设置：

- **狗** 可以与 **猫** 共享。
- **狗** 不能与 **树** 共享。

要防止在群组的层次结构之外共享：

1. 进入群组的 **设置 > 通用** 页面。
1. 展开 **权限, LFS, 2FA** 部分。
1. 选择 **禁止成员发送邀请到 `<group_name>` 及其子群组**。
1. 选择 **保存修改**。

## 防止与群组共享项目

防止群组中的项目与另一个群组共享项目<!--[与另一个群组共享项目](../project/members/share_project_with_groups.md)-->以实现对项目访问的更严格控制。

要防止与其他组共享项目：

1. 进入群组的 **设置 > 通用** 页面。
1. 展开 **权限, LFS, 2FA** 部分。
1. 选择 **禁止与其他群组共享 `<group_name>` 中的项目**。
1. 选择 **保存修改**。

## 防止成员被添加到群组 **(PREMIUM)**

作为群组所有者，您可以阻止群组中所有项目的任何新项目成员资格，从而可以更严格地控制项目成员资格。

例如，如果您想锁定一个审计事件<!--[审计事件](../../administration/audit_events.md)-->的群组，则可以保证在审计期间不能修改项目成员资格。

您仍然可以邀请群组或向群组添加成员，隐式授予成员访问 **锁定** 群组中的项目的权限。

该设置不级联。子组中的项目观察子组配置，忽略父组。

要防止将成员添加到组中的项目：

1. 进入群组的 **设置 > 通用** 页面。
1. 展开 **权限, LFS, 2FA** 部分。
1. 在 **成员锁** 下，选择 **禁止向当前群组中的项目添加新成员**。
1. 选择 **保存修改**。

以前拥有权限的所有用户都不能再将成员添加到群组中。向项目添加新用户的 API 请求是不可能的。

## 将成员导出为 CSV **(PREMIUM)**

> 引入于 14.2。

FLAG:
在自助管理版上，默认情况下此功能可用。要隐藏每个群组的功能，请让管理员禁用 :ff_group_membership_export 标志<!--[禁用 :ff_group_membership_export 标志](../../administration/feature_flags.md)-->。<!--在 GitLab.com 上，此功能可用。-->

您可以将群组中的成员列表导出为 CSV。

1. 转到您的项目并选择 **项目信息 > 成员**。
1. 选择 **导出为 CSV**。
1. 生成 CSV 文件后，它会作为附件通过电子邮件发送给请求它的用户。

## 通过 IP 地址限制群组访问 **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/1985) in [GitLab Ultimate](https://about.gitlab.com/pricing/) 12.0.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/215410) to [GitLab Premium](https://about.gitlab.com/pricing/) in 13.1.
-->

为确保只有组织中的人员才能访问特定资源，您可以按 IP 地址限制对群组的访问。此群组级别设置适用于：

- GitLab UI，适用于子组、项目和议题。
- 在 12.3 及更高版本，适用于 API。

在配置 IP 地址限制之前，您应该考虑这些安全隐患：

- **SSH 请求**：虽然您可以通过 IP 地址限制来限制 HTTP 流量，但它们会导致 SSH 请求（包括通过 SSH 进行的 Git 操作）失败。
- **管理员和群组所有者**：具有这些权限级别的用户始终可以访问群组设置，无论 IP 限制如何，但从不​​允许的 IP 地址访问时，他们无法访问属于该组的项目。
- **GitLab API 和 runner 动态**：只有群组<!--[群组](../../api/groups.md)-->和项目<!--[项目](../../api/projects.md)--> API 受 IP 地址限制。当您注册 runner 时，它不受 IP 限制的约束。当 runner 请求新作业或更新作业状态时，它也不受 IP 限制的约束。但是，当正在运行的 CI/CD 作业从受限制的 IP 地址发送 Git 请求时，IP 限制会阻止代码被克隆。
- **用户仪表板活动**：用户可能仍会在其仪表板上看到来自 IP 受限组和项目的一些事件。活动可能包括推送、合并、发布或评论事件。

要通过 IP 地址限制群组访问：

1. 进入群组的 **设置 > 通用** 页面。
1. 展开 **权限, LFS, 2FA** 部分。
1. 在 **允许访问以下 IP 地址** 字段中，以 CIDR 表示法输入 IP 地址范围。
1. 选择 **保存修改**。

   ![Domain restriction by IP address](img/restrict-by-ip.gif)

## 按域名限制群组访问 **(PREMIUM)**

<!--
>- [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/7297) in [GitLab Premium](https://about.gitlab.com/pricing/) 12.2.
>- Support for specifying multiple email domains [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/33143) added in GitLab 13.1.
-->

您可以阻止将电子邮件地址在特定域名中的用户添加到群组及其项目。

按域名限制群组访问：

1. 进入群组的 **设置 > 通用** 页面。
1. 展开 **权限, LFS, 2FA** 部分。
1. 在 **通过电子邮件域名限制成员资格** 字段中，输入域名。
1. 选择 **保存修改**。

![Domain restriction by email](img/restrict-by-email.gif)

每当您尝试添加新用户时，都会将他们与此列表进行比较。

某些域名不能被限制，比如最受欢迎的公共电子邮件域名，例如：

- `gmail.com`, `yahoo.com`, `aol.com`, `icloud.com`
- `hotmail.com`, `hotmail.co.uk`, `hotmail.fr`
- `msn.com`, `live.com`, `outlook.com`

## 群组文件模板 **(PREMIUM)**

使用群组文件模板与群组中的每个项目共享一组常见文件类型的模板。它类似于实例模板仓库<!--[实例模板仓库](../admin_area/settings/instance_template_repository.md)-->。所选项目应遵循该页面上记录的相同命名约定。

您只能选择群组中的项目作为模板源。包括与群组共享的项目，但**排除**正在配置的群组的子组或父组中的项目。

您可以为子组和直接父组配置此功能。子组中的项目可以访问该子组以及任何直接父组的模板。

<!--要了解如何为议题和合并请求创建模板，请参阅 [描述模板](../project/description_templates.md)。-->

通过将群组设置为模板源，在群组级别定义项目模板。<!--[Learn more about group-level project templates](custom_project_templates.md).--> **(PREMIUM)**

### 启用群组文件模板 **(PREMIUM)**

要启用群组文件模板：

1. 进入群组的 **设置 > 通用** 页面。
1. 展开 **模板** 部分。
1. 选择一个项目作为模板库。
1. 选择 **保存修改**。

## 禁用电子邮件通知

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/23585) in GitLab 12.2.
-->

您可以禁用与群组相关的所有电子邮件通知，其中包括其子组和项目。

要禁用电子邮件通知：

1. 进入群组的 **设置 > 通用** 页面。
1. 展开 **权限, LFS, 2FA** 部分。
1. 选择 **禁用电子邮件通知**。
1. 选择 **保存修改**。

## 禁用群组提及

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/21301) in GitLab 12.6.
-->

您可以防止将用户添加到对话中，并在有人提及这些用户所属的群组时收到通知。

禁用提及的群组在自动完成下拉菜单中相应地可视化。

这对于拥有大量用户的群组特别有用。

要禁用群组提及：

1. 进入群组的 **设置 > 通用** 页面。
1. 展开 **权限, LFS, 2FA** 部分。
1. 选择 **禁用群组提及**。
1. 选择 **保存修改**。

## 启用延迟项目移除 **(PREMIUM)**

> - 引入于 13.2 版本。
> - 于 13.11 版本添加了继承和执行。
> - 于 14.2 版本添加了默认启用的实例设置。

可以为群组启用延迟项目删除。启用后，群组中的项目会在延迟一段时间后被删除。在此期间，项目处于只读状态，可以恢复。默认期限为 7 天，但可在实例级别进行配置。

在自助管理版本上，默认情况下会立即删除项目。
在 14.2 及更高版本中，管理员可以为新创建的群组中的项目更改默认设置<!--[更改默认设置](../admin_area/settings/visibility_and_access_controls.md#default-delayed-project-deletion)-->。

<!--On GitLab.com, see the [GitLab.com settings page](../gitlab_com/index.md#delayed-project-deletion) for
the default setting.-->

要启用群组中项目的延迟删除：

1. 进入群组的 **设置 > 通用** 页面。
1. 展开 **权限, LFS, 2FA** 部分。
1. 勾选 **启用延迟项目移除**。
1. 可选。要防止子组更改此设置，请选择 **对所有子组强制执行**。
1. 选择 **保存修改**。

NOTE:
在 13.11 及更高版本中，延迟项目删除的群组设置由子组继承。<!--如[级联设置](../../development/cascading_settings.md)中所述，-->继承可以被覆盖，除非由父组强制执行。

## 防止项目在群组外派生 **(PREMIUM)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/216987) in GitLab 13.3.
-->

默认情况下，群组中的项目可以派生。或者在[专业版](https://about.gitlab.cn/pricing/)或更高级别，您可以防止群组中的项目在当前顶级群组之外派生。

以前，此设置仅适用于在 SAML 中强制执行组管理帐户<!--[组管理帐户](saml_sso/group_managed_accounts.md)-->的群组。此设置将从 SAML 设置页面中删除，并迁移到群组设置页面。在过渡期间，这两种设置都会被考虑在内。如果其中之一设置为 `true`，则该群组不允许外部派生。

防止项目在群组外派生：

1. 进入顶级群组的 **设置 > 通用** 页面。
1. 展开 **权限, LFS, 2FA** 部分。
1. 勾选 **阻止项目派生到当前群组以外**。
1. 选择 **保存修改**。

现有的派生不会被删除。

## 群组推送规则 **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/34370) in GitLab 12.8.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/224129) in GitLab 13.4.
-->

群组推送规则允许群组维护者为特定组中新创建的项目设置推送规则<!--[推送规则](../../push_rules/push_rules.md)-->。

为群组配置推送规则：

1. 进入群组的 **推送规则** 页面。
1. 选择您想要的设置。
1. 选择 **保存推送规则**。

该群组的新子组基于以下任一条件设置推送规则：

- 定义了推送规则的最接近的父组。
- 在实例级别设置的推送规则，如果没有父组定义推送规则。

## 群组批准规则 **(PREMIUM)**

> 引入于 13.9 版本。部署在 `group_merge_request_approval_settings_feature_flag` 标记之后<!--[部署在 `group_merge_request_approval_settings_feature_flag` 标记之后](../../administration/feature_flags.md)-->，默认禁用。

FLAG:
在自助管理版上，默认情况下此功能不可用。为了使其可用，每个群组需要请管理员启用 `group_merge_request_approval_settings_feature_flag` 标记。<!--[启用`group_merge_request_approval_settings_feature_flag`标志](../../administration/feature_flags.md)-->

该功能尚未准备好用于生产用途。

群组批准规则是一个正在开发的功能，它提供了一个界面，用于在顶级群组级别管理项目合并请求批准规则<!--[项目合并请求批准规则](../project/merge_requests/approvals/index.md)-->。<!--[在实例级别](../admin_area/merge_requests_approvals.md)-->在实例级别配置规则后，您无法编辑锁定的规则。

要查看群组的合并请求批准规则 UI：

1. 进入顶级群组的 **设置 > 通用** 页面。
1. 展开 **合并请求批准** 部分。
1. 选择您想要的设置。
1. 选择 **保存修改**。

<!--
## Related topics

- [Group wikis](../project/wiki/index.md)
- [Maximum artifacts size](../admin_area/settings/continuous_integration.md#maximum-artifacts-size). **(FREE SELF)**
- [Repositories analytics](repositories_analytics/index.md): View overall activity of all projects with code coverage. **(PREMIUM)**
- [Contribution analytics](contribution_analytics/index.md): View the contributions (pushes, merge requests,
  and issues) of group members. **(PREMIUM)**
- [Issue analytics](issues_analytics/index.md): View a bar chart of your group's number of issues per month. **(PREMIUM)**
- Use GitLab as a [dependency proxy](../packages/dependency_proxy/index.md) for upstream Docker images.
- [Epics](epics/index.md): Track groups of issues that share a theme. **(ULTIMATE)**
- [Security Dashboard](../application_security/security_dashboard/index.md): View the vulnerabilities of all
  the projects in a group and its subgroups. **(ULTIMATE)**
- [Insights](insights/index.md): Configure insights like triage hygiene, issues created/closed per a given period, and
  average time for merge requests to be merged. **(ULTIMATE)**
- [Webhooks](../project/integrations/webhooks.md).
- [Kubernetes cluster integration](clusters/index.md).
- [Audit Events](../../administration/audit_events.md#group-events). **(PREMIUM)**
- [Pipelines quota](../admin_area/settings/continuous_integration.md): Keep track of the pipeline quota for the group.
- [Integrations](../admin_area/settings/project_integration_management.md).
- [Transfer a project into a group](../project/settings/index.md#transferring-an-existing-project-into-another-namespace).
- [Share a project with a group](../project/members/share_project_with_groups.md): Give all group members access to the project at once.
- [Lock the sharing with group feature](#prevent-a-project-from-being-shared-with-groups).
- [Enforce two-factor authentication (2FA)](../../security/two_factor_authentication.md#enforce-2fa-for-all-users-in-a-group): Enforce 2FA
  for all group members.
- Namespaces [API](../../api/namespaces.md) and [Rake tasks](../../raketasks/features.md)..
-->

## 故障排查

### 验证访问是否被 IP 限制阻止

如果用户在通常期望访问时看到 404，并且问题仅限于特定群组，请在 `auth.log` rails 日志中搜索以下一项或多项：

- `json.message`: `'Attempting to access IP restricted group'`
- `json.allowed`: `false`

在查看日志条目时，将 `remote.ip` 与群组[允许的 IP](#通过-ip-地址限制群组访问) 列表进行比较。

### 命名空间和群组的验证错误

14.4 及更高版本在创建或更新命名空间或组时执行以下检查：

- 命名空间不能有父级。
- 父组必须是组而不是命名空间。

如果显示以下错误，您可以禁用验证：

- `A user namespace cannot have a parent`.
- `A group cannot have a user namespace as its parent`.

万一您在安装实例中看到这些错误，尝试联系技术支持。