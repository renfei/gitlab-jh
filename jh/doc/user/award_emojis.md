---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用 emoji **(FREE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/1825) in GitLab 8.2.
> - GitLab 9.0 [introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/9570) the usage of native emoji if the platform
>   supports them and falls back to images or CSS sprites. This change greatly
>   improved award emoji performance overall.
-->

当您在线协作时，击掌和竖起大拇指的机会更少。您可以在[议题](project/issues/index.md)、合并请求<!--[合并请求](project/merge_requests/index.md)-->、代码片段<!--[代码片段](snippets.md)-->以及任何您可以使用主题的地方使用 emoji。

![Award emoji](img/award_emoji_select_v14_6.png)

使用 emoji 可以更轻松地提供和接收反馈，而无需长长的评论主题。

<!--For information on the relevant API, see [Award Emoji API](../api/award_emoji.md).-->

## 根据投票数对议题和合并请求进行排序

<!--> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/2781) in GitLab 8.5.-->

您可以根据收到的投票数对议题和合并请求进行快速排序。排序选项可以在下拉菜单中找到，如 “最受欢迎” 和 “最不受欢迎”。

![Votes sort options](img/award_emoji_votes_sort_options.png)

总票数未加总。具有 18 个赞成票和 5 个反对票的议题被认为比具有 17 个赞成票和没有反对票的议题更受欢迎。

## 为评论使用 emoji

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/4291) in GitLab 8.9.-->

当您想庆祝一项成就或同意某个意见时，使用 emoji 也可以应用于个人评论。

添加表情符号：

1. 在评论的右上角，选择微笑（**{slight-smile}**）。
1. 从下拉列表中选择一个表情符号。

要删除表情符号，请再次选择该表情符号。
