# 极狐Gitlab开发指南

本文档旨在了解为极狐Gitlab贡献代码所需的流程和技术信息。

## 代码贡献指南

- [极狐Gitlab代码贡献指南](contributing/index.md)
  - [代码开发约定以及风格指南](contributing/style_guides.md)

## 后端指南

- [极狐GitLab后端开发指南](backend_guide/index.md)

## 前端指南

- [极狐GitLab前端开发指南](frontend_guide/index.md)
  - [架构相关](frontend_guide/architecture.md)
  - [如何在极狐GitLab里进行开发](frontend_guide/how_to_develop.md)
