---
stage: none
group: unassigned
---

# 如何在 GitLab EE 的基础上开发

[前面](architecture.md)说到极狐GitLab是基于 GitLab EE 进行开发的，所以所有的功能都是从 GitLab EE 上继承过去的，而极狐GitLab时常会需要开发自己的功能，而我们不希望通过**覆盖**的方式去新建一个同名文件来使得 `jh/` 目录下文件得到加载，而是希望通过加载 `ce/` 或者 `ee/` 下的文件来加载 `jh/` 目录下的文件，以保持与上游 GitLab 的功能同步，并且包含有极狐GitLab独有的功能。

## 方法

**该方法我们简单可以理解为在上游通过特定的方式开辟入口，然后在 `jh/` 中使用该入口。**

要做到上述的行为，我们需要做的事情就是到上游 [GitLab 上游仓库](https://gitlab.com/gitlab-org/gitlab) 的仓库中为[极狐GitLab](https://gitlab.com/gitlab-jh/gitlab)提供一个入口，例如在 `_page.html.haml` 模板文件中，我们通过在该文件中添加入口代码。

### 添加入口

在这我们会需要在 HAML 模板中用到一个 Ruby 里定义的一个比较重要的全局函数 `render_if_exists`，详细用法如下：

> _page.html.haml

```haml
= render_if_exists "shared/footer/global_footer"
```

上游代码对应位置为 [_page.html.haml](https://gitlab.com/gitlab-org/gitlab/-/commit/b744bdcd8a5adc45d393a1cd8d84859df6bb26bf#41c204757064df1b3ba82473b5de39bdee5d80ef_31_32)，对应查看

该代码便会通过上下文查找对应的 `views/` 文件夹来加载我们需要的这个 `_global_footer.html.haml` 模板文件，上面说到：

> 在极狐GitLab的仓库代码中，`jh/` 目录下的文件的优先级顺序为 **`jh/` > `ee/` > `ce/`**。在这三个目录同时拥有某个文件时，所有 `jh/` 目录下的文件都会优于其他目录下的文件加载。

- 当极狐GitLab的应用运行时，通过优先加载 `jh/` 目录下的 `_global_footer.html.haml` 这个模板文件来渲染应用，从而达到功能定制的效果。

- 当上游应用独立运行时，上游应用并没有 `jh/` 这个目录，那么上游应用将会优先加载 `ee/` 目录下的 `_global_footer.html.haml` 文件，假如 `ee/` 下找不到该文件，则会去加载 `ce/` 下的该同名文件，如果 `ce/` 目录下也没有当前文件，则该方法不会有任何副作用。

### 使用入口

通过在对应的 `jh/` 目录下添加 `jh/app/views/shared/footer/_global_footer.html.haml` 文件，然后添加需要渲染的内容。

该示例代码可以在 [_global_footer.html.haml](https://gitlab.com/gitlab-jh/gitlab/-/blob/main-jh/jh/app/views/shared/footer/_global_footer.html.haml) 查看到。

## 路径别名(alias)

在JavaScript和Vue代码中，我们同样需要针对Gitlab EE或者Gitlab CE的代码进行拓展。因此声明了如下 `alias` ，在不同版本中解析为不同的文件路径。

例如 `any_else_ce`： 
> 在极狐Gitlab中会匹配到 `jh/app/javascript/` 目录。  
> 在Gitlab EE中会匹配到 `ee/app/javascript/` 目录。  
> 在Gitlab CE中会匹配到 `app/javascript/` 目录。  

| Alias  | Path in CE | Path in EE | Path in JH |
| ------ |   ------   |   ------   |   ------   |
| `jh_else_ce` | `app/javascript/xx/xx` | `app/javascript/xx/xx` | `jh/app/javascript/xx/xx` |
| `jh_else_ee` | `not contained` | `ee/app/javascript/xx/xx` | `jh/app/javascript/xx/xx` |
| `any_else_ce`| `app/javascript/xx/xx` | `ee/app/javascript/xx/xx` | `jh/app/javascript/xx/xx` |

> ***注意：*** 
- `jh_else_ee` 不应该出现在Gitlab CE的代码中，即除了 `jh/` 和 `ee/` 目录下的文件都不应该使用该别名。
- 如果某个文件仅在Gitlab CE存在，在 `jh/` 目录新增同名文件后，需要在Gitlab CE代码中将 `alias` 指定为 `jh_else_ce` 。
- 如果某个文件在Gitlab CE通过 `jh_else_ce` 引用后，又在 `ee/` 目录新增了同名文件进行覆盖，则需要在Gitlab CE代码中将 `alias` 改为 `any_else_ce` 。
