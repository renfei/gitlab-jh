---
stage: none
group: unassigned
---

# 极狐GitLab前端开发指南

本文档旨在介绍：

- 极狐GitLab是如何在 GitLab EE 的基础之上进行开发。
- 如何在极狐GitLab中开发特定功能以及如何向上游（[GitLab 上游仓库](https://gitlab.com/gitlab-org/gitlab)）本身进行开发和贡献。

更多未尽事项请参考 [Gitlab 前端开发指南](https://docs.gitlab.com/ee/development/fe_guide/index.html) 。

## 概览

极狐GitLab的所有功能开发是建立在 GitLab EE 之上的。后端使用 [Ruby on Rails](https://rubyonrails.org)，前端使用 JavaScript + [Vue.js 2.x](https://vuejs.org) 版本的**多页**应用。

## 开发的风格和约定

有关于代码开发约定以及风格请查看[代码风格指南](https://docs.gitlab.com/ee/development/fe_guide/style/) 。

## 架构相关

请查看[架构](architecture.md)。

## 如何在极狐GitLab里进行开发

请查看[开发流程](how_to_develop.md)
