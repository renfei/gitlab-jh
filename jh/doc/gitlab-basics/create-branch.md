---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: howto
---

# 如何创建分支 **(FREE)**

一个分支是在一个项目<!--[项目](../user/project/index.md)-->中的一个独立的开发路线。

当您创建一个分支时（在终端或者通过网页界面<!--[网页界面](../user/project/repository/web_editor.md#create-a-new-branch)-->），您是在创建某个分支当前状态的快照，通常是主干分支。之后，您可以开始实现自己的修改而不影响主代码库。您的修改历史会记录在分支上。

当您的修改准备好时，可以通过合并请求<!--[合并请求](../user/project/merge_requests/creating_merge_requests.md)-->将其合并到代码库的其余部分中。
