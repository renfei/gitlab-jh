import '~/pages/profiles/show';
import $ from 'jquery';
import PhoneValidator from 'jh/pages/sessions/new/phone_validator';
import VerificationCodeButton from 'jh/verification_code_button';

const phoneInput = $(`.js-validate-phone`);
let currentAreaCode = '+86';
const currentPhoneNumber = phoneInput.val().replace(/^\+(86|853|852)/, (match) => {
  if (match) {
    $('select.js-select-areacode').val(match);
    currentAreaCode = match;
  }
  return '';
});
const verificationField = $('.verification-code.form-group');
const phoneField = $('.phone.form-group');

const verificationButton = new VerificationCodeButton();
new PhoneValidator({ verificationButton, currentPhoneNumber, currentAreaCode }); // eslint-disable-line no-new

phoneInput.val(currentPhoneNumber);

if (phoneInput.length > 0) {
  phoneInput.on('input', () => {
    if (phoneInput.val() !== currentPhoneNumber) {
      verificationField.insertAfter(phoneField);
    } else {
      verificationField.remove();
    }
  });
  phoneInput.trigger('input');
}
