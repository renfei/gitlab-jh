# frozen_string_literal: true

class SmsController < ActionController::Metal # rubocop:disable Gitlab/NamespacedClass
  include AbstractController::Rendering
  include ActionController::ApiRendering
  include ActionController::Renderers
  include Recaptcha::Verify
  include VisitorIdCode
  include MergeAreaCodeAndPhone

  use_renderers :json

  def verification_code
    if !params[:phone]
      render json: { status: 'PARAMS_BLANK_ERROR' }
    elsif verify_captcha(params)

      merge_area_code_and_phone

      # rubocop: disable CodeReuse/ActiveRecord
      phone_verification = Phone::VerificationCode.find_or_initialize_by(phone: params[:phone])
      # rubocop: enable CodeReuse/ActiveRecord
      if phone_verification.created_at && phone_verification.created_at > 1.minute.ago
        render json: { status: 'SENDING_LIMIT_RATE_ERROR' }
      elsif (code = TencentSms.send_code(params[:phone]))

        phone_verification.assign_attributes(visitor_id_code: visitor_id_code, code: code, created_at: Time.zone.now)

        if phone_verification.save!
          render json: { status: 'OK' }
        else
          render json: { status: 'SAVE_CODE_ERROR' }
        end

      else
        render json: { status: 'SENDING_CODE_ERROR' }
      end
    else
      render json: { status: 'RECAPTCHA_ERROR' }
    end
  rescue StandardError => error
    render json: { status: 'ERROR', message: error }
  end

  private

  def verify_captcha(params)
    if ::Feature.enabled?(:real_name_system)
      TencentCaptcha.verify!(params[:ticket], request.ip, params[:rand_str])
    else
      Gitlab::Recaptcha.load_configurations!
      verify_recaptcha
    end
  end
end
