# frozen_string_literal: true

module JH
  module Users
    module TermsController
      extend ActiveSupport::Concern
      extend ::Gitlab::Utils::Override
      include CheckPhoneAndCode

      prepended do
        before_action :verify_code_received_by_phone, only: [:accept], if: -> { ::Gitlab.dev_or_test_env? || (::Gitlab.dev_env_or_com? && ::Gitlab.jh? && ::Feature.enabled?(:real_name_system)) }
      end

      override :index
      def index
        super

        return unless ::Feature.enabled?(:real_name_system)

        # rubocop: disable Gitlab/ModuleWithInstanceVariables
        if current_user && @term.accepted_by_user?(current_user)
          flash.now[:notice] = if current_user.phone_verified?
                                 s_("RealName|You have already accepted the terms and verified your phone.")
                               else
                                 s_("RealName|You have already accepted the terms but need to verify your phone.")
                               end
        end
        # rubocop: enable Gitlab/ModuleWithInstanceVariables
      end

      private

      def verify_code_received_by_phone
        return if current_user.phone_verified?

        # rubocop: disable Style/AndOr
        if (message = check_verification_code)
          flash[:alert] = message
          redirect_to terms_path, redirect: redirect_path and return
        else
          current_user.phone = params[:phone]
          unless current_user.save
            flash[:alert] = s_('RealName|Phone saved failed.')
            redirect_to terms_path, redirect: redirect_path and return
          end
        end
        # rubocop: enable Style/AndOr
      end
    end
  end
end
