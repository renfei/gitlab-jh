# frozen_string_literal: true

module JH
  module ProfilesController
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override
    include CheckPhoneAndCode

    prepended do
      before_action :verify_code_received_by_phone, only: [:update], if: -> { ::Gitlab.dev_env_or_com? && ::Gitlab.jh? && ::Feature.enabled?(:real_name_system) }
    end

    private

    override :user_params_attributes
    def user_params_attributes
      if ::Feature.enabled?(:real_name_system)
        super + [:phone, :verification_code, :area_code]
      else
        super
      end
    end

    def verify_code_received_by_phone
      check_verification_code
    end
  end
end
