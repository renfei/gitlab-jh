# frozen_string_literal: true

module CheckPhoneAndCode
  include VisitorIdCode
  include MergeAreaCodeAndPhone

  private

  def check_phone_code_params
    if !params[:phone] || !params[:verification_code]
      flash[:alert] = s_('RealName|Verification code or phone is blank.')
      # rubocop: disable Style/AndOr
      redirect_to terms_path, redirect: redirect_path and return
      # rubocop: enable Style/AndOr
    end
  end

  def check_verification_code
    case controller_name
    when 'terms'
      check_code_for_terms_page
    when 'registrations'
      check_code_for_registrations_page
    when 'profiles'
      check_code_for_profiles_page
    end
  end

  def verification_message(verification, vcode)
    if !verification
      s_('RealName|Verification code is nonexist.')
    elsif verification.created_at < 10.minutes.ago
      s_('RealName|Verification code is outdated.')
    elsif verification.code != vcode
      s_('RealName|Verification code is incorrect.')
    end
  end

  def check_code_for_terms_page
    merge_area_code_and_phone
    verification = Phone::VerificationCode.for_visitor(visitor_id_code, params[:phone]).first
    verification_message(verification, params[:verification_code])
  end

  def check_code_for_registrations_page
    merge_area_code_and_phone_with_user
    verification = Phone::VerificationCode.for_visitor(visitor_id_code, params[:user][:phone]).first

    if (message = verification_message(verification, params[:user][:verification_code]))
      redirect_to new_user_registration_path, status: :not_found, alert: message
    end
  end

  def check_code_for_profiles_page
    merge_area_code_and_phone_with_user
    return if params[:user][:phone] == current_user.phone

    verification = Phone::VerificationCode.for_visitor(visitor_id_code, params[:user][:phone]).first

    if (message = verification_message(verification, params[:user][:verification_code]))
      respond_to do |format|
        format.html { redirect_back_or_default(default: { action: 'show' }, options: { alert: message }) }
        format.json { render json: { message: message } }
      end
    end
  end
end
