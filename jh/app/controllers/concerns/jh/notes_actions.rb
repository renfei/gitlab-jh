# frozen_string_literal: true

module JH
  module NotesActions
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    override :update
    # rubocop:disable CodeReuse/ActiveRecord
    def update
      return super unless ::ContentValidation::Setting.check_enabled?(project)

      # Build a note object for content validation
      note = Notes::BuildService.new(project, current_user, update_note_params).execute

      if note.invalid? && note.errors.where(:note, :content_invalid).present?
        json = {
          valid: false,
          content_invalid: true,
          errors: note.errors.where(:note, :content_invalid).last.message
        }
        render json: json, status: :unprocessable_entity
        return
      end

      super
    end
    # rubocop:enable CodeReuse/ActiveRecord
  end
end
