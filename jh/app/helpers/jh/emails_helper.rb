# frozen_string_literal: true

module JH
  module EmailsHelper
    def header_logo
      if current_appearance&.header_logo?
        image_tag(
          current_appearance.header_logo_path,
          style: 'height: 50px'
        )
      else
        image_tag(
          image_url('mailers/gitlab_header_logo.png'),
          size: '103x73',
          alt: 'GitLab'
        )
      end
    end
  end
end
