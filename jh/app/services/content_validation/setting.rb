# frozen_string_literal: true

module ContentValidation
  class Setting
    class << self
      def block_enabled?(container)
        container = container.container if container.is_a?(Wiki)
        ::Gitlab.dev_env_or_com? && !container.private?
      end

      def content_validation_enable?
        ::Gitlab.dev_env_or_com? &&
        ::Gitlab::CurrentSettings.content_validation_endpoint_enabled?
      end

      def check_enabled?(container)
        return content_validation_enable? if container.nil?

        container = container.container if container.is_a?(Wiki)
        content_validation_enable? && !container.private?
      end
    end
  end
end
