# frozen_string_literal: true

module JH
  module Users
    module BuildService
      extend ::Gitlab::Utils::Override
      include ::Gitlab::Utils::StrongMemoize

      private

      override :signup_params
      def signup_params
        if ::Feature.enabled?(:real_name_system)
          super + [:phone, :area_code]
        else
          super
        end
      end
    end
  end
end
