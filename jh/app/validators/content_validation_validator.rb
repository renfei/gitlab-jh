# frozen_string_literal: true

# ContentValidation for input content
# rubocop:disable Gitlab/NamespacedClass
class ContentValidationValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return if value.blank?
    return unless record.public_send("#{attribute}_changed?") # rubocop:disable GitlabSecurity/PublicSend

    content_validation_service = ContentValidation::ContentValidationService.new
    record.errors.add(attribute, :content_invalid, message: s_('ContentValidation|sensitive or illegal characters involved')) unless content_validation_service.valid?(value)
  rescue StandardError => e
    # If the external service is down or other error happen, we don't want to block record save
    Gitlab::AppLogger.error(e)
  end
end
