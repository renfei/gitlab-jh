# frozen_string_literal: true

module JH
  # User JH mixin
  #
  # This module is intended to encapsulate JH-specific model logic
  # and be prepended in the  model

  module User
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    prepended do
      # Vertual attribute for receive verification code in register form
      attr_accessor :verification_code
      attr_accessor :area_code

      delegate :phone, :phone=, to: :user_detail, allow_nil: true

      include ContentValidateable
      validates :name, :skype, :linkedin, :twitter, :location, :organization, content_validation: true, if: :should_validate_content?
    end

    def phone_verified?
      return true if project_bot?

      phone.present?
    end
  end
end
