# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Users::TermsController do
  include TermsHelper

  let_it_be(:user) { create(:user) }

  let(:term) { create(:term) }
  let(:vcode) { '123456' }
  let(:phone_number) { '15612341234' }
  let(:area_code) { '+86' }
  let(:gitlab_session) { 'b356c5e5f623717dbf219975b859f25f' }

  describe 'POST #accept' do
    before do
      sign_in user
    end
    context 'when a user is signed in' do
      it 'saves that the user accepted the terms and verified the phone' do
        # send a sms
        Phone::VerificationCode.create!(
          visitor_id_code: gitlab_session, code: vcode, phone: area_code + phone_number,
          created_at: Time.zone.now
        )

        cookies[Gitlab::Application.config.session_options[:key]] = gitlab_session
        post :accept, params: { id: term.id, area_code: area_code, phone: phone_number, verification_code: vcode }

        # rubocop: disable CodeReuse/ActiveRecord
        agreement = user.term_agreements.find_by(term: term)
        # rubocop: enable CodeReuse/ActiveRecord
        expect(agreement.accepted).to eq(true)
        expect(user.phone_verified?).to eq(true)
        expect(user.phone).to eq(area_code + phone_number)
      end
    end
  end
end
