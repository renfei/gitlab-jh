# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Projects::NotesController do
  include ProjectForksHelper

  let(:user)    { create(:user) }
  let(:issue)   { create(:issue, project: project) }
  let(:note)    { create(:note, noteable: issue, project: project) }
  let(:request_params) do
    {
      namespace_id: project.namespace,
      project_id: project,
      id: note,
      format: :json,
      note: {
        note: "New comment"
      }
    }
  end

  describe 'PUT update' do
    context 'application setting content validation is enable' do
      before do
        sign_in(note.author)
        project.add_developer(note.author)
      end

      context 'note update invalid content with project is private' do
        before do
          stub_content_validation_request(false)
        end

        let(:project) { create(:project, :private) }

        it "updates the note" do
          expect { put :update, params: request_params }.to change { note.reset.note }
        end
      end

      context 'project is public' do
        let(:project) { create(:project, :public) }

        context 'note update with valid content' do
          before do
            stub_content_validation_request(true)
          end

          it "updates the note" do
            note_html = note.note_html
            expect { put :update, params: request_params }.to change { note.reset.note }
            expect(note.reset.note_html).not_to eq(note_html)
            expect(response).to have_gitlab_http_status(:ok)
          end
        end

        context 'note update with invalid content' do
          before do
            stub_content_validation_request(false)
          end

          it "updates the note" do
            note_html = note.note_html
            expect { put :update, params: request_params }.not_to change { note.reset.note }
            expect(note.reset.note_html).to eq(note_html)
            expect(response).to have_gitlab_http_status(:unprocessable_entity)
            response_body = Gitlab::Json.parse(response.body)
            expect(response_body).to eq({ "valid" => false, "content_invalid" => true, "errors" => "sensitive or illegal characters involved" })
          end
        end
      end
    end

    context 'application setting content validation is disabled' do
      before do
        sign_in(note.author)
        project.add_developer(note.author)
      end

      context 'note update invalid content with project is private' do
        before do
          stub_content_validation_request(false)
          stub_application_setting(content_validation_endpoint_enabled: false)
        end

        let(:project) { create(:project, :private) }

        it "updates the note" do
          expect { put :update, params: request_params }.to change { note.reset.note }
        end
      end

      context 'project is public' do
        let(:project) { create(:project, :public) }

        context 'note update with valid content' do
          before do
            stub_content_validation_request(true)
            stub_application_setting(content_validation_endpoint_enabled: false)
          end

          it "updates the note" do
            note_html = note.note_html
            expect { put :update, params: request_params }.to change { note.reset.note }
            expect(note.reset.note_html).not_to eq(note_html)
            expect(response).to have_gitlab_http_status(:ok)
          end
        end

        context 'note update with invalid content' do
          before do
            stub_content_validation_request(false)
            stub_application_setting(content_validation_endpoint_enabled: false)
          end

          it "updates the note" do
            note_html = note.note_html
            expect { put :update, params: request_params }.to change { note.reset.note }
            expect(note.reset.note_html).not_to eq(note_html)
            expect(response).to have_gitlab_http_status(:ok)
          end
        end
      end
    end
  end
end
