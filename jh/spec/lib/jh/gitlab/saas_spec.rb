# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ::Gitlab::Saas do
  using RSpec::Parameterized::TableSyntax

  where(:method_name, :test, :development, :result) do
    :com_url                        | false | false  | 'https://gitlab.cn'
    :com_url                        | false | true   | 'https://gitlab.cn'
    :com_url                        | true  | false  | 'https://gitlab.cn'
    :staging_com_url                | false | false  | 'https://staging.gitlab.cn'
    :staging_com_url                | false | true   | 'https://staging.gitlab.cn'
    :staging_com_url                | true  | false  | 'https://staging.gitlab.cn'
    :subdomain_regex                | false | false  | %r{\Ahttps://[a-z0-9]+\.gitlab\.cn\z}
    :subdomain_regex                | false | true   | %r{\Ahttps://[a-z0-9]+\.gitlab\.cn\z}
    :subdomain_regex                | true  | false  | %r{\Ahttps://[a-z0-9]+\.gitlab\.cn\z}
    :dev_url                        | false | false  | 'https://dev-ops.gitlab.cn'
    :dev_url                        | false | true   | 'https://dev-ops.gitlab.cn'
    :dev_url                        | true  | false  | 'https://dev-ops.gitlab.cn'
    :registry_prefix                | false | false  | 'gitlab-jh-public.tencentcloudcr.com'
    :registry_prefix                | false | true   | 'gitlab-jh-public.tencentcloudcr.com'
    :registry_prefix                | true  | false  | 'gitlab-jh-public.tencentcloudcr.com'
    :customer_support_url           | false | false  | 'https://support.gitlab.cn'
    :customer_support_url           | false | true   | 'https://support.gitlab.cn'
    :customer_support_url           | true  | false  | 'https://support.gitlab.cn'
    :customer_license_support_url   | false | false  | 'https://support.gitlab.cn/#/portal/submitticket/3'
    :customer_license_support_url   | false | true   | 'https://support.gitlab.cn/#/portal/submitticket/3'
    :customer_license_support_url   | true  | false  | 'https://support.gitlab.cn/#/portal/submitticket/3'
    :gitlab_com_status_url          | false | false  | 'https://status.gitlab.cn'
    :gitlab_com_status_url          | false | true   | 'https://status.gitlab.cn'
    :gitlab_com_status_url          | true  | false  | 'https://status.gitlab.cn'
    :commom_purchase_url            | false | false  | 'https://about.gitlab.cn/upgrade-plan'
    :commom_purchase_url            | false | true   | 'https://about.gitlab.cn/upgrade-plan'
    :commom_purchase_url            | true  | false  | 'https://about.gitlab.cn/upgrade-plan'
    :about_pricing_url              | false | false  | 'https://about.gitlab.cn/pricing'
    :about_pricing_url              | false | true   | 'https://about.gitlab.cn/pricing'
    :about_pricing_url              | true  | false  | 'https://about.gitlab.cn/pricing'
    :about_pricing_faq_url          | false | false  | 'https://about.gitlab.cn/pricing#faq'
    :about_pricing_faq_url          | false | true   | 'https://about.gitlab.cn/pricing#faq'
    :about_pricing_faq_url          | true  | false  | 'https://about.gitlab.cn/pricing#faq'
  end

  with_them do
    subject { described_class.method(method_name).call }

    before do
      allow(Rails).to receive_message_chain(:env, :test?).and_return(test)
      allow(Rails).to receive_message_chain(:env, :development?).and_return(development)
    end

    it { is_expected.to eq(result) }
  end
end
