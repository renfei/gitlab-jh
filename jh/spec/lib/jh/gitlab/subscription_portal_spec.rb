# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ::Gitlab::SubscriptionPortal do
  using RSpec::Parameterized::TableSyntax

  where(:method_name, :test, :development, :result) do
    :default_subscriptions_url        | false | false  | 'https://customers.gitlab.cn'
    :default_subscriptions_url        | false | true   | 'https://customers.stg.gitlab.cn'
    :default_subscriptions_url        | true  | false  | 'https://customers.stg.gitlab.cn'
    :payment_form_url                 | false | false  | 'https://customers.gitlab.cn/payment_forms/cc_validation'
    :payment_form_url                 | false | true   | 'https://customers.stg.gitlab.cn/payment_forms/cc_validation'
    :payment_form_url                 | true  | false  | 'https://customers.stg.gitlab.cn/payment_forms/cc_validation'
    :subscriptions_more_minutes_url   | false | false  | 'https://about.gitlab.cn/upgrade-plan'
    :subscriptions_more_minutes_url   | false | true   | 'https://about.gitlab.cn/upgrade-plan'
    :subscriptions_more_minutes_url   | true  | false  | 'https://about.gitlab.cn/upgrade-plan'
    :subscriptions_more_storage_url   | false | false  | 'https://about.gitlab.cn/upgrade-plan'
    :subscriptions_more_storage_url   | false | true   | 'https://about.gitlab.cn/upgrade-plan'
    :subscriptions_more_storage_url   | true  | false  | 'https://about.gitlab.cn/upgrade-plan'
    :subscriptions_comparison_url     | false | false  | 'https://about.gitlab.cn/pricing/saas/feature-comparison'
    :subscriptions_comparison_url     | false | true   | 'https://about.gitlab.cn/pricing/saas/feature-comparison'
    :subscriptions_comparison_url     | true  | false  | 'https://about.gitlab.cn/pricing/saas/feature-comparison'
  end

  with_them do
    subject { described_class.method(method_name).call }

    before do
      allow(Rails).to receive_message_chain(:env, :test?).and_return(test)
      allow(Rails).to receive_message_chain(:env, :development?).and_return(development)
    end

    it { is_expected.to eq(result) }
  end
end
