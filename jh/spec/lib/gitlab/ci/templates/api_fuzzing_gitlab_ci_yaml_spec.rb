# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'API-Fuzzing.gitlab-ci.yml' do
  subject(:template) { Gitlab::Template::GitlabCiYmlTemplate.find('API-Fuzzing') }

  describe 'the right content' do
    context 'when jh is enabled' do
      it 'includes tencentcloudcr' do
        expect(template.content.include?('tencentcloudcr')).to eq(true)
      end
    end
  end
end
